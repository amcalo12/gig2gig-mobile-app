import React, { Component } from "react";
import { View, Text, Linking, Platform, ActivityIndicator } from "react-native";
import styles from "./styles";
import Video from "react-native-video";

//Images
import Back from "../../../../../resources/images/audition/demo.jpeg";

//Custom
import Button from "utils/components/mainButton";
import ButtonClear from "utils/components/secondaryButton";
import Header from "utils/components/appHeader";
import ImageHeader from "utils/components/imageHeader";
import Card from "utils/components/mediaCards";

//Libs
import { Container, Content } from "native-base";
import VideoOnline from "utils/components/videoOnline";
// import getFile from 'utils/components/get_file';
import getFile from "utils/getFiles";
import { showMsj } from "utils/utils";
import getImageVideo from "utils/getImageVideo";
import RNThumbnail from "react-native-thumbnail";
import { createOnline, validateDataOnline, renameOnlineResource } from "api/online";
import uploadAsset from "utils/uploadAsset";
import uploadMusicFiles from "utils/uploadMusicFiles";
import SystemSetting from "react-native-system-setting";
import { requestPermission } from "utils/permissions";
import { ProcessingManager } from "react-native-video-processing";
import RenameModal from "utils/components/renameModal";
import { MESSAGES, showAlertWithConfirmation } from "utils/cons";

var VideoPlayer = require("react-native-native-video-player");

class Online extends Component {
  constructor(props) {
    super(props)

    this.currentVideo = null
  }
  state = {
    auditionData: {},
    videos: [],
    documents: [],
    loading: false,
    showVideo: [],
    loadingComplete: true,
    disableBtn: false,
    fileName: "",
    isVisible: false,
    isVideo: false,
    isRename: false,
    isVideoRenameShow: false,
    isDocRenameShow: false
  };

  componentDidMount = async () => {
    this.getMedia()
  };

  getMedia = async () => {
    console.log('this.props.navigation.state.params.data :>> ', this.props.navigation.state.params.data);
    const auditionData = await this.props.navigation.state.params.data;
    const idUser = await this.props.navigation.state.params.idUser;
    try {
      let videos = [];
      let documents = [];
      let resquestItems = await validateDataOnline({
        performer_id: idUser,
        appointment_id: auditionData.appointment_id
      });
      console.log("resquestItems", resquestItems);
      if (resquestItems.data.length) {
        for (const iterator of resquestItems.data) {
          if (iterator.type === "video") {
            // iterator.thumbnail =
            //   "https://images.pexels.com/photos/66134/pexels-photo-66134.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";
            iterator.thumbnail = iterator.thumbnail
            videos.push(iterator);
          } else {
            iterator.showOptions = true;
            documents.push(iterator);
          }
        }
      } else {
        for (const key in resquestItems.data) {
          let iterator = resquestItems.data[key];
          console.log(iterator);
          if (iterator.type === "video") {
            if (!iterator.thumbnail) {
              // iterator.thumbnail =
              //   "https://images.pexels.com/photos/66134/pexels-photo-66134.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";
              iterator.thumbnail = iterator.thumbnail
            }
            videos.push(iterator);
          } else {
            iterator.showOptions = true;
            documents.push(iterator);
          }
        }
      }
      if (videos.length > 0) {
        this.state.isVideoRenameShow = true
      }

      if (documents.length > 0) {
        this.state.isDocRenameShow = true
      }
      this.setState({
        disableBtn: true,
        auditionData,
        loadingComplete: false,
        videos,
        documents
      });
    } catch (error) {
      console.log("error", error);
      this.setState({
        disableBtn: false,
        auditionData,
        loadingComplete: false
      });
    }
  }

  handlePressUploadDocument = async (
    state,
    typeCustom = false,
    secondCustom = false
  ) => {
    try {
      let copyArray = [...this.state[state]];
      try {
        if (Platform.OS === "android") {
          const Permissions = await requestPermission("READ_EXTERNAL_STORAGE", {
            title: "Storage",
            message: "Allow G2G to access the storage of this device?"
          });
          if (Permissions) {
            let file = await getFile(2);
            // file.type = 3;
            this.currentVideo = file
            this.setState({ isVisible: true, fileName: file.filename, isVideo: false })
            copyArray.push(file);
            // console.log(file);
            this.setState({ [state]: copyArray });
          } else {
            SystemSetting.openAppSystemSettings();
          }
        } else {
          let file = await getFile(2);
          console.log('file selected:>> ', file);
          // file.type = 3;
          this.currentVideo = file
          this.setState({ isVisible: true, fileName: file.filename, isVideo: false })
          copyArray.push(file);
          // console.log(file);
          this.setState({ [state]: copyArray });
        }
      } catch (error) {
        if (typeof error === "object" && "errorCode" in error) {
          showMsj(error.string, true);
        }
        console.log("eeee", error);
      }
      // const file = await getFile(typeCustom, secondCustom);
    } catch (error) {
      if (typeof error === "object" && "errorCode" in error) {
        showMsj(error.string, true);
      }
      console.log(error);
    }
  };

  // handlePressUploadDocument = async (
  //   state,
  //   typeCustom = false,
  //   secondCustom = false
  // ) => {
  //   try {
  //     let copyArray = [...this.state[state]];
  //     try {
  //       if (Platform.OS === "android") {
  //         const Permissions = await requestPermission("READ_EXTERNAL_STORAGE", {
  //           title: "Storage",
  //           message: "Allow G2G to access the storage of this device?"
  //         });
  //         if (Permissions) {
  //           let file = await getFile(2);
  //           // file.type = 3;
  //           copyArray.push(file);
  //           console.log(file);
  //           this.setState({ [state]: copyArray });
  //         } else {
  //           SystemSetting.openAppSystemSettings();
  //         }
  //       } else {
  //         let file = await getFile(2);
  //         // file.type = 3;
  //         copyArray.push(file);
  //         console.log(file);
  //         this.setState({ [state]: copyArray });
  //       }
  //     } catch (error) {
  //       if (typeof error === "object" && "errorCode" in error) {
  //         showMsj(error.string, true);
  //       }
  //       console.log("eeee", error);
  //     }
  //     // const file = await getFile(typeCustom, secondCustom);
  //   } catch (error) {
  //     if (typeof error === "object" && "errorCode" in error) {
  //       showMsj(error.string, true);
  //     }
  //     console.log(error);
  //   }
  // };

  getResource = async () => {
    try {
      const myFile = await getImageVideo("video");
      console.log("myFile", myFile);
      return myFile;
    } catch (e) {
      if (e.didCancel) {
      } else {
        console.log("video error", e);
        return showMsj("An error has ocurred getting your file", true);
      }
    }
  };

  setVideo = async () => {
    const { videos } = this.state;

    let copyArray = [...videos];
    let video = await this.getResource();
    console.log("video", video);

    // this.currentVideo = video;
    // this.setState({ isVisible: true, fileName: "", isVideo: true })

    video.url = video.uri;
    // video.name = video.fileName ? video.fileName : 'Audition video';
    // video.name = this.state.fileName
    video.name = video.fileName ? video.fileName : "VIDEO_" + Math.random();

    if (video) {
      console.log("1 setVideo ==>" + JSON.stringify(video));
      video = this.getVideoExtension(video);
      console.log("2 setVideo ==>" + JSON.stringify(video));

      let uri = Platform.OS === "android" ? video.path : video.uri;
      let result = await RNThumbnail.get(uri);
      video.metadata = { contentType: `video/${video.type}` };
      video.thumbnail = result.path;
      copyArray.push(video);
      this.currentVideo = video;
      console.log("copyArray" + JSON.stringify(copyArray));
      this.setState({ videos: copyArray, fileName: video.name, isVisible: true, isVideo: true });
    };
  }

  // setVideo = async () => {
  //   const { videos } = this.state;

  //   let copyArray = [...videos];
  //   let video = await this.getResource();
  //   console.log("video", video);
  //   video.url = video.uri;
  //   // video.name = video.fileName ? video.fileName : 'Audition video';
  //   video.name = video.fileName ? video.fileName : "VIDEO_" + Math.random();

  //   if (video) {
  //     console.log("1 setVideo ==>" + JSON.stringify(video));
  //     video = this.getVideoExtension(video);
  //     console.log("2 setVideo ==>" + JSON.stringify(video));

  //     let uri = Platform.OS === "android" ? video.path : video.uri;
  //     let result = await RNThumbnail.get(uri);
  //     video.metadata = { contentType: `video/${video.type}` };
  //     video.thumbnail = result.path;
  //     copyArray.push(video);

  //     console.log("copyArray" + JSON.stringify(copyArray));

  //     this.setState({ videos: copyArray });
  //   }
  // };

  getVideoExtension = obj_video => {
    if (obj_video.path) {
      let temp = obj_video.path;
      let temp2 = temp.split(".");
      let len = temp2.length;
      let v_type = temp2[len - 1];
      console.log("v_type ==>" + v_type);
      obj_video.type = v_type;
    } else {
      let temp = obj_video.uri;
      let temp2 = temp.split(".");
      let len = temp2.length;
      let v_type = temp2[len - 1];
      console.log("v_type ==>" + v_type);
      obj_video.type = v_type;
    }

    return obj_video;
  };

  removeDoc = index => {
    let { documents } = this.state;
    let copy = [...documents];
    copy.splice(index, 1);
    this.setState({ documents: copy });
  };

  removeVideo = index => {
    let { videos } = this.state;
    let copy = [...videos];
    copy.splice(index, 1);
    this.setState({ videos: copy });
  };

  playVideo = (index, video) => {
    let { showVideo } = this.state;
    let copyArray = [...showVideo];
    copyArray[index] = true;
    if (Platform.OS === "android") {
      VideoPlayer.showVideoPlayer(video.url);
    } else {
      this.setState({ showVideo: copyArray }, () => {
        setTimeout(() => {
          this.videoRef.presentFullscreenPlayer();
        }, 500);
      });
    }
  };

  onFullscreenPlayerDidDismiss = index => {
    let { showVideo } = this.state;
    let copyArray = [...showVideo];
    copyArray[index] = false;
    this.setState({ showVideo: copyArray });
  };
  compressVideo = async source => {
    const options = {
      // width: 720,
      // height: 1280,
      bitrateMultiplier: 3,
      saveToCameraRoll: true, // default is false, iOS only
      saveWithCurrentDate: true, // default is false, iOS only
      minimumBitrate: 300000,
      removeAudio: false // default is false
    };
    return await new Promise((resolve, reject) => {
      ProcessingManager.compress(source, options) // like VideoPlayer compress options
        .then(data => {
          let path = data;
          if (Platform.OS === "android") {
            path = data.source;
          }
          console.log("Compress result", path);
          resolve(path);
        })
        .catch(error => {
          console.log("Compress error", error);
          reject("");
        });
    });
  };
  uploadDocuments = async () => {
    const auditionData = await this.props.navigation.state.params.data;
    let { videos, documents } = this.state;
    if (videos.length > 0) {
      if (documents.length > 0) {
        this.setState({ loading: true });
        try {
          for (const iterator of documents) {
            console.log("upload", iterator);
            let fileName = iterator.filename
              ? iterator.filename
              : "Document_" + new Date().getTime() + ".pdf";
            let subidaVideo = await uploadMusicFiles(
              "performance/auditions/online/docs",
              iterator.url,
              fileName,
              iterator.metadata
            );
            await createOnline({
              type: "doc",
              url: subidaVideo,
              name: fileName,
              appointment_id: auditionData.appointment_id,
              thumbnail: null
            });
          }
          for (const iterator of videos) {
            console.log("thumbnail", iterator);
            let compressedUrl = await this.compressVideo(
              Platform.OS === "android" ? iterator.path : iterator.uri
            );
            console.log("response URL=======");
            console.log(compressedUrl);
            // return;
            let subidaVideo = await uploadAsset(
              "performance/auditions/online/videos",
              // iterator.url,
              compressedUrl,
              iterator.name,
              iterator.metadata
            );
            let thumbnail = await uploadAsset(
              "performance/auditions/online/videosThumbnail",
              iterator.thumbnail,
              iterator.name
            );

            await createOnline({
              type: "video",
              url: subidaVideo,
              name: iterator.name,
              appointment_id: auditionData.appointment_id,
              thumbnail
            });
          }
          this.setState({ loading: false });
          showMsj("Documents uploaded successfully");
        } catch (error) {
          console.log("error", error);
          this.setState({ loading: false });
          return showMsj("There was a problem uploading your documents", true);
        }
        this.props.navigation.pop();
      } else {
        return showMsj("You must add at least one document", true);
      }
    } else {
      return showMsj("You must add at least one video", true);
    }
  };

  onCloseClick = () => {
    this.setState({ isVisible: false })
  }

  onSubmitClick = async () => {
    if (!this.props.navigation.state.params.isUpload) {
      this.setState({ loading: true });
      try {
        let params = {
          id: this.currentVideo.id,
          name: this.state.fileName,
          audition_id: this.state.auditionData.auditions_id
        }
        const response = await renameOnlineResource(params)
        console.log('response :', response);
        this.getMedia()
        this.setState({ loading: false, isVisible: false });
      } catch (error) {
        console.log("error", error);
        this.setState({ loading: false, isVisible: false });
      }
    } else {
      if (this.state.isVideo) {
        const { videos } = this.state;
        let copyArray = [...videos];

        let video = this.currentVideo;
        // video.url = video.uri;
        // video.name = video.fileName ? video.fileName : 'Audition video';
        // video.name = this.state.fileName


        if (video) {
          let index = this.state.videos.findIndex(data => data.url == this.currentVideo.url)
          console.log('index :>> ', index);
          if (index != -1) {
            this.state.videos[index].name = this.state.fileName
          }

          this.currentVideo = null
          this.setState({ videos: this.state.videos, fileName: "", isVisible: false });
          // console.log("1 setVideo ==>" + JSON.stringify(video));
          // video = this.getVideoExtension(video);
          // console.log("2 setVideo ==>" + JSON.stringify(video));

          // let uri = Platform.OS === "android" ? video.path : video.uri;
          // let result = await RNThumbnail.get(uri);
          // video.metadata = { contentType: `video/${video.type}` };
          // video.thumbnail = result.path;
          // copyArray.push(video);

          // console.log("copyArray" + JSON.stringify(copyArray));

          //   let index = this.state.videos

          //   this.currentVideo = null
          //   this.setState({ videos: copyArray, fileName: "", isVisible: false });
        }
      } else {

        let copyArray = [...this.state["documents"]];
        let file = this.currentVideo
        // file.filename = this.state.fileName

        let index = this.state.documents.findIndex(data => data.url == this.currentVideo.url)
        console.log('index :>> ', index);
        if (index != -1) {
          this.state.documents[index].filename = this.state.fileName
        }
        // copyArray.push(file);

        console.log(file);
        this.setState({ ["documents"]: this.state.documents, isVisible: false, fileName: "" }, () => {
          this.currentVideo = null
          console.log('copyArray DOC:', this.state);
        });
        // this.setState({ ["documents"]: copyArray, isVisible: false, fileName: "" }, () => {
        //   this.currentVideo = null
        //   console.log('copyArray DOC:', this.state);
        // });
      }
    }
  }

  onChangeText = (fileName) => {
    this.setState({ fileName })
  }

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    )
  }

  renameClick = (data) => {
    console.log('data :', data);
    this.currentData = data
    this.setState({ isVisible: true, fileName: data.name })
  }

  render() {
    const {
      auditionData,
      documents,
      videos,
      showVideo,
      loading,
      loadingComplete,
      disableBtn
    } = this.state;
    const {
      navigation: { navigate, dispatch }
    } = this.props;
    const isUpload = this.props.navigation.state.params.isUpload
    console.log('Header Image', auditionData);
    console.log('Header Image', auditionData.cover);
    return (
      <Container>
        <ImageHeader
          size="small"
          image={auditionData != undefined ? auditionData.cover != undefined ? auditionData.cover :
            auditionData.media != undefined ? auditionData.media : null : null}
        >
          <Header leftIcon="back" leftFn={() => this.props.navigation.pop()} />
        </ImageHeader>
        {this.renderRenameModal()}
        {loadingComplete ? (
          <View style={[styles.containerFunctions, styles.center]}>
            <ActivityIndicator size="large" color="#4D2545" />
          </View>
        ) : (
            <>
              <Content
                disableKBDismissScroll
                contentContainerStyle={{ alignItems: "center" }}
                style={styles.containerFunctions}
              >
                <Text style={styles.title}>{auditionData.title}</Text>
                <Text style={styles.subTitle}>
                  {auditionData.round ? "Round " + auditionData.round : ""}
                </Text>

                {auditionData.has_ended && videos.length == 0 && documents.length == 0 && isUpload ?
                  <Text style={[styles.subTitle, { marginHorizontal: 30, textAlign: 'center' }]}>{MESSAGES.AUDITION_SUBMISSION_DUE}</Text>
                  : null}
                {!auditionData.has_ended && videos.length == 0 && documents.length == 0 && !isUpload ?
                  <Text style={[styles.subTitle, { marginHorizontal: 30, textAlign: 'center' }]}>
                    {"No Media Found"}</Text>
                  : null}
                {videos.map((video, index) => {
                  return (
                    <>
                      {console.log('video.thumbnail :', video)}
                      <VideoOnline
                        key={`video1${index}`}
                        thumbnail={video.thumbnail}
                        onPress={() => this.playVideo(index, video)}
                        // isShowRename={this.state.isVideoRenameShow}
                        isShowRename={true}
                        isShowDelete={isUpload ? true : false}
                        onDelete={() => {
                          showAlertWithConfirmation("Are you sure you want to delete this video?", () => {
                            this.removeVideo(index)
                          })
                        }}
                        onRename={() => {
                          console.log('this.state.auditionData :', this.state.auditionData);
                          this.currentVideo = video
                          this.setState({
                            isVisible: true, isRename: true,
                            fileName: video.filename ? video.filename : video.name
                          })
                        }}
                        videoName={video.filename ? video.filename : video.name}
                      />
                      {showVideo[index] ? (
                        <Video
                          key={`video2${index}`}
                          fullscreen
                          onFullscreenPlayerDidDismiss={() =>
                            this.onFullscreenPlayerDidDismiss(index)
                          }
                          ignoreSilentSwitch="ignore"
                          playInBackground={false}
                          playWhenInactive={false}
                          source={{ uri: video.url }}
                          ref={ref => (this.videoRef = ref)}
                        />
                      ) : null}
                    </>
                  );
                })}
                {!auditionData.has_ended && isUpload ? <View style={styles.buttons}>
                  {disableBtn ? (
                    <Text style={styles.title}>Audition videos</Text>
                  ) : (
                      <ButtonClear text="Upload Videos" onPress={this.setVideo} />
                    )}
                </View>
                  : null}
                {documents.map((doc, index) => {
                  console.log('document part called ===========:>> ');
                  return (
                    <Card
                      key={`docs${index}`}
                      type="sheet"
                      fileText={
                        doc.filename
                          ? doc.filename
                          : doc.name
                            ? doc.name
                            : "Document_" + new Date().getTime() + ".pdf"
                      }
                      data={doc}
                      renameFile={() => {
                        console.log('rename Click :');
                        this.currentVideo = doc
                        this.setState({
                          isVisible: true, isRename: true,
                          fileName: doc.filename
                            ? doc.filename
                            : doc.name
                              ? doc.name
                              : "Document_" + new Date().getTime() + ".pdf"
                        })
                      }}
                      // showOptions={this.state.isDocRenameShow ? false : true}
                      showOptions={false}
                      deleteFile={isUpload ? () => {
                        setTimeout(() => {
                          showAlertWithConfirmation("Are you sure you want to delete this document?", () => {
                            this.removeDoc(index)
                          })
                        }, 500);
                      } : null}
                    />
                  );
                })}
                {!auditionData.has_ended && isUpload ? <View style={styles.buttons}>
                  {disableBtn ? (
                    <Text style={styles.title}>Audition documents</Text>
                  ) : (
                      <ButtonClear
                        text="Upload Documents"
                        onPress={() => this.handlePressUploadDocument("documents")}
                      />
                    )}
                </View> : null}
              </Content>
              {!auditionData.has_ended && isUpload ? <View style={styles.btnContainer}>
                {disableBtn ? null : (
                  <Button
                    loading={loading}
                    text="Submit"
                    onPress={this.uploadDocuments}
                  />
                )}
              </View> : null}
            </>
          )}
      </Container>
    );
  }
}
export default Online;
