import React, { Component } from "react";
import {
  Platform,
  Modal,
  View,
  Image,
  Text,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import styles from "./index.styles";
import Button from "utils/components/mainButton";
import ButtonClear from "utils/components/secondaryButton";
import Input from "utils/components/mainInput";
import TimeIcon from "resources/images/myAuditions/time-icon.png";
import CloseIcon from "resources/images/myAuditions/close-icon.png";
import Loading from "utils/components/loading";
import moment from "moment";
import Select from "utils/components/selectTimeUpcoming";
import auditionsApi from "api/auditionsApi";
import myAuditionsApi from "api/myAuditionsApi";
import { showMsj } from "utils/utils";
import { generalMsj } from "resources/l10n";
export default class ModalRequest extends Component {
  state = {
    modalVisible: false,
    setUpcoming: false,
    loading: false,
    timeSelected: null,
    message: false,
    loading: false
  };

  async setModalVisible() {
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    this.setState({
      userData,
      subscription: userData.details.subscription,
      modalVisible: !this.state.modalVisible
    });
  }

  switchToUpcoming = () => {
    this.setState({
      setUpcoming: !this.state.setUpcoming
    });
  };

  select = () => {
    this.refs.select.setModalVisible();
  };

  handleSelectedState = selected => {
    console.log(selected);
    this.setState({
      timeSelected: selected,
      message: false
    });
    this.select();
  };

  addToUpcomingAppointment = async () => {
    const { data } = this.props;
    const { timeSelected, loading } = this.state;
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));

    if (!timeSelected) {
      this.setState({
        message: true
      });
      return;
    }
    this.setState({ loading: true });
    const params = {
      slot: {
        slot: timeSelected.id,
        auditions: data.auditions_id,
        rol: data.rol
      }
    };

    try {
      const response = await myAuditionsApi.saveChecking(params, data.id);
      console.log(response);

      this.setState({
        loading: false,
        modalVisible: false,
        timeSelected: null,
        setUpcoming: false
      });
      showMsj("Saved to upcoming", false);
      this.props.reloadFn();
    } catch (e) {
      console.log(e);
      showMsj(e.data.error, true);
      this.setState({ loading: false, modalVisible: false, timeSelected: null, setUpcoming: false });
      // showMsj(generalMsj.error.message, true);
    }
  };

  saveAudition = async () => {
    //type 1 para guardarlo en upcoming
    //type 2 es para guardarlo en request
    const { data } = this.props;
    this.setState({ loading: true });

    try {
      const response = await myAuditionsApi.updateToUpcoming(data.id);
      console.log(response);
      this.setState({
        loading: false,
        modalVisible: false
      });

      showMsj(response.data, false);
      this.props.reloadFn();
    } catch (e) {
      console.log(e);
      this.setState({
        loading: false,
        modalVisible: false
      });

      if (e.status && e.status === 406) {
        return showMsj(e.data.error, true);
      }
      showMsj(generalMsj.error.message, true);
    }
  };
  render() {
    const {
      setUpcoming,
      loading,
      timeSelected,
      message,
      userData,
      subscription
    } = this.state;
    const { data, times, walkAppont, isFromUpcoming } = this.props;
    console.log(data);
    return (
      <Modal
        transparent={true}
        animationType="fade"
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setModalVisible();
          this.switchToUpcoming();
        }}
      >
        <View style={styles.container}>
          <View style={styles.info}>
            <View style={styles.imageContent}>
              {loading && (
                <Loading
                  style={{
                    top: "8%",
                    position: "absolute",
                    alignSelf: "center"
                  }}
                />
              )}
              <Image
                onLoadStart={() => this.setState({ loading: true })}
                onLoadEnd={() => this.setState({ loading: false })}
                style={styles.image}
                source={{ uri: data && data.media }}
              />
            </View>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.close}
              onPress={() => {
                this.setModalVisible();
                this.setState({ setUpcoming: false });
              }}
            >
              <Image source={CloseIcon} />
            </TouchableOpacity>
            <View style={styles.content}>
              <View>
                <Text style={styles.title}>{data && data.title}</Text>
                <Text style={styles.subTitle}>
                  {data && moment(data.date).format("MMMM Do, YYYY")}
                </Text>
                {setUpcoming && (
                  <View style={styles.aInfo}>
                    <Text style={styles.aInfoText}>Appointment Info</Text>
                    <Input
                      isTouchable
                      onPress={() => this.select()}
                      placeholder="Time"
                      data={timeSelected ? timeSelected.time : ""}
                      elevation={2}
                      icon={TimeIcon}
                    />
                    {message && (
                      <Text style={styles.aInfoText}>
                        You have to select an hour
                      </Text>
                    )}
                  </View>
                )}
              </View>
              <View>
                {!isFromUpcoming && !setUpcoming && walkAppont.length !== 0 && (
                  <View style={styles.button}>
                    <ButtonClear
                      loading={loading}
                      text="Add to Upcoming"
                      onPress={() => this.saveAudition()}
                    />
                  </View>
                )}
                {!setUpcoming && (
                  <Button
                    text="I Have an Appointment"
                    onPress={() => this.switchToUpcoming()}
                  />
                )}
                {setUpcoming && (
                  <View style={styles.button}>
                    <Button
                      loading={loading}
                      text={isFromUpcoming ? "Submit" : "Add to Upcoming"}
                      onPress={() => this.addToUpcomingAppointment()}
                    />
                  </View>
                )}
              </View>
            </View>
          </View>
        </View>
        <Select
          selectTitle="Select Time"
          data={times}
          onPress={selected => this.handleSelectedState(selected)}
          ref="select"
        />
      </Modal>
    );
  }
}
