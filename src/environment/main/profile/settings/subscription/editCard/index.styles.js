import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

const { width, height } = Dimensions.get('window')
export default StyleSheet.create({
  container:{
    paddingTop: 15,
    alignItems: 'center',
    width:'100%',
    paddingBottom: 15,
    height: height - 120
  },
  img:{
    marginRight:7
  },
  titleText:{
    paddingLeft: 5,
    marginTop: 20,
    marginBottom: 10,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: colorPalette.purple
  },
  imagesContainer:{
    flexDirection:'column',
    alignItems:'flex-start',
    width:'78%',
    marginTop:5,
    height:40
  },
  row:{
    flexDirection:'row',
  },
  secondPartContainer:{
    alignItems:'flex-start', 
    width:'80%'
  },
  buttonsContainer:{
    height:120,
    justifyContent:'space-between',
    marginTop:50
  }
});