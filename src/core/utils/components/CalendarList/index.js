import React, { Component } from 'react';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { CalendarList } from 'react-native-calendars';
class Calendar extends Component {
    state = {
        refreshCount: 1
    }
    render() {
        const { ranges, headerMonthPosition } = this.props
        return (
            <CalendarList
                onLayout={() => {
                    this.setState({ refreshCount: this.state.refreshCount + 1 })
                }}
                calendarWidth={"100%"}
                style={{ width: "100%", backgroundColor: "white" }}
                theme={{
                    monthTextColor: colorPalette.unionColor,
                    textMonthFontFamily: fontStyles.nexa_bold,
                    dayTextColor: '#4D2545',
                    todayTextColor: '#4D2545',
                    textMonthFontSize: 19,
                    'stylesheet.calendar.header': {
                        header: {
                            alignItems: headerMonthPosition
                        },
                    },
                    'stylesheet.day.period': {
                        text: {
                            color: '#782541',
                            marginTop: 11,
                            width: '100%',
                            textAlign: 'center',
                            fontSize: 13,
                            fontFamily: fontStyles.nexa_bold,
                        },
                        base: {
                            overflow: 'hidden',
                            height: 34,
                            alignItems: 'center',
                            width: 38,
                        }
                    }
                }}
                markedDates={ranges}
                markingType={'custom'}
                pastScrollRange={0}
                futureScrollRange={12}
            />
        );
    }
}

export default Calendar;