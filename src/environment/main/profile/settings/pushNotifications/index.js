import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Label from "utils/components/pushNotificationLabel";
import Loading from "utils/components/loading";
import pushNotifiactionViewModel from "./pushNotifiactionViewModel";
import { showMsj } from "utils/utils";
class SettingsView extends Component {
	state = {
		listNotification: [],
		loading: true
	}

	componentDidMount = async () => {
		await pushNotifiactionViewModel.getNotification(this)
	};

	handleBackPress = () => {
		const { navigation: { dispatch } } = this.props;
		return dispatch(StackActions.popToTop())
		return true;
	}

	renderNotifications = () => {
		return this.state.listNotification.map((res, i) => {
			const a = 'fdasfdsafdsa'

			return <Label key={i} text={res.code.replace('_', ' ')} value={res.status == 'off' ? false : true} onPress={this.changeValue.bind(this, res.id)} />
		})
	}

	changeValue = async (id) => {
		await pushNotifiactionViewModel.updateNotification(this, id)
	};

	render() {
		const { navigation: { navigate, dispatch } } = this.props;
		return (
			<Container>
				<Header
					leftIcon="backDark"
					leftFn={() => dispatch(StackActions.popToTop())}
					barStyle="dark-content"
					text="Push Notifications"
				/>
				<Content>
					{
						this.state.loading ?
							<Loading style={{ top: "40%", alignSelf: 'center', }} />
							:
							<View style={styles.container}>
								{this.renderNotifications()}
							</View>
					}
				</Content>
			</Container>
		);
	}
}
export default SettingsView;
