export const Assets = {
  defaultAvatar: require("./images/supportForum/default_avatar.jpg"),
  close: require("./images/close-icon-purple.png"),
  send: require("./images/audition/send.png"),
  social_share: require("./images/profile/share.png"),
  gig_avatar: require("./images/supportForum/avatar_logo.png"),
  rename: require("./images/myMedia/edit.png"),
  gig2gig: require("./images/gig2gig.png"),
  downArrow: require("./images/audition/down_arrow.png"),
  close: require("./images/back-icon-purple.png"),
  subscription: require("./images/profile/subscription.png"),
  delete: require("./images/delete-icon.png")

};