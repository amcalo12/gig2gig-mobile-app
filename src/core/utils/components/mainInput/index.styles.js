import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");

export default StyleSheet.create({
	container: {
		flexDirection: "row",
		height: height * 0.0734632683658,
		width: width * 0.75,
		backgroundColor: colorPalette.white,
		borderRadius: 33,
		alignItems: 'center',
		borderWidth: 1,
	},
	alignTop: {
		height: '100%',
	},
	touchableContainer: {
		flexDirection: "row",
		width: "100%",
		height: '100%',
		alignItems: 'center',
	},
	shortInput: {
		flexDirection: "row",
		height: height * 0.0734632683658,
		width: width * 0.357333333333,
		backgroundColor: colorPalette.white,
		borderRadius: 33,
		alignItems: 'center',
	},
	input: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 18,
		color: "#4D2545",
		paddingHorizontal: 20
	},
	input2: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 18,
		color: "#4D2545",
		paddingHorizontal: 20
	},
	icon: {
		marginBottom: height * 0.005,
	},
	leftIcon: {
		marginLeft: resize(20),
		marginBottom: height * 0.005,
	}
});