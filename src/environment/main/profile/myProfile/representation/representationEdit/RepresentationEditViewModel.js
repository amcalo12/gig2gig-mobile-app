import { NavigationActions, StackActions } from "react-navigation";
import { generalMsj, validateRepresentation } from "resources/l10n";
import { emailRegExp } from "utils/cons";
import { showMsj } from "utils/utils";
import myProfileApi from "api/myProfileApi";

export const changeRepresentation = context => {
  context.setState({
    manager: !context.state.manager,
    agent: !context.state.agent
  });
};

export const changeAllow = context => {
  context.setState({
    notifications: !context.state.notifications
  });
};

export const validate = context => {
  const { name, company, email } = context.state;
  if (name === "") {
    return showMsj(validateRepresentation.name.error, true);
  }
  if (company === "") {
    return showMsj(validateRepresentation.name.error, true);
  }
  if (email === "") {
    return showMsj(generalMsj.email.error, true);
  }
  if (!emailRegExp.test(email)) {
    return showMsj(generalMsj.email.invalid, true);
  }

  saveRepresentation(context);
};

export const saveRepresentation = async context => {
  const { manager, name, company, email, notifications } = context.state;
  const hasRepresentation =
    context.props.navigation.state.params.hasRepresentation;
  const isFromAuditionDetail =
    context.props.navigation.state.params.isFromAuditionDetail;

  context.setState({ loading: true });
  try {
    if (hasRepresentation) {
      var response = await myProfileApi.editRepresentation(
        { type: manager ? 1 : 2, name, company, email, notifications },
        hasRepresentation.id
      );
      showMsj(validateRepresentation.save.success, false);
      context.props.navigation.goBack();
    } else {
      var response = await myProfileApi.createRepresentation({
        type: manager ? 1 : 2,
        name,
        company,
        email,
        notifications
      });
      showMsj(validateRepresentation.save.success, false);
      if (isFromAuditionDetail) {
        return context.props.navigation.goBack();
      } else {
        return context.props.navigation.popToTop();
      }
    }
  } catch (e) {
    showMsj(generalMsj.error.message, true);
    // returnToMain(context);
  }
};

export const handleBack = context => {
  const {
    navigation: { goBack },
    navigation
  } = context.props;
  const hasRepresentation =
    context.props.navigation.state.params.hasRepresentation;
  const isFromAuditionDetail =
    context.props.navigation.state.params.isFromAuditionDetail;

  console.log(context.props.navigation.state.params.hasRepresentation);
  if (hasRepresentation) {
    return navigation.popToTop();
  } else if (isFromAuditionDetail) {
    return navigation.goBack();
  } else {
    returnToMain(context);
  }
};

export const returnToMain = context => {
  const {
    navigation: { dispatch }
  } = context.props;
  const navigateAction2 = NavigationActions.navigate({
    routeName: "Profile",

    params: {},

    action: NavigationActions.navigate({
      routeName: "MyProfile",
      params: { nextScreen: true }
    })
  });

  const navigateAction = NavigationActions.navigate({
    routeName: "App",

    params: {},

    action: navigateAction2
  });

  dispatch(
    StackActions.reset({
      index: 0, //Home screen of Tab A
      // key: null,
      actions: [navigateAction]
    })
  );
  // return dispatch(StackActions.popToTop());
};
