import React, { Component } from "react";
import { View, Text } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import InfoLabel from "utils/components/infoLabel";
import Button from "utils/components/secondaryButton";
class CreditInfoView extends Component {
  render() {
    const {
      navigation: {
        state: {
          params: { id, onGoBack },
        },
        navigate,
        goBack,
      },
    } = this.props;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => goBack()}
          barStyle="dark-content"
          text="Credits"
        />
        <Content>
          <View style={styles.container}>
            <View style={styles.labels}>
              <InfoLabel text={id.type} />
              <InfoLabel text={id.name} />
              <InfoLabel text={id.rol} />
              <InfoLabel text={id.production} />
              <InfoLabel text={id.year ? id.year : "Start year"} />
              <InfoLabel text={id.end_year ? id.end_year : "End year"} />
              <InfoLabel text={id.month ? id.month : "Month"} />

              {/* <View style={styles.labelGroup}>
                <InfoLabel isShort noLine text={id.month}/>
                <InfoLabel isShort noLine text={id.year}/>
              </View> */}
            </View>
            <Button
              text="Edit"
              onPress={() => navigate("CreditInfoEdit", { id, onGoBack })}
            />
          </View>
        </Content>
      </Container>
    );
  }
}
export default CreditInfoView;
