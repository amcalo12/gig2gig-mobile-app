import marketplaceApi from 'api/marketplaceApi';
import { showMsj } from 'utils/utils';
import { errorsValidateMsj } from 'resources/l10n';

const handleGetCategories = async (context, refreshing) => {
	context.setState({
		loading: refreshing ? false : true,
		refreshControlLoad: refreshing ? true : false,
	});
	try {
		const response = await marketplaceApi.categories();
		if (response) {
			const marketplaceOptions = response.data;
			const marketplaceFeature = response;
			context.setState({ marketplaceOptions, marketplaceFeature });
			context.setState({
				loading: false,
				refreshControlLoad: false,
				notFoudData: false,
			});
		}
	} catch (e) {
		context.setState({
			listData: [],
			loading: false,
			notFoudData: true,
			refreshControlLoad: false,
		});
	}
};
const timeOutSearch = context => {
	return setTimeout(() => {
		return context.setState({ searchEnable: true });
	}, 1000);
};
const setTextSearch = async (context, textSearch) => {
	context.setState({ textSearch, loading: true, setCards: true });
	if (textSearch != '') {
		try {
			const res = await marketplaceApi.cardsForText(textSearch);
			const listData = res.data;
			if (context.state.setCards) {
				context.setState({ listData, notFoudData: false });
			}
		} catch (error) {
			if (context.state.setCards) {
				context.setState({ listData: [], loading: false, notFoudData: true });
			}
		}
	} else {
		context.setState({ listData: [], notFoudData: false, setCards: false });
	}
	context.setState({ loading: false });
};

export default {
	handleGetCategories,
	setTextSearch,
};
