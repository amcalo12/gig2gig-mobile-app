// @flow
import firebase from "react-native-firebase";
// Optional flow type
import type { RemoteMessage } from "react-native-firebase";

export default async (message: RemoteMessage) => {
  console.log(message);
  const notification = new firebase.notifications.Notification()
    .setNotificationId(message["_messageId"])
    .setTitle(message["_data"]["title"])
    .setBody(message["_data"]["body"]);
  notification.android
    .setChannelId("channelId")
    .android.setSmallIcon("ic_launcher");
  notification.ios.setBadge(2);
  firebase.notifications().displayNotification(notification);

  return Promise.resolve();
};
