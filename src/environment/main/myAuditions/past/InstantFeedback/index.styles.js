import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  content: {
    marginTop: isIos ? resize(30, "height") : resize(25, "height"),
    alignItems: "center",
    paddingBottom: resize(40, "h")
  },
  contentFeed: {
    marginBottom: resize(25, "h")
  },
  btn: {
    width: resize(190)
  },
  containerSub: {
    paddingHorizontal: resize(20),
    minHeight: 180,
    justifyContent: "space-between",
    paddingVertical: 20,
    borderWidth: 1.5,
    marginHorizontal: 30,
    borderColor: colorPalette.purple,
    borderRadius: 20,
    marginTop: 30
  },
  reco: {
    marginTop: resize(20, "h")
  },
  commentTextStyle: {
    marginTop: resize(25, "h")
  },
  typeText: {
    alignSelf: "center",
    fontSize: 23,
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple
  },
  recomendationsEmpty: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 16,
    letterSpacing: 1,
    color: "#757575",
    marginBottom: resize(20, "height")
  },
  list: {
    paddingTop: resize(20, "h")
  },
  descriptionText: {
    alignSelf: "center",
    marginTop: 20,
    fontSize: 22,
    marginBottom: 50,
    textAlign: "center"
  },
  buttons: {
    alignItems: "center"
  },
  secondary: {
    height: resize(49, "height"),
    width: resize(229),
    alignItems: "center",
    justifyContent: "center",
    color: colorPalette.purple,
    borderWidth: 2,
    borderColor: colorPalette.purple,
    borderRadius: 13,
    marginBottom: resize(20, "height")
  },
  secondaryText: {
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
    fontSize: 18
  },
  primaryText: {
    fontFamily: fontStyles.nexa_bold,
    color: "#FFFFFF",
    fontSize: 18
  },
  primary: {
    height: resize(49, "height"),
    width: resize(229),
    alignItems: "center",
    justifyContent: "center",
    color: colorPalette.purple,
    borderRadius: 13,
    backgroundColor: colorPalette.purple
  },
  container: {
    alignItems: "center",
    width: resize(375)
  },
  title: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 28,
    letterSpacing: 1.4,
    color: colorPalette.purple,
    marginBottom: resize(20, "height")
  },
  subTitle: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 20,
    letterSpacing: 1,
    color: colorPalette.purple,
    marginBottom: resize(20, "height")
  },
  notHave: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    letterSpacing: 1,
    color: colorPalette.purple,
    fontWeight: "200",
    marginBottom: resize(22, "height")
  },
  emojiContainer: {
    width: resize(94),
    height: resize(94),
    alignItems: "center",
    justifyContent: "center",
    borderColor: colorPalette.purple,
    borderWidth: 3,
    borderRadius: 4,
    marginBottom: resize(25, "height")
  },
  buttonsContainer: {
    width: "100%",
    flexDirection: "row"
  },
  buttonsFeedBack: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    width: "50%",
    paddingHorizontal: resize(20),
    height: resize(75, "h"),
    marginBottom: resize(25, "height"),
  },
  label: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 23,
    letterSpacing: 1.15,
    color: colorPalette.purple
  },
  button: {
    width: resize(152),
    height: resize(38, "height")
  },
  action: {
    marginTop: resize(25, "height")
  }
});
