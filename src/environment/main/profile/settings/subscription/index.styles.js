import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container: {
    paddingHorizontal: resize(20),
    height: '100%',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  cont: {
    height: resize(370, 'h'),
    justifyContent: 'space-between',
  },
});