import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { colorPalette } from 'resources/colors';
import { fontStyles } from 'resources/fonts';

export default styles = StyleSheet.create({
  modalContainer: {
    flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: colorPalette.SHADOW_COLOR
  },
  modalSubContainer: {
    borderRadius: 8, backgroundColor: colorPalette.white, shadowOpacity: 0.2,
    elevation: 5, width: "70%", padding: 20
  },
  title: { fontFamily: fontStyles.nexa_bold, textAlign: "center" },
  text: { fontSize: 14, lineHeight: 30, fontFamily: fontStyles.nexa_light, marginTop: 10, textAlign: 'center' }
});
