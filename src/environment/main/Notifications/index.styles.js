import { StyleSheet, Platform, Dimensions } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";
import DeviceInfo from 'react-native-device-info';
const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
const isIos = Platform.OS === "ios"

export default StyleSheet.create({
  container: {
    paddingHorizontal: resize(25)
  },
  notFoundStyle: {
    color: colorPalette.purple,
    fontSize: 18,
    fontFamily: fontStyles.nexa_bold,
    width: '100%',
    textAlign: 'center',
    marginTop: 15
  },
  swipeout: {
    backgroundColor: 'transparent'
  },
  header: {
    alignItems: "center",
    position: "relative",
    marginTop: isIos ? resize(DeviceInfo.hasNotch() ? 60 : 30, "height") : resize(15, "height"),
    marginHorizontal: resize(25),
    marginBottom: resize(20, "height"),
    height: resize(30, "height")
  },
  close: {
    position: "absolute",
    left: 0,
    top: 0,
    bottom: 0
  },
  headerText: {
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    letterSpacing: 0.9,
    marginTop: resize(1, "height")
  },
  notificationContainer: {
    marginTop: resize(20, "height")
  },
  bold: {
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
  },
});