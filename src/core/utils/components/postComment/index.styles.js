import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";

export default StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  cardContainer: {
    width: "100%",
    paddingVertical: resize(20),
    borderBottomColor: 'rgba(0,0,0,0.3)',
    borderBottomWidth: 0.5
  },
  avatarAndNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: resize(26, 'h'),
    height: resize(26, 'h'),
    borderRadius: resize(13, 'h'),
  },
  nameTxt: {
    paddingLeft: resize(7),
    color: '#4d2545',
    fontFamily: 'Nexa Bold',
    fontSize: resize(12, 'h'),
    fontWeight: '400',
  },
  titleTxt: {
    color: '#4d2545',
    fontFamily: 'Nexa Bold',
    fontSize: resize(16, 'h'),
    fontWeight: '400',
    paddingTop: resize(20, 'h')
  },
  bodyTxt: {
    paddingTop: resize(10, 'h'),
    color: '#757575',
    fontFamily: 'Nexa Bold',
    fontSize: resize(10, 'h'),
    fontWeight: '300',
    lineHeight: resize(18, 'h'),
  },
  dateTxt: {
    color: '#d6d6d6',
    fontFamily: 'Nexa Light',
    fontSize: resize(10, 'h'),
    fontWeight: '400',
    paddingTop: resize(15, 'h'),
  }
});