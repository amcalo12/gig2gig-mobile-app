import marketplaceApi from "api/marketplaceApi";
import { showMsj } from "utils/utils";
import { errorsValidateMsj } from "resources/l10n";

const handleGetCardsForFilter = async context => {
    context.setState({loading:true, notFoudData: false})
    const { textFilter } = context.state
    try{
      const response = await marketplaceApi.cardsForFilter(textFilter.id)
      const listData = response.data
      context.setState({listData})
      
      context.setState({loading: false});
    }catch(e){
      context.setState({loading:false, notFoudData: true})
    }
}

const searchCardsWithText = async (context) => {
  context.setState({loading:true, setCards:true})
  const { textSearch, textFilter } = context.state
  if(textSearch != ''){
    try {
      const response = await marketplaceApi.cardsForFilterAndText(textFilter.id,textSearch)
      const listDataWithFilter = response.data
      if(context.state.setCards){
        context.setState({listDataWithFilter,loading: false, notFoudData: false});
      }
    } catch (error) {
      if(context.state.setCards){
        context.setState({loading:false, notFoudData: true})
      }
    }
  }else{
    context.setState({loading:false,notFoudData: false, listDataWithFilter:[], setCards:false})
  }
}
const optionSelected = async (context) =>{
  handleGetCardsForFilter(context)
}

export default {
  handleGetCardsForFilter,
  searchCardsWithText,
  optionSelected
}