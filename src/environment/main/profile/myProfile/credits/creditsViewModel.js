import creditsApi from "api/creditsApi";
import { showMsj } from "utils/utils";

const getCredits = async (context) => {
  try {
    const res = await creditsApi.getCredits()
    console.log('CREDITS', res)
    const listCredits = res.data
    context.setState({ listCredits, loading: false })
  } catch (error) {
    context.setState({ listCredits: [], loading: false, notFoundData: true })
  }
}
const addCredit = async (context, credit) => {
  const oldList = context.state.listCredits
  const listCredits = [...oldList, credit]
  context.setState({ listCredits, notFoundData: false })
}
const updateCredit = async (context, credit) => {
  const listCredits = [...context.state.listCredits]
  const index = listCredits.findIndex(i => i.id === credit.id)
  listCredits[index] = credit
  context.setState({ listCredits })
}
const deleteCredit = async (context, id) => {
  var { listCredits } = context.state
  try {
    const index = listCredits.findIndex(res => id === res.id)
    listCredits[index] = { ...listCredits[index], loading: true }
    context.setState({ listCredits })
    await creditsApi.deleteCredits(id)
    listCredits = listCredits.filter((res) => res.id != id)
    context.setState({ listCredits })
    return showMsj('Credit successfully deleted')
  } catch (error) {
    return showMsj('Error', true)
  }
}
export default {
  getCredits,
  addCredit,
  updateCredit,
  deleteCredit
}