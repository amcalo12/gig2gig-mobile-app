import React, { Component } from "react";
import { View, Text, RefreshControl } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import ButtonAction from "utils/components/buttonActionFloat";
import PlusIcon from "resources/images/myMedia/plus-icon.png";
import Card from "utils/components/mediaCards";
import uploadAsset from "utils/uploadAsset";
import Loading from "utils/components/loading";
import { showMsj, openUrl, shareContent } from "utils/utils";
import myMediaApi from "api/myMediaApi";
import Modal from "utils/components/modalUploadFiles";
import { generalMsj, shareMsj } from "resources/l10n";
import { refreshControlColors } from "resources/colors";
import getImageVideo from "utils/getImageVideo";
import { getThumbnail } from "utils/cons";
import RenameModal from "utils/components/renameModal";
import myProfileApi from "api/myProfileApi";

class PhotosView extends Component {
	constructor(props) {
		super(props)
		this.currentData = null
	}
	state = {
		loading: false,
		refreshing: false,
		files: null,
		modalMessage: "",
		fileName: "",
		isVisible: false,
		selectedFile: null
	}

	async componentDidMount() {
		this.getFiles();
	}

	_onRefresh = async () => {
		this.getFiles(true);
	}

	getFiles = async (refresh = false) => {
		if (refresh) {
			this.setState({ refreshing: true });
		} else {
			this.setState({ loading: true });
		}
		try {
			const response = await myMediaApi.getUserMediaByType("image");
			this.setState({ files: response.data, loading: false, refreshing: false, selectedFile: null, fileName: "" });
		} catch (e) {
			this.setState({ loading: false, refreshing: false, selectedFile: null, fileName: "" });
			return showMsj(generalMsj.error.message, true);
		}
	}

	saveUrlFile = async () => {
		const file = await this.getResource();
		console.log('file :', file);
		if (file) {
			this.setState({ isVisible: true, selectedFile: file })
		}
	}

	uploadFile = async () => {
		const file = this.state.selectedFile;
		if (file.uri) {
			let type = file.fileName.split('.');
			type = type[type.length - 1];
			let metadata = {
				contentType: `image/${type}`,
			}
			try {
				this.refs.modal.setModalVisible();
				const fileUrl = await uploadAsset('my_media', file.uri, file.fileName, metadata);
				const thumbnailImage = await getThumbnail(file.uri)
				const thumbnailURL = await uploadAsset('my_media/thumbnail', thumbnailImage.uri, file.fileName, metadata);

				let fileData = {
					url: fileUrl,
					name: this.state.fileName,
					thumbnail: thumbnailURL,
					type: "image"
				}
				return fileData;
			} catch (e) {
				console.log('Error 2', e)
				this.refs.modal.setModalVisible();
				return showMsj('An error has ocurred Uploading your file', true);
			}
		}
	}

	// saveUrlFile = async () => {
	// 	const fileUrl = await this.uploadFile();
	// 	if (fileUrl.url) {
	// 		this.setState({ modalMessage: "Uploading your file" });
	// 		try {
	// 			const response = await myMediaApi.addUserMedia(fileUrl);
	// 			this.refs.modal.setModalVisible();
	// 			showMsj(response.data, false);
	// 			this.getFiles();
	// 		} catch (e) {
	// 			this.refs.modal.setModalVisible();
	// 			return showMsj('An error has ocurred saving your file', true);
	// 		}
	// 	}
	// }

	// uploadFile = async () => {
	// 	const file = await this.getResource();
	// 	console.log('file', file)
	// 	if (file.uri) {
	// 		let type = file.fileName.split('.');
	// 		type = type[type.length - 1];
	// 		let metadata = {
	// 			contentType: `image/${type}`,
	// 		}
	// 		try {
	// 			this.refs.modal.setModalVisible();
	// 			const fileUrl = await uploadAsset('phone/users/files', file.uri, file.fileName, metadata);
	// 			const thumbnailImage = await getThumbnail(file.uri)
	// 			const thumbnailURL = await uploadAsset('phone/users/files', thumbnailImage.uri, file.fileName, metadata);

	// 			let fileData = {
	// 				url: fileUrl,
	// 				name: file.fileName,
	// 				thumbnail: thumbnailURL,
	// 				type: "image"
	// 			}
	// 			return fileData;
	// 		} catch (e) {
	// 			console.log('Error 2', e)
	// 			this.refs.modal.setModalVisible();
	// 			return showMsj('An error has ocurred Uploading your file', true);
	// 		}
	// 	}
	// }

	getResource = async () => {
		try {
			const myFile = await getImageVideo();
			console.log('myFile', myFile)
			return myFile;
		} catch (e) {
			console.log('Error 1', e)
		}
	}

	deleteFile = async (id) => {
		this.setState({ modalMessage: "Deleting your file" });
		this.refs.modal.setModalVisible();
		try {
			const response = await myMediaApi.deleteMediaUserFile(id);
			setTimeout(async () => {
				await this.refs.modal.setModalVisible();
				showMsj(response.data, false);
			}, 1000);
			this.getFiles();
		} catch (e) {
			setTimeout(async () => {
				await this.refs.modal.setModalVisible();
				showMsj(response.data, false);
			}, 1000);
			showMsj('An error has ocurred deleting your file', true);
		}
	}

	shareFile = async (data) => {
		const message = `${shareMsj.myMedia.message}\n${data.url}`
		const response = await shareContent(message);
		if (response) {
			showMsj("Shared", false);
		}
		return response;
	}

	openFile = (url) => {
		openUrl(url);
	}

	onCloseClick = () => {
		this.setState({ isVisible: false })
	}

	onSubmitClick = async () => {
		this.setState({ isVisible: false })
		if (this.state.selectedFile != null) {
			const fileUrl = await this.uploadFile();
			if (fileUrl.url) {
				this.setState({ modalMessage: "Uploading your file" });
				try {
					const response = await myMediaApi.addUserMedia(fileUrl);
					this.refs.modal.setModalVisible();
					showMsj(response.data, false);
					this.getFiles();
				} catch (e) {
					this.refs.modal.setModalVisible();
					return showMsj('An error has ocurred saving your file', true);
				}
			}
		} else {
			this.setState({ loading: true });
			try {
				let params = {
					id: this.currentData.id,
					name: this.state.fileName
				}
				const response = await myProfileApi.renameResource(params)
				console.log('response :', response);
				this.getFiles();
				// this.setState({ loading: false });
			} catch (error) {
				this.setState({ loading: false });
				showMsj(errorsValidateMsj.editProfile.error.message, true)
			}
		}
	}

	onChangeText = (fileName) => {
		this.setState({ fileName })
	}

	renderRenameModal = () => {
		return (
			<RenameModal
				isVisible={this.state.isVisible}
				onCloseClick={this.onCloseClick}
				onSubmitClick={this.onSubmitClick}
				value={this.state.fileName}
				onChangeText={this.onChangeText}
			/>
		)
	}

	renameClick = (data) => {
		console.log('data :', data);
		this.currentData = data
		this.setState({ isVisible: true, fileName: data.name })
	}

	render() {
		const { loading, files, modalMessage } = this.state;
		const { navigation: { navigate, dispatch } } = this.props;
		return (
			<Container>
				<Header
					leftIcon="backDark"
					leftFn={() => dispatch(StackActions.popToTop())}
					barStyle="dark-content"
					text="Photos"
				/>
				<Content
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={() => this._onRefresh()}
							colors={refreshControlColors}
						/>
					}
				>
					{this.renderRenameModal()}
					<View style={styles.container}>
						{
							loading && <Loading />
						}
						{
							files &&
							files.map((option, i) => (
								<Card
									key={i}
									type="photo"
									fileText={option.name}
									data={option}
									mediaImage={option.thumbnail != null ? option.thumbnail : option.url}
									deleteFile={this.deleteFile}
									shareFile={(data) => this.shareFile(data)}
									openFile={this.openFile}
									renameFile={(data) => this.renameClick(data)}
								/>
							))
						}
						{
							(files && files.length === 0) &&
							<Text style={styles.text}>You don't have any files added</Text>
						}
					</View>
				</Content>
				<ButtonAction icon={PlusIcon} onPress={() => this.saveUrlFile()} />
				<Modal ref='modal' message={modalMessage} />
			</Container>
		);
	}
}
export default PhotosView;
