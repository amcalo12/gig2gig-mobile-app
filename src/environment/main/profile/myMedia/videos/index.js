import React, { Component } from "react";
import { View, Text, RefreshControl, Alert } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import ButtonAction from "utils/components/buttonActionFloat";
import PlusIcon from "resources/images/myMedia/plus-icon.png";
import uploadAsset from "utils/uploadAsset";
import Loading from "utils/components/loading";
import { showMsj, openUrl, shareContent } from "utils/utils";
import myMediaApi from "api/myMediaApi";
import Modal from "utils/components/modalUploadFiles";
import { generalMsj, shareMsj } from "resources/l10n";
import { refreshControlColors } from "resources/colors";
import getImageVideo from "utils/getImageVideo";
import AddVideoModal from "./addVideoModal";
import VideoCard from "utils/components/videoCard";
import isYoutubeUrl from "utils/isYoutubeUrl";
import RNThumbnail from "react-native-thumbnail";
import RenameModal from "utils/components/renameModal";
import myProfileApi from "api/myProfileApi";

class VideosView extends Component {
  constructor(props){
    super(props)
    this.currentData = null
  }
  state = {
    loading: false,
    refreshing: false,
    files: null,
    modalMessage: "",
    visible: false,
    isVisible:false
  };

  async componentDidMount() {
    this.getFiles();
  }

  _onRefresh = async () => {
    this.getFiles(true);
  };

  getFiles = async (refresh = false) => {
    if (refresh) {
      this.setState({ refreshing: true });
    } else {
      this.setState({ loading: true });
    }
    try {
      const response = await myMediaApi.getUserMediaByType("video");
      console.log("responseresponse", response);
      this.setState({
        files: response.data,
        loading: false,
        refreshing: false
      });
    } catch (e) {
      this.setState({ loading: false, refreshing: false });
      return showMsj(generalMsj.error.message, true);
    }
  };

  saveUrlFile = async () => {
    this.setState({ modalMessage: "Uploading your file" });
    const fileUrl = await this.uploadFile();
    console.log("fileUrlfileUrlfileUrl", fileUrl);
    try {
      const response = await myMediaApi.addUserMedia(fileUrl);
      this.refs.modal.setModalVisible();
      showMsj(response.data, false);
      this.getFiles();
    } catch (e) {
      console.log(e);
      if (fileUrl) {
        this.refs.modal.setModalVisible();
        return showMsj("An error has ocurred saving your file", true);
      }
    }
  };

  uploadFile = async () => {
    const file = await this.getResource();
    if (file) {
      let metadata = {
        contentType: `video/${file.type}`
      };
      try {
        const min = 1;
        const max = 10000;
        const rand = min + Math.random() * (max - min);
        this.refs.modal.setModalVisible();
        const fileUrl = await uploadAsset(
          // "phone/users/files",
          "my_media",
          file.uri,
          file.fileName,
          metadata
        );
        let fileData = {
          url: fileUrl,
          name: `VID_${rand}.mp4`,
          type: "video"
        };
        return fileData;
      } catch (e) {
        console.log("22", e);
        this.refs.modal.setModalVisible();
        return showMsj("An error has ocurred Uploading your file", true);
      }
    }
  };

  getResource = async () => {
    try {
      const myFile = await getImageVideo("video");
      return myFile;
    } catch (e) {
      if (e.didCancel) {
      } else {
        this.refs.modal.setModalVisible();

        return showMsj("An error has ocurred getting your file", true);
      }
    }
  };

  deleteFile = async id => {
    this.setState({ modalMessage: "Deleting your file" });
    this.refs.modal.setModalVisible();
    try {
      const response = await myMediaApi.deleteMediaUserFile(id);
      setTimeout(async () => {
        await this.refs.modal.setModalVisible();
        showMsj(response.data, false);
      }, 1000);
      await this.getFiles();
    } catch (e) {
      this.refs.modal.setModalVisible();
      showMsj("An error has ocurred deleting your file", true);
    }
  };

  shareFile = async data => {
    const message = `${shareMsj.myMedia.message}\n${data.url}`;
    const response = await shareContent(message);
    if (response) {
      showMsj("Shared", false);
    }
    return response;
  };

  openFile = url => {
    openUrl(url);
  };

  addVideo = () => {
    this.setState({
      visible: true
    });
  };

  onPressBackModal = async () => {
    this.setState({
      visible: false
    });
    await this.getFiles();
  };

  openDeleteConfirmationAlert(id) {
    Alert.alert(
      "Delete Video",
      "Are you sure you want to delete this video?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => this.deleteFile(id)
        }
      ],
      { cancelable: false }
    );
  }

  onCloseClick = () => {
		this.setState({ isVisible: false })
	}

	onSubmitClick = async () => {
		this.setState({ isVisible: false })
	
			this.setState({ loading: true });
			try {
				let params = {
					id: this.currentData.id,
					name: this.state.fileName
				}
				const response = await myProfileApi.renameResource(params)
				console.log('response :', response);
				this.getFiles();
				// this.setState({ loading: false });
			} catch (error) {
				this.setState({ loading: false });
				showMsj(errorsValidateMsj.editProfile.error.message, true)
			}
		
	}

	onChangeText = (fileName) => {
		this.setState({ fileName })
	}

	renderRenameModal = () => {
		return (
			<RenameModal
				isVisible={this.state.isVisible}
				onCloseClick={this.onCloseClick}
				onSubmitClick={this.onSubmitClick}
				value={this.state.fileName}
				onChangeText={this.onChangeText}
			/>
		)
	}

  render() {
    console.log("SIIII");
    const { loading, files, modalMessage, visible } = this.state;
    const {
      navigation: { navigate, dispatch }
    } = this.props;

    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          barStyle="dark-content"
          text="Videos"
        />
        <Content
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this._onRefresh()}
              colors={refreshControlColors}
            />
          }
        >
          {this.renderRenameModal()}
          <View style={styles.container}>
            {loading && <Loading />}
            {files &&
              files.map((option, i) => {
                console.log(option);
                return (
                  <VideoCard
                    key={i}
                    name={option.name}
                    url={
                      isYoutubeUrl(option.url)
                        ? isYoutubeUrl(option.url)
                        : option.url
                    }
                    isYouTubeUrl={isYoutubeUrl(option.url)}
                    // url={isYoutubeUrl(option.url)}
                    // isYouTubeUrl={true}
                    onDeletePress={() => {
                      this.openDeleteConfirmationAlert(option.id);
                    }}
                    onRename={()=>{
                      this.currentData = option
                        this.setState({isVisible:true,fileName:option.name})
                    }}
                  />
                );
              })}
            {files && files.length === 0 && (
              <Text style={styles.text}>You don't have any files added</Text>
            )}
          </View>
        </Content>
        <ButtonAction icon={PlusIcon} onPress={() => this.addVideo()} />
        <Modal ref="modal" message={modalMessage} />
        {!loading && (
          <AddVideoModal
            onPressBackModal={() => this.onPressBackModal()}
            visible={visible}
          />
        )}
      </Container>
    );
  }
}
export default VideosView;
