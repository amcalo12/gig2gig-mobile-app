import React, { Component } from "react";
import { TextInput, View, Image, Platform, TouchableOpacity, Text, Dimensions } from "react-native";
import { Card } from "native-base";
import styles from "./index.styles";
// const isIos = Platform.OS === "ios";


class Input extends Component {
	getStyles() {
		let inputStyles = []
		let touchableStyles = []
		const { multiline, short, customWidth, margin, elevation, isIos, customHeigth } = this.props;

		if (short) {
			inputStyles.push(styles.shortInput);
		} else {
			inputStyles.push(styles.container);
		}

		if (multiline) {
			inputStyles.push({ paddingTop: 20 })
		}

		if (customWidth) {
			if (customHeigth) {
				inputStyles.push({ height: customHeigth, width: customWidth, marginBottom: margin, elevation: elevation })
			}
			inputStyles.push({ width: customWidth, marginBottom: margin, elevation: elevation })
		} else {
			inputStyles.push({ marginBottom: margin, elevation: elevation })
		}

		return inputStyles;


	}

	render() {
		const {
			placeholder,
			onEdit,
			data,
			isSecure,
			keyboardType,
			icon,
			leftIcon,
			short,
			isTouchable,
			onPress,
			multiline,
			alignTop,
			numberOfLines,
			reference,
			autoCapital
		} = this.props;

		return (
			<Card
				style={this.getStyles()}>
				<TouchableOpacity
					ref={view => {
						if (reference && view) {
							reference(view)
						}
					}}
					activeOpacity={isTouchable ? 0.2 : 1}
					style={styles.touchableContainer}
					onPress={isTouchable ? () => onPress() : null}
				>
					{
						leftIcon &&
						<View>
							<Image style={styles.leftIcon} source={leftIcon} />
						</View>
					}
					{
						isTouchable &&
						<Text numberOfLines={numberOfLines} style={[styles.input2, { width: icon ? (short ? '75%' : '85%') : '100%' }, autoCapital ? { textTransform: "capitalize" } : {}]}>{(!data) ? placeholder : data}</Text>
					}
					{
						!isTouchable &&
						<TextInput
							multiline={multiline}
							style={[styles.input, { width: icon ? (short ? '75%' : '85%') : '100%' }, alignTop && styles.alignTop]}
							placeholder={placeholder}
							placeholderTextColor="#4D2545"
							onChangeText={(text) => onEdit(text)}
							value={data}
							secureTextEntry={isSecure}
							keyboardType={keyboardType}
							// keyboardType={"email-address"}
							autoCapitalize={autoCapital ? "sentences" : "none"}
						/>
					}
					{
						icon &&
						<Image style={styles.icon} source={icon} />
					}
				</TouchableOpacity>
			</Card>
		);
	}
}
Input.defaultProps = {
	customWidth: null,
	customHeigth: null,
	isSecure: false,
	margin: 20,
	short: false,
	elevation: 0,
	keyboardType: "default",
	multiline: false,
	alignTop: false,
	numberOfLines: 2
};
export default Input;
