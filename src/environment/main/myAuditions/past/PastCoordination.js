import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import InstantFeedbackView from "./InstantFeedback";

const PastStack = createStackNavigator(
  {
    InstantFeedback: {
      screen: InstantFeedbackView,
      navigationOptions: { header: null }
    },
  },
  {
    initialRouteName: "InstantFeedback",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default PastStack;
