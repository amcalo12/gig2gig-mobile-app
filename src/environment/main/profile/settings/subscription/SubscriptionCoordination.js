import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import SubscriptionView from ".";
import ManageView from "./manage";
import CardDetailsView from "./cardDetails";
import AddCard from "./addCard";
import EditCard from "./editCard";

const SubscriptionStack = createStackNavigator(
  {
     Subscription: {
       screen: SubscriptionView,
       navigationOptions: {
         header: null
       }
     },
     Manage: {
       screen: ManageView,
       navigationOptions: {
         header: null
       }
     },
     CardDetails: {
       screen: CardDetailsView,
       navigationOptions: {
         header: null
       }
     },
     AddCard: {
      screen: AddCard,
      navigationOptions: {
        header: null
      },
    },
    EditCard: {
      screen: EditCard,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "CardDetails",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default SubscriptionStack;
