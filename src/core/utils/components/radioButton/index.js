import React, { Component } from "react";
import { Text, TouchableOpacity, Image } from "react-native";
import styles from "./index.styles";
import RadioButtonChecked from "resources/images/radio-button-checked.png";
import RadioButtonEmpty from "resources/images/radio-button.png";


class RadioButton extends Component {

  render() {
    const {text, checked, onPress} = this.props;
    return (
        <TouchableOpacity activeOpacity={0.6} style={styles.container} onPress={()=>onPress()}>
          <Image source={checked ? RadioButtonChecked : RadioButtonEmpty}/>
          <Text style={styles.text}>{text}</Text>
        </TouchableOpacity>
    );
  }
}
RadioButton.defaultProps = {
  checked:false
};
export default RadioButton;
