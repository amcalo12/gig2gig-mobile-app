import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import {resize} from "utils/utils"

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  content:{
    marginTop: isIos ? height * 0.01 : height * 0.006
  },
  container:{
    width:"100%",
    marginTop: height * 0.02,
    paddingHorizontal: height * 0.025,
    marginBottom: height * 0.1
  },
  filtersContainer:{
    flexDirection:"row",
    marginBottom: height * 0.016,
    flexWrap: "wrap"
  },
  filtersMarginBottom: {
    marginBottom: resize(8, "height")
  },
  titleContainer:{
    flexDirection:"row",
    alignItems: 'center',
    marginBottom: height * 0.018,
    width:resize(340),
  },
  title:{
    fontFamily:fontStyles.nexa_bold,
    fontSize:24,
    color:colorPalette.purple,
    letterSpacing:2,
    width:"70%",
  },
  subtitle:{
    fontFamily:fontStyles.nexa_bold,
    fontSize:15,
    color:colorPalette.purple,
    letterSpacing:1,
    // marginBottom: height * 0.005,
    width: "30%",
    textAlign:"right",

  },
  roleTitle:{
    fontFamily: fontStyles.nexa_bold,
    fontSize:20,
    color:colorPalette.purple,
    letterSpacing:1,
    marginBottom: height * 0.010
  },
  text:{
    fontFamily:fontStyles.nexa_light,
    fontSize:13,
    color: colorPalette.purple,
    letterSpacing: 1,
    lineHeight: height * 0.026
  },
  actionButtonsContainer:{
    flexDirection:"row",
    justifyContent:"space-around",
    marginTop: height * 0.035
  },
  actionButtons: {
    width: resize(164)
  }

});