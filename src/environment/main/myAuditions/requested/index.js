import React, { Component } from "react";
import { View, Text, RefreshControl, ScrollView, AsyncStorage } from "react-native";
import { Content, Spinner } from "native-base";
import styles from "./index.styles";
import Card from "utils/components/auditionCard";
import Modal from "utils/components/ModalRequest";
import myAuditionsApi from "api/myAuditionsApi";
import Loading from "utils/components/loading";
import AuditionCard from "utils/components/auditionCard";
import { refreshControlColors } from "resources/colors";
import auditionsApi from "api/auditionsApi";
import { showMsj } from "utils/utils";
import { generalMsj } from "resources/l10n";
import { withNavigationFocus } from "react-navigation";
import Swipeout from "react-native-swipeout";

class RequestedTab extends Component {
  state = {
    loading: false,
    refreshing: false,
    auditions: [],
    modalData: null,
    times: [],
    walkAppont: [],
    cellOpen: null,
    idUser: null,
  };

  _onRefresh = async () => {
    // this.getRequestedAuditions(true);
    this.props.reloadFn();
  };

  async componentDidMount() {
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    this.setState({ idUser: userData.id });
  }

  show = async data => {
    const { idUser } = this.state;
    const { navigation: { navigate } } = this.props;
    if (data.online === 1) {
      navigate("Online", { data, idUser, isUpload: true })
    } else {
      this.setState({ modalData: data });
      this.refs.modal.setModalVisible();
      this.getTimesUpcoming(data.appointment_id);
      this.getWalk(data.appointment_id);
    }

  };

  getWalk = async id => {
    try {
      const walkAppont = await auditionsApi.gestWalk(id);
      this.setState({ walkAppont: walkAppont.data.slots });
    } catch (error) {
      console.log(error);
      this.setState({ walkAppont: [] });
    }
  };

  getTimesUpcoming = async id => {
    const response = await myAuditionsApi.getTimesToUpcoming(id);
    this.setState({
      times: response.data.slots
    });
  };
  removeAudition = async id => {
    try {
      this.setState({ loading: true });
      let res = await myAuditionsApi.swipeToRemoveAudition(id);
      this.setState({ cellOpen: null });
      this.props.reloadFn();
      setTimeout(() => {
        this.setState({ loading: false });
      }, 5000);
    } catch (e) {
      this.setState({ cellOpen: null });
    }
  };
  render() {
    const { modalData, times, walkAppont } = this.state;
    const { data, reloadFn } = this.props;

    return (
      <ScrollView
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={() => this._onRefresh()}
            colors={refreshControlColors}
          />
        }
      >
        {!data && <Loading />}
        {data &&
          data.map((item, idx) => {
            console.log('item online :>> ', item);
            let isOnline = item.online === 1;
            return (
              <Swipeout
                style={styles.swipeout}
                key={idx}
                // openRight={cellOpen == i}
                close={this.state.cellOpen != idx}
                onClose={(sectionId, rowId, direction) => { }}
                onOpen={(sectionId, rowId, direction) => {
                  this.setState({
                    cellOpen: idx
                  });
                }}
                right={[
                  {
                    onPress: () => {
                      this.removeAudition(item.id);
                    },
                    text: "Delete",
                    color: "white",
                    backgroundColor: "#93183e",
                    activeOpacity: 0.7
                  }
                ]}
              >
                <AuditionCard
                  key={idx}
                  data={item}
                  isSubmit
                  isOnline={item.online === 1}
                  onPress={() => this.show(item)}
                  isRequested
                />
              </Swipeout>
            );
          })}
        {data && data.length === 0 && (
          <Text style={styles.text}>
            You don't have any requested auditions
          </Text>
        )}
        {this.state.loading && (
          <View
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              width: "100%",
              height: "100%",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Spinner color={"rgba(0,0,0,0.5)"} />
          </View>
        )}
        <Modal
          ref="modal"
          times={times}
          data={modalData}
          reloadFn={() => reloadFn()}
          walkAppont={walkAppont}
        />
      </ScrollView>
    );
  }
}
export default withNavigationFocus(RequestedTab);
