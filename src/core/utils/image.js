import * as firebase from "firebase";
import { Platform, PermissionsAndroid } from "react-native";
import RNFetchBlob from "rn-fetch-blob";
import ImagePicker from "react-native-image-picker";
import moment from "moment";

export const takePhotoOptions = {
	title: "Select a photo",
	cameraType: "back",
	mediaType: "photo",
	maxWidth: 640,
	maxHeight: 427,
	// allowsEditing:true,
	storageOptions: {
		cameraRoll: true,
		path: "itemfiles",
		skipBackup: true // No need to backup to iCloud
	}
};


export const uploadImage = (image, mime = "application/octet-stream") => {
	return new Promise((resolve, reject) => {
		const uploadUri =
			Platform.OS === "ios" ? image.uri.replace("file://", "") : image.uri;
		let uploadBlob = null;

		const Blob = RNFetchBlob.polyfill.Blob;
		const fs = RNFetchBlob.fs;
		window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
		window.Blob = Blob;

		const imageRef = firebase
			.storage()
			.ref(moment().format())
			.child(moment().format());

		fs.readFile(uploadUri, "base64")
			.then(data => {
				return Blob.build(data, { type: `${mime};BASE64` });
			})
			.then(blob => {
				uploadBlob = blob;
				console.log('blob', blob)
				return imageRef.put(blob, { contentType: mime });
			})
			.then(() => {
				uploadBlob.close();
				return imageRef.getDownloadURL();
			})
			.then(url => {
				resolve(url);
			})
			.catch(error => {
				reject(error);
			});
	});
};

export const asyncGetPicture = async () => {
	return new Promise((resolve, reject) => {
		ImagePicker.showImagePicker(takePhotoOptions, response => {
			console.log('image response selected :', response);
			if (response.didCancel) {
				reject(response);
			} else if (response.error) {
				reject(response);
			} else {
				resolve(response);
			}
		});
	});
};
