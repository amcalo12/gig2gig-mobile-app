import { apiHandler } from "./apiMethods";
import { API_METHOD } from "utils/cons";

export const createOnline = async (data) =>
	apiHandler(
		API_METHOD.POST,
		`a/media/online`,
		data,
	);
;

export const renameOnlineResource = async (data) =>
	apiHandler(
		API_METHOD.POST,
		`media/update/AuditionVideoName`,
		data,
	);
;
export const validateDataOnline = async (data) =>
	apiHandler(
		API_METHOD.GET,
		`media/online`,
		data,
		false,
		true
	);
;
