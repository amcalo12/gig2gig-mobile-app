import { Platform } from "react-native";
import auditionsApi from "api/auditionsApi";
import { generalMsj, shareMsj } from "resources/l10n";
import { showMsj, shareContent } from "utils/utils";
import { directionMaps, getLocation } from "utils/utils";
import SystemSetting from "react-native-system-setting";
import { requestPermission } from "utils/permissions";

export const loadAuditionData = async context => {
  context.setState({ loading: true });
  const id = context.props.navigation.state.params.id;
  const auditionCover = context.props.navigation.state.params.image;
  try {
    const auditionData = await auditionsApi.getAuditionData(id);
    const walkAppont = await auditionsApi.gestWalk(
      auditionData.data.appointment_id
    );
    console.log('auditionData.data.location :>> ', auditionData.data.location);
    if (auditionData.data.location) {
      console.log('auditionData.data.location :>> INSIDE');
      const locationName = await getPlaceName(
        auditionData.data.location.latitude,
        auditionData.data.location.longitude
      );
      console.log('locationName :>> ', locationName);
      context.setState({
        auditionData: auditionData.data,
        auditionCover,
        region: {
          latitude: Number(auditionData.data.location.latitude),
          longitude: Number(auditionData.data.location.longitude),
          latitudeDelta: Number(auditionData.data.location.latitudeDelta),
          longitudeDelta: Number(auditionData.data.location.longitudeDelta)
        },
        marker: {
          latitude: Number(auditionData.data.location.latitude),
          longitude: Number(auditionData.data.location.longitude)
        },
        locationName,
        loading: false,
        walkAppont: walkAppont.data.slots
      });
    } else {
      context.setState({
        auditionData: auditionData.data,
        auditionCover,
        loading: false,
        walkAppont: walkAppont.data.slots
      });
    }
  } catch (e) {
    console.log(e);
    context.setState({ loading: false });
  }
};

export const showGoogleMaps = async context => {
  const myLocation = await getLocation();
  const {
    geometry: {
      location: { lat, lng }
    }
  } = myLocation;
  // directionMaps(`http://maps.apple.com/?ll=${this.state.location.coords.latitude},${this.state.location.coords.longitude}`)
  directionMaps(
    `http://maps.google.com/maps?saddr=${lat},${lng}&daddr=${context.state.region.latitude},${context.state.region.longitude}`
  );
  // directionMaps(lat,lng);
};

export const getPlaceName = async (latitude, longitude) => {
  const myLocation = await getLocation(latitude, longitude);
  console.log('myLocation :>> ', myLocation);
  return myLocation && myLocation.formatted_address
    ? myLocation.formatted_address
    : myLocation && myLocation.address_components.length > 0
      ? myLocation.address_components[0].short_name
      : "";
};

export const handleLocation = async context => {
  const isAndroid = Platform.OS === "android";
  if (isAndroid) {
    const Permissions = await requestPermission("ACCESS_FINE_LOCATION", {
      title: "Location",
      message: "¿Allow G2G to access the location of this device?"
    });
    if (Permissions) {
      const locationEnable = await SystemSetting.isLocationEnabled();
      if (locationEnable) {
        try {
          showGoogleMaps(context);
        } catch (e) {
          console.log(e);
          context.setState({ loading: false });
          return showMsj("A location error has ocurred", true);
        }
      } else {
        SystemSetting.switchLocation(() => {
          SystemSetting.isLocationEnabled().then(async enable => {
            const state = enable ? "On" : "Off";
            console.log("Current location is " + state);
            if (enable) {
              try {
                showGoogleMaps(context);
              } catch (e) {
                context.setState({ loading: false });
                return showMsj("A location error has ocurred", true);
              }
            } else {
              context.setState({
                loading: false
              });
              return showMsj(
                errorsValidateMsj.signup.location.messagePermission,
                true
              );
            }
          });
        });
      }
    } else {
      context.setState({
        loading: false
      });
      return showMsj(errorsValidateMsj.signup.location.messagePermission, true);
    }
  } else {
    try {
      showGoogleMaps(context);
    } catch (e) {
      context.setState({ locationLoad: false });
      return showMsj("A location error has ocurred", true);
    }
  }
};

export const shareAudition = context => {
  const { auditionData } = context.state;
  const message = `${shareMsj.Auditions.first} ${auditionData.title} ${shareMsj.Auditions.second}
                    \n${shareMsj.Auditions.third}
                    ${shareMsj.Auditions.Apple}
                    ${shareMsj.Auditions.Android}
                    `;
  shareContent(message);
};
