import React, { Component } from "react";
import { View } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import PlusIcon from "resources/images/myCalendar/plus-icon.png"
import { withNavigationFocus } from 'react-navigation';
import { StackActions } from 'react-navigation';
import Calendar from 'utils/components/CalendarList'
import myCalendarViewModel from './myCalendarViewModel'
import Loading from "utils/components/loading";
class MyCalendarView extends Component {
  constructor() {
    super()
    this.state = {
      ranges: {},
      dates: [],
      loading: true
    }
  }
  addEvent = (date) => {
    const dates = this.state.dates
    dates.push(date)
    this.setState({ dates })
    myCalendarViewModel.addEventInCalendar(date, this)
  }
  async componentDidMount() {
    await myCalendarViewModel._handlerGetEvents(this)
  }

  async componentDidUpdate(prevProps) {
    if ((prevProps.isFocused !== this.props.isFocused) && (this.props.isFocused)) {
      await myCalendarViewModel._handlerGetEvents(this)
    }
  }

  render() {
    const { navigation: { navigate, dispatch } } = this.props;

    const {
      loading
    } = this.state

    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          barStyle="dark-content"
          text="My Calendar"
          rightIcon={PlusIcon}
          rightFn={() => navigate("AddEvent", { 'onGoBack': this.addEvent })}
        />
        <Content>
          {console.log('this.state.ranges :', this.state.ranges)}
          <View style={styles.container}>
            {
              loading ?
                <Loading />
                :
                <Calendar
                  ranges={this.state.ranges}
                  headerMonthPosition={'flex-start'}
                />
            }
          </View>
        </Content>
      </Container>
    );
  }
}
export default withNavigationFocus(MyCalendarView);
