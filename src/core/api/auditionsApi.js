import { apiHandler } from './apiMethods';
import { API_METHOD, API_MODULES, API_ROUTES } from 'utils/cons';

const getAuditions = async () =>
	apiHandler(API_METHOD.GET, API_ROUTES.AUDITIONS.GET_AUDITIONS);

const getAuditionData = async id =>
	apiHandler(API_METHOD.GET, `${API_ROUTES.AUDITIONS.GET_AUDITION_DATA}${id}`);

const getFilterAuditions = async props =>
	apiHandler(API_METHOD.POST, API_ROUTES.AUDITIONS.GET_FILTER_AUDITIONS, props);

export const flagAudition = async props =>
	apiHandler(API_METHOD.POST, `a/auditions/banned`, props);

const requestAudition = async props =>
	apiHandler(API_METHOD.POST, API_ROUTES.AUDITIONS.REQUEST_AUDITION, props);

const gestWalk = async id =>
	apiHandler(API_METHOD.GET, `appointments/show/${id}/walk`);

export default {
	getAuditions,
	getAuditionData,
	getFilterAuditions,
	requestAudition,
	gestWalk,
};
