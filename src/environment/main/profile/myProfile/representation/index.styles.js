import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container: {
    marginBottom: resize(30, "height"),
    height: resize(520, "height")
  },
  labels: {
    paddingLeft: resize(40),
  },
  headerText: {
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 17,
    alignSelf: 'center',
    textTransform: "capitalize",
    paddingVertical: resize(20, "height"),
  },
  button: {
    marginTop: resize(170, "height"),
  }
});