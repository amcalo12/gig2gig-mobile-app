import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const categories = async props =>
  apiHandler(
    API_METHOD.GET,
    API_MODULES.MARKETPLACE_CATEGORIES,
  );

export const createMarket = async (data) =>
  apiHandler(
    API_METHOD.POST,
    `a/marketplaces/create`,
    data,
  );



const cardsForFilter = async id =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.MARKETPLACE_CATEGORIES}/${id}/${API_ROUTES.MARKETPLACE_CATEGORIES.SEARCH_FOR_CATEGORIES}`
  );

const cardsForText = async title =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.MARKETPLACE}/${API_ROUTES.MARKETPLACE.CARDS_FOR_TEXT}${title}`,
    null,
    false
  );

const cardsForFilterAndText = async (id, title) =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.MARKETPLACE_CATEGORIES}/${id}/${API_ROUTES.MARKETPLACE_CATEGORIES.SEARCH}${title}`,
    null,
    false
  );


export default {
  categories,
  cardsForFilter,
  cardsForText,
  cardsForFilterAndText
};
