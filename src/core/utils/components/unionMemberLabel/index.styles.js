import {StyleSheet, Platform, Dimensions} from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils"
const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
  item:{
    width: "100%",
    flexDirection: "row",
    justifyContent:"space-between",
    alignItems:"center",
    borderBottomWidth: 1,
    borderColor: "#D6D6D6",
    paddingVertical: isIos ? resize(20, "height") : resize(15, "height"),
  },
  noLine:{
    borderBottomWidth: 0
  },
  itemText:{
    fontFamily:fontStyles.nexa_light,
    fontSize:20,
    color:colorPalette.purple
  },
  icon:{
    marginRight:20
  }
});