import React, { Component } from "react";
import { View, Text, RefreshControl, AsyncStorage } from "react-native";
import { Content, Spinner } from "native-base";
import styles from "./index.styles";
import Card from "utils/components/auditionCard";
import { withNavigationFocus } from "react-navigation";
import myAuditionsApi from "api/myAuditionsApi";
import Loading from "utils/components/loading";
import { showMsj } from "utils/utils";
import { generalMsj } from "resources/l10n";
import { refreshControlColors } from "resources/colors";
import auditionsApi from "api/auditionsApi";
import Modal from "utils/components/ModalRequest";
import Swipeout from "react-native-swipeout";

class UpcomingTab extends Component {
  state = {
    loading: false,
    refreshing: false,
    auditions: [],
    idUser: null,
    modalData: null,
    times: [],
    walkAppont: [],
    selectedAuditionId: null,
    cellOpen: null
  };

  async componentDidMount() {
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    this.setState({ idUser: userData.id });
  }

  _onRefresh = async () => {
    // this.getUpcomingAuditions(true);
    this.props.reloadFn();
  };

  show = async data => {
    this.setState({ modalData: data });
    this.getTimesUpcoming(data, data.appointment_id);
    // this.getWalk(data.appointment_id);
  };

  // getWalk = async id => {
  //   try {
  //     const walkAppont = await auditionsApi.gestWalk(id);
  //     console.log("walkAppont list");
  //     console.log(JSON.stringify(walkAppont));
  //     this.setState({ walkAppont: walkAppont.data.slots });
  //   } catch (error) {
  //     console.log(error);
  //     this.setState({ walkAppont: [] });
  //   }
  // };

  getTimesUpcoming = async (item, id) => {
    const response = await myAuditionsApi.getTimesToUpcoming(id);
    if (response.data.slots && response.data.slots.length > 0) {
      this.refs.modal.setModalVisible();
      this.setState({
        times: response.data.slots
      });
    } else {
      this.moveToUpcoming(item, this.state.idUser);
    }
  };
  moveToUpcoming = (item, idUser) => {
    this.props.navigation.navigate("Upcoming", {
      data: item,
      idUser,
      refreshListCallback: this._onRefresh.bind(this)
    });
  };
  removeAudition = async id => {
    try {
      this.setState({ loading: true });
      let res = await myAuditionsApi.swipeToRemoveAudition(id);
      this.setState({ cellOpen: null });
      this.props.reloadFn();
      setTimeout(() => {
        this.setState({ loading: false });
      }, 5000);
    } catch (e) {
      this.setState({ cellOpen: null });
    }
  };
  render() {
    const {
      auditions,
      loading,
      idUser,
      times,
      modalData,
      walkAppont
    } = this.state;
    const {
      data,
      navigation: { navigate }
    } = this.props;

    return (
      <Content
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={() => this._onRefresh()}
            colors={refreshControlColors}
          />
        }
      >
        {data &&
          data.map((item, idx) => {
            let online = item.online === 1;
            if (online) {
              console.log('item online:>> ', item);
              return (
                <Swipeout
                  style={styles.swipeout}
                  key={idx}
                  // openRight={cellOpen == i}
                  close={this.state.cellOpen != idx}
                  onClose={(sectionId, rowId, direction) => { }}
                  onOpen={(sectionId, rowId, direction) => {
                    this.setState({
                      cellOpen: idx
                    });
                  }}
                  right={[
                    {
                      onPress: () => {
                        this.removeAudition(item.id);
                      },
                      text: "Delete",
                      color: "white",
                      backgroundColor: "#93183e",
                      activeOpacity: 0.7
                    }
                  ]}
                >
                  <Card
                    key={idx}
                    data={item}
                    isOnline
                    isSubmitted
                    onPress={() => navigate("Online", { data: item, idUser, isUpload: false })}
                  />
                </Swipeout>
              );
            }
            if (!online) {
              return (
                <Swipeout
                  style={styles.swipeout}
                  key={idx}
                  // openRight={cellOpen == i}
                  close={this.state.cellOpen != idx}
                  onClose={(sectionId, rowId, direction) => { }}
                  onOpen={(sectionId, rowId, direction) => {
                    this.setState({
                      cellOpen: idx
                    });
                  }}
                  right={[
                    {
                      onPress: () => {
                        this.removeAudition(item.id);
                      },
                      text: "Delete",
                      color: "white",
                      backgroundColor: "#93183e",
                      activeOpacity: 0.7
                    }
                  ]}
                >
                  <Card
                    key={idx}
                    data={item}
                    isUpcoming
                    onPress={() => {
                      if (item.round && item.round > 1) {
                        if (item.hour !== null) {
                          this.setState({ selectedAuditionId: null });
                          this.moveToUpcoming(item, idUser);
                        } else {
                          this.setState({ selectedAuditionId: item.id });
                          this.show(item);
                        }
                      } else {
                        this.setState({ selectedAuditionId: null });
                        this.moveToUpcoming(item, idUser);
                      }
                    }}
                  />
                </Swipeout>
              );
            }
          })}
        {data && data.length === 0 && (
          <Text style={styles.text}>You don't have any upcoming auditions</Text>
        )}
        {!data ? <Loading /> : null}
        {this.state.loading && (
          <View
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              width: "100%",
              height: "100%",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Spinner color={"rgba(0,0,0,0.5)"} />
          </View>
        )}

        <Modal
          ref="modal"
          times={times}
          data={modalData}
          reloadFn={() => {
            this.props.reloadFn();
            setTimeout(() => {
              if (this.state.selectedAuditionId !== null) {
                let auditionData = null;
                data.map((item, idx) => {
                  if (item.id === this.state.selectedAuditionId) {
                    auditionData = data[idx];
                  }
                });
                if (auditionData !== null) {
                  this.moveToUpcoming(auditionData, idUser);
                }
              }
            }, 1500);
          }}
          walkAppont={walkAppont}
          isFromUpcoming={true}
        />
      </Content>
    );
  }
}
export default withNavigationFocus(UpcomingTab);
