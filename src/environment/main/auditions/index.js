import React, { Component } from "react";
import { View, RefreshControl, Text, AsyncStorage, Dimensions, Platform, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import AuditionCard from "utils/components/auditionCard";
import ModalSerch from "utils/components/searchModal";
import Loading from "utils/components/loading";
import { refreshControlColors, colorPalette } from "resources/colors";
import { withNavigationFocus } from "react-navigation";
import {
  loadAuditions,
  show,
  _onRefresh,
  findAudition
} from "./AuditionsViewModel";
import { fontStyles } from "resources/fonts";
import auditionsApi from "api/auditionsApi";

//clear_input
const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);
class AuditionsView extends Component {
  constructor(props) {
    super(props);
    this.isFirstTime = true;
  }

  state = {
    auditions: [],
    loading: false,
    refreshing: false,
    locationTitle: "",
    auditionFilters: {
      "base": "",
      "union": "",
      "contract": "",
      "production": "",
      "lat": "",
      "lng": ""
    },
    filterText: "",
    styleData: null,
    handleNewData: false
  };

  async componentDidUpdate(props) {
    // if (props.isFocused !== this.props.isFocused && this.props.isFocused) {
    //   loadAuditions(false, this);
    // }
  }

  async componentDidMount() {
    AsyncStorage.getItem("isFirstTime").then(
      data => {
        if (data != null) {
          let asyncData = JSON.parse(data);
          console.log(" get data ", data);
          this.isFirstTime = asyncData.isFirstTime;
          console.log(" this.isFirstTime ", this.isFirstTime);
        }
      },
      fail => { }
    );
    loadAuditions(false, this);
  }

  getFilters = auditionFilters => {
    this.setState({
      auditionFilters
    });
  };

  callBack = async (data) => {
    console.log('data :', data);
    // alert(JSON.stringify(data))

    this.state.auditionFilters['lat'] = data.latitude
    this.state.auditionFilters['lng'] = data.longitude
    console.log('this.state.auditionFilter :', this.state.auditionFilters);

    let params = {
      base: this.state.auditionFilters.base || "",
      union: this.state.auditionFilters.union || "",
      contract: this.state.auditionFilters.contract || "",
      production: this.state.auditionFilters.production || "",
      lat: data.latitude,
      lng: data.longitude
    }
    console.log('params ======= :', params);
    this.setState({ loading: true, locationTitle: data.locationTitle });
    try {
      const auditions = await auditionsApi.getFilterAuditions(params);
      console.log('auditions :', auditions);
      if (auditions.data != undefined) {
        this.setState({ auditions: auditions.data, loading: false });
      } else {
        this.setState({ auditions: [], loading: false });
      }
    } catch (e) {
      console.log(e)
      this.setState({ loading: false, auditions: [] });
    }
    // findAudition(this, false)
  }

  onClearLocation = async () => {
    this.state.auditionFilters['lat'] = ""
    this.state.auditionFilters['lng'] = ""
    console.log('this.state.auditionFilter :', this.state.auditionFilters);

    let params = {
      base: this.state.auditionFilters.base || "",
      union: this.state.auditionFilters.union || "",
      contract: this.state.auditionFilters.contract || "",
      production: this.state.auditionFilters.production || "",
      lat: "",
      lng: ""
    }
    console.log('params ======= :', params);
    this.setState({ loading: true, locationTitle: "" });
    try {
      const auditions = await auditionsApi.getFilterAuditions(params);
      if (auditions.data != undefined) {
        this.setState({ auditions: auditions.data, loading: false });
      } else {
        this.setState({ auditions: [], loading: false });
      }
      // this.setState({ auditions: auditions.data, loading: false });
    } catch (e) {
      console.log(e)
      this.setState({ loading: false, auditions: [] });
    }
  }

  show = styles => {
    this.setState({
      styleData: styles
    });
    this.refs.modal.setModalVisible();
  };

  handleData = () => {
    this.setState({ handleNewData: false });
  };

  render() {
    const {
      navigation: { navigate }
    } = this.props;
    const {
      auditions,
      loading,
      auditionFilters,
      filterText,
      styleData,
      handleNewData
    } = this.state;
    let auditionsToRender = auditions.filter(audition => {
      let title = audition.title.toUpperCase();
      return title.match(new RegExp(filterText.toUpperCase(), "g"));
    });

    return (
      <Container>
        <Header
          isSearch={true}
          onPressclearData={() => {
            this.setState({ filterText: "" });
            loadAuditions(false, this);
          }}
          onChangeText={filterText => {
            this.setState({ filterText });
            if (filterText.length === 0) {
              loadAuditions(false, this);
            }
          }}
          value={filterText}
          onSearch={() => alert("find audition")}
          onPress={this.show}
        />

        <Content
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => _onRefresh(this)}
              colors={refreshControlColors}
            />
          }
        >
          <View style={{
            width: width * 0.893333333333, alignSelf: "center", marginTop: 10,
            flexDirection: "row", alignItems: "center"
          }}>
            <Text style={{
              flex: 1, color: colorPalette.mainButtonBg, fontSize: 16,
              fontFamily: fontStyles.nexa_bold
            }}>{this.state.locationTitle != "" ? this.state.locationTitle : "Select Location"}</Text>
            <TouchableOpacity style={{ marginHorizontal: 5, padding: 5 }} onPress={() => {
              this.props.navigation.navigate("Location", { callBack: this.callBack })
            }}>
              <Text style={{
                color: colorPalette.mainButtonBg, fontSize: 12,
                fontFamily: fontStyles.nexa_light
              }}>{"Change"}</Text>
            </TouchableOpacity>

            {this.state.locationTitle != "" ? <TouchableOpacity style={{ marginLeft: 5, padding: 5, }} onPress={this.onClearLocation}>
              <Text style={{
                color: colorPalette.mainButtonBg, fontSize: 12,
                fontFamily: fontStyles.nexa_light,
              }}>{"Clear"}</Text>
            </TouchableOpacity> : null}
          </View>
          <View style={styles.container}>
            {loading && <Loading />}

            {filterText.length > 0 && (
              <Text style={styles.searchText}>
                {auditionsToRender.length !== 0
                  ? `Search for '${filterText}'`
                  : `No results for '${filterText}'`}
              </Text>
            )}
            {!loading &&
              auditionsToRender.length !== 0 &&
              auditionsToRender.map((item, idx) => {
                if (item.status !== 2) {
                  return (
                    <AuditionCard
                      key={idx}
                      isSearchAudition={true}
                      data={item}
                      onPress={() => {
                        // navigate("AuditionIntro", {
                        //   id: item.id,
                        //   image: item.cover
                        // });
                        if (this.isFirstTime) {
                          AsyncStorage.setItem(
                            "isFirstTime",
                            JSON.stringify({ isFirstTime: false })
                          ).then(
                            data => {
                              console.log(" data ", data);
                              this.isFirstTime = false;
                              navigate("AuditionIntro", {
                                id: item.id,
                                image: item.cover
                              });
                            },
                            fail => { }
                          );
                        } else {
                          navigate("AuditionDetail", {
                            id: item.id,
                            image: item.cover
                          });
                        }
                      }}
                    />
                  );
                }
              })}
            {!loading &&
              auditionsToRender.length === 0 &&
              filterText.length === 0 && <Text>No Results Found</Text>}
          </View>
          <ModalSerch
            onChangeText={filterText => {
              this.setState({ filterText });
            }}
            handleNewData={handleNewData}
            handleData={this.handleData}
            ref="modal"
            styleData={styleData}
            text={filterText}
            filtersFn={this.getFilters}
            filterSearch={auditionFilters}
            onPress={() => findAudition(this, true)}
          />
        </Content>
      </Container>
    );
  }
}
export default withNavigationFocus(AuditionsView);
