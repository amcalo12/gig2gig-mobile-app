import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import AuditionView from "./";
import AuditionDetailView from "./auditionDetail";
import AuditionRoleDetailView from "./auditionRoleDetail";
import RepresentationEditView from "../profile/myProfile/representation/representationEdit";
import AuditionIntro from "./AuditionIntro";
import Location from "./locationSelect";
import Online from "../myAuditions/upcoming/online";

const AuditionStack = createStackNavigator(
  {
    Audition: {
      screen: AuditionView,
      navigationOptions: { header: null }
    },
    AuditionIntro: {
      screen: AuditionIntro,
      navigationOptions: {
        header: null
      }
    },
    AuditionDetail: {
      screen: AuditionDetailView,
      navigationOptions: {
        header: null
      }
    },
    AuditionRoleDetail: {
      screen: AuditionRoleDetailView,
      navigationOptions: {
        header: null
      }
    },
    RepresentationEditView: {
      screen: RepresentationEditView,
      navigationOptions: {
        header: null
      }
    },
    Location: {
      screen: Location,
      navigationOptions: {
        header: null
      }
    },
    Online: {
      screen: Online,
      navigationOptions: { header: null }
    },
  },
  {
    initialRouteName: "Audition",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

export default AuditionStack;
