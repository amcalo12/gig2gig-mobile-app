import { StackActions } from "react-navigation";
import myProfileApi from "api/myProfileApi";

export const loadRepresentation = async (context, loadScreen = true) => {
  context.setState({ loadScreen });
  const {
    navigation: { navigate }
  } = context.props;
  try {
    const response = await myProfileApi.getRepresentation();
    console.log(response);
    context.setState({
      loadScreen: false,
      userRepresentation: response.data
    });
  } catch (e) {
    console.log(e);
    return navigate("RepresentationEdit", {
      hasRepresentation: null,
      isFromAuditionDetail: false
    });
  }
};

export const handleBackPress = context => {
  const {
    navigation: { dispatch }
  } = context.props;
  return dispatch(StackActions.popToTop());
  return true;
};
