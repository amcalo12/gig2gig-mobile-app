export const colorPalette = {
	white: "#FFFFFF",
	mainButtonBg: "#4D2545",
	purple: "#4D2545",
	//filters buttons colors
	unionColor: "#4D2545",
	contractColor: "#D8893A",
	productionColor: "#93183E",
	onlineColor: "#4D2545",
	SHADOW_COLOR: "rgba(0, 0, 0, 0.50)",
	LIGHT_GREY: "#D8D7D8",
	black: "#000000",
	borderColor: '#F0F0F0'
};

export const spinnerColor = {
	color: "#4D2545",
}

export const indicatorAppColor = {
	color: "#4D2545"
}

export const refreshControlColors = ["#4d2545", "#783a6c", "#ac539a"]