import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container: {
    alignItems: "center",
    width: resize(290),
    alignSelf: 'center',
    justifyContent: "space-between",
    height: resize(520, "height")
  },
  form: {
    width: "100%"
  },
  input: {
    width: "100%"
  },
  inputGroup: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  button: {
    marginVertical: resize(30, "height")
  }
});