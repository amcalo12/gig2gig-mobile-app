import React, { Component } from "react";
import { View, Platform } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import Input from "utils/components/mainInput";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
import Button from "utils/components/mainButton";
import DateSelect from "utils/components/dateSelect";
import addAppearanceViewModel from "./addAppearanceViewModel";
import Picker from "react-native-picker";

class AddAppearance extends Component {
  state = {
    height: "",
    weight: "",
    eyes: "",
    hair: "",
    race: "",
    flare: "",
    gender_pronouns: "",
    loading: false,
  };
  changeHair = (hair) => {
    this.setState({ hair: hair.name });
  };
  changeEye = (eye) => {
    this.setState({ eyes: eye.name });
  };
  changeRace = (race) => {
    this.setState({ race: race });
  };
  changeFlare = (flare) => {
    this.setState({ flare: flare.name });
  };
  changePronouns = (pronouns) => {
    this.setState({ gender_pronouns: pronouns.name });
  };
  render() {
    const {
      navigation: { navigate, goBack },
    } = this.props;
    const {
      height,
      weight,
      eyes,
      hair,
      race,
      flare,
      gender_pronouns,
      loading,
    } = this.state;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => {
            goBack();
            Picker.hide();
          }}
          barStyle="dark-content"
          text="Appearance"
        />
        <Content contentContainerStyle={{ paddingBottom: 100 }}>
          <View style={styles.container}>
            <View style={styles.form}>
              {Platform.OS == "ios" ? (
                <Input
                  data={`${height} Height`}
                  placeholder={"Height"}
                  isTouchable
                  elevation={1.5}
                  onPress={() => addAppearanceViewModel.showPickerIOS(this)}
                  customWidth={styles.input.width}
                />
              ) : (
                <DateSelect
                  customWidth={"100%"}
                  placeholder={"Height"}
                  icon={DownArrowIcon}
                  data={addAppearanceViewModel.getFeets()}
                  onChange={(height) => this.setState({ height })}
                  elevation={1.5}
                  lng
                />
              )}
              <Input
                data={weight}
                placeholder="Weight"
                keyboardType={"decimal-pad"}
                elevation={1.5}
                onEdit={(weight) => this.setState({ weight })}
                customWidth={styles.input.width}
              />
              <Input
                data={hair}
                placeholder="Hair Color"
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("Hair", { onGoBack: this.changeHair });
                }}
              />
              <Input
                data={eyes}
                placeholder="Eye Color"
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("EyeColor", { onGoBack: this.changeEye });
                }}
              />
              <Input
                data={race}
                placeholder="Ethnicity"
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("Ethnicity", { onGoBack: this.changeRace });
                }}
              />
              <Input
                data={flare}
                // placeholder="Personal Flare"
                placeholder="Personal Flair"
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("PersonalFlare", { onGoBack: this.changeFlare });
                }}
              />

              <Input
                data={
                  gender_pronouns != undefined && gender_pronouns != null
                    ? gender_pronouns
                    : ""
                }
                placeholder={"Gender Pronouns"}
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("GenderPronouns", { onGoBack: this.changePronouns });
                }}
              />
            </View>
            <View style={styles.button}>
              <Button
                loading={loading}
                text="Save"
                onPress={() => addAppearanceViewModel.handlePressSave(this)}
              />
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
export default AddAppearance;
