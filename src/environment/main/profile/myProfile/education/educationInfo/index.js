import React, { Component } from "react";
import { View, Text } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import InfoLabel from "utils/components/infoLabel";
import Button from "utils/components/secondaryButton"
class EducationInfoView extends Component {

  render() {
    const {navigation : {state : {params: {id: {school, degree, instructor, location, year, id}, onGoBack}},navigate, goBack}} = this.props;
    return (
      <Container>
        <Header
        leftIcon="backDark"
        leftFn={()=>goBack()}
        barStyle="dark-content"
        text="Education & Training"
        />
        <Content>
          <View style={styles.container}>
            <View style={styles.labels}>
              <InfoLabel text={school}/>
              <InfoLabel text={degree}/>
              <InfoLabel text={instructor}/>
              <InfoLabel text={location}/>
              <InfoLabel isShort noLine text={year}/>
            </View>
            <Button text="Edit" onPress={()=>navigate("EducationInfoEdit",{id: {school, degree, instructor, location, year, id}, onGoBack})}/>
          </View>
        </Content>
      </Container>
    );
  }
}
export default EducationInfoView;
