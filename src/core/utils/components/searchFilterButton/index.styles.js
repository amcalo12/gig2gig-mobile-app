import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");


export default StyleSheet.create({
  container: {
    borderWidth:1,
    borderRadius:20,
    paddingHorizontal: height * 0.025,
    paddingVertical: height * 0.01,
    marginRight: height * 0.01,
    justifyContent:"center",
    marginBottom: height * 0.02
  },
  text:{
    fontFamily: fontStyles.nexa_bold,
    fontSize:13
  },
  unionContainer:{
    borderColor: colorPalette.unionColor
  },
  unionActive: {
    backgroundColor: colorPalette.unionColor,
    borderColor: "transparent"
  },
  unionText: {
    color: colorPalette.unionColor
  },
  contractContainer:{
    borderColor: colorPalette.contractColor
  },
  contractActive: {
    backgroundColor: colorPalette.contractColor,
    borderColor: "transparent"
  },
  contractText: {
    color: colorPalette.contractColor
  },
  productionContainer:{
    borderColor: colorPalette.productionColor
  },
  productionActive: {
    backgroundColor: colorPalette.productionColor,
    borderColor: "transparent"
  },
  productionText: {
    color: colorPalette.productionColor
  },
  textActive:{
    color:"#FFFFFF"
  }
});