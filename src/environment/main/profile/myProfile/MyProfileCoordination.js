import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import MyProfileView from ".";
import MyInfoStack from "./myInfo/MyInfoCoordination";
import CreditsStack from "./credits/CreditsCoordination";
import EducationStack from "./education/EducationCoordination";
import UnionMembershipStack from "./unionMembership/UnionMembershipCoordination";
import SkillsStack from "./skills/SkillsCoordination";
import RepresentationStack from "./representation/RepresentationCoordination";
import AppearanceStack from "./appearance/AppearanceCoordination";
import SocialStack from "./social/SocialStack";
import Social from "./social";
import MyInfoEditView from "./myInfo/myInfoEdit";

const MyProfileStack = createStackNavigator(
  {
    MyProfile: {
      screen: MyProfileView,
      navigationOptions: {
        header: null
      }
    },
    MyInfo: {
      screen: MyInfoEditView,
      navigationOptions: {
        header: null
      }
    },
    // MyInfo: {
    //   screen: MyInfoStack,
    //   navigationOptions: {
    //     header: null
    //   }
    // },
    Credits: {
      screen: CreditsStack,
      navigationOptions: {
        header: null
      }
    },
    Education: {
      screen: EducationStack,
      navigationOptions: {
        header: null
      }
    },
    UnionMembership: {
      screen: UnionMembershipStack,
      navigationOptions: {
        header: null
      }
    },
    Skills: {
      screen: SkillsStack,
      navigationOptions: {
        header: null
      }
    },
    Representation: {
      screen: RepresentationStack,
      navigationOptions: {
        header: null
      }
    },
    Appearance: {
      screen: AppearanceStack,
      navigationOptions: {
        header: null
      }
    },
    Social: {
      screen: SocialStack,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "MyProfile",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default MyProfileStack;
