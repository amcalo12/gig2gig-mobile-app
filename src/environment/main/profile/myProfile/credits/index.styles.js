import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    paddingLeft: resize(40),
  },
  notFoundText:{
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 17,
    width:'100%',
    textAlign:'center',
    paddingTop: 25,
  }

});