import { apiHandler } from "./apiMethods";
import { API_METHOD } from "utils/cons";

export const getAllForum = async () =>
  apiHandler(
    API_METHOD.GET,
    `a/forum/posts`,
  )

export const getAllForumByTitle = async (search = '') =>
  apiHandler(
    API_METHOD.GET,
    `a/forum/posts/find_by_title?value=${search}`,
    null,
    false
  )


export const getAllBlogs = async () =>
  apiHandler(
    API_METHOD.GET,
    `a/blog/posts`,
  )

export const getForum = async (id = '') =>
  apiHandler(
    API_METHOD.GET,
    `a/forum/posts/${id}/comments`,
  )

export const getTopics = async () =>
  apiHandler(
    API_METHOD.GET,
    `a/topics`
  )

export const addPost = async (data) =>
  apiHandler(
    API_METHOD.POST,
    `a/forum/posts`,
    data
  )

export const getByTopics = async (data) =>
  apiHandler(
    API_METHOD.POST,
    `a/forum/posts/find_by_topics`,
    data
  )

export const addCommentToPost = async (data, id) =>
  apiHandler(
    API_METHOD.POST,
    `a/forum/posts/${id}/comments`,
    data
  )