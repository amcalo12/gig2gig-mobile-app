import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import MarketplaceView from ".";
import FilterView from "./filter";
import InfoView from "./filter/info"
const MarketplaceStack = createStackNavigator(
  {
    Marketplace: {
      screen: MarketplaceView,
      navigationOptions: { header: null }
    },
    Filter: {
      screen: FilterView,
      navigationOptions: {
        header: null
      }
    },
    Info: {
      screen: InfoView,
      navigationOptions: {
        header: null
      }
    },
  },
  {
    initialRouteName: "Marketplace",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default MarketplaceStack;
