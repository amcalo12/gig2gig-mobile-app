import React, { Component } from "react";
import { View, Modal, Text } from "react-native";
import Header from "utils/components/appHeader";
import { Container, Content, Root } from "native-base";
import styles from "./index.styles";
import { showMsj, openUrl, shareContent } from "utils/utils";
import Input from "utils/components/mainInput";
import MainButton from "utils/components/mainButton";
import myMediaApi from "api/myMediaApi";
import isYoutubeUrl from "utils/isYoutubeUrl";

class AddVideoModal extends Component {
  state = {
    visible: this.props.visible,
    url: "",
    name: "",
    loading: false
  };

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.visible == true && this.state.visible == false) {
      this.setState({
        visible: nextProps.visible
      });
    }
    if (nextProps.visible == false && this.state.visible == true) {
      this.setState({
        visible: nextProps.visible
      });
    }
  }
  async componentDidMount() { }

  onPressAddVideo = async () => {
    const { url, name } = this.state;

    if (name == "") {
      showMsj("Name is required", true);
    }

    if (url != null && url != "") {
      // const youtubeUrl = isYoutubeUrl(url);

      // if (!youtubeUrl) {
      //   return showMsj("This url is not from youtube", true);
      // }

      this.setState({
        loading: true
      });
      try {
        const min = 1;
        const max = 10000;
        const rand = min + Math.random() * (max - min);

        let fileData = {
          url: url,
          // name: `VID_${rand}.mp4`,
          name: name,
          type: "video"
        };

        await myMediaApi.addUserMedia(fileData);

        this.props.onPressBackModal();

        showMsj("Successfull");

        this.setState({
          loading: false
        });
      } catch (error) {
        this.setState({
          loading: false
        });
      }
    } else {
      showMsj("Url is required", true);
    }
  };

  render() {
    const { visible, url, name, loading } = this.state;

    return (
      <Modal animationType={"slide"} visible={visible}>
        <Root>
          <Container>
            <Header
              leftIcon="backDark"
              leftFn={() => this.props.onPressBackModal()}
              barStyle="dark-content"
              text="Add Video Url"
            />
            <Content bounces={false} contentContainerStyle={styles.content}>
              <View style={{ width: "80%" }}>
                <Input
                  data={name}
                  placeholder={"Video Name"}
                  elevation={1.5}
                  // customWidth={styles.input.width}
                  onEdit={name => this.setState({ name })}
                />
                <Input
                  data={url}
                  placeholder={"Video URL"}
                  elevation={1.5}
                  // customWidth={styles.input.width}
                  onEdit={url => this.setState({ url })}
                />
                <Text style={styles.rules}>
                  {/* It can only be a youtube url */}
                  Supported video links are Vimeo, Dropbox, G Drive,YouTube
                </Text>
              </View>
              <MainButton
                text={"Add Video"}
                loading={loading}
                onPress={() => this.onPressAddVideo()}
              />
            </Content>
          </Container>
        </Root>
      </Modal>
    );
  }
}
export default AddVideoModal;
