import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import ButtonAction from "utils/components/buttonActionFloat";
import PlusIcon from "resources/images/myMedia/plus-icon.png";
import Card from "utils/components/mediaCards";
import myMediaApi from "api/myMediaApi";
import Loading from "utils/components/loading";
class AuditionMaterialsView extends Component {

  state = {
    auditions: null,
    loading: false
  }

  async componentDidMount() {
    this.getAuditionsMedia();
  }

  getAuditionsMedia = async () => {
    this.setState({ loading: true });
    try {
      const response = await myMediaApi.getAuditionMediaUser();
      console.log(response)
      this.setState({ loading: false, auditions: response.data });
    } catch (e) {
      console.log(e);
      this.setState({ auditions: [], loading: false });
    }
  }

  render() {
    const { loading, auditions } = this.state;
    const { navigation: { navigate, dispatch } } = this.props;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          barStyle="dark-content"
          text="Audition Materials"
        />
        <Content>
          <View style={styles.container}>
            {
              loading &&
              <Loading />
            }
            {
              auditions &&
              auditions.map((option, i) => {
                console.log('option :', option);
                return <Card
                  key={i}
                  type="audition"
                  mediaImage={option.cover || option.url}
                  fileText={option.name}
                  onPress={() => navigate("AuditionMaterialsDetails", { title: option.name, data: option.files })}
                />
              })
            }
            {
              auditions &&
              auditions.length === 0 &&
              // <Text>
              //   Not have auditions
              // </Text>
              <Text>
                You don't have any audition materials
              </Text>
            }
          </View>
        </Content>
        {/* <ButtonAction icon={PlusIcon} onPress={()=>alert("button")}/> */}
      </Container>
    );
  }
}
export default AuditionMaterialsView;
