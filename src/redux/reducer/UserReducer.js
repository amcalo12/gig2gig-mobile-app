import { TYPE_SAVE_USER_DATA, TYPE_CLEAR_USER_DATA } from "../action/UserAction"


const initialState = {
  userData: undefined,
}

export function userReducer(state = initialState, action) {
  switch (action.type) {
    case TYPE_SAVE_USER_DATA:
      console.log(" ============ USER DATA SAVE ============ ", action)
      return Object.assign({}, state, {
        userData: action.value
      })
    case TYPE_CLEAR_USER_DATA:
      return Object.assign({}, state, {
        userData: undefined
      })
    default:
      return state
  }
}