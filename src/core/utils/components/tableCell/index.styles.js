import {StyleSheet, Platform, Dimensions} from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils"
export default StyleSheet.create({
  button:{
    width:'100%',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    height:70,
    paddingHorizontal: 23,
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleText:{
    fontSize:25,
    marginTop: 15,
    color:colorPalette.purple,
    fontFamily: fontStyles.nexa_bold,
  },
  sinlgeText:{
    fontSize:21,
    color:colorPalette.purple,
    paddingLeft:8
  },
  typeText:{
    fontSize:21,
    color:colorPalette.purple,
    fontFamily: fontStyles.nexa_bold,
    paddingLeft:8
  },
  expireText:{
    fontSize:15,
    color:colorPalette.purple,
    paddingLeft:8
  },
  card:{
    flexDirection:'row', 
    alignItems: 'center',
    paddingLeft:8
  },
  icon:{
    position:'absolute',
    zIndex:100,
    right:23,
  }
});