import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Plan from "utils/components/subscriptionPlan";
import { showMsj } from "utils/utils";
class SubscriptionView extends Component {
	render() {
		const { navigation: { state: { params: { payData, subscription } }, navigate, dispatch } } = this.props;
		return (
			<Container>
				<Header
					leftIcon="backDark"
					leftFn={() => dispatch(StackActions.pop())}
					barStyle="dark-content"
					text="Subscription"
				/>
				<Content
					contentContainerStyle={styles.container}
					bounces={false}
				>
					<View
						style={styles.cont}
					>
						<Plan
							title="Basic"
							text="Profile creation + personal QR code"
							buttonText={subscription === "1" ? "Active" : "Purchase"}
							disable={subscription === "1" ? true : false}
							onPress={(data) => payData ? navigate("Manage", { data, plan: 1 }) : showMsj('First add payment method', true)}
						/>
						{/* <Plan
            title="Tier 2"
            text="Profile Creation, Agent Appointment Requests, Audition Feedback & Marketplace"
            buttonText={subscription === "2" ? "Active" : "Purchase"}
            disable={subscription === "2" ? true : false}
            onPress={(data)=> payData? navigate("Manage",{data, plan: 2}) : showMsj('First add payment method', true)}
            /> */}
						<Plan
							title="Premium"
							text="All Regular features + Document Storage"
							buttonText={subscription === "3" ? "Active" : "Purchase"}
							disable={subscription === "3" ? true : false}
							onPress={(data) => payData ? navigate("Manage", { data, plan: 3 }) : showMsj('First add payment method', true)}
						/>
					</View>
				</Content>
			</Container>
		);
	}
}
export default SubscriptionView;
