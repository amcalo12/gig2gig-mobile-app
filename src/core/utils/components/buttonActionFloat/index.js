import React, { Component } from 'react';
import { TouchableOpacity, Image } from 'react-native';
import styles from './index.styles';
import LinearGradient from 'react-native-linear-gradient';
import { gradientColors } from "utils/utils";

export default class ButtonActionFloat extends Component {

  render() {
    const { icon, onPress } = this.props;
    return (
      <LinearGradient colors={gradientColors} style={styles.container}>
        <TouchableOpacity style={styles.content} activeOpacity={0.6} onPress={() => onPress()}>
          <Image source={icon} />
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}