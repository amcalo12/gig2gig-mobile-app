import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    alignItems:"center",
    marginBottom: resize(60, "height"),
    marginTop: resize(6, "height"),
  },
  text: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: colorPalette.purple,
    marginTop: resize(40, "height")
  }
});