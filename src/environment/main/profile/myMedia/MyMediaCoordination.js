import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import MyMediaView from ".";
import VideosStack from "./videos/VideosCoordination";
import MusicStack from "./music/MusicCoordination";
import PhotosStack from "./photos/PhotosCoordination";
import DocumentsStack from "./documents/DocumentsCoordination";
import SheetMusicStack from "./sheetMusic/SheetMusicCoordination";
import AuditionMaterialsStack from "./auditionsMaterials/AuditionMaterialsCoordination";

const MyMediaStack = createStackNavigator(
  {
     MyMedia: {
       screen: MyMediaView,
       navigationOptions: {
         header: null
       }
     },
      Videos: {
       screen: VideosStack,
       navigationOptions: {
         header: null
       }
     },
     Music: {
       screen: MusicStack,
       navigationOptions: {
         header: null
       }
     },
     Photos: {
       screen: PhotosStack,
       navigationOptions: {
         header: null
       }
     },
     Documents: {
       screen: DocumentsStack,
       navigationOptions: {
         header: null
       }
     },
     SheetMusic: {
       screen: SheetMusicStack,
       navigationOptions: {
         header: null
       }
     },
     AuditionMaterials: {
       screen: AuditionMaterialsStack,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "MyMedia",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default MyMediaStack;
