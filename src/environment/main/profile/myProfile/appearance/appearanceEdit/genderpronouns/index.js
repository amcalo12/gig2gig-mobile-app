import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import Label from "utils/components/unionMemberLabel";
import { AndroidBackHandler } from "react-navigation-backhandler";
import { pronounsOptions } from "resources/l10n";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import Input from "utils/components/mainInput";

class GenderPronouns extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pronouns: pronounsOptions,
    };
  }

  handleBackPress = () => {
    const {
      navigation: { goBack }
    } = this.props;
    return goBack();
    return true;
  };
  changeChecked = (option, i) => {
    const {
      navigation: {
        navigate,
        state: {
          params: { onGoBack }
        }
      }
    } = this.props;
    const { pronouns } = this.state;
    const pronouns2 = [];
    pronouns.map(res => {
      if (res.name === option.name) {
        // res.checked = !res.checked;
        res.checked = true
      } else {
        res.checked = false
      }

      pronouns2.push(res);
    });
    this.setState({ pronouns: pronouns2 });
    onGoBack(option)
    // onGoBack(selectedRace.replace(/^,/, ""));
  };
  render() {
    const {
      navigation: {
        navigate,
        state: {
          params: { onGoBack }
        } }
    } = this.props;
    const { pronouns } = this.state;
    return (
      <AndroidBackHandler onBackPress={() => this.handleBackPress()}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => this.handleBackPress()}
            barStyle="dark-content"
            text="Gender Pronouns"
          />
          <Content>
            <View style={styles.container}>
              {pronouns.map((option, idx) => (
                <Label
                  key={idx}
                  text={option.name}
                  checked={option.checked}
                  onPress={() => this.changeChecked(option, idx)}
                />
              ))}
            </View>

          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default GenderPronouns;
