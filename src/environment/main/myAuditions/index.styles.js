import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { isIphoneX } from "utils/cons";

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");


export default StyleSheet.create({
  tabBarUnderlineStyle: {
    backgroundColor: colorPalette.purple,
    height: 2,
    width: '20%',
    marginLeft: "5%",
    marginBottom: 8,
  },
  tabContainerStyle: {
    elevation: 0,
    borderBottomWidth: 0,
    backgroundColor: "#FFFFFF",

  },
  tabHeading: {
    backgroundColor: "#FFFFFF",
  },
  tabTextActive: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 14,
    color: colorPalette.purple,
  },
  tabText: {
    fontFamily: fontStyles.nexa_light,
    fontSize: 14,
    color: colorPalette.purple,
  },
  dropDownContainer: {
    width: '70%',
    backgroundColor: colorPalette.white,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    alignSelf: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: isIphoneX() ? 50 : 30,
    zIndex: 999,
    marginTop: 5,
    borderRadius: 4
  }
});