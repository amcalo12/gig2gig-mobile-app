import React, { Component } from "react";
import { View, Text, Platform } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import calendarIcon from "resources/images/calendar-icon.png";
import { StackActions } from 'react-navigation';
import Input from "utils/components/mainInput";
import downArrowIcon from "resources/images/down-arrow-icon.png";
import Select from "utils/components/selectSpinner";
import Button from "utils/components/mainButton";
import DateTimePicker from 'react-native-modal-datetime-picker';
import { maximumDateAddEvent, minimumDateAddEvent } from "utils/cons";
import addEventViewModel from './addEventViewModel';
import { auditionFilters, calendarProductionType } from 'resources/l10n.js'
import { resize } from "utils/utils";
class addEventView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectName: '',
      startDate: '',
      endDate: '',
      isDateTimePickerVisible: false,
      productionType: '',
      loading: false,
      eventType: '',
      eventTypePositionY: 0,
      productionTypePositionY: 0
    }
  }

  setName = (projectName) => {
    this.setState({
      projectName,
    })
  }
  addToCalendary = async () => {
    await addEventViewModel.addEvent(this)
  }
  endDatePress = () => {
    const { startDate } = this.state;
    if (startDate) {
      this.setState({ type: 'end' });
      return (addEventViewModel._showDateTimePicker(this));
    }
  }
  onEventTypeCardClick() {
    if (this.eventTypeRef) {
      this.eventTypeRef.measure((fx, fy, width, height, px, py) => {
        let yPos = Platform.OS === "android" ? py + 20 : py + 60;
        this.setState({ eventTypePositionY: yPos });
        addEventViewModel.selectEventType(this)
      })
    }
  }

  onProductionTypeCardClick() {
    if (this.productionTypeRef) {
      this.productionTypeRef.measure((fx, fy, width, height, px, py) => {
        let yPos = Platform.OS === "android" ? py + 20 : py + 60;
        this.setState({ productionTypePositionY: yPos });
        addEventViewModel.select(this)
      })
    }
  }
  render() {
    const { navigation: { navigate, dispatch } } = this.props;
    const { startDate, endDate } = this.state
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => { dispatch(StackActions.popToTop()) }}
          barStyle="dark-content"
          text="Add Event"
        />
        <Content>
          <View style={styles.container}>
            {/* <Input data={this.state.eventType}
              reference={view => {
                this.eventTypeRef = view
              }}
              placeholder={'Event type'} icon={downArrowIcon} customWidth={styles.input.width} isTouchable onPress={() => this.onEventTypeCardClick()} /> */}
            <Input data={this.state.productionType}
              reference={view => {
                this.productionTypeRef = view
              }}
              placeholder={'Production type'} icon={downArrowIcon} customWidth={styles.input.width} isTouchable onPress={() => this.onProductionTypeCardClick()} />
            <Input data={this.state.projectName} placeholder={"Project name"} elevation={2} customWidth={styles.input.width} margin={10} onEdit={(text) => this.setName(text)} />
            <Text style={styles.label}>Start Date</Text>
            <Input data={startDate} placeholder={'Start date'} icon={calendarIcon} customWidth={styles.input.width} isTouchable onPress={() => { this.setState({ type: 'start' }); return (addEventViewModel._showDateTimePicker(this)) }} />
            <Text style={styles.label}>End Date</Text>
            <Input data={endDate} placeholder={'End date'} icon={calendarIcon} customWidth={styles.input.width} isTouchable onPress={() => this.endDatePress()} />
          </View>
          <View style={styles.button}>
            <Button loading={this.state.loading} onPress={() => this.addToCalendary()} text="Add To Calendar" />
          </View>
          <Select
            selectTitle="Production type"
            data={calendarProductionType.options}
            marginTop={this.state.productionTypePositionY}
            isCustomSpinner={true}
            onPress={(a) => { addEventViewModel.select(this); this.setState({ productionType: a.name }) }}
            ref="select"
          />
          <Select
            selectTitle="Event Type"
            data={auditionFilters[3].options}
            marginTop={this.state.eventTypePositionY}
            isCustomSpinner={true}
            onPress={(a) => { addEventViewModel.selectEventType(this); this.setState({ eventType: a.name }) }}
            ref="selectEventType"
          />
          <DateTimePicker
            //Its necesary in android
            minimumDate={minimumDateAddEvent(new Date())}
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={(date) => addEventViewModel._handleDatePicked(this, date)}
            onCancel={() => addEventViewModel._hideDateTimePicker(this)}
          />
        </Content>
      </Container>
    );
  }
}
export default addEventView;
