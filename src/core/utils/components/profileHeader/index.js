import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import styles from "./index.styles";
import RadioButtonChecked from "resources/images/radio-button-checked.png";
import RadioButtonEmpty from "resources/images/radio-button.png";
import Loading from "utils/components/loading";


class ProfileHeader extends Component {
  state = {
    loading: false
  }

  render() {
    const { picture, name, job, member, city } = this.props;
    const { loading } = this.state;
    return (
      <View style={styles.headerContainer}>
        <View style={styles.profileContainer}>
          <View style={styles.avatar}>
            {
              loading &&
              <Loading style={{ top: "10%", alignSelf: 'center', }} />
            }
            <Image
              onLoadStart={() => this.setState({ loading: true })}
              onLoadEnd={() => this.setState({ loading: false })}
              style={styles.image} source={picture ? { uri: picture } : null} />
          </View>
        </View>
        <View style={styles.profileText}>
          <Text numberOfLines={2} style={styles.name}>{name}</Text>
          <Text style={styles.job}>{job}</Text>
          <Text style={styles.member}>{member}</Text>
          <Text style={styles.city}>{city}</Text>
        </View>
      </View>
    );
  }
}
ProfileHeader.defaultProps = {

};
export default ProfileHeader;
