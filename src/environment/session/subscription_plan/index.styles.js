import { Dimensions, StyleSheet, Platform } from "react-native";
// import { fontSizeDefault } from "utils/utils";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";

const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);

export default StyleSheet.create({
  container: {
    height
  },
  background: {
    width: "100%",
    height: "100%",
    zIndex: 0,
    justifyContent: "center",
    alignItems: "center",
    // position: "relative"
  },
  logoContainer: {
    // flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // justifyContent: "center",
    marginTop: 50
    // marginBottom: height * 0.08
  },
  backButton: {
    position: "absolute", left: 20, top: 65
  },
  text: {
    color: colorPalette.white,
    fontFamily: fontStyles.break_semibold,
    fontSize: 30,
    paddingBottom: 5
  },
  mediumText: {
    marginTop: height * 0.015
  },
  itemContainer: { marginLeft: 40, height: 450, width: 300, borderRadius: 8, backgroundColor: colorPalette.white },
  headerContainer: {
    backgroundColor: colorPalette.contractColor, justifyContent: 'center',
    alignItems: 'center', paddingVertical: 20, borderTopLeftRadius: 8, borderTopRightRadius: 8
  },
  headerTitle: { fontFamily: fontStyles.nexa_light, color: colorPalette.white },
  priceContainer: { justifyContent: 'center', alignItems: 'center', paddingVertical: 60 },
  price: { fontFamily: fontStyles.nexa_bold, color: colorPalette.black, fontSize: 40 },
  duration: { fontFamily: fontStyles.nexa_light, color: colorPalette.black, fontSize: 20, marginTop: 8 },
  buttonContainer: {
    backgroundColor: colorPalette.mainButtonBg, borderWidth: 1, marginHorizontal: 30,
    borderRadius: 20, paddingHorizontal: 30, paddingVertical: 10,
    // height: height * 0.0734632683658,
    // height: height * 0.0500632683658,
    height: 40, alignItems: 'center', justifyContent: 'center'
  },
  buttonTitle: { color: colorPalette.white, fontSize: 16, fontFamily: fontStyles.nexa_bold, textAlign: 'center' },
  description: {
    textAlign: 'center', fontFamily: fontStyles.nexa_light, fontSize: 12,
    marginVertical: 40, marginHorizontal: 30, lineHeight: 18
  },
  dotContainer: { margin: 5, height: 10, width: 10, borderRadius: 10, borderWidth: 1, borderColor: colorPalette.white }
});
