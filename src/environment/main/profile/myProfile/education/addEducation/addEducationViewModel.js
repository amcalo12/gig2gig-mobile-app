import Picker from "react-native-picker";
import moment from "moment";
import educationApi from "api/educationApi";
import { showMsj } from "utils/utils";

const convertYears = () => {
  const year = []
  const years = getYears()
  years.map((res,i) => year[i] = {label: res.toString(),value:res.toString()})
  
  return year
}
const getYears = () => {
  const year = moment().year()
  const allYears = []
  for (let i = 0; i < 70; i++) {
    allYears[i] = year - i
  }
  return allYears
}
const handlePressSelect = (context, selected) => {
  context.setState({productionType: selected.name})
  select(context);
}

export const select = (context) => {
  context.refs.select.setModalVisible();
}
const handlePressSave = async(context) => {
  const {school, degree, instructor, location, year} = context.state
  context.setState({loading:true})
  if(school != '' && degree != '' && year != ''){
    const body = { school, degree, instructor, location, year }
    try {
      const res = await educationApi.createEducation(body)
      context.props.navigation.goBack()
      context.setState({loading:false})
      context.props.navigation.state.params.onGoBack(res.data)
      return showMsj('Education created', false)
    } catch (error) {
      context.setState({loading:false})
      return showMsj('he education could not be created', true)
    }
  }else{
    context.setState({loading:false})
    return showMsj('All fields as required', true)
  }
}
export default {
  handlePressSelect,
  handlePressSave,
  select,
  convertYears
}