import { NavigationActions, StackActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  console.log('BEFORE :111');
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
  console.log('AFTER :222');
}

function redirect(data) {
  console.log('BEFORE :');
  _navigator.dispatch(
    StackActions.reset({ index: 0, key: 'App', actions: [NavigationActions.navigate("App")] })
  );
  console.log('AFTER :');
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName: 'MyAuditions',
      action: NavigationActions.navigate({
        routeName: 'OfflineInstantFeedback',
        params: {
          userId: data.performer_id,
          data: data
        }
      })
    })
  )
}

// add other navigation functions that you need and export them

export default {
  navigate,
  redirect,
  setTopLevelNavigator,
};