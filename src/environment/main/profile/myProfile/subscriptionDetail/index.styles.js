import { Dimensions, StyleSheet, Platform } from "react-native";
// import { fontSizeDefault } from "utils/utils";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";

const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);

export default StyleSheet.create({
  container: {
    height
  },
  background: {
    width: "100%",
    height: "100%",
    zIndex: 0,
    // justifyContent: "center",
    alignItems: "center",
    // position: "relative"
  },
  logoContainer: {
    // flex: 1,
    flexDirection: "row",
    // alignItems: "center",
    // justifyContent: "center",
    marginTop: 50
    // marginBottom: height * 0.08
  },
  backButton: {
    position: "absolute", left: 20, top: Platform.OS == "ios" ? 45 : 50
  },
  subscriptionTitle: {
    fontFamily: fontStyles.nexa_bold, fontSize: 18,
    textAlign: 'center', color: colorPalette.white
  },
  itemContainer: { height: Platform.OS == "ios" ? 400 : 450, width: 300, borderRadius: 8, backgroundColor: colorPalette.white },
  headerContainer: {
    backgroundColor: colorPalette.contractColor, justifyContent: 'center',
    alignItems: 'center', paddingVertical: 20, borderTopLeftRadius: 8, borderTopRightRadius: 8
  },
  headerTitle: { fontFamily: fontStyles.nexa_bold, color: colorPalette.white },
  priceContainer: { justifyContent: 'center', alignItems: 'center', paddingTop: 40 },
  price: { fontFamily: fontStyles.nexa_bold, color: colorPalette.black, fontSize: 40 },
  buttonContainer: {
    backgroundColor: colorPalette.mainButtonBg, borderWidth: 1, marginHorizontal: 30,
    borderRadius: 20, paddingHorizontal: 30, paddingVertical: 10, marginTop: 30,
  },
  buttonTitle: { color: colorPalette.white, fontSize: 16, fontFamily: fontStyles.nexa_bold, textAlign: 'center' },
  restoreContainer: {
    marginHorizontal: 30,
    borderRadius: 20, paddingHorizontal: 30, paddingVertical: 10, marginTop: 30, marginBottom: 100,
  },
  restoreButton: { color: colorPalette.white, fontSize: 16, fontFamily: fontStyles.nexa_bold, textAlign: 'center' },
  duration: { fontFamily: fontStyles.nexa_light, color: colorPalette.black, fontSize: 20, marginTop: 8 },
  freeText: { fontFamily: fontStyles.nexa_bold, color: colorPalette.black, fontSize: 14, margin: 10, textAlign: 'center', lineHeight: 20 },
  description: {
    textAlign: 'center', fontFamily: fontStyles.nexa_light, fontSize: 12,
    marginVertical: 20, marginHorizontal: 30, lineHeight: 18
  },
  dotContainer: { margin: 5, height: 10, width: 10, borderRadius: 10, borderWidth: 1, borderColor: colorPalette.white }
});
