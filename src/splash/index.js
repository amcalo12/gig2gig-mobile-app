import React, { Component } from "react";
import SplashScreen from "react-native-splash-screen";
import { AsyncStorage, Platform } from "react-native";

//routes
import LocalNotification from "utils/components/localNotifications";
import firebase from "react-native-firebase";
import pushNotificationApi from "api/pushNotificationApi";
import DeviceInfo from "react-native-device-info";

class Splash extends Component {
  componentDidMount = async () => {
    SplashScreen.hide();
    try {
      const accessToken = await AsyncStorage.getItem("Authorization");
      if (accessToken) {
        const token = await firebase.messaging().getToken();
        let uniqueId = await DeviceInfo.getUniqueId();
        await pushNotificationApi.setPushToken({
          pushkey: token,
          device_id: uniqueId,
          device_type: Platform.OS
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return <LocalNotification />;
  }
}

export default Splash;
