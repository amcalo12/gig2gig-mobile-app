import moment from 'moment';
import myCalendarApi from 'api/myCalendarApi';
import { errorsValidateMsj } from "resources/l10n";
import { showMsj } from "utils/utils";
const addEvent = async (context) => {
	context.setState({ loading: true })
	var {
		projectName,
		startDate,
		endDate,
		productionType,
		eventType
	} = context.state;
	const s = startDate;
	const e = endDate;
	const { props: { navigation: { state: { params: { onGoBack } } } } } = context
	try {
		startDate = moment(startDate, "MM-DD-YYYY").format("YYYY-MM-DD");
		endDate = moment(endDate, "MM-DD-YYYY").format("YYYY-MM-DD");
		console.log('startDate', startDate)
		console.log('endDate', endDate)
		// if ( eventType != '' && projectName != '' && startDate != '' && endDate != '' && productionType != '' ) {
		if (projectName != '' && startDate != '' && endDate != '' && productionType != '') {
			if (e >= s) {
				console.log({ project_name: projectName, start_date: startDate, end_date: endDate, production_type: productionType })
				// const data = { event_type: eventType, project_name: projectName, start_date: startDate, end_date: endDate, production_type: productionType }
				const data = { project_name: projectName, start_date: startDate, end_date: endDate, production_type: productionType }
				await myCalendarApi.addEvent(data)
				const year = moment().year()
				const start = `${startDate}-${year}`
				const end = `${endDate}-${year}`
				onGoBack({ start_date: start, end_date: end })
				context.setState({ loading: false })
				context.props.navigation.goBack()
				return showMsj('Successful')
			} else {
				context.setState({ loading: false })
				return showMsj(errorsValidateMsj.addEvent.endDateInvalid.message, true)
			}
		} else {
			context.setState({ loading: false })
			return showMsj(errorsValidateMsj.addEvent.dataRequired.message, true)
		}
	} catch (error) {
		context.setState({ loading: false })
		if (error.hasOwnProperty('data')) {
			if (error.data.hasOwnProperty('error')) {
				return showMsj(error.data.error, true)
			}
		}
		return showMsj("There's has been an error trying to execute your request, please try again.", true);
	}
}

const _showDateTimePicker = (context) => context.setState({
	isDateTimePickerVisible: true
});

const _hideDateTimePicker = (context) => context.setState({
	isDateTimePickerVisible: false
});
const select = (context) => {
	context.refs.select.setModalVisible();
}
const _handleDatePicked = (context, date) => {
	const dateToPicker = moment(date).format("MM-DD-YYYY");

	if (context.state.type === 'start') {
		context.setState({
			startDate: dateToPicker
		});
	} else {
		context.setState({
			endDate: dateToPicker
		});
	}
	_hideDateTimePicker(context);
};
const selectEventType = (context) => {
	context.refs.selectEventType.setModalVisible();
}

export default {
	addEvent,
	_showDateTimePicker,
	_hideDateTimePicker,
	_handleDatePicked,
	select,
	selectEventType
}