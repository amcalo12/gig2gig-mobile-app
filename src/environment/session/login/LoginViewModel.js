import { AsyncStorage, Platform } from "react-native";
import userApi from "api/userApi";
import firebase from "react-native-firebase";
import pushNotificationApi from "api/pushNotificationApi";
import { showMsj } from "utils/utils";
import { errorsValidateMsj } from "resources/l10n";
import { NavigationActions, StackActions } from "react-navigation";
import DeviceInfo from "react-native-device-info";

const handleLogin = async context => {
  context.setState({
    loading: true
  });
  const token = await firebase.messaging().getToken();
  console.log("tokentokentoken", token);
  const { email, password } = context.state;
  const {
    navigation: { navigate }
  } = context.props;
  try {
    if (email === "") {
      context.setState({ loading: false });
      return showMsj(errorsValidateMsj.login.email.message, true);
    }
    if (password === "") {
      context.setState({ loading: false });
      return showMsj(errorsValidateMsj.login.password.message, true);
    }
    const response = await userApi.login({ email, password, type: "2" }); //type 2 for performer login
    console.log("response", response);
    if (response.data.details.type == "2") {
      handleUserInfo(context, response, { email, password });
    } else {
      context.setState({ loading: false });
      return showMsj("Unauthorized", true);
    }
  } catch (e) {
    // console.log(e);
    console.log("eerror=====>", e);
    context.setState({ loading: false });
    if (e.status == 403) {
      return showMsj(e.data.error, true);
    }
    return showMsj(errorsValidateMsj.login.error.message, true);
  }
};

const handleUserInfo = async (context, response, userData) => {
  const {
    navigation: { navigate }
  } = context.props;
  let data = response.data
  data['isPaidUser'] = data.is_premium ? true : false
  const storeUser = [
    ["Authorization", response.token_type + " " + response.access_token],
    // ["userData", JSON.stringify(response.data)]
    ["userData", JSON.stringify(data)]
  ];

  await AsyncStorage.multiSet(storeUser);
  try {
    const token = await firebase.messaging().getToken();
    console.log("tokentokentoken", token);
    let uniqueId = await DeviceInfo.getUniqueId();
    await pushNotificationApi.setPushToken({
      pushkey: token,
      device_id: uniqueId,
      device_type: Platform.OS
    });
  } catch (error) {
    console.log(error);
  }
  context.setState({ loading: false, email: "", password: "" });
  if (data.is_profile_completed == 0) {
    return context.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({
          routeName: "App",
          action: NavigationActions.navigate({
            routeName: 'Profile',
            action: NavigationActions.navigate({
              routeName: 'MyProfile',
              action: NavigationActions.navigate({
                routeName: 'MyInfo',
                params: { userData: data, isFromHome: true }
                // action: NavigationActions.navigate({
                //   routeName: 'MyInfoEdit', params: { userData: data, isFromHome: true }
                // })
              })
            })
          })
        })]
      })
    );
  } else {
    return context.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "App" })]
      })
    );
  }
};

export default {
  handleLogin
};
