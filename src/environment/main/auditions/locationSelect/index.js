import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Platform, StatusBar, Image } from 'react-native';
import { Container, Card, Input } from 'native-base';
import { Assets } from 'resources/assets';
import { colorPalette } from 'resources/colors';
import { resize } from 'utils/utils';
import { DEVICE } from 'utils/cons';
import { fontStyles } from 'resources/fonts';
import { styles } from './index.style';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import PlacesInput from 'react-native-places-input';

export default class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: ""
    };
  }

  onChangeText = (text) => {
    this.setState({ location: text })
  }
  componentDidMount() {
    if (Platform.OS === "ios") {
      // your code using Geolocation and asking for authorisation with

      navigator.geolocation.requestAuthorization()
    } else {
      // ask for PermissionAndroid as written in your code
    }
  }
  onCurrentLocation = () => {
    navigator.geolocation.getCurrentPosition((position) => {
      console.log('position :', position);
      data = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        locationTitle: "Current Location"
      }

      if (this.props.navigation.state.params.callBack != undefined) {
        this.props.navigation.state.params.callBack(data)
        this.props.navigation.goBack()
      }
    },
      (error) => alert(JSON.stringify(error)),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 });
  }

  onAddressPress = (details) => {
    console.log(" DATA ====== 111", details)
    data = {
      latitude: details.geometry.location.lat,
      longitude: details.geometry.location.lng,
      locationTitle: details.formatted_address
    }

    if (this.props.navigation.state.params.callBack != undefined) {
      this.props.navigation.state.params.callBack(data)
      this.props.navigation.goBack()
    }
  }

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="transparent" barStyle="dark-content" />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginTop: 50
          }}
        >
          <TouchableOpacity
            style={{ position: "absolute", left: 20, padding: 10 }}
            onPress={() => {
              this.props.navigation.goBack()
            }}
          >
            <Image source={Assets.close} />
          </TouchableOpacity>
          <Text style={{ fontFamily: fontStyles.nexa_bold, color: colorPalette.mainButtonBg, fontSize: resize(18) }}>
            {"Location"}
          </Text>
        </View>

        <Card style={{
          borderRadius: 25, backgroundColor: colorPalette.white,
          marginHorizontal: 10, paddingLeft: 40, marginTop: 20,
          width: "90%", alignSelf: "center", flexDirection: "row", alignItems: "center"
        }}>
          <Image
            style={{
              position: "absolute", top: 15, left: 20,
              width: 20, height: 20
            }}
            source={Assets.send}
            resizeMode={"contain"} />
          {/* <Input
            placeholderTextColor='#4D2545'
            placeholder="Enter city, state, or zip code"
            style={styles.searchInput}
            onChangeText={this.onChangeText}
            value={this.state.location}
          // clearButtonMode={'while-editing'}
          /> */}
          <View>
            <PlacesInput
              googleApiKey={"AIzaSyCBwvwOsPR82AjeUx5o3FUvr4syuoNFrLI"}
              placeHolder={"Enter city, state, or zip code"}
              stylesContainer={{
                width: resize(300),
                position: 'relative',
                // alignSelf: 'stretch',
                margin: 0,
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                shadowOpacity: 0,
              }}
              stylesList={{
                // top: 50,
                width: DEVICE.DEVICE_WIDTH - 100,
                position: 'relative',
                // borderColor: '#dedede',
                // borderLeftWidth: 1,
                // borderRightWidth: 1,
                // borderBottomWidth: 1,
                // left: -1,
                // right: -1
              }}
              language={"en"}
              onSelect={place => {
                console.log("data ========>", place.result.geometry)
                this.onAddressPress(place.result)
              }}
            />
          </View>
          {/* <GooglePlacesAutocomplete
            autoFocus={false}
            placeholder='Enter city, state, or zip code'
            minLength={2}
            returnKeyType='default'
            keyboardAppearance='light'
            listViewDisplayed='false'
            fetchDetails={true}
            renderDescription={row => row.description}
            onPress={(data, details = null) => this.onAddressPress(data, details)}
            getDefaultValue={() => ''}
            query={{
              key: 'AIzaSyCBwvwOsPR82AjeUx5o3FUvr4syuoNFrLI',
              language: 'en'
            }}
            styles={{
              textInputContainer: {
                marginLeft: 2, width: "90%",
                borderTopWidth: 0,
                borderBottomWidth: 0, marginVertical: 2, backgroundColor: colorPalette.white
              },
              listView: {
                width: "90%"
              }
            }}
            nearbyPlacesAPI='GooglePlacesSearch'
            GooglePlacesSearchQuery={{
              rankby: 'distance',
              type: 'cafe'
            }}
            // GooglePlacesDetailsQuery={{
            //   fields: 'formatted_address',
            // }}
            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
            debounce={200}
          /> */}
        </Card>
        <TouchableOpacity style={{
          width: "80%", alignSelf: "center",
          paddingVertical: 10,
          marginTop: 10, marginHorizontal: 10,
          borderBottomColor: colorPalette.LIGHT_GREY, borderBottomWidth: 1
        }} onPress={this.onCurrentLocation}>
          <Text style={{ color: colorPalette.mainButtonBg, fontFamily: fontStyles.nexa_bold }}>{"Use Current Location"}</Text>
        </TouchableOpacity>
      </Container>
    );
  }
}
