import React, { Component } from "react";
import { View, Text, Platform, AsyncStorage, TouchableOpacity } from "react-native";
import { Container, Content, Tab, Tabs, TabHeading } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import NotificationIcon from "resources/images/notification-icon.png";
import Requested from "./requested";
import Upcoming from "./upcoming";
import Past from "./past";
import AllAuditionsFeedback from "./auditionFeedback"
import { withNavigationFocus } from "react-navigation";
import myAuditionsApi from "api/myAuditionsApi";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";
import { isIphoneX, AUDITION_DROPDOWN } from "utils/cons";
import moment from "moment";
const isIos = Platform.OS === "ios";

const DropDownText = (props) => {
  return (
    <TouchableOpacity style={{ width: '100%', alignItems: 'center' }} onPress={props.onPress}>
      <Text style={{
        fontFamily: fontStyles.nexa_light, fontSize: 18,
        color: colorPalette.mainButtonBg, paddingVertical: 15
      }}>{props.title}</Text>
      {/* <View style={{ width: '70%', backgroundColor: colorPalette.borderColor, height: 1 }} /> */}
      {!props.hideBottom ? <View style={{ width: '70%', backgroundColor: colorPalette.borderColor, height: 1 }} /> : null}
    </TouchableOpacity>
  )
}

class MyAuditionsView extends Component {
  constructor(props) {
    super(props)
    this.data = [{ "id": 989, "appointment_id": 828, "auditions_id": 508, "online": 0, "rol": "680", "rol_name": ["ROLE 1"], "id_user": 74, "title": "LATEST AUDITION WITH DRAG APIS", "date": "2020-03-16", "hour": null, "union": "UNION", "contract": "PAID", "production": ["THEATER", "FILM", "VOICEOVER", "COMMERCIALS", "PERFORMING ARTS", "MODELING", "TV & VIDEO"], "media": "https://firebasestorage.googleapis.com/v0/b/dd-gig2gig.appspot.com/o/temp%2F35744740-681b-11ea-918a-559a8e975475.png?alt=media&token=6dc8cda7-423b-4056-898f-d6b2f968ecc7", "media_thumbnail": null, "media_name": "1024-500.png", "number_roles": 1, "round": 1, "comment": "", "status": 1, "assign_no": null }, { "id": 968, "appointment_id": 778, "auditions_id": 484, "online": 0, "rol": "639", "rol_name": ["role 1"], "id_user": 74, "title": "test new notification issue", "date": "2020-03-27", "hour": null, "union": "UNION", "contract": "PAID", "production": ["THEATER"], "media": "https://firebasestorage.googleapis.com/v0/b/dd-gig2gig.appspot.com/o/temp%2F00541910-5e1f-11ea-a894-9df08a90ec99.jpg?alt=media&token=28857d78-9209-4e87-a4eb-7e4721e5aaef", "media_thumbnail": null, "media_name": "300px-Bell_UH-1A_Iroquois_in_flight.jpg", "number_roles": 1, "round": 1, "comment": "", "status": 1, "assign_no": null }, { "id": 951, "appointment_id": 762, "auditions_id": 471, "online": 1, "rol": "616", "rol_name": ["Role"], "id_user": 25, "title": "Appointment Change", "date": null, "hour": null, "union": "NONUNION", "contract": "UNPAID", "production": ["THEATER", "FILM", "VOICEOVER"], "media": "https://firebasestorage.googleapis.com/v0/b/dd-gig2gig.appspot.com/o/tablet%2Fauditions%2Fcover%2F2020-02-19T14:39:09%2B05:30?alt=media&token=751e6707-c361-476d-8ed9-7bebc4d81210", "media_thumbnail": null, "media_name": "Appointment Change.jpg", "number_roles": 1, "round": 1, "comment": "", "status": 1, "assign_no": null }, { "id": 950, "appointment_id": 767, "auditions_id": 474, "online": 0, "rol": "620", "rol_name": ["Role"], "id_user": 25, "title": "Audition Update", "date": "2020-02-19", "hour": null, "union": "NONUNION", "contract": "UNPAID", "production": ["FILM", "VOICEOVER", "PERFORMING ARTS"], "media": "https://firebasestorage.googleapis.com/v0/b/dd-gig2gig.appspot.com/o/tablet%2Fauditions%2Fcover%2F2020-02-19T15:28:38%2B05:30?alt=media&token=31e9daa0-7da9-4c6d-809e-c7e6a55c40d8", "media_thumbnail": null, "media_name": "Audition Update.jpg", "number_roles": 1, "round": 1, "comment": "", "status": 0, "assign_no": null }]
  }
  state = {
    activeTab: 0,
    newNotification: false,
    requestedAuditions: null,
    upcomingAuditions: null,
    pastAuditions: null,
    showDropDown: false,
    title: AUDITION_DROPDOWN.MY_AUDITIONS
  };
  // async componentDidUpdate(props, state) {
  //   if (props.isFocused !== this.props.isFocused && this.props.isFocused) {
  //     this.refs.Requested.getRequestedAuditions();
  //   }
  // }
  async componentDidMount() {
    this.onFocus();
    this.props.navigation.addListener("didFocus", this.onFocus);
    await this.getAllAuditions();
  }

  onFocus = async () => {
    this.setState({ newNotification: false });
    const newNotification = await AsyncStorage.getItem("newNotification");
    newNotification
      ? this.setState({ newNotification: true })
      : this.setState({ newNotification: false });
  };
  componentDidUpdate(props, state) {
    if (props.isFocused !== this.props.isFocused && this.props.isFocused) {
      this.loadUpcomingTab();
      this.getAllAuditions();
    }
  }
  loadUpcomingTab = () => {
    const checkIn = this.props.navigation.getParam("checkIn", 0);
    this.changeTab(checkIn);
  };
  changeTab = i => {
    const {
      navigation: { setParams }
    } = this.props;
    this.setState({
      activeTab: i
    });
    setParams({ checkIn: i });
  };

  _onRefresh = () => {
    this.setState({ refreshing: true });
  };

  getAllAuditions = () => {
    this.getRequestedAuditions();
    this.getUpcomingAuditions();
    this.getPastAuditions();
  };

  getRequestedAuditions = async () => {
    try {
      const requestedAuditions = await myAuditionsApi.getAuditionsRequested(this.state.title == AUDITION_DROPDOWN.MY_AUDITIONS ? 0 : 1);
      this.setState({
        requestedAuditions: requestedAuditions.data
      });
    } catch (e) {
      this.setState({
        requestedAuditions: []
      });
      console.log(e);
    }
  };

  getUpcomingAuditions = async () => {
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    console.log('userData :>> ', userData);
    try {
      const upcomingAuditions = await myAuditionsApi.getUpcomingAuditions(this.state.title == AUDITION_DROPDOWN.MY_AUDITIONS ? 0 : 1);
      this.setState({
        upcomingAuditions: upcomingAuditions.data
      });
    } catch (e) {
      this.setState({
        upcomingAuditions: []
      });
      console.log(e);
    }
  };

  getPastAuditions = async () => {
    try {
      const pastAuditions = await myAuditionsApi.getAuditionsPast(this.state.title == AUDITION_DROPDOWN.MY_AUDITIONS ? 0 : 1);

      this.setState({
        pastAuditions: pastAuditions.data
      });
    } catch (e) {
      this.setState({
        pastAuditions: []
      });
    }
  };

  getAllFeedbackAuditions = async () => {
    try {
      const feedBackAuditions = await myAuditionsApi.getAuditionsPast(2);

      this.setState({
        feedBackAuditions: feedBackAuditions.data
      });
    } catch (e) {
      this.setState({
        feedBackAuditions: []
      });
    }
  };

  render() {
    const {
      navigation: { navigate }
    } = this.props;
    const {
      activeTab,
      newNotification,
      requestedAuditions,
      upcomingAuditions,
      pastAuditions,
      title,
      showDropDown,
      feedBackAuditions
    } = this.state;
    return (
      <Container>
        <Header
          rightIcon="notification"
          barStyle="dark-content"
          text={title || AUDITION_DROPDOWN.MY_AUDITIONS}
          isAudition={true}
          onTitlePress={() => this.setState({ showDropDown: true })}
          newNotification={newNotification}
        />

        {showDropDown ?
          <View style={styles.dropDownContainer}>
            <DropDownText title={AUDITION_DROPDOWN.MY_AUDITIONS}
              onPress={() => this.setState({ showDropDown: false, title: AUDITION_DROPDOWN.MY_AUDITIONS }, () => {
                this.getAllAuditions()
              })} />
            <DropDownText title={AUDITION_DROPDOWN.ONLINE_SUBMISSION}
              onPress={() => this.setState({ showDropDown: false, title: AUDITION_DROPDOWN.ONLINE_SUBMISSION }, () => {
                this.getAllAuditions()
              })} />
            <DropDownText title={AUDITION_DROPDOWN.FEEDBACK} hideBottom={false}
              onPress={() => this.setState({ showDropDown: false, title: AUDITION_DROPDOWN.FEEDBACK }, () => {
                this.getAllFeedbackAuditions()
              })} />
          </View> : null}

        {(title == AUDITION_DROPDOWN.MY_AUDITIONS || title == AUDITION_DROPDOWN.ONLINE_SUBMISSION) ? <Tabs
          locked={true}
          tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
          tabContainerStyle={styles.tabContainerStyle}
          onChangeTab={({ i }) => {
            this.changeTab(i);
          }}
          page={this.state.activeTab}
        >
          <Tab
            heading={
              <TabHeading style={styles.tabHeading}>
                <Text
                  style={
                    activeTab === 0 ? styles.tabTextActive : styles.tabText
                  }
                >
                  {title == AUDITION_DROPDOWN.MY_AUDITIONS ? 'Requested' : 'Submit'}
                </Text>
              </TabHeading>
            }
          >
            <Requested
              reloadFn={() => this.getAllAuditions()}
              data={requestedAuditions}
            />
          </Tab>
          <Tab
            heading={
              <TabHeading style={styles.tabHeading}>
                <Text
                  style={
                    activeTab === 1 ? styles.tabTextActive : styles.tabText
                  }
                >
                  {title == AUDITION_DROPDOWN.MY_AUDITIONS ? 'Upcoming' : 'Submitted'}
                </Text>
              </TabHeading>
            }
          >
            <Upcoming
              reloadFn={() => this.getAllAuditions()}
              data={upcomingAuditions}
            />
          </Tab>
          <Tab
            heading={
              <TabHeading style={styles.tabHeading}>
                <Text
                  style={
                    activeTab === 2 ? styles.tabTextActive : styles.tabText
                  }
                >
                  Past
                </Text>
              </TabHeading>
            }
          >
            <Past
              reloadFn={() => this.getAllAuditions()}
              data={pastAuditions}
            />
          </Tab>
        </Tabs>
          :
          <AllAuditionsFeedback
            reloadFn={() => this.getAllFeedbackAuditions()}
            data={feedBackAuditions}
            {...this.props}
          />
        }

      </Container>
    );
  }
}
export default withNavigationFocus(MyAuditionsView);
