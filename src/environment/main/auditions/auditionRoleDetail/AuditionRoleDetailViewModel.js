import { Platform } from "react-native";
import auditionsApi from "api/auditionsApi";
import { generalMsj } from "resources/l10n";
import { showMsj } from "utils/utils";
import { directionMaps, getLocation } from "utils/utils";
import SystemSetting from 'react-native-system-setting';
import {requestPermission} from 'utils/permissions';

export const loadAuditionData = async (context) =>{
  context.setState({loading:true});
  const id = context.props.navigation.state.params.id;
  const auditionCover = context.props.navigation.state.params.image;

  try{
    const auditionData = await auditionsApi.getAuditionData(id);
    const locationName = await getPlaceName(context);
    console.log(auditionData)
    context.setState({auditionData:auditionData.data, auditionCover,loading:false, locationName});
  }catch(e){
    console.log(e)
    context.setState({loading:false});
  }
}

