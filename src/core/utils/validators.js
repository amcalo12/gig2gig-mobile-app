// import en from '../locale/en';

const regex = {
	email: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
	password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!%*#?&:()._-]{8,}$/,
	url: /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
};

export const validateFields = async (state = {}) => {
	let errorMessage = '';
	for (var key in state) {
		const _state = state[key];
		const _regex = regex[_state.type];
		let field = state[key].name
		if (_state.required) {
			if (_state.value === '' || _state.value === null) {
				// errorMessage = 'All fields are required'
				errorMessage = field + ' is required'
				break;
			} else if (_regex) {
				if (!_regex.test(_state.value)) {
					// errorMessage = 'Please enter valid data';
					errorMessage = 'Please enter valid email address';
					break;
				}
			} else if (_state.type === 'confirmPassword') {
				if (_state.value !== state['password'].value) {
					errorMessage = 'All fields are required'
					break;
				}
			} else if (_state.type === 'boolean') {
				if (!_state.value) {
					errorMessage = 'All fields are required'
					break;
				}
			}
		}
	}

	if (errorMessage !== '') {
		return new Promise.reject(errorMessage);
	}

	return new Promise.resolve(true);
};

export const isUrl = url => {
	var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

	return regexp.test(url);
};

const capitalize = (s) => {
	if (typeof s !== 'string') return ''
	return s.charAt(0).toUpperCase() + s.slice(1)
}