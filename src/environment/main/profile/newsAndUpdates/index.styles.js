import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container: {

  },
  searchInput: {
    width: resize(317),
  },
  empty: {
    width: '100%',
    textAlign: 'center',
    paddingTop: resize(20, 'h'),
    color: '#757575',
    fontFamily: 'Nexa Bold',
    fontSize: resize(10, 'h'),
    fontWeight: '300',
  },
});