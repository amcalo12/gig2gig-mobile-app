import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    marginBottom: resize(30, "height"),
    justifyContent:"space-between",
    height:resize(520, "height")
  },
  labels: {
    paddingLeft: resize(40),
  },
  labelGroup:{
    width:resize(375),
    flexDirection: 'row',
  },
  button:{
    marginBottom: resize(30, "height"),
  },
  notFoundText:{
    color: colorPalette.purple,
    fontSize: 20,
    width:'100%',
    textAlign:'center',
    paddingTop: 25,
  }
});