import React, { Component } from "react";
import { TouchableOpacity, Image, Text, View } from "react-native";
import { Card } from "native-base";
import styles from "./index.styles";
import Loading from "utils/components/loading";
import { DEVICE } from "utils/cons";
class MarketplaceCard extends Component {
  constructor(props) {
    super(props)
    const { title, image } = this.props.data

    this.state = {
      loading: false,
      title,
      img: image ? image.url : 'https://f4.bcbits.com/img/a1173748748_16.jpg'
    }
  }

  render() {
    const { onPress } = this.props;
    const { loading, img, title } = this.state;
    return (
      <TouchableOpacity activeOpacity={0.7} onPress={() => onPress()}>
        {/* {console.log('(DEVICE.DEVICE_WIDTH - 50) :>> ', (DEVICE.DEVICE_WIDTH - 50))} */}
        <Card style={styles.card}>
          <View style={styles.imageContainer}>
            {
              loading &&
              <Loading style={{ top: "15%", alignSelf: 'center', }} />
            }
            <Image
              onLoadStart={() => this.setState({ loading: true })}
              onLoadEnd={() => this.setState({ loading: false })}
              style={styles.image}
              source={{ uri: img }}
              resizeMode={"contain"}
            // resizeMethod={"scale"}
            />
          </View>
          <View style={styles.textContainer}>
            <Text numberOfLines={2} style={styles.text}>{title}</Text>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}
export default MarketplaceCard;

MarketplaceCard.defaultProps = {

}
