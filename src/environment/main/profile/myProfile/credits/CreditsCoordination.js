import { createStackNavigator } from "react-navigation";
import MyInfoView from ".";
import CreditsView from ".";
import CreditInfoView from "./creditInfo";
import CreditInfoEdit from "./creditInfo/creditInfoEdit";
import AddCredit from "./addCredit";

const CreditsStack = createStackNavigator(
  {
     Credits: {
       screen: CreditsView,
       navigationOptions: {
         header: null
       }
     },
     CreditInfo: {
       screen: CreditInfoView,
       navigationOptions: {
         header: null
       }
     },
     CreditInfoEdit: {
       screen: CreditInfoEdit,
       navigationOptions: {
         header: null
       }
     },
     AddCredit: {
      screen: AddCredit,
      navigationOptions: {
        header: null
      }
    },
  },
  {
    initialRouteName: "Credits",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default CreditsStack;
