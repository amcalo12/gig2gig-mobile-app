import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
  AsyncStorage,
  Platform
} from "react-native";
import { Toast } from "native-base";
import { BarIndicator } from "react-native-indicators";
import Background from "resources/images/login/background.png";
import styles from "./index.styles";
import Input from "utils/components/mainInput";
import Button from "utils/components/mainButton";
import userApi from "api/userApi";
import { showMsj } from "utils/utils";
import LoginViewModel from "./LoginViewModel";
import { indicatorAppColor } from "resources/colors";
import { NavigationActions, StackActions } from "react-navigation";



class LoginView extends Component {
  constructor(props) {
    super(props);
    console.log(" CHECK SCREEN PROPS ", this.props)
    this.state = {
      email: "",
      password: "",
      loading: false,
      loadingScreen: true
    };
  }
  async componentDidMount() {
    const { loadingScreen } = this.state;
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    console.log('userData :>> ', userData);
    this.setState({ loadingScreen: true });
    if (userData !== null) {
      const userInfo = await userApi.getUserInfo(userData.id);
      let data = userInfo.data
      data['isPaidUser'] = data.is_premium ? true : false
      console.log('data :>> ', data);
      await AsyncStorage.setItem("userData", JSON.stringify(data));
      const { navigate } = this.props.navigation;
      console.log('this.props.screenProps :', this.props.screenProps);
      // showMsj(this.props.screenProps.isFromNotification)
      // if (!this.props.screenProps.isFromNotification) {
      // setTimeout(() => {
      // this.props.navigation.dispatch(
      //   StackActions.reset({
      //     index: 0,
      //     actions: [NavigationActions.navigate({ routeName: "App" })]
      //   })
      // );

      if (data.is_profile_completed == 1) {
        this.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "App" })]
          })
        );
      } else {
        this.props.navigation.dispatch(
          // StackActions.reset({
          //   index: 0,
          //   actions: [NavigationActions.navigate({
          //     routeName: "App",
          //     action: NavigationActions.navigate({
          //       routeName: 'Profile',
          //       action: NavigationActions.navigate({
          //         routeName: 'MyProfile',
          //         action: NavigationActions.navigate({
          //           routeName: 'MyInfoEdit', params: { userData: data, isFromHome: true }
          //         })
          //       })
          //     })
          //   })]
          // })
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({
              routeName: "App",
              action: NavigationActions.navigate({
                routeName: 'Profile',
                action: NavigationActions.navigate({
                  routeName: 'MyProfile',
                  action: NavigationActions.navigate({
                    routeName: 'MyInfo',
                    params: { userData: data, isFromHome: true }
                    // action: NavigationActions.navigate({
                    //   routeName: 'MyInfoEdit', params: { userData: data, isFromHome: true }
                    // })
                  })
                })
              })
            })]
          })
        )
      }


      this.setLoadScreen();
      // }, 500);
      // }
    } else {
      this.setState({ loadingScreen: false });
    }
  }


  setLoadScreen() {
    this.setState({ loadingScreen: false });
  }

  render() {
    const { email, password, loading, loadingScreen } = this.state;
    const {
      navigation: { navigate }
    } = this.props;
    if (loadingScreen) {
      return (
        <View style={styles.waitScreen}>
          <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
          <BarIndicator count={5} color={indicatorAppColor.color} />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <StatusBar
            translucent
            barStyle="light-content"
            backgroundColor="transparent"
          />
          <ImageBackground source={Background} style={styles.background}>
            <View style={styles.formContainer}>
              <View style={styles.logoContainer}>
                <Text style={styles.text}>gig</Text>
                <Text style={[styles.text, styles.mediumText]}>2</Text>
                <Text style={styles.text}>gig</Text>
              </View>
              <Input
                placeholder="Email"
                onEdit={email => this.setState({ email })}
                data={email}
                keyboardType="email-address"
              />
              <Input
                placeholder="Password"
                onEdit={password => this.setState({ password })}
                data={password}
                isSecure={true}
              />
              <Button
                text="Log In"
                onPress={() => LoginViewModel.handleLogin(this)}
                loading={loading}
              />
              <TouchableOpacity onPress={() => navigate("Forgot")}>
                <Text style={[styles.formText, styles.withMargin]}>
                  Forgot Password?
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.positionText}
                // onPress={() => navigate("Create")}
                onPress={() => navigate("SubscriptionPlan", { isFromHome: false })}
              // onPress={() => navigate("PurchaseComplete")}
              >
                <Text style={[styles.formText, { fontSize: 20 }]}>
                  {/* Create an Account */}
                  Get Started Now!
                </Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      );
    }
  }
}
export default LoginView;
