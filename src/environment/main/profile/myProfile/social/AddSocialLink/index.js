import React, { Component } from "react";
import { View, Platform, AsyncStorage } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import Input from "utils/components/mainInput";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
import Button from "utils/components/mainButton";
import userApi, { addSocial } from "api/userApi";
import { showMsj } from "utils/utils";
import Loading from "utils/components/loading";

const twitter_url_prefix = "https://twitter.com/";
const instagram_url_prefix = "https://www.instagram.com/";
const facebook_url_prefix = "https://www.facebook.com/";
const linkedin_url_prefix = "https://www.linkedin.com/";

class AddSocialLink extends Component {
  constructor(props) {
    super(props);
    this.isAdd = this.props.navigation.state.params.isAdd;
    this.userData = this.props.navigation.state.params.userData;
    this.state = {
      twitter: this.userData.twitter != undefined ? this.userData.twitter : "",
      instagram:
        this.userData.instagram != undefined ? this.userData.instagram : "",
      facebook:
        this.userData.facebook != undefined ? this.userData.facebook : "",
      linkedin:
        this.userData.linkedin != undefined ? this.userData.linkedin : "",
      loading: false,
      notFoundData: false,
    };
  }
  componentWillMount() {
    let addedTwitterUrl = this.userData.twitter;
    let addedInstagramUrl = this.userData.instagram;
    let addedFacebookUrl = this.userData.facebook;
    let addedLinkedInUrl = this.userData.linkedin;

    this.setState({
      twitter:
        addedTwitterUrl != undefined
          ? addedTwitterUrl.startsWith(twitter_url_prefix)
            ? addedTwitterUrl.replace(twitter_url_prefix, "")
            : addedTwitterUrl
          : "",
      instagram:
        addedInstagramUrl != undefined
          ? addedInstagramUrl.startsWith(instagram_url_prefix)
            ? addedInstagramUrl.replace(instagram_url_prefix, "")
            : addedInstagramUrl
          : "",
      facebook:
        addedFacebookUrl != undefined
          ? addedFacebookUrl.startsWith(facebook_url_prefix)
            ? addedFacebookUrl.replace(facebook_url_prefix, "")
            : addedFacebookUrl
          : "",
      linkedin:
        addedLinkedInUrl != undefined
          ? addedLinkedInUrl.startsWith(linkedin_url_prefix)
            ? addedLinkedInUrl.replace(linkedin_url_prefix, "")
            : addedLinkedInUrl
          : "",
    });
  }

  onSaveClick = async () => {
    // console.log(" CHECK VALID 1111 ", this.handleValidateURL());
    // if (!this.handleValidateURL()) {
    //   return;
    // }
    let twitterUrl = this.state.twitter;
    let facebookUrl = this.state.facebook;
    let instagramUrl = this.state.instagram;
    let linkedinUrl = this.state.linkedin;

    let params = {
      twitter:
        this.state.twitter !== ""
          ? twitterUrl.startsWith(twitter_url_prefix)
            ? twitterUrl
            : twitter_url_prefix + this.state.twitter
          : "",
      facebook:
        this.state.facebook !== ""
          ? facebookUrl.startsWith(facebook_url_prefix)
            ? facebookUrl
            : facebook_url_prefix + this.state.facebook
          : "",
      instagram:
        this.state.instagram !== ""
          ? instagramUrl.startsWith(instagram_url_prefix)
            ? instagramUrl
            : instagram_url_prefix + this.state.instagram
          : "",
      linkedin:
        this.state.linkedin !== ""
          ? linkedinUrl.startsWith(linkedin_url_prefix)
            ? linkedinUrl
            : linkedin_url_prefix + this.state.linkedin
          : "",
    };
    this.setState({ loading: true });
    // const response = await addSocial(this.props.navigation.state.params.userData.user_id, params);
    addSocial(this.props.navigation.state.params.userData.user_id, params)
      .then((data) => {
        showMsj("Social details saved successfully.");
        console.log(" SOCIAL LINK ADD RESPONSE ", data);
        this.setState({ loading: false });
        if (this.props.navigation.state.params.callBack != undefined) {
          this.props.navigation.state.params.callBack(params);
        }
        userApi
          .getUserInfo(this.props.navigation.state.params.userData.user_id)
          .then((userInfo) => {
            AsyncStorage.setItem("userData", JSON.stringify(userInfo.data));
          });
      })
      .catch((error) => {
        this.setState({ loading: false });
        showMsj("Social details not save, please try again!", true);
        console.log(" SOCIAL LINK ADD ERROR ", error);
      });
  };

  handleValidateURL = () => {
    let isValid = true;
    if (
      this.state.twitter != "" &&
      !this.isUrl(twitter_url_prefix + this.state.twitter)
    ) {
      console.log(" CHECK VALID222 ", isValid);
      showMsj("The twitter field is not a valid URL.", true);
      isValid = false;
    } else if (
      this.state.facebook != "" &&
      !this.isUrl(facebook_url_prefix + this.state.facebook)
    ) {
      showMsj("The facebook field is not a valid URL.", true);
      isValid = false;
    } else if (
      this.state.linkedin != "" &&
      !this.isUrl(linkedin_url_prefix + this.state.linkedin)
    ) {
      showMsj("The linkedin field is not a valid URL.", true);
      isValid = false;
    } else if (
      this.state.instagram != "" &&
      !this.isUrl(instagram_url_prefix + this.state.instagram)
    ) {
      showMsj("The instagram field is not a valid URL.", true);
      isValid = false;
    }
    console.log(" CHECK VALID ", isValid);
    return isValid;
  };

  isUrl = (url) => {
    var regexp = /^[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    var regexp2 = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    let valor = false;
    if (regexp.test(url) || regexp2.test(url)) {
      valor = true;
    }
    return valor;
  };

  render() {
    const {
      navigation: { navigate, goBack },
    } = this.props;
    const {
      twitter,
      instagram,
      facebook,
      linkedin,
      loading,
      notFoundData,
    } = this.state;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => goBack()}
          barStyle="dark-content"
          text="Social"
        />
        <Content contentContainerStyle={{ paddingBottom: 100 }}>
          {/* {this.state.loading ? <Loading style={{ top: "40%", alignSelf: "center" }} /> : null} */}
          <View style={styles.container}>
            <View style={styles.form}>
              <Input
                data={twitter}
                placeholder="Twitter"
                keyboardType={"email-address"}
                elevation={1.5}
                onEdit={(twitter) => this.setState({ twitter })}
                customWidth={styles.input.width}
              />

              <Input
                data={instagram}
                placeholder="Instagram"
                keyboardType={"email-address"}
                elevation={1.5}
                onEdit={(instagram) => this.setState({ instagram })}
                customWidth={styles.input.width}
              />

              <Input
                data={facebook}
                placeholder="Facebook"
                keyboardType={"email-address"}
                elevation={1.5}
                onEdit={(facebook) => this.setState({ facebook })}
                customWidth={styles.input.width}
              />

              <Input
                data={linkedin}
                placeholder="LinkedIn"
                keyboardType={"email-address"}
                elevation={1.5}
                onEdit={(linkedin) => this.setState({ linkedin })}
                customWidth={styles.input.width}
              />
            </View>
            <View style={styles.button}>
              <Button
                loading={this.state.loading}
                text={this.isAdd ? "Save" : "Update"}
                onPress={this.onSaveClick}
              />
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

export default AddSocialLink;
