import React, { Component } from "react";
import { View, Image, FlatList } from "react-native";
import { Container, Content, Text } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import ImageHeader from "utils/components/imageHeader";
import CheckedIcon from "resources/images/myAuditions/checked.png";
import CheckIcon from "resources/images/myAuditions/check.png";
import Loading from "utils/components/loading";
import moment from "moment";
import myAuditionsApi from "api/myAuditionsApi";

class Waitlist extends Component {

	state = {
		data: null,
		loading: false,
		waitlist: []
	}

	async componentDidMount() {
		try {
			const data = this.props.navigation.state.params.data;
			const res = await myAuditionsApi.getWaitlist(data.appointment_id);
			let appoint = res.data.sort((a, b) => a.slot_id - b.slot_id)
			this.setState({
				data,
				loading: true,
				waitlist: appoint
			});
		} catch (e) {
			this.setState({ loading: false });
		}
	}

	render() {
		const {
			loading,
			data,
			waitlist
		} = this.state;

		const { navigation: { goBack } } = this.props;

		return (
			<Container>
				<ImageHeader image={data ? data.media : null}>
					<Header
						leftIcon="back"
						leftFn={() => goBack()}
					/>
				</ImageHeader>
				<Content contentContainerStyle={styles.content}>

					{
						data ?
							<View style={styles.container}>
								<Text style={styles.title}>{data.title}</Text>
								<Text style={styles.subTitle}>{moment(data.date).format("MMMM Do, YYYY")}</Text>
								<Text style={styles.titleUpdate}>Waitlist</Text>
								<FlatList
									keyExtractor={(a, i) => `${i}`}
									renderItem={(item) => (
										<View
											style={styles.label}
										>
											<Text style={styles.labelName}>
												{item.item.name}
											</Text>
											<Text style={styles.labelTime}>
												{item.item.time}
											</Text>
										</View>
									)}
									data={waitlist}
									ListEmptyComponent={
										// <Text style={styles.empty}>This audition not have waitlist</Text>
										<Text style={styles.empty}>This audition has no waitlist</Text>
									}
								/>
							</View>
							:
							<Loading />
					}
				</Content>
			</Container>
		);
	}
}
export default Waitlist;
