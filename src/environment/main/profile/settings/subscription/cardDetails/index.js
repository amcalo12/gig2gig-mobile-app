import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Loading from "utils/components/loading";
import TableCell from "utils/components/tableCell";
import cardDetailModelView from './cardDetailModelView'
class SubscriptionView extends Component {
  state = {
    data: null,
    loading:true,
    date: ''
  }
  componentDidMount = async () => {
    this.props.navigation.addListener('didFocus', this.onFocus);
    await cardDetailModelView.getPaymentInfo(this)
  };

  onFocus = async () => {
    await cardDetailModelView.getPlan(this)
  }
    
  render() {
    const { loading, data, plan, date } = this.state
    const {navigation : { state: { params: { userData }}}} = this.props;
    const {navigation : { state: { params: { userData: { details : { subscription } } } }, navigate, dispatch}} = this.props;
    return (
      <Container>
        <Header
        leftIcon="backDark"
        leftFn={()=>dispatch(StackActions.pop())}
        barStyle="dark-content"
        text="Subscription Details"
        />
        <Content>
          {loading ?
              <Loading style={{top:"40%", alignSelf: 'center',}}/>
              :
              <View style={styles.container}>
                <TableCell disabled title={'Active'}/>
                <TableCell typeText={`Tier ${plan}`} expire={`Expires ${date}`} nextIcon press={() => navigate('Subscription',{payData: data, subscription:plan})}/>
                
                <TableCell disabled title={'Payment method'} customStyle={{marginTop: 35,}}/>
                { data ?
                  <TableCell card sinlgeText={`•••• •••• •••• ${data.last4}`} nextIcon press={() => navigate('EditCard', {data, 'update':(data)=> cardDetailModelView.updateCard(this,data)})}/>
                  :
                  <TableCell sinlgeText={'Add Payment Method'} nextIcon press={() => navigate('AddCard',{addPayment:(info) => cardDetailModelView.addPaymentMethod(this,info)})}/>
                }
              </View>
          }
        </Content>
      </Container>
    );
  }
}
export default SubscriptionView;
