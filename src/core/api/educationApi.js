import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const createEducation = async props =>
  apiHandler(
    API_METHOD.POST,
    `${API_MODULES.EDUCATION}${API_ROUTES.EDUCATIONS.CREATE}`,
    props
  );

const updateEducation = async (props, id) =>
  apiHandler(
    API_METHOD.PUT,
    `${API_MODULES.EDUCATION}${API_ROUTES.EDUCATIONS.UPDATE}/${id}`,
    props
  );

const getEducation = async props =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.EDUCATION}${API_ROUTES.EDUCATIONS.SHOW}`,
  );
const deleteEducation = async props =>
  apiHandler(
    API_METHOD.DELETE,
    `${API_MODULES.EDUCATION}${API_ROUTES.EDUCATIONS.DELETE}/${props}`,
  );

export default {
  createEducation,
  getEducation,
  updateEducation,
  deleteEducation
};
