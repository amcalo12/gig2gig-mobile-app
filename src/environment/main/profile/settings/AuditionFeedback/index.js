import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Label from "utils/components/pushNotificationLabel";
import Loading from "utils/components/loading";
import AuditionFeedbackViewModel from "./AuditionFeedbackViewModel";

class AuditionFeedback extends Component {

	state = {
		loading: true,
		viewFeedback: 'off',
		viewRecommendations: 'off'
	}

	componentDidMount = async () => {
		await AuditionFeedbackViewModel.getMyAuditionConfig(this)
	};

	handleBackPress = () => {
		const { navigation: { dispatch } } = this.props;
		return dispatch(StackActions.popToTop())
		return true;
	}

	changeValue = async (key, id) => {
		console.log(key, id);
		await AuditionFeedbackViewModel.updateFeedback(this, key, id)
	};

	render() {
		const { navigation: { navigate, dispatch } } = this.props;
		const {
			viewFeedback,
			viewRecommendations
		} = this.state
		return (
			<Container>
				<Header
					leftIcon="backDark"
					leftFn={() => dispatch(StackActions.popToTop())}
					barStyle="dark-content"
					text="Audition Feedback"
				/>
				<Content>
					{
						this.state.loading ?
							<Loading style={{ top: "40%", alignSelf: 'center', }} />
							:
							<View style={styles.container}>
								<Label
									text={'View Feedback'}
									value={viewFeedback.value == 0 ? false : true}
									onPress={this.changeValue.bind(this, 'viewFeedback', viewFeedback.id)} />
								<Label
									text={'View Recommendations'}
									value={viewRecommendations.value == 0 ? false : true}
									onPress={this.changeValue.bind(this, 'viewRecommendations', viewRecommendations.id)} />
							</View>
					}
				</Content>
			</Container>
		);
	}
}

export default AuditionFeedback;
