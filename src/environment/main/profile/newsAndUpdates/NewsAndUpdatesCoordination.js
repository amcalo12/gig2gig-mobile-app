import { createStackNavigator } from "react-navigation";
import NewAndUpdates from './index';
import NewAndUpdatesDetail from './newsAndUpdatesDetail';

const NewsAndUpdatesStack = createStackNavigator(
  {
    NewAndUpdates: {
      screen: NewAndUpdates,
      navigationOptions: {
        header: null
      }
    },
    NewAndUpdatesDetail: {
      screen: NewAndUpdatesDetail,
      navigationOptions: {
        header: null
      }
    }
  },
);

export default NewsAndUpdatesStack;
