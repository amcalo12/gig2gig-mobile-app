import React, { Component } from 'react';
import {
  Modal,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import {
  Text, Card
} from 'native-base';
import styles from './index.styles';
import Button from "utils/components/mainButton";
import { resize } from "utils/utils";
import Label from "utils/components/unionMemberLabel";
import { SafeAreaView } from 'react-navigation';

export default class ModalTopics extends Component {
  state = {
    modalVisible: this.props.visible,
    topics: this.props.topics
  };

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.visible == true && this.state.modalVisible == false) {
      this.setState({
        modalVisible: nextProps.visible,
        topics: nextState.topics
      })
    }
  }

  toggleCheck = (index, value) => {
    let { topics } = this.state;
    const { topic_ids, context } = this.props;

    topics[index].active = !value;

    if (topics[index].active) {
      topic_ids.push({ id: topics[index].id })
      context.setState({
        topic_ids
      })
    } else {
      const indexx = topic_ids.findIndex(res => res.id == topics[index].id)
      topic_ids.splice(indexx, 1)
      context.setState({
        topic_ids
      })
    }

    console.log(topics[index])

    this.setState({ topics });
  };


  render() {
    return (
      <Modal
        transparent={true}
        animationType="fade"
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setState({ modalVisible: false })
        }}>
        <SafeAreaView style={styles.container}>
          <Card style={styles.info}>
            <Text style={styles.title}>Topics</Text>
            <ScrollView style={styles.scrollView}>
              {
                this.state.topics.length != 0 ?
                  this.state.topics.map((option, idx) => (
                    <Label key={idx} text={option.title} checked={option.active ? option.active : false} onPress={() => this.toggleCheck(idx, option.active ? option.active : false)} />
                  ))
                  :
                  <Text
                    style={styles.empty}
                  >Not found topics</Text>
              }
            </ScrollView>
            <Button text="Done" customWidth={resize(174)} onPress={() => this.setState({ modalVisible: false })} />
          </Card>
        </SafeAreaView>
      </Modal>
    );
  }
}