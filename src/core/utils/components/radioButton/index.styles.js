import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");

export default StyleSheet.create({
  container:{
    flexDirection:"row",
    alignItems: 'center',
    // marginHorizontal: resize(15, "height")
  },
  text:{
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 17,
    marginLeft: resize(8, "height"),
  }
});