import React, { Component } from "react";
import { View, Text, RefreshControl,AsyncStorage } from "react-native";
import { Content } from "native-base";
import styles from "./index.styles";
import Card from "utils/components/auditionCard";
import { withNavigation } from "react-navigation";
import myAuditionsApi from "api/myAuditionsApi";
import Loading from "utils/components/loading";
import { showMsj } from "utils/utils";
import { generalMsj } from "resources/l10n";
import { refreshControlColors } from "resources/colors";
import { openSubscriptionDialog, checkSubscriptionAndRedirect } from "utils/cons";
class PastTab extends Component {
  state = {
    loading: false,
    refreshing: false,
    auditions: []
  };

  _onRefresh = async () => {
    //  this.getPastAuditions(true);
    this.props.reloadFn();
  };

  async componentDidMount() {
    this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
    console.log('userData :>> ', this.userData);    
  }

  render() {
    const { auditions, loading } = this.state;
    const {
      data,
      navigation: { navigate }
    } = this.props;

    return (
      <Content
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={() => this._onRefresh()}
            colors={refreshControlColors}
          />
        }
      >
        {!data && <Loading />}
        {data &&
          data.map((item, idx) => {
            return (
              <Card
                key={idx}
                data={item}
                isPast
                onPress={() => {
                  // navigate("Past", { data: item })
                  // return
                  if(this.userData.isPaidUser){
                    navigate("Past", { data: item })
                  }else{
                    checkSubscriptionAndRedirect(this.userData,'SubscriptionPlan',this.props)
                    // openSubscriptionDialog(() => {
                    //   this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true })
                    // })
                  }
                }}
              />
            );
          })}
        {data && data.length === 0 && (
          <Text style={styles.text}>You don't have any past auditions</Text>
        )}
      </Content>
    );
  }
}
export default withNavigation(PastTab);
