import React, { Component } from "react";
import { View, Image, TouchableOpacity, TextInput } from "react-native";
import { Container, Content, Text, Card } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import ImageHeader from "utils/components/imageHeader";
import Button from "utils/components/mainButton";
import ButtonClear from "utils/components/secondaryButton";
import { StackActions } from "react-navigation";
import QRCode from "react-native-qr-generator";
import moment from "moment";
import PlusIcon from "resources/images/plus-icon.png";
import { colorPalette } from "resources/colors";

class CheckInView extends Component {
  state = {
    auditionData: null,
    QRData: null,
    assignedNumber: 0
  };

  async componentDidMount() {
    const auditionData = await this.props.navigation.state.params.data;
    const idUser = await this.props.navigation.state.params.idUser;
    let QRData = JSON.stringify({
      appointmentId: auditionData.appointment_id,
      auditionId: auditionData.auditions_id,
      userId: idUser,
      rolId: auditionData.rol,
      hour: auditionData.hour
    });
    this.setState({
      auditionData,
      QRData
    });
  }

  render() {
    const { auditionData, QRData } = this.state;
    const {
      navigation: { navigate, dispatch }
    } = this.props;
    return (
      <Container>
        <ImageHeader
          size="small"
          image={auditionData ? auditionData.media : null}
        >
          <Header leftIcon="back" leftFn={() => this.props.navigation.pop()} />
        </ImageHeader>
        {auditionData && (
          <Content>
            <View style={styles.container}>
              <Text style={styles.title}>{auditionData.title}</Text>
              <Text style={styles.subTitle}>
                {auditionData.round ? "Round " + auditionData.round : ""}
              </Text>
              <Text style={styles.subTitle}>
                {moment(auditionData.date).format("MMMM Do, YYYY")}
              </Text>
              <Text style={styles.appointment}>
                {auditionData.hour && (
                  <Text style={styles.bold}>
                    {moment(auditionData.hour, "h:mma").format("h:mma")}
                  </Text>
                )}
                {auditionData.hour && ` Appointment`}
              </Text>
              {QRData && (
                <QRCode
                  style={styles.code}
                  size={200}
                  value={QRData}
                  foregroundColor="#000000"
                  backgroundColor="#FFFFFF"
                />
              )}
              <View style={styles.buttons}>
                <View
                  activeOpacity={0.6}
                  style={styles.inputTextContainer}
                  onPress={() => {}}
                >
                  <TextInput
                    placeholder={"Add Number"}
                    style={styles.assignedNumberInput}
                    value={this.state.assignedNumber}
                    placeholderTextColor={colorPalette.purple}
                    placeholderStyle={{ textAlign: "center" }}
                    keyboardType="number-pad"
                    onChangeText={text => {
                      this.setState({ assignedNumber: text });
                    }}
                  />
                  {/* <Text style={styles.assignedNoTextStyle}>Add New Skills</Text> */}
                  <TouchableOpacity
                    style={{
                      position: "absolute",
                      right: 10,
                      padding: 10
                    }}
                  >
                    <Image source={PlusIcon} />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.buttons}>
                <ButtonClear
                  text="View Updates"
                  onPress={() => navigate("Updates", { data: auditionData })}
                />
              </View>
              <View style={styles.buttons}>
                <ButtonClear
                  text="View Waitlist"
                  onPress={() => navigate("Waitlist", { data: auditionData })}
                />
              </View>
              <View style={styles.buttons}>
                <ButtonClear text="Instant Feedback" onPress={() => {}} />
              </View>
              <Button
                text="Finish"
                onPress={() => dispatch(StackActions.popToTop())}
              />
            </View>
          </Content>
        )}
      </Container>
    );
  }
}
export default CheckInView;
