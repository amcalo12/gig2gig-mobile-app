import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    paddingTop: 15,
    alignItems: 'center',
    width:'100%',
    paddingBottom: 15,
  },
  img:{
    marginRight:7
  },
  titleText:{
    paddingLeft: 5,
    marginTop: 20,
    marginBottom: 10,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: colorPalette.purple
  },
  imagesContainer:{
    flexDirection:'column',
    alignItems:'flex-start',
    width:'78%',
    marginBottom: 15,
    marginTop:5
  },
  row:{
    flexDirection:'row'
  },
  secondPartContainer:{
    alignItems:'flex-start', 
    width:'80%'
  }
});