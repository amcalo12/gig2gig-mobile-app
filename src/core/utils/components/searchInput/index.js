import React, { Component } from 'react';
import { Image, View, TouchableOpacity, TextInput } from 'react-native';
import { Input, Card } from 'native-base';
import styles from './index.styles';
import FilterIcon from "resources/images/tabNavigation/filter-icon.png";
import searchIcon from "resources/images/tabNavigation/search-icon.png";
import clear_input from 'resources/images/clear_input_x.png';

export default class SearchInput extends Component {

  render() {
    const { textInput, onChangeText, leftIcon, onPress, onEdit, value, onPressclearData } = this.props;
    return (
      <Card style={[styles.searchContainer, this.props.styles]}>
        <Image source={searchIcon} />
        <Input
          placeholderTextColor='#4D2545'
          placeholder="Search"
          style={styles.searchInput}
          onChangeText={(text) => onChangeText(text)}
          value={value}
          clearButtonMode={'while-editing'}
        />
        {/* {
          onPressclearData && value != '' &&
          <TouchableOpacity
            style={styles.onClearData}
            onPress={() => onPressclearData()}
          >
            <Image
              source={clear_input}
            />
          </TouchableOpacity>
        } */}
        {
          (leftIcon) &&
          <TouchableOpacity
            activeOpacity={0.7}
            ref={(button) => this.modal = button}
            // onPress={()=>onPress()}
            onPress={() => { this.modal.measure((a, b, width, height, px, py) => onPress({ a, b, width, height, px, py })) }}
            style={styles.filterButton}
          >
            <Image source={FilterIcon} />
          </TouchableOpacity>
        }
      </Card>
    )
  }
}

SearchInput.defaultProps = {
  textInput: null,
  onChangeText: null,
  leftIcon: null,
  onPressclearData: null
}