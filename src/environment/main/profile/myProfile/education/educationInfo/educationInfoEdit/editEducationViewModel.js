import Picker from "react-native-picker";
import moment from "moment";
import educationApi from "api/educationApi";
import { showMsj } from "utils/utils";

const convertYears = () => {
  const year = []
  const years = getYears()
  years.map((res,i) => year[i] = {label: res.toString(),value:res.toString()})
  
  return year
}
const getYears = () => {
  const year = moment().year()
  const allYears = []
  for (let i = 0; i < 70; i++) {
    allYears[i] = year - i
  }
  return allYears
}
const handlePressSelect = (context, selected) => {
  context.setState({productionType: selected.name})
  select(context);
}

export const select = (context) => {
  context.refs.select.setModalVisible();
}
const handlePressSave = async(context) => {
  var {school, degree, instructor, location, year} = context.state
  const { id } = context.props.navigation.state.params
  if(school === ''){ school = id.school }
  if(degree === ''){ degree = id.degree }
  if(instructor === ''){ instructor = id.instructor }
  if(location === ''){ location = id.location }
  if(year === ''){ year = id.year }

  context.setState({loading:true})
  if(school != '' || degree != '' || instructor != '' || location != '' || year != ''){
    const body = {school, degree, instructor, location, year}
    try {
      await educationApi.updateEducation(body, id.id)
      context.props.navigation.popToTop()
      context.setState({loading:false})
      context.props.navigation.state.params.onGoBack({...body, id:id.id})
      return showMsj('Education update', false)
    } catch (error) {
      context.setState({loading:false})
      return showMsj('he education could not be update', true)
    }
  }else{
    context.setState({loading:false})
    return showMsj('All fields as required', true)
  }
}
export default {
  convertYears,
  handlePressSelect,
  handlePressSave,
  select
}