import React, { Component } from "react";
import { View, Text, RefreshControl, AsyncStorage } from "react-native";
import { Content, Spinner } from "native-base";
import styles from "./style";
import Card from "utils/components/auditionCard";
import { withNavigationFocus } from "react-navigation";
import myAuditionsApi from "api/myAuditionsApi";
import Loading from "utils/components/loading";
import { showMsj } from "utils/utils";
import { generalMsj } from "resources/l10n";
import { refreshControlColors } from "resources/colors";
import auditionsApi from "api/auditionsApi";
import Modal from "utils/components/ModalRequest";
import Swipeout from "react-native-swipeout";
import { checkSubscriptionAndRedirect } from "utils/cons";

class AllAuditionsFeedback extends Component {
  state = {
    loading: false,
    refreshing: false,
    auditions: []
  };

  _onRefresh = async () => {
    //  this.getPastAuditions(true);
    this.props.reloadFn();
  };

  async componentDidMount() {
    this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
    console.log('userData :>> ', this.userData);    
  }
  
  render() {
    const { auditions, loading } = this.state;
    const {
      data,
      navigation: { navigate }
    } = this.props;

    return (
      <Content
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={() => this._onRefresh()}
            colors={refreshControlColors}
          />
        }
      >
        {!data && <Loading />}
        {data &&
          data.map((item, idx) => {
            return (
              <Card
                key={idx}
                data={item}
                isPast
                onPress={() => {
                  if(this.userData.isPaidUser){
                    navigate("Past", { data: item })
                  }else{
                    checkSubscriptionAndRedirect(this.userData,'SubscriptionPlan',this.props)
                    // openSubscriptionDialog(() => {
                    //   this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true })
                    // })
                  }
                }}
              />
            );
          })}
        {data && data.length === 0 && (
          <Text style={styles.text}>You don't have any past auditions</Text>
        )}
      </Content>
    );
  }
}
export default withNavigationFocus(AllAuditionsFeedback);
