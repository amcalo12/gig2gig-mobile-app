import React, { Component } from "react";
import { Root } from "native-base";
import firebase from "react-native-firebase";
import * as FirebaseWeb from "firebase";
import type { RemoteMessage } from "react-native-firebase";
import { FIREBASE_WEB_CONFIG } from "utils/cons"
import Splash from './src/splash';
import "utils/networkDebugger";
import { updateMomentLocale } from "utils/utils";
import NavigationService from "./src/NavigationService";
import DeviceInfo from "react-native-device-info";
import pushNotificationApi from "api/pushNotificationApi";
import { AsyncStorage } from "react-native";
import Router from "./src/router";
import SplashScreen from "react-native-splash-screen";


// for error '_iterator2[typeof Symbol === "function" ? Symbol.iterator : "@@iterator"]()
global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');

// const combineReducer = combineReducers({
//   userReducer: userReducer,
// });

// export const store = createStore(combineReducer);

/*
  If redux persist is later needed, simple wrap the content inside of provider with a
  <PersistGate loading={null} persistor={persistor}>
 */
export default class App extends Component {
  async componentDidMount() {
    firebase.messaging().subscribeToTopic("blog");
    // TODO: You: Do firebase things
    const enabled = await firebase.messaging().hasPermission();
    if (!enabled) {
      await firebase.messaging().requestPermission();
    } else {
      this.messageListener = firebase
        .messaging()
        .onMessage((message: RemoteMessage) => {
          console.log('message :', message);
          const notification = new firebase.notifications.Notification()
            .setNotificationId(message["_messageId"])
            .setTitle(message["_data"]["title"])
            .setBody(message["_data"]["body"]);
          notification.android
            .setChannelId("channelId")
            .android.setSmallIcon("ic_launcher");
          notification.ios.setBadge(2);
          firebase.notifications().displayNotification(notification);
        });
    }


  }

  initializeFirebase() {
    if (!FirebaseWeb.apps.length) {
      // Initialize Firebase
      FirebaseWeb.initializeApp(FIREBASE_WEB_CONFIG);
    }

  }

  async componentDidMount() {
    // SplashScreen.hide();
    this.initializeFirebase();
    // this.removeNotificationListener = firebase.notifications().onNotification((notification) => {
    //   console.log(" notification received foreground", notification)
    //   // Process your notification as required
    // });

    // this.removeNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    //   // Get the action triggered by the notification being opened
    //   const action = notificationOpen.action;
    //   // Get information about the notification that was opened
    //   const notification = notificationOpen.notification;
    //   console.log(" notification received background", notificationOpen)

    //   NavigationService.redirect()
    // });

    // firebase.notifications().getInitialNotification()
    //   .then((notificationOpen) => {
    //     if (notificationOpen) {
    //       console.log(" notification received close", notificationOpen)
    //       // App was opened by a notification
    //       // Get the action triggered by the notification being opened
    //       const action = notificationOpen.action;
    //       // Get information about the notification that was opened
    //       const notification = notificationOpen.notification;
    //     }
    //   });
    // try {
    //   const accessToken = await AsyncStorage.getItem("Authorization");
    //   if (accessToken) {
    //     const token = await firebase.messaging().getToken();
    //     let uniqueId = await DeviceInfo.getUniqueId();
    //     await pushNotificationApi.setPushToken({
    //       pushkey: token,
    //       device_id: uniqueId
    //     });
    //   }
    // } catch (error) {
    //   console.log(error);
    // }
  }

  componentWillUnmount() {
    // this.removeNotificationListener()
    // this.removeNotificationOpenedListener();
    if (this.messageListener)
      this.messageListener();
  }

  render() {
    return (
      // <Provider store={store}>
      <Root>
        <Splash />
      </Root>
      // </Provider>

      // <Root>
      //   <Router
      //     ref={nav => {
      //       // this.navigator = nav;
      //       NavigationService.setTopLevelNavigator(nav);
      //     }}
      //   />
      // </Root>
    );
  }
}
