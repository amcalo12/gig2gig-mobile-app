import { apiHandler } from "./apiMethods";
import { API_METHOD, API_ROUTES } from "utils/cons";

const editUserInfo = async (props, id) =>
  apiHandler(
    API_METHOD.PUT,
    `${API_ROUTES.MY_PROFILE.UPDATE_USER}${id}`,
    props
  );


const getRepresentation = async () =>
  apiHandler(
    API_METHOD.GET,
    API_ROUTES.MY_PROFILE.GET_REPRESENTATION
  );

const createRepresentation = async props =>
  apiHandler(
    API_METHOD.POST,
    API_ROUTES.MY_PROFILE.CREATE_REPRESENTATION,
    props
  );

const editRepresentation = async (props, id) =>
  apiHandler(
    API_METHOD.PUT,
    `${API_ROUTES.MY_PROFILE.EDIT_REPRESENTATION}${id}`,
    props
  );

const getSkillsList = async () =>
  apiHandler(
    API_METHOD.GET,
    API_ROUTES.MY_PROFILE.GET_SKILL_LIST
  );

const getUserSkillsList = async () =>
  apiHandler(
    API_METHOD.GET,
    API_ROUTES.MY_PROFILE.GET_USER_SKILL_LIST
  );

const addSkill = async (id) =>
  apiHandler(
    API_METHOD.POST,
    API_ROUTES.MY_PROFILE.ADD_SKILL,
    id
  );

const deleteSkill = async (id) =>
  apiHandler(
    API_METHOD.DELETE,
    `${API_ROUTES.MY_PROFILE.DELETE_SKILL}${id}`,
  );


const renameResource = async (params) =>
  apiHandler(
    API_METHOD.POST,
    `media/update/ResourceName`,
    params
  );

export default {
  editUserInfo,
  getRepresentation,
  createRepresentation,
  editRepresentation,
  getSkillsList,
  getUserSkillsList,
  addSkill,
  deleteSkill,
  renameResource
};
