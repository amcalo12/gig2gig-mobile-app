import { createStackNavigator } from "react-navigation";
import SheetMusicView from ".";

const SheetMusicStack = createStackNavigator(
  {
     SheetMusic: {
       screen: SheetMusicView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "SheetMusic",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default SheetMusicStack;
