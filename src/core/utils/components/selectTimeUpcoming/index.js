import React, { Component } from 'react';
import {
  Modal,
  View,
  Image,
  TouchableOpacity,
  Platform, ScrollView, Dimensions
} from 'react-native';
import {
  Text
} from 'native-base';
import styles from './index.styles';
const { width, height } = Dimensions.get("screen");

export default class SelectTimeUpcoming extends Component {
  state = {
    selectVisible: false,
  };

  setModalVisible() {
    this.setState({
      selectVisible: !this.state.selectVisible
    });
  }


  render(){
    const {data, selectTitle, onPress} = this.props;
    return(
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.selectVisible}
        onRequestClose={() => this.setState({ selectVisible: false })}
      >
        <TouchableOpacity
          style={styles.container}
          onPress={() => this.setState({ selectVisible: false })}
        >
          <View style={styles.selectContainer}>
            <ScrollView
              scrollEnabled={true}
              bounces={false}
              style={styles.selectScroll}
            >
              <TouchableOpacity
              activeOpacity={1}
                // onPress={() => this.handleSetCurrentDay(null)}
              style={styles.selectTitle}
              >
                <Text style={styles.selectOptionsTitleText}>{selectTitle}</Text>
              </TouchableOpacity>
              {
                data.length > 0 &&
                data.map((item, idx) => (
                <TouchableOpacity
                  key={idx}
                  onPress={() => onPress(item)}
                  style={styles.selectOptions}
                >
                  <Text style={styles.selectOptionsText}>{item.time}</Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}