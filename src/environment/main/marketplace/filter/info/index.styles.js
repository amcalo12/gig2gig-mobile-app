import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import {resize} from "utils/utils"

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  content:{
    marginTop: isIos ? height * 0.04 : height * 0.03,
    paddingHorizontal: resize(20),
  },
  title:{
    width:resize(320),
    fontFamily:fontStyles.nexa_bold,
    fontSize:24,
    color:colorPalette.purple,
  },
  label:{
    width:"100%",
    flexDirection:"row"
  },
  labelLeft:{
    width:"15%",
    justifyContent: "center",
    paddingLeft: resize(3),
  },
  labelRight:{
    width: "85%",
    borderColor:"#F0F0F0",
    borderBottomWidth: 1,
    height:resize(60, "height"),
    justifyContent:"center"
  },
  labelText:{
    fontFamily:fontStyles.nexa_light,
    fontSize:14,
    color:colorPalette.purple
  },
  services:{
    fontFamily:fontStyles.nexa_bold,
    fontSize:18,
    color:colorPalette.purple,
    letterSpacing:0.9,
    marginTop:resize(20, "height")
  },
  serviceText:{
    fontFamily:fontStyles.nexa_light,
    fontSize:14,
    color:colorPalette.purple,
    letterSpacing:0.7,
    marginTop: resize(20, "height"),
    marginBottom: resize(40, "height")
  }
});