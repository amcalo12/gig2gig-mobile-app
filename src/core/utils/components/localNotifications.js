import React, { Component } from "react";
import { View, Platform } from "react-native";
import firebase from "react-native-firebase";
import Router from "../../../router";
import NavigationService from "../../../NavigationService";
const PATTERN = [1000, 2000, 3000];
class LocalNotification extends Component {
  constructor(props) {
    super(props)
    this.isFromNotification = false
    this.state = {
      isFromNotification: false
    }
  }
  navigator;

  componentDidMount() {
    this.openNotification();
    this.openNotificationWileAppIsClosed();
    this.showNotification();
  }

  componentWillUnmount() {
    this.removeNotificationListener();
    this.removeNotificationOpenedListener();
    if (this.messageListener) this.messageListener();
  }

  openNotificationWileAppIsClosed = async () => {
    const notificationOpen: NotificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      // App was opened by a notification
      // Get the action triggered by the notification being opened
      const action = notificationOpen.action;
      // Get information about the notification that was opened
      const notification: Notification = notificationOpen.notification;
      if (notificationOpen.notification.data.type == "instant_feedback") {
        this.isFromNotification = true
        this.setState({ isFromNotification: true })
        // setTimeout(() => {
        NavigationService.redirect(notificationOpen.notification.data)
        // }, 500);
      }
      console.log({ action, notification });
    }
  };

  showNotification = () => {
    this.messageListener = firebase
      .messaging()
      .onMessage((message: RemoteMessage) => {
        this.displayNotification(message);
      });
    this.removeNotificationListener = firebase
      .notifications()
      .onNotification((notification: Notification) => {
        this.displayNotification(notification);
      });
  };

  displayNotification = notification => {
    const channelId = new firebase.notifications.Android.Channel(
      "Default",
      "Default",
      firebase.notifications.Android.Importance.High
    );
    firebase.notifications().android.createChannel(channelId);

    let notification_to_be_displayed = new firebase.notifications.Notification({
      data: notification._data,
      sound: "default",
      show_in_foreground: true,
      title: notification._title ? notification._title : "",
      body: notification._body
    });

    if (Platform.OS == "android") {
      notification_to_be_displayed.android
        .setPriority(firebase.notifications.Android.Priority.High)
        .android.setChannelId("Default")
        .android.setVibrate(1000)
        .android.setSmallIcon("ic_launcher");
    }

    firebase.notifications().displayNotification(notification_to_be_displayed);
  };

  openNotification = () => {
    this.removeNotificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen: NotificationOpen) => {
        console.log(notificationOpen);
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        // Get information about the notification that was opened
        const notification: Notification = notificationOpen.notification;

        if (notificationOpen.notification.data.type == "instant_feedback") {
          NavigationService.redirect(notificationOpen.notification.data)
        }
      });
  };

  render() {
    try {
      return (
        <View style={{ flex: 1 }}>
          <Router
            ref={nav => {
              this.navigator = nav;
              NavigationService.setTopLevelNavigator(nav);
            }}
            screenProps={{ isFromNotification: this.state.isFromNotification }}
          />
        </View>
      );
    } catch (error) {
      return null;
    }
  }
}

export default LocalNotification;
