import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  content: {
    alignItems: 'center',
    paddingHorizontal: resize(15),
    paddingVertical: resize(15, 'h')
  },
  img: {
    width: '100%',
    height: resize(150, 'h'),
    borderRadius: 8,
  },
  body: {
    color: '#757575',
    fontFamily: 'Nexa Light',
    fontSize: resize(12),
    fontWeight: '400',
    letterSpacing: resize(0.24),
    lineHeight: resize(14, 'h'),
    marginTop: resize(29, 'h')
  }
});