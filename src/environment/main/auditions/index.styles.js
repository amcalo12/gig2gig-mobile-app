import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");


export default StyleSheet.create({
  container:{
    width:width,
    // height:height * 0.804512743628,
    alignItems: 'center',
    // justifyContent:"space-around"
    marginTop: height * 0.02,
  },
  spinner:{
    // marginTop:resize(30, "height"),
    position:"absolute",
    top: resize(30, "height"),
  },
  searchText:{
    fontFamily:fontStyles.nexa_bold,
    color:colorPalette.purple,
    fontSize:16,
    marginTop: resize(10, "height"),
    marginBottom: resize(20, "height"),
  }
});