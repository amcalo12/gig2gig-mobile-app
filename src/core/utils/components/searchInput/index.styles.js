import { Dimensions, StyleSheet, Platform } from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");

export default StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: 'center'
  },
  onClearData: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    width: resize(40),
  },
  searchContainer: {
    flexDirection: "row",
    alignItems: 'center',
    paddingLeft: 15,
    backgroundColor: "#FFFFFF",
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    // borderRadius:30,
    borderRadius: 20,
    borderColor: "transparent",
    // elevation:8,
    // // shadowOffset: { width: 10, height: 10 },
    // shadowOffset: { width: 0, height: 0 },
    // // shadowColor: "#FAFAFA",
    // shadowColor: 'rgba(0, 0, 0, 0.1)',
    // // shadowOpacity: 0.01,
    // // shadowRadius: 10,
    // shadowRadius: 15,
    marginHorizontal: 10,
    height: resize(37, "height"),
    width: "100%",
    alignSelf: "center"
  },
  searchInput: {
    color: "#4D2545",
    fontFamily: fontStyles.nexa_light,
    letterSpacing: 1,
    paddingLeft: 10,
    paddingRight: 10
    // height: height * 0.0554722638681,
    // width: width * 0.893333333333
  },
  filterButton: {
    paddingRight: 15
  }
});