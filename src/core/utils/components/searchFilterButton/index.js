import React, { Component } from "react";
import { TouchableOpacity, Image, Text, View } from "react-native";
import styles from "./index.styles"
class SerchFilterButton extends Component {

  render() {
    const {text, type, active, onPress} = this.props;
    let containerStyle="";
    let textStyle = "";
    if(type==="union"){
      containerStyle = active ? "unionActive" : "unionContainer";
      textStyle = active ? "textActive" : "unionText";
    } else if (type === "contract") {
      containerStyle = active ? "contractActive" : "contractContainer";
      textStyle = active ? "textActive" : "contractText";
    } else if (type === "production") {
      containerStyle = active ? "productionActive" : "productionContainer";
      textStyle = active ? "textActive" : "productionText";
    }
    return (
        <TouchableOpacity
        activeOpacity={1}
        style={[styles.container, styles[containerStyle]]}
        onPress={()=>onPress()}
        >
          <Text style={[styles.text, styles[textStyle]]}>{text}</Text>
        </TouchableOpacity>
    );
  }
}
export default SerchFilterButton;
SerchFilterButton.defaultProps = {
  type:"union",
  text:"UNION",
  small:true
}