package com.g2g.phone.android;

import android.app.Application;

import com.dooboolab.RNIap.RNIapPackage;
import com.facebook.react.ReactApplication;
import com.airbnb.android.react.maps.MapsPackage;
import com.g2g.phone.android.BuildConfig;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.shahenlibrary.RNVideoProcessingPackage;
import com.zapper.QRCodePackage;
import com.ninty.system.setting.SystemSettingPackage;
import com.beefe.picker.PickerViewPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.imagepicker.ImagePickerPackage;
// import com.RNFetchBlob.RNFetchBlobPackage;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;

import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.reactnativecommunity.webview.RNCWebViewPackage;

import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage;
import me.hauvo.thumbnail.RNThumbnailPackage;

import com.brentvatne.react.ReactVideoPackage;
import com.wog.videoplayer.VideoPlayerPackage;
import com.RNFetchBlob.RNFetchBlobPackage;

import java.util.Arrays;
import java.util.List;

//React Youtube
import com.inprogress.reactnativeyoutube.ReactNativeYouTube;

//firebase
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.storage.RNFirebaseStoragePackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;

//react native splash screen
import org.devio.rn.splashscreen.SplashScreenReactPackage;

//react navigation
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;

public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(new MainReactPackage(),
                    new MapsPackage(),
                    new QRCodePackage(),
                    new SystemSettingPackage(),
                    new PickerViewPackage(),
                    new LinearGradientPackage(),
                    new ImagePickerPackage(),
                    // new RNFetchBlobPackage(),
                    new DocumentPickerPackage(),
                    new RNDeviceInfo(),
                    new RNFirebasePackage(),
                    new RNFirebaseStoragePackage(),
                    new RNFirebaseMessagingPackage(),
                    new RNFirebaseNotificationsPackage(),
                    new SplashScreenReactPackage(),
                    new ReactNativeYouTube(),
                    new RNGestureHandlerPackage(),
                    new RNCWebViewPackage(),
                    new RNThumbnailPackage(),
                    new ReactVideoPackage(),
                    new VideoPlayerPackage(),
                    new RNFetchBlobPackage(),
                    new RNVideoProcessingPackage(),
                    new ReactNativeContacts(),
                    new PickerPackage(),
                    new RNIapPackage(),
                    new ImageResizerPackage(),
                    new RNFirebaseAnalyticsPackage(),
                    new RNFirebaseCrashlyticsPackage()
            );

        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
