import { createStackNavigator } from "react-navigation";
import AppearanceView from ".";
import AppearanceEditView from "./appearanceEdit";
import AddAppearance from "./addApearance";
import HairView from "./appearanceEdit/hair";
import EthnicityView from "./appearanceEdit/ethnicity";
import EyeColorView from "./appearanceEdit/eyeColor";
import PersonalFlare from "./appearanceEdit/flare";
import GenderPronouns from "./appearanceEdit/genderpronouns";

const AppearanceStack = createStackNavigator(
  {
    Appearance: {
      screen: AppearanceView,
      navigationOptions: {
        header: null
      }
    },
    AppearanceEdit: {
      screen: AppearanceEditView,
      navigationOptions: {
        header: null
      }
    },
    Hair: {
      screen: HairView,
      navigationOptions: {
        header: null
      }
    },
    EyeColor: {
      screen: EyeColorView,
      navigationOptions: {
        header: null
      }
    },
    Ethnicity: {
      screen: EthnicityView,
      navigationOptions: {
        header: null
      }
    },
    PersonalFlare: {
      screen: PersonalFlare,
      navigationOptions: {
        header: null
      }
    },
    GenderPronouns: {
      screen: GenderPronouns,
      navigationOptions: {
        header: null
      }
    },
    AddAppearance: {
      screen: AddAppearance,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "Appearance",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default AppearanceStack;
