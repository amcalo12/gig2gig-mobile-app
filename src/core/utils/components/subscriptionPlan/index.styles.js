import { StyleSheet, Platform, Dimensions } from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils"
const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
  subscriptionContainer: {
    alignItems: "center",
  },
  title: {
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    letterSpacing: 0.9,
    marginBottom: resize(20, "height"),

  },
  text: {
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_light,
    fontSize: 18,
    letterSpacing: 0.9,
    textAlign: "center",
    marginBottom: resize(20, "height"),
    width: resize(220)
  },
  button: {
    borderWidth: 2,
    borderRadius: 13,
    borderColor: colorPalette.purple,
    width: resize(229),
    height: resize(49, "height"),
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
    fontSize: 18
  },
  buttonOpacity: {
    borderColor: "#E3E3E3"
  },
  buttonOpacityText: {
    color: "#E3E3E3"
  }
});