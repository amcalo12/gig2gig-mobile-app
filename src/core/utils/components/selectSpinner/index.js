import React, { Component } from 'react';
import {
  Modal,
  View,
  Image,
  TouchableOpacity,
  Platform, ScrollView, Dimensions
} from 'react-native';
import {
  Text, Card
} from 'native-base';
import styles from './index.styles';
const { width, height } = Dimensions.get("screen");

export default class SelectSpinner extends Component {
  state = {
    selectVisible: false,
    top: 0
  };

  setModalVisible() {
    this.setState({
      selectVisible: !this.state.selectVisible
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isCustomSpinner) {
      if (prevProps.marginTop !== this.props.marginTop) {
        this.setState({ top: this.props.marginTop })
      }
    }
  }
  getCustomContainerStyle() {
    if (this.props.isCustomSpinner) {
      return {
        flex: 1,
        width: "100%",
        height: "100%",
        alignItems: "center",
        position: 'absolute',
        top: this.state.top
      }
    } else {
      return styles.container;
    }
  }
  render() {
    const { data, selectTitle, onPress, top } = this.props;
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.selectVisible}
        onRequestClose={() => this.setState({ selectVisible: false })}
      >
        <TouchableOpacity
          style={this.getCustomContainerStyle()}
          onPress={() => this.setState({ selectVisible: false })}
        >
          <Card style={[styles.selectContainer, top && { marginTop: top }, top && styles.top]}>
            <ScrollView
              scrollEnabled={true}
              bounces={false}
              style={styles.selectScroll}
            >
              <TouchableOpacity
                activeOpacity={1}
                // onPress={() => this.handleSetCurrentDay(null)}
                style={styles.selectTitle}
              >
                <Text style={styles.selectOptionsTitleText}>{selectTitle}</Text>
              </TouchableOpacity>
              {data.map((item, idx) => {

                if (item.name.toUpperCase() !== "ANY") {
                  return (
                    <TouchableOpacity
                      key={idx}
                      onPress={() => onPress(item)}
                      style={styles.selectOptions}
                    >
                      <Text style={styles.selectOptionsText}>{item.name}</Text>
                    </TouchableOpacity>
                  )

                }
              })}
            </ScrollView>
          </Card>
        </TouchableOpacity>
      </Modal>
    );
  }
}