import appearanceApi from 'api/appearanceApi'

const getAppearance = async (context) => {
  context.setState({ loading: true })

  try {
    const res = await appearanceApi.getAppearance()
    const userInfo = res.data
    context.setState({ userInfo, loading: false })
  } catch (error) {
    context.setState({ notFoundData: true, loading: false })
  }
}
const update = async (context, userInfo) => {
  await getAppearance(context)
  context.setState({ notFoundData: false })
}

export default {
  getAppearance,
  update
}