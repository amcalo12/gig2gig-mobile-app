import {StyleSheet} from 'react-native';
import { fontStyles } from 'resources/fonts';
import { colorPalette } from 'resources/colors';


export default StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.15)'
  },
  info:{
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: 300,
    height: 180,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 20,
    position:"relative"
  },
  text:{
    fontSize: 16,
    fontFamily: fontStyles.nexa_bold,
    color:colorPalette.purple,
    textAlign:"center"
  }
});