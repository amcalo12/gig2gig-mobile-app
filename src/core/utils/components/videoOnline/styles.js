import { StyleSheet } from 'react-native';
import { resize } from 'utils/utils';
import { fontStyles } from 'resources/fonts';

const styles = StyleSheet.create({
	container: {
		width: '90%',
		alignSelf: 'center',
		height: resize(143, 'h'),
		borderRadius: resize(13),
		overflow: 'hidden',
		marginBottom: resize(25, 'h')
	},
	video: {
		width: '100%',
		flex: 111,
		justifyContent: 'center',
		alignItems: 'center'
	},
	playVideoIndicator: {
		alignItems: 'center',
		justifyContent: 'center',
		width: resize(50),
		height: resize(50),
		borderRadius: resize(25),
		backgroundColor: 'rgba(255,255,255,.3)',
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5
	},
	textContainer: {
		backgroundColor: '#4D2545',
		width: '100%',
		flex: 32,
		paddingHorizontal: resize(15),
		justifyContent: 'center'
	},
	textName: {
		fontFamily: fontStyles.nexa_bold,
		color: 'white',
		letterSpacing: 1,
		fontSize: resize(14)
	}
})
export default styles;
