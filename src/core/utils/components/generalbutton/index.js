import React, { Component } from "react";
import { View, TouchableOpacity, Image, Text, ScrollView, Platform, Dimensions } from "react-native";
import styles from "./index.style";
import AddIcon from "resources/images/audition/add-icon.png";
import { fontStyles } from "resources/fonts";
import { colorPalette } from "resources/colors";
import Loading from "../loading";

export const ApplyButton = props => {
  return (
    <TouchableOpacity style={[styles.buttonContainer, props.style]} onPress={props.onClick}>
      <Text style={styles.buttonText}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export const AddMaterialButton = props => {
  return (
    <TouchableOpacity style={[styles.borderButtonContainer, props.style]} onPress={props.onClick}>
      {
        props.loading &&
        <Loading style={{ position: "absolute", left: 0, right: 0 }} />
      }
      <Image style={{ marginRight: 10 }} source={AddIcon} />
      <Text style={{
        fontFamily: fontStyles.nexa_bold,
        fontSize: 13,
        color: colorPalette.mainButtonBg,
        textAlign: 'center',
      }}>{props.title}</Text>
    </TouchableOpacity>
  )
}