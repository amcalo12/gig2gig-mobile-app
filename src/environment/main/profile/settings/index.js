import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  AsyncStorage,
  Alert,
  Linking,
  Platform
} from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import pushNotificationApi from "api/pushNotificationApi";
import styles from "./index.styles";
import { StackActions, NavigationActions } from "react-navigation";
import LinearGradient from "react-native-linear-gradient";
import { gradientColors } from "utils/utils";
import RightArrow from "resources/images/right-arrow.png";
import Label from "utils/components/labelMain";
import { settingsOptions } from "resources/l10n";
import settingsViewModel from "./settingsViewModel";
import DeviceInfo from "react-native-device-info";
import { openSubscriptionDialog, checkSubscriptionAndRedirect } from "utils/cons";

class SettingsView extends Component {
  state = {
    listSettings: {},
    userData: {
      details: {
        first_name: "",
        last_name: ""
      },
      image: ""
    }
  };
  componentDidMount = async () => {
    await settingsViewModel.getUserData(this);
    await settingsViewModel.getSettings(this);
  };

  showModal = () => {
    Alert.alert(
      "Sign Out",
      "Are you sure you want to sign out?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => this.logOut()
        }
      ],
      { cancelable: false }
    );
  };

  logOut = async () => {
    try {
      let uniqueId = await DeviceInfo.getUniqueId();
      await pushNotificationApi.setPushToken({
        pushkey: "",
        device_id: uniqueId,
        device_type: Platform.OS
      });
    } catch (error) {
      console.log(error);
    }
    AsyncStorage.clear();
    return this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Initial" })]
      })
    );
  };

  render() {
    const { userData } = this.state;
    const {
      navigation: { navigate, dispatch }
    } = this.props;
    const {
      userData: {
        details: { first_name, last_name },
        image: { url }
      },
      listSettings
    } = this.state;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          barStyle="dark-content"
          text="Settings"
        />
        <View style={styles.headerContainer}>
          <View style={styles.imageContainer}>
            <Image style={styles.image} source={{ uri: url }} />
          </View>
          <Text
            numberOfLines={2}
            style={styles.headerText}
          >{`${first_name} ${last_name}`}</Text>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              // AsyncStorage.clear();
              // return this.props.navigation.navigate("Login");
              this.showModal();
            }}
          >
            <LinearGradient colors={gradientColors} style={styles.headerButton}>
              <Text style={styles.headerButtonText}>Sign out</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
        <Content>
          <View style={styles.container}>
            {settingsOptions.map((option, idx) => {
              if (option.name === "Contact Us") {
                return (
                  <Label
                    touch={true}
                    key={idx}
                    text={option.name}
                    icon={RightArrow}
                    onPress={() =>
                      Linking.openURL("mailto:support@gig2gig.com")
                    }
                  />
                );
              } else if (option.name != "Subscription") {
                console.log('option.name 11:>> ', option.name);
                return (
                  <Label
                    touch={true}
                    key={idx}
                    text={option.name}
                    icon={RightArrow}
                    onPress={() => {
                      if (option.name == 'Marketplace') {
                        if (this.state.userData.isPaidUser) {
                          navigate(option.navigate, { data: listSettings })
                        } else {
                          checkSubscriptionAndRedirect(this.state.userData, 'SubscriptionPlan', this.props)
                          // openSubscriptionDialog(() => {
                          //   this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true })
                          // })
                        }
                      } else {
                        navigate(option.navigate, { data: listSettings })
                      }
                    }}
                  />
                );
              } else {
                console.log('option.name :>> ', option.name);
                return (
                  <Label
                    touch={true}
                    key={idx}
                    text={option.name}
                    icon={RightArrow}
                    onPress={() => {
                      navigate(option.navigate, {
                        data: listSettings,
                        userData
                      })
                    }}
                  />
                );
              }
            })}
          </View>
        </Content>
      </Container>
    );
  }
}
export default SettingsView;
