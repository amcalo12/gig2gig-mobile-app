import React, { Component } from 'react';
import { TouchableOpacity, Image, View, Text } from 'react-native';
import { Card } from "native-base";
import styles from './index.styles';
import Mp4Icon from "resources/images/myMedia/mp4-icon.png";
import Mp3Icon from "resources/images/myMedia/mp3-icon.png";
import PhotoIcon from "resources/images/myMedia/photos-icon.png";
import DocumentIcon from "resources/images/myMedia/documents-icon.png";
import SheetIcon from "resources/images/myMedia/sheet-icon.png";
import Points from "resources/images/myMedia/points.png";
import UrlIcon from "resources/images/myMedia/link.png";
import LinearGradient from 'react-native-linear-gradient';
import { gradientColors } from "utils/utils";
import Options from "utils/components/popUpOptionsMedia";

class CardContainer extends Component {
	render() {
		const { type, onPress } = this.props;
		if (type === "audition") {
			return (
				<TouchableOpacity activeOpacity={0.6} style={styles.action} onPress={() => onPress()}>
					{this.props.children}
				</TouchableOpacity>
			)
		} else {
			return (
				<View style={styles.action}>
					{this.props.children}
				</View>
			)
		}
	}
}
class MediaCards extends Component {
	state = {
		stylesData: null
	}
	getIcon() {
		const { type } = this.props;
		console.log('type 11111 :', this.props);
		let icon;
		if (type === "video") {
			icon = Mp4Icon;
		} else if (type === "audio") {
			icon = Mp3Icon;
		} else if (type === "photo" || type === "image") {
			icon = PhotoIcon;
		} else if (type === "document" || type === "doc") {
			icon = DocumentIcon;
		} else if (type === "sheet" && this.props.isAudition) {
			icon = UrlIcon;
		} else if (type === "sheet") {
			icon = SheetIcon;
		}
		return icon;
	}
	getIconText() {
		// const { fileText } = this.props;

		// let type = fileText.split('.');
		// type = type[type.length - 1];
		// if (this.props.type === "sheet" && this.props.isAudition) {
		// 	return "URL"
		// } else {
		// 	return type.toUpperCase();
		// }

		const { type, fileText } = this.props;
		console.log('type 11111 :', type);
		let typeText;
		if (type === "video") {
			typeText = "MP4";
		} else if (type === "audio") {
			typeText = "MP3";
		} else if (type === "photo" || type === "image") {
			typeText = "PhotoIcon";
		} else if (type === "document" || type === "doc") {
			typeText = "PDF";
		} else if (type === "sheet" && this.props.isAudition) {
			typeText = "URL";
		} else if (type === "sheet") {
			typeText = "PDF";
		}
		return typeText;

	}

	getLeftSide() {
		const { type, mediaImage } = this.props;
		let leftSide;
		if (type === "audition" || type === "photo" || type === "image" || type == "video") {
			leftSide = <View style={styles.mediaIcon}>
				{console.log(" MEDIA URL ", mediaImage)}
				<Image style={styles.mediaImage} source={{ uri: mediaImage }} />
			</View>
		} else {
			leftSide = <LinearGradient colors={gradientColors} style={styles.mediaIcon}>
				<View style={styles.iconLogo}>
					<Image style={[styles.icon, { tintColor: "#ffffff" }]} source={this.getIcon()} />
					<Text style={styles.iconLogoText}>{this.getIconText()}</Text>
				</View>
			</LinearGradient>
		}

		return leftSide
	}

	show = (data) => {
		this.setState({ stylesData: data })
		this.refs.modalOptions.setModalVisible();
	}

	render() {
		const { type, fileText, onPress, data, deleteFile, renameFile, shareFile, openFile, showOptions } = this.props;
		return (
			<Card style={styles.container}>
				<CardContainer type={type} onPress={onPress}>
					{this.getLeftSide()}
					<View style={styles.file}>
						<Text numberOfLines={2} style={styles.fileTitle}>{fileText}</Text>
					</View>
				</CardContainer>
				{
					(type !== "audition" && !showOptions) &&
					<TouchableOpacity ref={(button) => this.modal = button} activeOpacity={0.6} style={styles.points}
						onPress={() => { this.modal.measure((a, b, width, height, px, py) => this.show({ a, b, width, height, px, py })) }}
					>
						<Image source={Points} />
					</TouchableOpacity>
				}
				{console.log('showOptions card:>> ', renameFile)}
				<Options
					ref="modalOptions"
					stylesData={this.state.stylesData}
					data={data}
					deleteFile={deleteFile}
					shareFile={shareFile}
					openFile={openFile}
					renameFile={renameFile != null ? renameFile : undefined}
				/>
			</Card>
		);
	}
}

export default MediaCards;