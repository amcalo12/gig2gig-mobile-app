import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    alignItems:"center",
    width: resize(290),
    alignSelf: 'center',
    marginTop : resize(20, "height")
  },
  input:{
    width:"100%"
  },
  label:{
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    alignSelf:"flex-start",
    marginVertical: resize(10, "height"),
    paddingLeft: resize(20),
  },
  inputGroup:{
    width:"100%",
    flexDirection:"row",
    justifyContent:"space-between"
  },
  button: {
    marginTop:resize(50, "height")
  }
});