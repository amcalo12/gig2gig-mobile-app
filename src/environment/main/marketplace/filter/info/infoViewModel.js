import { Share } from 'react-native'
const share = async (data) => {
    try {
        const result = await Share.share({
          message:
            data,
        });
  
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
          } else {
          }
        } else if (result.action === Share.dismissedAction) {
        }
      } catch (error) {
        alert(error.message);
      }
}

export default {
    share
}