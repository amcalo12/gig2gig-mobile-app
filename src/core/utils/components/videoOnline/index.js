import React from 'react';
import { View, Image, ImageBackground, TouchableOpacity, Text } from 'react-native';
import styles from './styles';

//Images
import Play from '../../../../resources/images/audition/playBtn.png';
import { Assets } from 'resources/assets';
import { colorPalette } from 'resources/colors';
import { fontStyles } from 'resources/fonts';


const VideoOnline = ({ videoName, hasButtom, isShowRename, onRename, onPress, thumbnail, isShowDelete, onDelete }) => (
	<View style={styles.container}>
		<ImageBackground resizeMod='cover' source={{ uri: thumbnail }} style={styles.video}>
			<TouchableOpacity onPress={onPress} style={styles.playVideoIndicator}>
				<Image resizeMode='contain' source={Play} />
			</TouchableOpacity>
		</ImageBackground>
		<View style={{ flexDirection: "row", padding: 10, backgroundColor: colorPalette.mainButtonBg }}>
			<Text style={{ flex: 1, color: "#ffffff", fontFamily: fontStyles.nexa_bold }}>{videoName}</Text>
			{isShowRename ? <TouchableOpacity onPress={onRename}>
				<Image source={Assets.rename} style={{ width: 20, height: 20, tintColor: "#ffffff" }} resizeMode={"contain"} />
			</TouchableOpacity>
				: null}
			{isShowDelete ? <TouchableOpacity onPress={onDelete}>
				<Image source={Assets.delete} style={{ width: 20, height: 20, marginLeft: 10, tintColor: "#ffffff" }} resizeMode={"contain"} />
			</TouchableOpacity>
				: null}
		</View>
		{hasButtom
			? <View style={styles.textContainer}>
				<Text style={styles.textName}>{videoName}</Text>
			</View>
			: null
		}
	</View>
)

VideoOnline.defaultProps = {
	hasButtom: false
}

export default VideoOnline;
