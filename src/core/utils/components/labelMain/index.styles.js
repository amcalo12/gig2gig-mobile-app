import {StyleSheet, Platform, Dimensions} from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils"
const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
  container:{
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
    flexDirection: 'row',
    justifyContent:"space-between",
    paddingVertical:isIos ? resize(15, "height") : resize(10, "height"),
    alignItems:"center"
  },
  text:{
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_light,
    fontSize: 20,
    letterSpacing: 1,
  },
  iconContainer:{
    width:resize(40),
    height: resize(30, "height"),
    marginRight: resize(15),
    marginTop: resize(2, "height"),
    alignItems: 'center',
    justifyContent:"center"
  },
  icon:{
    resizeMode:"contain"
  }
});