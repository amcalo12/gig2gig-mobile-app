import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { Container, Content, Card } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Input from "utils/components/mainInput";
import MainButton from "utils/components/mainButton";
import { setFeatureListing } from "api/userApi";
import { showMsj } from "utils/utils";
import { validateFields } from "utils/validators";

class FeatureListing extends Component {

	state = {
		loadingBtn: false,
		formData: {
			businessName: {
				value: '',
				type: '',
				required: true,
				name: 'First Name'
			},
			email: {
				value: '',
				type: 'email',
				required: true,
				name: 'Email'
			},
		}
	}

	_onPressSubmit = async () => {
		const { formData } = this.state
		const { dispatch } = this.props;
		try {
			await validateFields(formData)
			if (this.state.loadingBtn) return '';
			this.setState({ loadingBtn: true })
			let body = {
				business_name: formData.businessName.value,
				email: formData.email.value
			}
			await setFeatureListing(body);
			this.setState({ loadingBtn: false })
			showMsj('successfully created');
			this.props.navigation.goBack();
		} catch (error) {
			console.log('error', error)
			this.setState({ loadingBtn: false })
			if (error.data) {
				showMsj(error.data.message, true)
			}
			else {
				showMsj(error, true)
			}
		}
	}

	_onChange = (key, value) => {
		const { formData } = this.state
		formData[key].value = value
		this.setState({
			formData: {
				...formData
			}
		})
	}

	render() {
		const { navigation: { navigate, dispatch } } = this.props;

		const { formData: { businessName, email, }, loadingBtn } = this.state

		return (
			<Container>
				<Header
					leftIcon="backDark"
					leftFn={() => dispatch(StackActions.popToTop())}
					barStyle="dark-content"
					text="Feature Listing"
				/>
				<Content
					bounces={false}
					contentContainerStyle={styles.content}
				>
					<Input
						placeholder="First Name"
						data={businessName.value}
						onEdit={text => this._onChange('businessName', text)}
						elevation={3}
						customWidth={styles.input.width}
						margin={10} />
					<Input
						placeholder="Email"
						data={email.value}
						onEdit={text => this._onChange('email', text)}
						elevation={3}
						customWidth={styles.input.width}
						margin={10} />
					<View
						style={styles.separator}
					/>
					<MainButton
						loading={loadingBtn}
						text={'Submit'}
						onPress={() => this._onPressSubmit()}
					/>
				</Content>
			</Container>
		);
	}
}
export default FeatureListing;
