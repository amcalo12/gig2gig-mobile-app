import { PermissionsAndroid, Platform } from "react-native";

export const requestPermission = async (Permission, PermissionModalText) => {
  try {
    if (Platform.OS === "ios") {
      return true;
    }
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS[Permission],
      {
        title: PermissionModalText.title,
        message: PermissionModalText.message,
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};
