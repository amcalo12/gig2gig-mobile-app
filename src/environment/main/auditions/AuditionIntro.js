//import liraries
import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  Platform
} from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
const { width, height } = Dimensions.get("window");
import { NavigationActions, StackActions } from "react-navigation";

const isIphoneX =
  Platform.OS === "ios" &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (height === 812 || width === 812);

const slides = [
  {
    key: "appointment",
    title: "Acquire Your Appointment",
    text:
      "Saving the audition will allow you to attend as a walk-in. Requesting the audition will automatically request an appointment from your representation.",
    image: "audition_1",
    backgroundColor: "#59b2ab"
  },
  {
    key: "appointment-time",
    title: "Submit Your Appointment Time",
    text:
      "Once you've recieved your appointment time, select your time to add the audition to upcoming.",
    image: "audition_2",
    backgroundColor: "#febe29"
  },
  {
    key: "check-in",
    title: "Check-In",
    text:
      "Use your unique QR code in your Upcoming audition card to check-in to the audition.",
    image: "audition_3",
    backgroundColor: "#22bcb5"
  }
];

// create a component
class AuditionIntro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false
    };
  }

  _renderItem = ({ item }) => {
    return (
      <View style={{ width: "100%", height: "100%" }}>
        <Image source={{ uri: item.image }} style={styles.imageStyle} />
        <View style={styles.introContainer}>
          <View style={styles.textContainer}>
            <Text style={styles.titleText}>{item.title}</Text>
            <Text style={styles.subTitleText}>{item.text}</Text>
          </View>
        </View>
      </View>
    );
  };
  _onDone = () => {
    this.navigateToApp();
  };

  navigateToApp() {
    this.props.navigation.replace("AuditionDetail", {
      id: this.props.navigation.state.params.id,
      image: this.props.navigation.state.params.image
    });
    // this.props.navigation.dispatch(
    //   StackActions.reset({
    //     index: 0,
    //     actions: [NavigationActions.navigate({ routeName: "App" })]
    //   })
    // );
  }

  _onSkip = () => {
    this.navigateToApp();
  };
  render() {
    return (
      <AppIntroSlider
        renderItem={this._renderItem}
        slides={slides}
        dotStyle={styles.dotStyle}
        activeDotStyle={styles.activeDotStyle}
        onDone={this._onDone}
        showSkipButton={true}
        onSkip={this._onSkip}
        showNextButton={true}
        buttonTextStyle={{
          color: colorPalette.purple,
          fontSize: 14,
          fontFamily: fontStyles.nexa_bold
        }}
      />
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  dotStyle: {
    backgroundColor: colorPalette.purple,
    width: 6,
    height: 6,
    borderRadius: 3,
    marginHorizontal: 6
  },
  activeDotStyle: {
    backgroundColor: colorPalette.purple,
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 6
  },
  imageStyle: { width: "100%", flex: 1 },
  titleText: {
    textAlign: "center",
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    letterSpacing: 1.2,
    marginBottom: 15,
    color: colorPalette.purple
  },
  subTitleText: {
    textAlign: "center",
    fontFamily: fontStyles.nexa_light,
    fontSize: 14,
    letterSpacing: 0.55,
    color: colorPalette.purple
  },
  introContainer: {
    width: "100%",
    paddingBottom: 60 + (isIphoneX ? 34 : 0),
    justifyContent: "center",
    alignItems: "center"
  },
  textContainer: {
    width: "75%",
    justifyContent: "center",
    alignContent: "center",
    paddingTop: 15,
    paddingBottom: 15
  }
});

//make this component available to the app
export default AuditionIntro;
