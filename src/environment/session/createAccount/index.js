import React, { Component } from "react";
import { View, Text, ImageBackground, StatusBar, PermissionsAndroid, TouchableOpacity, Image, Platform, Keyboard } from "react-native";
import { Container, Content } from "native-base";
import Background from "resources/images/login/background.png";
import addIcon from "resources/images/addUserImage.png"
import styles, { width1, width2 } from "./index.styles";
import Input from "utils/components/mainInput";
import Button from "utils/components/mainButton";
import Header from "utils/components/mainHeader";
import calendarIcon from "resources/images/calendar-icon.png";
import downArrowIcon from "resources/images/down-arrow-icon.png";
import locationIcon from "resources/images/location-clear-icon.png";
import ModalUnionMember from "utils/components/ModalUnionMember";
import Select from "utils/components/selectSpinner";
import { AndroidBackHandler } from 'react-navigation-backhandler';
import states from "utils/states.json";
import DateTimePicker from 'react-native-modal-datetime-picker';
import Loading from "utils/components/loading";
import { maximumDate, minimumDate, showAlert, showAlertWithClick } from "utils/cons";
import moment from "moment";
import {
	handleBackPress,
	handleNextStep,
	select,
	countrySelect,
	show,
	_showDateTimePicker,
	_hideDateTimePicker,
	_handleDatePicked,
	handleSelectedState,
	handleSelectedCountry,
	handleLocation,
	handleSelectedUnionMembers,
	handleGetMoreImageFormCameraRoll,
	handleUploadImage
} from "./CreateAccountViewModel";
import SelectRadius from "utils/components/select_radius";
import RenameModal from "utils/components/renameModal";
// import Contacts from 'react-native-contacts';
import * as RNIap from 'react-native-iap';
import GeneralModal from "utils/components/GeneralModal";
import firebase from "react-native-firebase";

const countryList = require('../../../resources/country_list.json');
// const genders = [
// 	{ label: 'Male', value: 'male' },
// 	{ label: 'Female', value: 'female' },
// 	{ label: 'Other', value: 'other' },
// ];
const genders = [
	{ label: 'Agender', value: 'agender' },
	{ label: 'Female', value: 'female' },
	{ label: 'Gender diverse', value: 'gender diverse' },
	{ label: 'Gender expansive', value: 'gender expansive' },
	{ label: 'Gender fluid', value: 'gender fluid' },
	{ label: 'Genderqueer', value: 'genderqueer' },
	{ label: 'Intersex', value: 'intersex' },
	{ label: 'Male', value: 'male' },
	{ label: 'Non-binary', value: 'non-binary' },
	{ label: 'Transfemale/transfeminine', value: 'transfemale/transfeminine' },
	{ label: 'Transmale/transmasculine', value: 'transmale/transmasculine' },
	{ label: 'Two-spirit', value: 'two-spirit' },
	{ label: 'Self describe', value: 'self describe' },
	{ label: 'Prefer not to answer', value: 'Prefer not to answer' },
];
let purchaseErrorSubscription;
class CreateAccount extends Component {

	constructor(props) {
		super(props)
		console.log('countryList :>> ', countryList[1]);
	}

	state = {
		step: 1,
		loading: false,
		locationLoad: false,
		firstName: '',
		lastName: '',
		name: '',
		email: '',
		password: '',
		re_password: '',
		address: '',
		city: '',
		state: '',
		zip: '',
		birthdate: '',
		birth: '',
		image: '',
		stageName: '',
		pwTitle: '',
		personalWebsite: '',
		location: '',
		unionMemberShip: '',
		unionMemberShipName: '',
		isDateTimePickerVisible: false,
		gender: '',
		thumbnail: "",
		resource_name: "",
		isVisible: false,
		country: '',
		messageToShow: "",
		genderDesc: ''
	}

	// componentDidMount() {
	// Contacts.getAll((err, contacts) => {
	// 	if (err === 'denied') {
	// 		// error
	// 		alert(JSON.stringify(contacts))
	// 	} else {
	// 		alert(JSON.stringify(contacts))
	// 		console.log(" CONTACT ARRAY ", contacts)
	// 		// contacts returned in Array
	// 	}
	// })
	// PermissionsAndroid.request(
	// 	PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
	// 	{
	// 		'title': 'Contacts',
	// 		'message': 'This app would like to view your contacts.',
	// 		'buttonPositive': 'Please accept bare mortal'
	// 	}
	// ).then(() => {
	// 	Contacts.getAll((err, contacts) => {
	// 		if (err === 'denied') {
	// 			// error
	// 			alert(JSON.stringify(contacts))
	// 		} else {
	// 			alert(JSON.stringify(contacts))
	// 			console.log(" CONTACT ARRAY ", contacts)
	// 			// contacts returned in Array
	// 		}
	// 	})
	// })
	// }

	async componentDidMount() {

		const result = await RNIap.initConnection();
		// console.log('sku :>> ', this.props.navigation.state.params.sku);
		// console.log('planName :>> ', this.props.navigation.state.params.planName);
		// this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
		// console.log('userData :>> purchase', this.userData);
		purchaseErrorSubscription = RNIap.purchaseErrorListener(
			(error) => {
				console.log('purchaseErrorListener', error);
				// this.setState({ loading: false, messageToShow: '' }, () => {
				// 	showAlert(error.message);
				// })
				let logParams = {
					// email: this.userData.email,
					time: Date.now(),
					error: error.message
				}
				firebase.crashlytics().recordError(888, JSON.stringify(logParams));
				// showAlert(error.message);
				showAlertWithClick(error.message, () => {
					this.setState({ loading: false })
				})
				// firebase.analytics().logEvent("PURCHASE_ERROR", { error: error.message })
			},
		);
	}

	onChangeGender = (gender) => {
		this.setState({ gender });
	}

	onChangeZip = (value) => {
		if (value.length <= 5) {
			let zip = String(value).replace(/[^0-9]/g, '');
			this.setState({ zip });
		}
	}

	onCloseClick = () => {
		this.setState({ isVisible: false })
	}

	onSubmitClick = async () => {
		this.setState({ isVisible: false })
	}

	onChangeText = (resource_name) => {
		this.setState({ resource_name })
	}

	renderRenameModal = () => {
		return (
			<RenameModal
				isVisible={this.state.isVisible}
				onCloseClick={this.onCloseClick}
				onSubmitClick={this.onSubmitClick}
				value={this.state.resource_name}
				onChangeText={this.onChangeText}
			/>
		)
	}

	renderLoadingModal = () => {
		return (
			<GeneralModal
				visible={this.state.loading}
				message={this.state.messageToShow}
			/>
		)
	}

	render() {
		const {
			step,
			loading,
			locationLoad,
			firstName,
			lastName,
			email,
			password,
			re_password,
			address,
			city,
			state,
			zip,
			birthdate,
			image,
			stageName,
			pwTitle,
			location,
			unionMemberShip,
			unionMemberShipName,
			personalWebsite,
			gender,
			country,
			genderDesc
		} = this.state;

		return (
			<AndroidBackHandler onBackPress={() => handleBackPress(this)}>
				<Container style={styles.container}>
					{this.renderLoadingModal()}
					{this.renderRenameModal()}
					<View style={styles.imageBackground}>
						<Image source={Background} style={styles.backgroundImage} />
					</View>
					<Header leftFn={() => handleBackPress(this)} />
					<Content>

						<View style={styles.background}>
							{
								step === 3
									?
									<TouchableOpacity style={styles.userImage} onPress={() => handleGetMoreImageFormCameraRoll(this)}>
										<Image style={image ? styles.userPicture : styles.userIcon} source={image ? { uri: image.uri } : addIcon} />
									</TouchableOpacity>
									: null
							}
							{step === 4 ? <Text style={styles.headText}>Create Your Account</Text> : null}
							{
								step === 1 &&
								<View style={styles.formContainer}>
									<Input placeholder="First Name" onEdit={firstName => this.setState({ firstName })} data={firstName} autoCapital={true} />
									<Input placeholder="Last Name" onEdit={lastName => this.setState({ lastName })} data={lastName} autoCapital={true} />
									<Input placeholder="Email" onEdit={email => this.setState({ email })} data={email} keyboardType="email-address" />
									<Input placeholder="Password" onEdit={password => this.setState({ password })} data={password} isSecure={true} />
									<Input placeholder="Re-Enter Password" onEdit={re_password => this.setState({ re_password })} data={re_password} isSecure={true} />
								</View>
							}
							{
								step === 2 &&
								<View style={styles.formContainer}>
									<Input placeholder="Country" data={country.name ? country.name : ""} icon={downArrowIcon} isTouchable onPress={(data) => countrySelect(this)} />
									{/* <Input placeholder="Address" data={address} onEdit={address => this.setState({ address })} />
									<Input placeholder="City" data={city} onEdit={city => this.setState({ city })} />
									<View style={styles.displayRow}>
										<Input numberOfLines={1} customWidth={width1} placeholder="State" data={state.name ? state.name : ""} icon={downArrowIcon} isTouchable onPress={() => select(this)} />
										<Input customWidth={width2} placeholder="Zip" onEdit={this.onChangeZip} data={zip} keyboardType="number-pad" />
									</View> */}
									{
										console.log('birthdatebirthdatebirthdate', birthdate)
									}
									<Input placeholder="Birth Date" data={birthdate != '' ? moment(birthdate).format('MM/DD/YYYY') : 'Birth Date'} icon={calendarIcon} isTouchable onPress={() => {
										Keyboard.dismiss();
										_showDateTimePicker(this)
									}
									} />
								</View>
							}
							{
								step === 3 &&
								<View style={styles.formContainer}>
									<Input placeholder="Stage Name" autoCapital={true} data={stageName} onEdit={stageName => this.setState({ stageName })} />
									<Input placeholder="Professional/Working Title" autoCapital={true} data={pwTitle} onEdit={pwTitle => this.setState({ pwTitle })} />
									<SelectRadius
										placeholder={'Gender Identity'}
										value={gender}
										onChange={this.onChangeGender}
										data={genders}
									/>
									{gender.toLowerCase() == String("self describe").toLowerCase() ?
										<Input placeholder="Self describe"
											autoCapital={true}
											data={genderDesc}
											onEdit={genderDesc => this.setState({ genderDesc })} />
										: null}
									{/* <Input placeholder="Personal Website" data={personalWebsite} onEdit={personalWebsite => this.setState({ personalWebsite })} /> */}
									<View style={styles.loadingContainer}>
										{
											locationLoad &&
											<Loading color="#ffffff" style={styles.loading} />
										}
										{/* <Input placeholder="Location" data={location} icon={locationIcon} isTouchable onPress={()=>handleLocation(this)}/> */}
									</View>
									{/* <Input data={unionMemberShipName} placeholder="Union Membership" icon={downArrowIcon} isTouchable onPress={() => show(this)} /> */}
								</View>
							}
							{
								step === 4 &&
								<View style={styles.formContainer}>
									<Input
										// placeholder="Personal Website"
										placeholder="Website (optional)"
										data={personalWebsite} onEdit={personalWebsite => this.setState({ personalWebsite })} />
									<Input data={unionMemberShipName} placeholder="Union Membership" icon={downArrowIcon} isTouchable onPress={() => show(this)} />
								</View>
							}
							<Button loading={loading} text="Next" onPress={() => handleNextStep(this)} />
						</View>
						<ModalUnionMember onSelected={(data) => handleSelectedUnionMembers(this, data)} ref='modal' />
						<Select
							selectTitle="Select a State"
							data={states}
							onPress={(selected) => handleSelectedState(selected, this)}
							ref="select" />
						<Select
							selectTitle="Select a Country"
							data={countryList}
							onPress={(selected) => handleSelectedCountry(selected, this)}
							ref="country" />
						<DateTimePicker
							maximumDate={maximumDate()}
							minimumDate={minimumDate()}
							date={this.state.birthdate != "" ? new Date(this.state.birth) : maximumDate()}
							isVisible={this.state.isDateTimePickerVisible}
							onConfirm={(date) => _handleDatePicked(date, this)}
							onCancel={() => _hideDateTimePicker(this)}
						/>
					</Content>
				</Container>
			</AndroidBackHandler>
		);
	}
}
export default CreateAccount;
