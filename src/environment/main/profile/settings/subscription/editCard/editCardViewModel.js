import {monthsNumber} from "resources/l10n";
import moment from 'moment';
import { showMsj } from "utils/utils";
import subscriptionApi from "api/subscriptionApi";
const getMonths = () => {
  const month = []
  monthsNumber.map((res,i) => month[i] = {label:res,value:res})
  return month
}
const convertYears = () => {
  const year = []
  const years = getYears()
  years.map((res,i) => year[i] = {label: res.toString(),value:res.toString()})

  return year
}
const getYears = () => {
  const year = moment().year()
  const allYears = []
  for (let i = 0; i < 20; i++) {
    allYears[i] = year + i
  }
  return allYears
}
const createBodyData = (context) => {
  const { cardNumber, cvvCode, month, year } = context.state
  const body= {
      "card[number]": cardNumber,
      "card[cvc]": cvvCode,
      "card[exp_month]": parseInt(month),
      "card[exp_year]": parseInt(year)
  }
  var formBody = [];
  for (var property in body) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(body[property]);
      formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");
  return(formBody)
}

const getTokenCard = async (context) => {
  const body = createBodyData(context)
  try {
    const res =  await fetch(`https://api.stripe.com/v1/tokens`, {
      method: 'post',
      body,
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          "Authorization": `Bearer pk_test_xUbN9WpUJ0FfGMfueDVfQqBc00s5d5WikM`,
      },
    })
    const bodyData = await res.json()
    if(bodyData.error){
      return showMsj(bodyData.error.message, true)
    }else{
      return bodyData.id
    }
  } catch (error) {
    return showMsj(error.message, true)
  }
}
const getPaymentInfo = async (context) => {
  const {data} = context.props.navigation.state.params
  context.setState({card:data})
}
const edit = async (context) => {
  context.setState({loading:true})
  const { cardNumber, cvvCode, month, year } = context.state
  if(cardNumber != '' && cvvCode != '' && month != '' && year != ''){
    try {
      const token = await getTokenCard(context)
      await subscriptionApi.updatePaymentMethod({token_card:token})
      context.props.navigation.state.params.update({ last4: cardNumber.slice(12,16), cvvCode, exp_month: month, exp_year: year })
      context.setState({loading:false})
      context.props.navigation.goBack()
      return showMsj('Successful')
    } catch (error) {
      console.log(error)
      context.setState({loading:false})
      return showMsj('Error', true)
    }
  }else{
    context.setState({loading:false})
    return showMsj('All fields are required', true)
  }
}
export default {
  getPaymentInfo,
  getMonths,
  convertYears,
  edit
}