import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    alignItems:"center",
    width: resize(290),
    alignSelf: 'center',
  },
  userImage:{
    backgroundColor:"#d9d9d9",
    justifyContent:"center",
    alignItems:"center",
    width: resize(142),
    height: resize(142),
    borderRadius:130,
    alignSelf:"center",
    overflow:"hidden",
    marginTop: resize(10, "height"),
    marginBottom: resize(20, "height"),
  },
  userPicture:{
    width:"100%",
    height:"100%",
    resizeMode:"cover"
  },
  input:{
    width:"100%"
  },
  inputGroup:{
    width:"100%",
    flexDirection:"row",
    justifyContent:"space-between"
  },
  loadingContainer:{
    position:"relative"
  },
  loading:{
    position: "absolute",
    bottom:resize(6, "height"),
    right: resize(-36),
    zIndex: 100
  },
  button: {
    marginVertical:resize(20, "height")
  }
});