import { AsyncStorage, Keyboard } from "react-native";
import axios from "axios";
import { BASE_URL } from "utils/cons";

/**
 *
 * @param url
 * @returns {string}
 */
export const buildUrl = url => {
	return [BASE_URL, "api", url].join("/");
};

export const buildHeaders = async () => {
	const headers = {
		"Content-Type": "application/json",
		// "Accept": "application/json"
	};

	const accessToken = await AsyncStorage.getItem("Authorization");
	if (accessToken) {
		headers["Authorization"] = accessToken;
	}
	return headers;
};

const buildRequest = async (method, url, body = null, params = false) => {
	let request = {
		method: method,
		url: buildUrl(url),
		headers: await buildHeaders()
	};
	if (method !== "GET" && body !== null) {
		request["data"] = body;
	}
	if (params) {
		request["params"] = body;
	}
	return new Promise.resolve(request);
};

export const apiHandler = async (method, url, body = null, key = true, params = false) => {

	if (key) {
		Keyboard.dismiss();
	}
	try {
		console.log("API METHOD ==> " + method)
		console.log("API BODY ==> " + JSON.stringify(body))
		console.log("API PARAMS ==> " + JSON.stringify(params))
		console.log("ALL URL ==> " + url)
		const request = await buildRequest(method, url, body, params);
		console.log("request================")
		console.log(request)
		const result = await axios(request);
		console.log("response================")
		console.log(JSON.stringify(result))
		return Promise.resolve(result.data);
	} catch (err) {
		console.log('API ERROR :', err);
		console.log('API ERROR :', err.response);
		// console.log(err)
		return Promise.reject(err.response);
	}
};
