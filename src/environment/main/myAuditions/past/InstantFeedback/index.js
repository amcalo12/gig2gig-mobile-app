import React, { Component } from "react";
import {
  View,
  Image,
  AsyncStorage,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { Container, Content, Text } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Loading from "utils/components/loading";
import ImageHeader from "utils/components/imageHeader";
import Button from "utils/components/mainButton";
// import StarsFace from "resources/images/myAuditions/stars-face.png";
import { StackActions } from "react-navigation";
import myAuditionsApi from "api/myAuditionsApi";
import StarsFace from "resources/images/myAuditions/star-emoji.png";
import SmileFace from "resources/images/myAuditions/smile-emoji.png";
import GoodFace from "resources/images/myAuditions/good-emoji.png";
import DoubtFace from "resources/images/myAuditions/doubt-emoji.png";
import BadFace from "resources/images/myAuditions/bad-emoji.png";
import { getUserSettings } from "api/userApi";

class InstantFeedbackView extends Component {
  state = {
    feedback: null,
    loading: false,
    subscription: null,
    userData: null,
    recomendations: [],
    viewFeedback: {},
    viewRecommendations: {},
  };

  async componentDidMount() {
    this.setState({ loading: true });
    let data = this.props.navigation.state.params.data;
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    try {
      const response = await myAuditionsApi.getUserFeedback(
        data.appointment_id
      );
      const userSettings = await getUserSettings();
      const viewFeedback = userSettings.data.filter(
        (res) => res.setting == "FEEDBACK"
      )[0];
      const viewRecommendations = userSettings.data.filter(
        (res) => res.setting == "RECOMMENDATION"
      )[0];
      this.setState({
        feedback: response.data,
        subscription: userData.details.subscription,
        userData: userData,
        viewFeedback,
        viewRecommendations,
      });
    } catch (e) {
      // this.setState({ loading: false });
    }

    try {
      const recomendations = await myAuditionsApi.getUserFeedbackRecomendation(
        data.auditions_id,
        data.appointment_id
      );

      this.setState({
        recomendations: recomendations.data,
      });
      this.setState({ loading: false });
    } catch (e) {
      console.log("error");
      console.log(e);
      this.setState({ loading: false });
    }
  }

  render() {
    const {
      navigation: {
        navigate,
        dispatch,
        state: {
          params: { data },
        },
      },
    } = this.props;

    const {
      loading,
      feedback,
      subscription,
      recomendations,
      viewFeedback,
      viewRecommendations,
    } = this.state;

    const faces = [
      {
        id: 1,
        icon: StarsFace,
      },
      {
        id: 2,
        icon: SmileFace,
      },
      {
        id: 3,
        icon: GoodFace,
      },
      {
        id: 4,
        icon: DoubtFace,
      },
      {
        id: 5,
        icon: BadFace,
      },
    ];
    return (
      <Container>
        <ImageHeader image={data.media} size="small">
          <Header
            leftIcon="back"
            leftFn={() => dispatch(StackActions.popToTop())}
          />
        </ImageHeader>
        <Content contentContainerStyle={styles.content}>
          {subscription != "" && (
            <View style={styles.contentFeed}>
              <View style={styles.container}>
                <Text style={styles.title}>{data.title}</Text>
                {!loading && feedback ? (
                  <Text style={styles.subTitle}>Feedback</Text>
                ) : null}
                {!loading && feedback ? (
                  <Text style={styles.subTitle}>
                    {feedback.round ? "For Round " + feedback.round : ""}
                  </Text>
                ) : null}
                {loading && <Loading />}
                {!loading &&
                  feedback &&
                  feedback.hasOwnProperty("evaluation") ? (
                    feedback.evaluation != null ? (
                      feedback.round ? (
                        feedback.round !== 1 ? (
                          <View style={styles.emojiContainer}>
                            {console.log(
                              "feedback.evaluation :>> ",
                              feedback.evaluation
                            )}
                            {feedback.evaluation != null ? (
                              <Image
                                source={
                                  feedback.evaluation === 0
                                    ? BadFace
                                    : faces.find(
                                      (faces) =>
                                        faces.id === feedback.evaluation
                                    ).icon
                                }
                              />
                            ) : null}
                          </View>
                        ) : null
                      ) : null
                    ) : null
                  ) : (
                    !loading && (
                      <Text style={styles.notHave}>You no have feedback</Text>
                    )
                  )}
                {!loading &&
                  feedback &&
                  feedback.hasOwnProperty("evaluation") &&
                  viewFeedback.value == 1 && (
                    <React.Fragment>
                      <View style={styles.buttonsContainer}>
                        {feedback.round && feedback.round !== 1 && (
                          <View style={styles.buttonsFeedBack}>
                            <Text style={styles.label}>Call Back</Text>
                            <Button
                              text={
                                feedback.callback === 1
                                  ? "Yes"
                                  : feedback.callback === 0
                                    ? "No"
                                    : ""
                              }
                              customWidth={styles.button.width}
                              customHeight={styles.button.height}
                            />
                          </View>
                        )}
                        {feedback.round && feedback.round !== 1 && (
                          <View style={styles.buttonsFeedBack}>
                            <Text style={styles.label}>Work On</Text>
                            <Button
                              text={
                                feedback.work != null
                                  ? feedback.work.charAt(0).toUpperCase() +
                                  feedback.work.slice(1)
                                  : ""
                              }
                              customWidth={styles.button.width}
                              customHeight={styles.button.height}
                            />
                          </View>
                        )}
                        {feedback.round && feedback.round === 1 && (
                          <View
                            style={[
                              styles.buttonsFeedBack,
                              { justifyContent: "center", width: "100%" },
                            ]}
                          >
                            <Text style={styles.label}>Call Back</Text>
                            <Button
                              text={
                                feedback.simple_feedback !== null
                                  ? feedback.simple_feedback
                                  : ""
                              }
                              customWidth={styles.button.width}
                              customHeight={styles.button.height}
                            />
                          </View>
                        )}
                        <View style={styles.action}></View>
                      </View>
                    </React.Fragment>
                  )}
                {!loading && feedback != undefined && feedback.comment != undefined && feedback.comment != null && (
                  <React.Fragment>
                    <Text style={[styles.label, { marginBottom: 5 }]}>
                      {"Comment"}
                    </Text>

                    {feedback.comment !== null &&
                      feedback.comment !== "" ? (
                        <Text style={[styles.recomendationsEmpty, { marginHorizontal: 10 }]}>
                          {feedback.comment}
                        </Text>
                      ) : null}
                  </React.Fragment>
                )}
                {!loading && viewRecommendations.value == 1 && (
                  <React.Fragment>
                    <Text style={[styles.label, { marginBottom: 5 }]}>
                      Recommendation
                    </Text>

                    {feedback.recommendation !== null &&
                      feedback.recommendation !== "" ? (
                        <Text style={styles.recomendationsEmpty}>
                          {feedback.recommendation}
                        </Text>
                      ) : (
                        <Text style={styles.recomendationsEmpty}>
                          {/* You not have recomendations */}
                        You do not have recommendation
                        </Text>
                      )}
                  </React.Fragment>
                )}
                {!loading && recomendations.length > 0 && (
                  <React.Fragment>
                    <Text style={[styles.label, { marginBottom: 5 }]}>
                      Suggested Vendors
                    </Text>
                    {recomendations.map((item, idx) => (
                      <TouchableOpacity
                        style={{
                          width: "100%",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                        onPress={() => {
                          if (item.markeplace) {
                            navigate("Info", {
                              data: item.markeplace,
                            });
                          }
                        }}
                      >
                        <Text
                          style={[
                            styles.recomendationsEmpty,
                            {
                              textAlign: "center",
                              marginBottom: 0,
                              padding: 10,
                            },
                          ]}
                        >
                          {item.markeplace
                            ? item.markeplace.title
                            : item.marketplace
                              ? item.marketplace.title
                              : ""}
                        </Text>
                        {idx !== recomendations.length - 1 && (
                          <View
                            style={{
                              height: 1,
                              width: "80%",
                              backgroundColor: "rgb(159,159,159)",
                            }}
                          />
                        )}
                      </TouchableOpacity>
                    ))}
                  </React.Fragment>
                )}
              </View>
            </View>
          )}
          {!loading && (
            <Button
              customWidth={styles.btn.width}
              text="Finish"
              onPress={() => dispatch(StackActions.popToTop())}
            />
          )}
        </Content>
      </Container>
    );
  }
}
export default InstantFeedbackView;
