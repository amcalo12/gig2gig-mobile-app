import { StyleSheet, Platform } from "react-native";
import { resize } from "utils/utils";
import DeviceInfo from 'react-native-device-info';

export default StyleSheet.create({
  container: {
    width: resize(375),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  safe: {
    minHeight: 55,
  },
  footer: {
    backgroundColor: 'white',
    justifyContent: 'space-around',
    alignItems: "center",
    ...Platform.select({
      ios: {
        paddingBottom: DeviceInfo.hasNotch() ? resize(12, "height") : 0,
      }
    }),
    elevation: 0,
    borderTopColor: "transparent",
  },
  tab: {
    alignItems: 'center',
    height: '100%',
    width: '20%',
    paddingTop: resize(10, 'h')
  },
  mT: {
    // marginTop: resize(5, 'h')
  }
});