import { createStackNavigator } from "react-navigation";

import Social from ".";
import AddSocialLink from "./AddSocialLink";

const SocialStack = createStackNavigator(
  {
    Social: {
      screen: Social,
      navigationOptions: {
        header: null
      }
    },
    AddSocialLink: {
      screen: AddSocialLink,
      navigationOptions: {
        header: null
      }
    },
  },
  {
    initialRouteName: "Social",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default SocialStack;
