import { showMsj } from "utils/utils";
import { AsyncStorage } from "react-native";
import subscriptionApi from "api/subscriptionApi";
const updatePlan = async (context) => {
  var {navigation : { state : {params : {plan} } } } = context.props;
  context.setState({loading:true})
  try{
    response = await subscriptionApi.updatePlan({plan})
    console.log(response);
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    userData.details.subscription = `${plan}`
    await AsyncStorage.setItem("userData", JSON.stringify(userData))
    context.props.navigation.popToTop()
    context.setState({loading:false})
    return showMsj('Plan Updated')
  }catch(err){
    console.log(err)
    context.setState({loading:false})
    return showMsj('Error',true)
  }
}

export default {
  updatePlan
}