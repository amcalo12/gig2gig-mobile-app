import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Input from "utils/components/mainInput";
import Button from "utils/components/mainButton";
import DateSelect from "utils/components/dateSelect";
import SecondaryButton from "utils/components/secondaryButton";
import editCardViewModel from "./editCardViewModel";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
class EditCard extends Component {
  state = {
    card:{},
    cvvCode:'',
    cardNumber:'',
    year: '',
    month: '',
    loading: false
  }
  componentDidMount = () => {
    editCardViewModel.getPaymentInfo(this)
  };
  
  setCard = (cardNumber) => {
    this.setState({cardNumber})
  }
  render() {
    const { cvvCode, cardNumber, year, month, loading } = this.state
    const {navigation : {navigate , dispatch}} = this.props;
    return (
      <Container>
        <Header
        leftIcon="backDark"
        leftFn={()=>dispatch(StackActions.pop())}
        barStyle="dark-content"
        text="Add Credit Card"
        />
        <Content>
          <View style={styles.container}>
            <View style={styles.imagesContainer}>
              <View style={styles.row}>
                <Image source={require('resources/images/cards/paypal/Image_4.png')} style={styles.img}/>
                <Image source={require('resources/images/cards/visa/Image_5.png')} style={styles.img}/>
                <Image source={require('resources/images/cards/master/Image_6.png')} style={styles.img}/>
                <Image source={require('resources/images/cards/american/Image_7.png')} style={styles.img}/>
              </View>
            </View>
            <View>
            <Input placeholder="Card Number" customWidth={'80%'} onEdit={cardNumber => cardNumber.length <= 16 ? this.setState({ cardNumber }): null} data={cardNumber} keyboardType={'numeric'}/>
              <View style={styles.secondPartContainer}>
              <Input placeholder="CVV" short onEdit={cvvCode => cvvCode.length <= 3 ? this.setState({ cvvCode }) : null} data={cvvCode} keyboardType={'numeric'}/>
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <DateSelect
                  placeholder={'Month'}
                  icon={DownArrowIcon}
                  data={editCardViewModel.getMonths(this)}
                  onChange={(month) => this.setState({month})}
                  short
                  elevation={1.5}
                />
                <DateSelect
                  placeholder={'Year'}
                  icon={DownArrowIcon}
                  data={editCardViewModel.convertYears()}
                  onChange={(year, i) => {this.setState({year})}}
                  short
                  elevation={1.5}
                />
                </View>
              </View>
            </View>
            <View style={styles.buttonsContainer}>
              <Button onPress={() => editCardViewModel.edit(this)} text={'Edit'} loading={loading}/>
              {/* <SecondaryButton onPress={() => alert('1010')} text={'Delete'}/> */}
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
export default EditCard;
