import React, { Component } from "react";
import { View, TouchableOpacity, Image, ScrollView, Platform, Dimensions, AsyncStorage } from "react-native";
import { Container, Content, Text } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import DownloadIcon from "resources/images/download-clear.png";
import Role from "utils/components/roleAudition";
import Filter from "utils/components/auditionFilterButton";
import ImageHeader from "utils/components/imageHeader";
import Loading from "utils/components/loading";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import LocationIcon from "resources/images/audition/location.png";
import flagIcon from "resources/images/flag.png";
import { convertTimeToHour, showMsj } from "utils/utils";
import {
  loadAuditionData,
  handleLocation,
  shareAudition
} from "./AuditionsDetailViewModel";
import auditionsApi, { flagAudition } from "api/auditionsApi";
import moment from "moment";
import { geoLocationKey } from "utils/cons";
import RoleSelectionModal from "utils/components/roleSelection";
import Button from "utils/components/mainButton";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
import ButtonClear from "utils/components/secondaryButton";
import { AddMaterialButton, ApplyButton } from "utils/components/generalbutton";
import myMediaApi from "api/myMediaApi";
import { generalMsj } from "resources/l10n";
import myProfileApi from "api/myProfileApi";
import { StackActions } from "react-navigation";

class AuditionDetailView extends Component {
  constructor(props) {
    super(props);
    this.userData = {}
    this.rolesList = []

  }

  state = {
    auditionData: {},
    auditionCover: null,
    loading: true,
    locationResult: null,
    region: {
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
    },
    marker: {
      latitude: 37.78825,
      longitude: -122.4324
    },
    locationName: "",
    loadingFlag: false,
    walkAppont: [],
    isRoleSelection: false,
    isRequest: false,
    isLoading: false,
    isSubmit: false
  };

  componentDidMount = async () => {
    console.log("paso");
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    this.userData = userData
    // this.setState({ idUser: userData.id });
    loadAuditionData(this, false);
  };

  onPressFlag = async () => {
    const { auditionData } = this.state;

    try {
      this.setState({
        loadingFlag: true
      });
      await flagAudition({
        audition_id: auditionData.id
      });
      showMsj("Flag successful");
      this.setState({
        loadingFlag: false
      });
      this.props.navigation.goBack();
    } catch (error) {
      this.setState({
        loadingFlag: false
      });
      console.log("error", error);
    }
  };

  onCloseClick = () => {
    this.setState({ isRoleSelection: false })
  }

  onItemSelect = (data) => {
    let newAuditionData = this.state.auditionData;
    newAuditionData['roles'] = data
    this.setState({ auditionData: newAuditionData })
  }

  checkForRepresentation = async (audition, type, online, roles) => {
    //type 1 para guardarlo en upcoming
    //type 2 es para guardarlo en request
    try {
      const response = await myProfileApi.getRepresentation();
      console.log("representation=====");
      console.log(response);
      this.saveAudition(audition, type, online, roles);
    } catch (e) {
      this.askToAddRepresentation(audition, type, online, roles);
      console.log(e);
    }
  };
  askToAddRepresentation(audition, type, online) {
    Alert.alert(
      "",
      "Currently you don't have Representation",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "Go to Representation",
          onPress: () =>
            this.props.navigation.navigate("RepresentationEditView", {
              hasRepresentation: null,
              isFromAuditionDetail: true
            })
        }
      ],
      { cancelable: false }
    );
  }
  saveAudition = async (audition, type, online, roles) => {
    const {
      navigation: { navigate, dispatch }
    } = this.props;
    console.log(audition);
    this.setState({
      loadingSave: type === 1 ? true : false,
      loadingRequest: type === 2 ? true : false
    });
    // const params = {
    //   appointment: audition.appointmentId,
    //   rol: audition.rol,
    //   type: type,
    //   online
    // };
    console.log('audition :', audition);
    console.log('audition STATE:', this.state.auditionData);
    const params = {
      appointment: audition.appointment_id,
      rol: roles,
      type: type,
      online
    };
    console.log('params save Audition:', params);
    // return
    try {
      const response = await auditionsApi.requestAudition(params);
      this.setState({
        loadingSave: false,
        loadingRequest: false
      });
      showMsj(response.data, false);
      if (this.state.isSubmit) {
        return navigate("Online", { data: this.state.auditionData, idUser: this.userData.id, isUpload: true })
      }
      return dispatch(StackActions.popToTop());
    } catch (e) {
      console.log(e);
      this.setState({
        loadingSave: false,
        loadingRequest: false
      });

      if (e.status && e.status === 406) {
        if (e.data.data) {
          return showMsj(e.data.data, true);
        } else {
          return showMsj(e.data.error, true);
        }
      }
      showMsj(generalMsj.error.message, true);
    }
  };

  onClick = (roles) => {
    console.log('Roles :', roles);

    let selectedRoles = roles.filter(data => data.checked)
    console.log('Roles 111:', selectedRoles);

    let rolesString = ""
    selectedRoles.map(data => {
      rolesString = rolesString + "," + data.id
    })

    console.log('Roles 222:', rolesString.replace(/^,/, ''));
    // return
    if (this.state.isRequest) {
      console.log('isRequest :', "Request Audition");
      // this.checkForRepresentation(this.state.auditionData, 2, this.state.auditionData.online, rolesString.replace(/^,/, ''))
      this.saveAudition(this.state.auditionData, 2, this.state.auditionData.online, rolesString.replace(/^,/, ''));
    } else {
      // this.checkForRepresentation(this.state.auditionData, 1, this.state.auditionData.online, rolesString.replace(/^,/, ''))
      this.saveAudition(this.state.auditionData, this.state.auditionData.online ? 2 : 1, this.state.auditionData.online, rolesString.replace(/^,/, ''));
      console.log('isRequest :', "Save Audition");
    }
  }

  renderRoleSelection = () => {
    const {
      auditionData
    } = this.state;
    return (
      <RoleSelectionModal
        isVisible={this.state.isRoleSelection}
        isRequest={this.state.isRequest}
        isSubmit={this.state.isSubmit}
        onCloseClick={this.onCloseClick}
        rolesList={this.state.auditionData.roles || []}
        // onItemSelect={this.onItemSelect}
        onClick={this.onClick}
        isOnline={auditionData.online}
      />
    )
  }

  saveAuditionMaterials = async id => {
    this.setState({ isLoading: true });
    try {
      console.log("audition", id);
      const response = await myMediaApi.addMediaToUser({ audition: id });
      this.setState({ isLoading: false });
      return showMsj("Audition materials added successfully", false);
    } catch (e) {
      console.log(e);
      this.setState({ isLoading: false });
      if (e.status && e.status === 406) {
        return showMsj(e.data.data, true);
      }
      showMsj(generalMsj.error.message, true);
    }
  };

  render() {
    const {
      navigation: { goBack },
      navigation: { navigate }
    } = this.props;

    const {
      auditionData,
      loadingFlag,
      auditionCover,
      loading,
      locationName,
      walkAppont
    } = this.state;
    let walk = walkAppont ? walkAppont.length : 0;
    console.log("audition data=====", auditionData);
    console.log(JSON.stringify(auditionData));
    return (
      <Container>
        {this.renderRoleSelection()}
        <ImageHeader image={auditionCover} flag>
          <Header
            showSecond
            loadingFlag={loadingFlag}
            rightFn2={() => this.onPressFlag()}
            leftIcon="back"
            leftFn={() => goBack()}
            rightIcon={DownloadIcon}
            rightFn={() => shareAudition(this)}
          />
        </ImageHeader>
        {loading ? (
          <Loading />
        ) : (
            <Content showsVerticalScrollIndicator={false} style={styles.content}>
              <View style={styles.container}>
                <View style={styles.filtersContainer}>
                  <Filter
                    small={false}
                    text={auditionData.union}
                    marginBottom={styles.filtersMarginBottom.marginBottom}
                  />
                  <Filter
                    small={false}
                    type="contract"
                    text={auditionData.contract}
                    marginBottom={styles.filtersMarginBottom.marginBottom}
                  />
                  {auditionData.production &&
                    auditionData.production.map((option, idx) => (
                      <Filter
                        key={idx}
                        small={false}
                        type="production"
                        text={option}
                        marginBottom={styles.filtersMarginBottom.marginBottom}
                      />
                    ))}
                  {auditionData.online === 1 && (
                    <Filter
                      small={false}
                      type="online"
                      text={"ONLINE SUBMISSION"}
                      marginBottom={styles.filtersMarginBottom.marginBottom}
                    />
                  )}
                </View>
                <View style={styles.titleContainer}>
                  <Text style={styles.title}>{auditionData.title}</Text>
                  <Text style={styles.subtitle}>
                    {convertTimeToHour(auditionData.create)}
                  </Text>
                </View>
                <Text style={styles.text}>{auditionData.description}</Text>
              </View>
              <View style={styles.rolesContainer}>
                <Text style={styles.rolesTitle}>
                  {auditionData.roles != undefined ? auditionData.roles.length : 0} roles
                  available
              </Text>
                <ScrollView
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  contentContainerStyle={styles.rolesScroll}
                >
                  {auditionData.roles &&
                    auditionData.roles.map((option, idx) => (
                      <Role
                        key={idx}
                        role={option.name}
                        image={option.image.thumbnail != null ? option.image.thumbnail : option.image.url}
                        onPress={() =>
                          navigate("AuditionRoleDetail", {
                            roleData: option,
                            auditionData: {
                              title: auditionData.title,
                              time: auditionData.time,
                              auditionId: option.auditions_id,
                              appointmentId: auditionData.appointment_id,
                              rol: option.id,
                              create: auditionData.create,
                              contract: auditionData.contract,
                              production: auditionData.production,
                              union: auditionData.union,
                              online: auditionData.online,
                              walkAppont
                            }
                          })
                        }
                      />
                    ))}
                </ScrollView>
              </View>
              <View style={styles.extraInfo}>
                <Text style={styles.properties}>
                  Contract Type:{" "}
                  <Text style={styles.text}>
                    {auditionData && auditionData.contract
                      ? auditionData.contract.toUpperCase()
                      : ""}
                  </Text>
                </Text>
                <Text style={styles.properties}>
                  Contract Dates:{" "}
                  <Text style={styles.text}>
                    {auditionData.dates != undefined && auditionData.dates.length > 0
                      ? auditionData.dates[0].from &&
                        auditionData.dates[0].from !== "" &&
                        auditionData.dates[0].from !== null
                        ? auditionData.dates[0].to &&
                          auditionData.dates[0].to !== "" &&
                          auditionData.dates[0].to !== null
                          ? moment(auditionData.dates[0].from).format(
                            "MMMM DD, YYYY"
                          ) +
                          ` - ` +
                          moment(auditionData.dates[0].to).format(
                            "MMMM DD, YYYY"
                          )
                          : ""
                        : ""
                      : ""}
                  </Text>
                </Text>
                <Text style={styles.properties}>
                  Rehearsal Dates:{" "}
                  <Text style={styles.text}>
                    {auditionData.dates != undefined && auditionData.dates.length > 1
                      ? auditionData.dates[1].from &&
                        auditionData.dates[1].from !== "" &&
                        auditionData.dates[1].from !== null
                        ? auditionData.dates[1].to &&
                          auditionData.dates[1].to !== "" &&
                          auditionData.dates[1].to !== null
                          ? moment(auditionData.dates[1].from).format(
                            "MMMM DD, YYYY"
                          ) +
                          ` - ` +
                          moment(auditionData.dates[1].to).format(
                            "MMMM DD, YYYY"
                          )
                          : ""
                        : ""
                      : ""}
                  </Text>
                </Text>
              </View>

              <View style={styles.extraInfo}>
                <Text style={[styles.properties, styles.rolesContainer]}>
                  Personnel Information
              </Text>
                <Text style={styles.text}>
                  {auditionData.personal_information}{" "}
                </Text>
              </View>

              <View style={styles.extraInfo}>
                <Text style={[styles.properties, styles.rolesContainer]}>
                  Additional Information
              </Text>
                <Text style={styles.text}>{auditionData.additional_info} </Text>
              </View>

              <View style={styles.finalInfo}>
                <Text style={[styles.properties, styles.adjust]}>
                  {auditionData.agency}
                </Text>
                {/* <Text style={styles.text}>
                {auditionData.director.details.address}{" "}
              </Text>
              <Text style={styles.text}>
                {auditionData.director.details.city}{" "}
              </Text> */}
              </View>

              <View style={styles.locationContainer}>
                <Text style={styles.locationTitle}>{locationName}</Text>
                <View style={styles.locationMap}>
                  {auditionData.location ? (
                    <MapView
                      style={{ flex: 1, width: "100%", height: "100%" }}
                      initialRegion={this.state.region}
                    >
                      <Marker
                        coordinate={this.state.marker}
                        onPress={() => handleLocation(this)}
                      >
                        <TouchableOpacity>
                          <Image source={LocationIcon} />
                        </TouchableOpacity>
                      </Marker>
                    </MapView>
                  ) : null}
                </View>
                <View style={{ marginTop: 10 }}>

                  <AddMaterialButton title={"Audition Materials to My Media"} loading={this.state.isLoading} onClick={() => {
                    this.saveAuditionMaterials(auditionData.id)
                  }} />

                  {!auditionData.online ? <ApplyButton title={"Request Audition from Agent"} onClick={() => {
                    this.setState({ isRoleSelection: true, isRequest: true, isSubmit: false })
                    // this.checkForRepresentation(auditionData, 2, online)
                  }} />
                    : null}

                  {auditionData.online ? <ApplyButton title={"Submit"} onClick={() => {
                    this.setState({ isRoleSelection: true, isRequest: false, isSubmit: true })
                    // this.checkForRepresentation(auditionData, 2, online)
                  }} />
                    : null}
                  {auditionData.online || walk !== 0 ? <ApplyButton style={{ backgroundColor: "#FEA84B" }} title={auditionData.online ? "Save for later" : "Save Audition"} onClick={() => {
                    this.setState({ isRoleSelection: true, isRequest: false, isSubmit: false })
                    // this.checkForRepresentation(auditionData, 2, online)
                  }} />
                    : null}

                </View>
              </View>
            </Content>
          )}
      </Container>
    );
  }
}
export default AuditionDetailView;


// const ApplyButton = props => {
//   return (
//     <TouchableOpacity style={[styles.buttonContainer, props.style]} onPress={props.onClick}>
//       <Text style={styles.buttonText}>{props.title}</Text>
//     </TouchableOpacity>
//   )
// }

// const AddMaterialButton = props => {
//   return (
//     <TouchableOpacity style={[styles.borderButtonContainer, props.style]} onPress={props.onClick}>
//       <Image style={{ marginRight: 10 }} source={AddIcon} />
//       <Text style={{
//         fontFamily: fontStyles.nexa_bold,
//         fontSize: 13,
//         color: colorPalette.mainButtonBg,
//         textAlign: 'center',
//       }}>{props.title}</Text>
//     </TouchableOpacity>
//   )
// }