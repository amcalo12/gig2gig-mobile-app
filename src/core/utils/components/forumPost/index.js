import React, { Component } from "react";
import { TouchableOpacity, Linking, Text, View, Platform } from "react-native";
import { Card, Spinner } from "native-base";
import styles from "./index.styles";
import moment from "moment";
import { Image } from 'react-native-elements';
import { Assets } from "resources/assets";
import { resize } from "utils/utils";
import HTML from 'react-native-render-html';

type Props = {
  onPress: Function,
  title: String,
  avatar: String,
  body: String,
  time_ago: String,
  topic: String,
  name: String
}

export default function ForumPost(props: Props) {

  const {
    onPress,
    title,
    avatar,
    body,
    time_ago,
    topic,
    name,
    is_admin
  } = props
  console.log(" is_admin ", is_admin)
  return (
    <TouchableOpacity
      activeOpacity={0.6}
      style={styles.container}
      onPress={() => onPress()}>
      <Card
        style={styles.cardContainer}>
        <View
          style={styles.header}
        >
          <View
            style={styles.avatarAndNameContainer}
          >
            <Image
              // source={{ uri: "https://picsum.photos/200/300" }}
              source={is_admin == 1 ? Assets.gig_avatar : { uri: avatar }}
              placeholderStyle={{ backgroundColor: 'transparent' }}
              style={styles.avatar}
              defaultSource={is_admin == 1 ? Assets.gig_avatar : Assets.defaultAvatar}
              // PlaceholderContent={
              //   <Spinner
              //     color={'rgba(0,0,0,0.5)'}
              //     size={'small'}
              //   />
              // }
              resizeMode={"contain"}
            />
            <Text
              style={styles.nameTxt}
            >
              {name}
            </Text>
          </View>
        </View>
        <Text
          style={styles.titleTxt}
        >
          {title}
        </Text>
        {/* <Text
          style={styles.bodyTxt}
          numberOfLines={4}
        >
          {body.replace(/(<([^>]+)>)/g, "")}
        </Text> */}
        <HTML
          html={body}
          containerStyle={{
            marginTop: resize(5),
          }}
          onLinkPress={(evt, href) => { Linking.openURL(href); }}
        />
        <Text
          style={styles.dateTxt}
        >
          {time_ago}
        </Text>
      </Card>
    </TouchableOpacity>
  );
}

ForumPost.defaultProps = {
  onPress: () => { },
  title: '',
  avatar: 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png',
  body: '',
  time_ago: '',
  topic: '',
  name: '',
}
