import React, { Component } from 'react';
import { Spinner } from "native-base";
import styles from './index.styles';
import { spinnerColor } from "resources/colors"

export default class Loading extends Component {

  render() {
    const { color, onButton, style } = this.props;
    return (
      <Spinner style={style ? style : styles.spinner} color={color ? color : spinnerColor.color} />
    );
  }
}