import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");


export default StyleSheet.create({
	container: {
		borderWidth: 1,
		borderRadius: 4,
		paddingHorizontal: height * 0.005,
		paddingVertical: height * 0.004,
		marginRight: height * 0.006,
		justifyContent: "center"
	},
	containerBig: {
		borderWidth: 1,
		borderRadius: 4,
		paddingHorizontal: height * 0.009,
		paddingVertical: height * 0.004,
		marginRight: height * 0.02,
		justifyContent: "center"
	},
	text: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 8,
		...Platform.select({
			ios: {
				marginTop: resize(2, height),
			},
		}),
	},
	textBig: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 11,
		...Platform.select({
			ios: {
				marginTop: resize(2, height),
			},
		}),
	},
	unionContainer: {
		borderColor: colorPalette.unionColor
	},
	unionText: {
		color: colorPalette.unionColor
	},
	contractContainer: {
		borderColor: colorPalette.contractColor
	},
	contractText: {
		color: colorPalette.contractColor
	},
	productionContainer: {
		borderColor: colorPalette.productionColor
	},
	productionText: {
		color: colorPalette.productionColor
	},
	onlineContainer: {
		borderColor: colorPalette.onlineColor
	},
	onlineText: {
		color: colorPalette.onlineColor
	},
});