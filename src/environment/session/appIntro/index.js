//import liraries
import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  Platform
} from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
const { width, height } = Dimensions.get("window");
import { NavigationActions, StackActions } from "react-navigation";

const isIphoneX =
  Platform.OS === "ios" &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (height === 812 || width === 812);

const slides = [
  {
    key: "somethun",
    title: "Tell Us About Yourself",
    text:
      "Complete your profile to include Credits, Education & Training, Union Membership and more!",
    image: "tutorial_1",
    backgroundColor: "#59b2ab"
  },
  {
    key: "somethun-dos",
    title: "Manage Your Media",
    text:
      "Stop carrying around heavy binders of sheet music, cuts and headshots.",
    image: "tutorial_2",
    backgroundColor: "#febe29"
  },
  {
    key: "somethun1",
    title: "Manage Your Schedule",
    text:
      "Keep track of your upcoming events and allow casting to view your schedule.",
    image: "tutorial_3",
    backgroundColor: "#22bcb5"
  },
  {
    key: "somethun1",
    title: "Explore Auditions",
    text: "Search Upcoming Auditions.\nExplore Roles and find your next role!",
    image: "tutorial_4",
    backgroundColor: "blue"
  },
  {
    key: "somethun1",
    title: "Checking In",
    text:
      "Once your appointment is secured.\n easily scan in to the audition with your personal QR code.",
    image: "tutorial_5",
    backgroundColor: "gray"
  },
  {
    key: "somethun1",
    title: "Feedback",
    text:
      "Audition Feedback that allows you to grow as a performer.",
    image: "tutorial_6",
    backgroundColor: "gray"
  }
];

// create a component
class AppIntroView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false
    };
  }

  _renderItem = ({ item }) => {
    return (
      <View style={{ width: "100%", height: "100%" }}>
        <Image source={{ uri: item.image }} style={styles.imageStyle} />
        <View style={styles.introContainer}>
          <View style={styles.textContainer}>
            <Text style={styles.titleText}>{item.title}</Text>
            <Text style={styles.subTitleText}>{item.text}</Text>
          </View>
        </View>
      </View>
    );
  };
  _onDone = () => {
    this.navigateToApp();
  };

  navigateToApp() {
    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "App" })]
      })
    );
  }

  _onSkip = () => {
    this.navigateToApp();
  };
  render() {
    return (
      <AppIntroSlider
        renderItem={this._renderItem}
        slides={slides}
        dotStyle={styles.dotStyle}
        activeDotStyle={styles.activeDotStyle}
        onDone={this._onDone}
        showSkipButton={true}
        onSkip={this._onSkip}
        showNextButton={true}
        buttonTextStyle={{
          color: colorPalette.purple,
          fontSize: 14,
          fontFamily: fontStyles.nexa_bold
        }}
      />
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  dotStyle: {
    backgroundColor: colorPalette.purple,
    width: 6,
    height: 6,
    borderRadius: 3,
    marginHorizontal: 6
  },
  activeDotStyle: {
    backgroundColor: colorPalette.purple,
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 6
  },
  imageStyle: { width: "100%", flex: 1 },
  titleText: {
    textAlign: "center",
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    letterSpacing: 1.2,
    marginBottom: 15,
    color: colorPalette.purple
  },
  subTitleText: {
    textAlign: "center",
    fontFamily: fontStyles.nexa_light,
    fontSize: 14,
    letterSpacing: 0.55,
    color: colorPalette.purple
  },
  introContainer: {
    width: "100%",
    paddingBottom: 60 + (isIphoneX ? 34 : 0),
    justifyContent: "center",
    alignItems: "center"
  },
  textContainer: {
    width: "75%",
    justifyContent: "center",
    alignContent: "center",
    paddingTop: 15,
    paddingBottom: 15
  }
});

//make this component available to the app
export default AppIntroView;
