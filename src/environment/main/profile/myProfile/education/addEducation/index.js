import React, { Component } from "react";
import { View, Text } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Input from "utils/components/mainInput";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
import Button from "utils/components/mainButton";
import DateSelect from "utils/components/dateSelect"
import addEducationViewModel from "./addEducationViewModel";
class AddEducation extends Component {
	state = {
		school: '',
		degree: '',
		instructor: '',
		location: '',
		show: true,
		year: '',
		loading: false
	}
	render() {
		const { navigation: { navigate, goBack } } = this.props;
		const { school, degree, instructor, location, show, year, loading } = this.state
		return (
			<Container>
				<Header
					leftIcon="backDark"
					leftFn={() => goBack()}
					barStyle="dark-content"
					text="Education & Training"
				/>
				<Content>
					<View style={styles.container}>
						<View style={styles.form}>
							<Input placeholder="School" data={school} onEdit={(school) => this.setState({ school })} elevation={1.5} customWidth={styles.input.width} margin={10} />
							<Input placeholder="Degree/Course" data={degree} onEdit={(degree) => this.setState({ degree })} elevation={1.5} customWidth={styles.input.width} margin={10} />
							<Input placeholder="Instructor (optional)" data={instructor} onEdit={(instructor) => this.setState({ instructor })} elevation={1.5} customWidth={styles.input.width} margin={10} />
							<Input placeholder="Location (optional)" data={location} onEdit={(location) => this.setState({ location })} elevation={1.5} customWidth={styles.input.width} margin={10} />
							<DateSelect
								placeholder={'Year'}
								icon={DownArrowIcon}
								data={addEducationViewModel.convertYears(this)}
								onChange={(year) => this.setState({ year })}
								short
								elevation={1.5}
							/>
						</View>
						<View style={styles.button}>
							<Button text="Save" loading={loading} onPress={async () => await addEducationViewModel.handlePressSave(this)} />
						</View>
					</View>
				</Content>
			</Container>
		);
	}
}
export default AddEducation;
