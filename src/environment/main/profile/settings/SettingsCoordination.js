import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import SettingsView from ".";
import PushNotificationsView from "./pushNotifications";
import SubscriptionStack from "./subscription/SubscriptionCoordination";
import TermsOfUseView from "./termsOfUse";
import PrivacyPolicyView from "./privacyPolicy";
import HelpView from "./help";
import AppInfoView from "./appInfo";
import ContactUsView from "./contactUs";
import MarketplaceSettingsStack from "./marketplace/MarketplaceSettingsCoordination";
import AuditionFeedback from './AuditionFeedback';

const SettingsStack = createStackNavigator(
  {
    Settings: {
      screen: SettingsView,
      navigationOptions: {
        header: null
      }
    },
    PushNotifications: {
      screen: PushNotificationsView,
      navigationOptions: {
        header: null
      }
    },
    AuditionFeedback: {
      screen: AuditionFeedback,
      navigationOptions: {
        header: null
      }
    },
    MarketplaceComing: {
      screen: MarketplaceSettingsStack,
      navigationOptions: {
        header: null
      }
    },
    Subscription: {
      screen: SubscriptionStack,
      navigationOptions: {
        header: null
      }
    },
    TermsOfUse: {
      screen: TermsOfUseView,
      navigationOptions: {
        header: null
      }
    },
    PrivacyPolicy: {
      screen: PrivacyPolicyView,
      navigationOptions: {
        header: null
      }
    },
    Help: {
      screen: HelpView,
      navigationOptions: {
        header: null
      }
    },
    AppInfo: {
      screen: AppInfoView,
      navigationOptions: {
        header: null
      }
    },
    ContactUs: {
      screen: ContactUsView,
      navigationOptions: {
        header: null
      }
    },
  },
  {
    initialRouteName: "Settings",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default SettingsStack;
