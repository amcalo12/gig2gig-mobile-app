import React, { Component } from "react";
import { View, AsyncStorage } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Label from "utils/components/unionMemberLabel";
import { AndroidBackHandler } from 'react-navigation-backhandler';
import { handleBackPress, toggleCheck } from "./unionMembershipViewModel";
import userApi from 'api/userApi';
import Loading from "utils/components/loading";
class UnionMembershipView extends Component {

  state = {
    loading: false,
    checked: false,
    unionMembers: [
      {
        name: "None"
      },
      {
        name: "SAG-AFTRA"
      },
      {
        name: "Equity (U.S.)"
      },
      {
        name: "NonEquity (U.K.)"
      },
      {
        name: "Equity (Ireland)"
      },
      {
        name: "ACTRA"
      },
      {
        name: "AFM"
      },
      {
        name: "AGMA"
      },
      {
        name: "AGVA"
      },
      {
        name: "BECTU"
      },
      {
        name: "Equity Eligible"
      },
      {
        name: "SAG-AFTRA Eligible"
      }
    ],
    editedUnionMembers: null
  };

  async componentDidMount() {
    // const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    const response = await userApi.getUnionMembershipList();
    this.state.unionMembers.map((option, idx) => {
      const result = response.data.find(um => um.name === option.name);
      if (result) {
        option.active = true
      }
    })
    // console.log(userData.union_members);
    this.setState({
      unionMembers: this.state.unionMembers
    });
  }

  render() {
    const { loading } = this.state;
    const { navigation: { navigate } } = this.props;
    return (
      <AndroidBackHandler onBackPress={() => handleBackPress(this)}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => handleBackPress(this)}
            barStyle="dark-content"
            text="Union Membership"
          />
          {
            loading &&
            <Loading />
          }
          <Content>
            <View style={styles.container}>
              {
                this.state.unionMembers.map((option, idx) => (
                  <Label key={idx} text={option.name} checked={option.active ? option.active : false} onPress={() => toggleCheck(this, idx, option.active ? option.active : false, option.name)} />
                ))
              }
            </View>
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default UnionMembershipView;
