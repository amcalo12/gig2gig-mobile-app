import { createStackNavigator } from "react-navigation";
import PhotosView from ".";

const PhotosStack = createStackNavigator(
  {
     Photos: {
       screen: PhotosView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "Photos",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default PhotosStack;
