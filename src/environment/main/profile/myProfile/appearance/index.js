import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import InfoLabel from "utils/components/infoLabel";
import Button from "utils/components/secondaryButton";
import Loading from "utils/components/loading";
import PlusIcon from "resources/images/myCalendar/plus-icon.png";
import { AndroidBackHandler } from "react-navigation-backhandler";
import appearanceViewModel from "./appearanceViewModel";

class AppearanceView extends Component {
  state = {
    userInfo: {
      height: "",
      weight: "",
      eyes: "",
      hair: "",
      race: "",
    },
    loading: true,
    notFoundData: false,
  };
  componentDidMount = async () => {
    await appearanceViewModel.getAppearance(this);
  };

  handleBackPress = () => {
    const {
      navigation: { dispatch },
    } = this.props;
    return dispatch(StackActions.popToTop());
    return true;
  };

  render() {
    const {
      navigation: { navigate },
    } = this.props;
    const {
      userInfo: {
        height,
        weight,
        eyes,
        hair,
        race,
        personal_flare,
        id,
        gender_pronouns,
      },
      loading,
      notFoundData,
    } = this.state;
    return (
      <AndroidBackHandler onBackPress={() => this.handleBackPress()}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => this.handleBackPress()}
            barStyle="dark-content"
            text="Appearance"
            rightIcon={notFoundData ? PlusIcon : "none"}
            rightFn={
              notFoundData === true
                ? () =>
                    navigate("AddAppearance", {
                      onGoBack: (data) =>
                        appearanceViewModel.update(this, data),
                    })
                : null
            }
          />
          <Content>
            {loading ? (
              <Loading style={{ top: "40%", alignSelf: "center" }} />
            ) : notFoundData ? (
              <View style={styles.container}>
                <Text style={styles.notFoundText}>
                  You haven't added your information
                </Text>
              </View>
            ) : (
              <View style={styles.container}>
                <View style={styles.labels}>
                  <InfoLabel
                    text={
                      height === "" || height === null
                        ? "Height"
                        : `${height} Height`
                    }
                  />
                  <InfoLabel
                    text={weight === "" || weight === null ? "Weight" : weight}
                  />
                  <InfoLabel
                    text={hair === "" || hair === null ? "Hair Color" : hair}
                  />
                  <InfoLabel
                    text={eyes === "" || eyes === null ? "Eye Color" : eyes}
                  />
                  <InfoLabel
                    text={race === "" || race === null ? "Ethnicity" : race}
                  />
                  <InfoLabel
                    text={
                      personal_flare === "" || personal_flare === null
                        ? "personal_flare"
                        : personal_flare
                    }
                  />
                  <InfoLabel
                    noLine
                    text={
                      gender_pronouns === "" || gender_pronouns === null
                        ? "Gender Pronouns"
                        : gender_pronouns
                    }
                  />
                </View>
                <View style={styles.button}>
                  <Button
                    text="Edit"
                    onPress={() =>
                      navigate("AppearanceEdit", {
                        userInfo: {
                          height,
                          weight,
                          eyes,
                          hair,
                          race,
                          personal_flare,
                          id,
                          gender_pronouns,
                        },
                        update: (data) =>
                          appearanceViewModel.update(this, data),
                      })
                    }
                  />
                </View>
              </View>
            )}
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default AppearanceView;
