import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);

export default StyleSheet.create({
  container: {
    alignItems: "center",
    paddingTop: resize(10, "height"),
    width: "100%",
    height: "100%"
  },
  text: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: colorPalette.purple,
    marginTop: resize(40, "height")
  },
  swipeout: {
    backgroundColor: "transparent"
  }
});
