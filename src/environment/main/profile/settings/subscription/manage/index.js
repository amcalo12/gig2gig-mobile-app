import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Button from "utils/components/mainButton";
import manageModelView from './manageModelView';
class ManageView extends Component {
  state = {
    loading: false
  }
  render() {
    const { loading } = this.state
    const {navigation : {navigate, dispatch, state : {params : {data} } }, press } = this.props;
    return (
      <Container>
        <Header
        leftIcon="backDark"
        leftFn={()=>dispatch(StackActions.pop())}
        barStyle="dark-content"
        text="Subscription"
        />
        <Content>
          <View style={styles.container}>
            <View>
              <Text style={styles.typeText}>{data.type}</Text>
              <Text style={styles.descriptionText}>{data.description}</Text>
            </View>
            <Button text='Upgrade' loading={loading} onPress={()=> manageModelView.updatePlan(this)}/>
          </View>
        </Content>
      </Container>
    );
  }
}
export default ManageView;
