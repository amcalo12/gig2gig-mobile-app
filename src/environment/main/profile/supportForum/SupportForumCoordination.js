import { createStackNavigator } from "react-navigation";
import SupportForum from './index';
import AddSupportForum from './addSupportForum';
import DetailSupportForum from './detailSupportForum';

const SupportForumStack = createStackNavigator(
  {
    SupportForum: {
      screen: SupportForum,
      navigationOptions: {
        header: null
      }
    },
    AddSupportForum: {
      screen: AddSupportForum,
      navigationOptions: {
        header: null
      }
    },
    DetailSupportForum: {
      screen: DetailSupportForum,
      navigationOptions: {
        header: null
      }
    }
  },
);

export default SupportForumStack;
