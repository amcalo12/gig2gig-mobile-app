import React, { Component } from 'react';
import {
  View, Text, FlatList, StatusBar, Image,
  Platform, TouchableOpacity, ImageBackground, Alert, AsyncStorage
} from 'react-native';
import Background from "resources/images/login/background.png";
import styles from "./index.styles";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { SafeAreaView } from 'react-navigation';
import { Assets } from 'resources/assets';
import ProgressLoader from 'utils/components/progressLoader';
import userApi from 'api/userApi';
import moment from 'moment';
import { checkSubscriptionAndRedirect, MESSAGES, showAlert, IN_APP_FEATURE, EXPORT_USER_PLAN_NAME } from 'utils/cons';

export default class SubscriptionDetail extends Component {
  constructor(props) {
    super(props);
    // this.currentIndex = 0
    this.state = {
      isLoading: false,
      userData: undefined
    };
  }

  async componentDidMount() {
    this.getSubscriptionDetails()
  }

  getSubscriptionDetails = async () => {
    this.setState({ isLoading: true })
    try {
      const userData = JSON.parse(await AsyncStorage.getItem("userData"));
      console.log('componentDidMount Called:', userData);
      const userInfo = await userApi.getUserInfo(userData.id);
      let data = userInfo.data
      data['isPaidUser'] = data.is_premium ? true : false
      await AsyncStorage.setItem("userData", JSON.stringify(data));
      // if (data.subscription != null && !data.isPaidUser) {
      //   checkSubscriptionAndRedirect(userInfo.data, 'SubscriptionPlan', this.props)
      // }
      this.setState({ isLoading: false, userData: userInfo.data })
    } catch (error) {
      console.log('error :>> ', error);
      this.setState({ isLoading: false })
    }
  }

  callback = async () => {
    this.getSubscriptionDetails()
  }

  render() {
    return (
      <View style={styles.container}>
        <ProgressLoader isLoading={this.state.isLoading} />
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="transparent"
        />
        <ImageBackground source={Background} style={styles.background}>
          <TouchableOpacity style={styles.backButton} onPress={() => this.props.navigation.goBack()}>
            <Image source={Assets.close} style={{ tintColor: colorPalette.white }} />
          </TouchableOpacity>
          <View style={styles.logoContainer}>
            <Text style={styles.subscriptionTitle}>{"Subscription Details"}</Text>
            {/* <Text style={styles.subscriptionTitle}>{"Details"}</Text> */}
          </View>
          {this.state.userData != undefined && this.state.userData.subscription != null && !this.state.userData.is_premium ?
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <View style={styles.itemContainer}>
                <View style={styles.headerContainer}>
                  <Text style={styles.headerTitle}>{'Subscription Expired'}</Text>
                </View>

                <View style={{ flex: 1, justifyContent: "center", alignItems: 'center', marginBottom: 30 }}>
                  <Text style={styles.description}>{this.state.userData.subscription.purchase_platform == Platform.OS || this.state.userData.subscription.name == EXPORT_USER_PLAN_NAME ? MESSAGES.PLAN_EXPIRE : MESSAGES.PLAN_EXPIRE_DIFFERENT_OS}</Text>
                </View>

                {this.state.userData.subscription != null ?
                  <TouchableOpacity style={[styles.buttonContainer, { marginBottom: 30 }]} onPress={() => this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true, callback: this.callback })}>
                    <Text style={styles.buttonTitle}>{"Subscribe"}</Text>
                  </TouchableOpacity>
                  : null
                }
              </View>
            </View>
            :
            this.state.userData != undefined && this.state.userData.subscription != null && this.state.userData.subscription.name == EXPORT_USER_PLAN_NAME ?
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={[styles.itemContainer, { justifyContent: 'space-between', paddingBottom: 20 }]}>
                  <View style={styles.headerContainer}>
                    <Text style={styles.headerTitle}>{this.state.userData.subscription != null && !this.state.userData.isPaidUser ? 'Your Subscription is expired' : 'Your Active Subscription'}</Text>
                  </View>
                  {console.log('this.state.userData.subscription :>> ', this.state.userData)}

                  <View style={styles.priceContainer}>
                    <Text style={styles.freeText}>{MESSAGES.PREMIUM_FREE_ANNUAL}</Text>
                  </View>

                  <Text style={styles.description}>{IN_APP_FEATURE.PREMIUM}</Text>

                  {this.state.userData.subscription != null ? <View>
                    <Text style={{ marginTop: 10, textAlign: "center", fontFamily: fontStyles.nexa_light, fontSize: 16 }}>{"End Date : " + moment(this.state.userData.subscription.ends_at).format('MM/DD/YYYY')}</Text>
                  </View>
                    : null}
                </View>
              </View>
              :
              this.state.userData != undefined ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={styles.itemContainer}>
                    <View style={styles.headerContainer}>
                      <Text style={styles.headerTitle}>{this.state.userData.subscription != null && !this.state.userData.isPaidUser ? 'Your Subscription is expired' : 'Your Active Subscription'}</Text>
                    </View>
                    {console.log('this.state.userData.subscription :>> ', this.state.userData)}

                    <View style={styles.priceContainer}>
                      <Text style={styles.price}>{this.state.userData.subscription != null ? ('$' + this.state.userData.subscription.purchased_price) : "Free"}</Text>
                      {this.state.userData.subscription != null ? <Text style={styles.duration}>{this.state.userData.subscription.name == "Monthly" ? "Monthly" : "Annual"}</Text> : null}
                    </View>

                    {this.state.userData.subscription != null ?
                      <Text style={styles.description}>{IN_APP_FEATURE.PREMIUM}</Text>
                      :
                      <Text style={styles.description}>{IN_APP_FEATURE.FREE}</Text>
                    }

                    {this.state.userData.subscription != null ? <View>
                      {this.state.userData.subscription.name != EXPORT_USER_PLAN_NAME ? <Text style={{ textAlign: "center", fontFamily: fontStyles.nexa_light, fontSize: 16 }}>{"Start Date : " + moment(this.state.userData.subscription.purchased_at).format('MM/DD/YYYY')}</Text> : null}
                      <Text style={{ marginTop: 10, textAlign: "center", fontFamily: fontStyles.nexa_light, fontSize: 16 }}>{"End Date : " + moment(this.state.userData.subscription.ends_at).format('MM/DD/YYYY')}</Text>
                    </View>
                      : null}

                    <TouchableOpacity style={styles.buttonContainer} onPress={() => {
                      if (this.state.userData.isPaidUser) {
                        // showAlert(MESSAGES.CANCEL_SUBSCRIPTION)
                        showAlert(Platform.OS == "ios" ? MESSAGES.CANCEL_IOS : MESSAGES.CANCEL_ANDROID)
                      } else {
                        this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true, callback: this.callback })
                      }
                    }}>
                      <Text style={styles.buttonTitle}>{this.state.userData.isPaidUser ? "Cancel Subscription" : "UPGRADE"}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                : null
          }
          {/* <TouchableOpacity style={styles.restoreContainer} onPress={() => this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true })}>
            <Text style={styles.restoreButton}>{"Restore Purchase"}</Text>
          </TouchableOpacity> */}
        </ImageBackground>
      </View >
    );
  }
}
