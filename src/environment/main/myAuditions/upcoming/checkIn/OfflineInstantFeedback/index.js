import React, { Component } from "react";
import { View, Image, FlatList } from "react-native";
import { Container, Content, Text } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import ImageHeader from "utils/components/imageHeader";
import myAuditionsApi from "api/myAuditionsApi";
import Loading from "utils/components/loading";
import AuditionCard from "utils/components/auditionCard";

class OfflineInstantFeedback extends Component {
  state = {
    data: null,
    loading: false,
    auditionData: null
  };

  async componentWillMount() {
    try {
      const data = this.props.navigation.state.params.data;
      const userId = this.props.navigation.state.params.userId;
      this.setState({ loading: true, auditionData: data });
      const res = await myAuditionsApi.getInstantFeedback(
        data.appointment_id,
        userId
      );
      console.log("feedback response===");
      console.log(res);
      this.setState({ loading: false, data: res.data, auditionData: res.data.audition[0] });
    } catch (e) {
      console.log("feedback error===");
      console.log(e);
      this.setState({ loading: false });
    }
  }

  render() {
    const { loading, data, auditionData } = this.state;

    const {
      navigation: { goBack }
    } = this.props;

    return (
      <Container>
        <ImageHeader
          // image={
          //   auditionData
          //     ? auditionData.media
          //       ? auditionData.media
          //       : null
          //     : null
          // }
          image={
            auditionData != undefined ? auditionData.media || auditionData.cover : null
          }
        >
          <Header leftIcon="back" leftFn={() => goBack()} />
        </ImageHeader>
        {!loading && (
          <Content contentContainerStyle={styles.content}>
            <Text style={styles.title}>Instant Feedback</Text>
            <Text style={styles.subTitle}>
              {auditionData
                ? auditionData.round
                  ? "Round " + auditionData.round
                  : ""
                : ""}
            </Text>
            {data !== null && (
              <View>
                <Text style={styles.messageTitle}>Message</Text>
                <Text style={styles.feedbackMessage}>
                  {data
                    ? data.feedback
                      ? data.feedback.comment
                        ? data.feedback.comment
                        : ""
                      : ""
                    : ""}
                </Text>
                <Text style={styles.messageTitle}>Suggested Audition</Text>
                {data && data.suggested_audition && (
                  <AuditionCard
                    isSearchAudition={true}
                    data={data.suggested_audition[0]}
                    onPress={() =>
                      this.props.navigation.navigate("AuditionDetail", {
                        id: data.suggested_audition[0].id,
                        image: data.suggested_audition[0].cover
                      })
                    }
                  />
                )}
              </View>
            )}

            {data === null && (
              <Text
                style={[
                  styles.messageTitle,
                  { textAlign: "center", marginTop: 20 }
                ]}
              >
                You don't have any Instant feedback
              </Text>
            )}
          </Content>
        )}
        {loading && <Loading />}
      </Container>
    );
  }
}
export default OfflineInstantFeedback;
