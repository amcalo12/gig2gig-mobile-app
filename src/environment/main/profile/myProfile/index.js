import React, { Component } from "react";
import { View, AsyncStorage, Alert, Platform } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Button from "utils/components/squareButton";
import Profile from "utils/components/profileHeader";
import { profileOptions, myProfileOptions } from "resources/l10n";
import {
  StackActions,
  withNavigationFocus,
  NavigationActions
} from "react-navigation";
import { AndroidBackHandler } from "react-navigation-backhandler";
import pushNotificationApi from "../../../../core/api/pushNotificationApi";
import DeviceInfo from "react-native-device-info";
import userApi from "api/userApi";
import { openSubscriptionDialog, checkSubscriptionAndRedirect } from "utils/cons";


class MyProfileView extends Component {

  constructor(props) {
    super(props)
    this.socialData = {}
  }

  state = {
    nextScreen: false,
    userData: null,
    newNotification: false,
  };

  async componentWillReceiveProps(next) {
    if (next.isFocused) {
      const userData = JSON.parse(await AsyncStorage.getItem("userData"));
      console.log(" user Data ", userData)
      this.setState({
        userData
      });
    }
  }

  async componentDidMount() {
    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    console.log('componentDidMount Called:', userData);
    const userInfo = await userApi.getUserInfo(userData.id);
    // this.props.saveUserData(userInfo)
    console.log('userInfo :>> ', userInfo);
    let data = userInfo.data
    data['isPaidUser'] = data.is_premium ? true : false
    await AsyncStorage.setItem("userData", JSON.stringify(data));
    if (this.props.navigation.state.params) {
      this.setState({
        nextScreen: true
      });
    }
    this.onFocus();
    // const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    this.setState({
      userData: data
    });
    this.props.navigation.addListener("didFocus", this.onFocus);
  }

  onFocus = async () => {
    this.setState({ newNotification: false });
    const newNotification = await AsyncStorage.getItem("newNotification");
    newNotification
      ? this.setState({ newNotification: true })
      : this.setState({ newNotification: false });
  };

  handleBackPress = () => {
    const { nextScreen } = this.state;
    const {
      navigation: { dispatch }
    } = this.props;
    if (nextScreen) {
      this.handleScreen();
    } else {
      return dispatch(StackActions.popToTop());
    }
    return true;
  };

  handleScreen = () => {
    this.setState({
      nextScreen: !this.state.nextScreen
    });
  };
  handleProfileOptionPress = async option => {
    if (option.navigate === 0) {
      this.showModal();
    } else if (option.navigate === 1) {
      this.handleScreen();
    } else {
      if (option.navigate == 'SupportForum') {
        console.log('this.state.userData.isPaidUser :>> ', this.state.userData);
        console.log('this.state.userData.isPaidUser :>> ', this.state.userData.isPaidUser);
        if (this.state.userData.isPaidUser) {
          this.props.navigation.navigate(option.navigate);
        } else {
          checkSubscriptionAndRedirect(this.state.userData, 'SubscriptionPlan', this.props)
          // openSubscriptionDialog(() => {
          //   this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true })
          // })
        }
      } else {
        this.props.navigation.navigate(option.navigate);
      }
    }
  };
  showModal = () => {
    Alert.alert(
      "Sign Out",
      "Are you sure you want to sign out?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => this.logOut()
        }
      ],
      { cancelable: false }
    );
  };
  logOut = async () => {
    try {
      let uniqueId = await DeviceInfo.getUniqueId();
      await pushNotificationApi.setPushToken({
        pushkey: "",
        device_id: uniqueId,
        device_type: Platform.OS
      });
    } catch (error) {
      console.log(error);
    }
    AsyncStorage.clear();
    return this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Initial" })]
      })
    );
  };
  render() {
    const {
      navigation: { navigate }
    } = this.props;
    const { nextScreen, userData, newNotification } = this.state;
    return (
      <AndroidBackHandler onBackPress={() => this.handleBackPress()}>
        <Container>
          {!nextScreen && (
            <Header
              rightIcon="notification"
              barStyle="dark-content"
              newNotification={newNotification}
            />
          )}
          {nextScreen && (
            <Header
              leftIcon="backDark"
              leftFn={() => this.handleScreen()}
              barStyle="dark-content"
            />
          )}
          <Content>
            <Profile
              picture={userData && userData.image != null ? userData.image.url : null}
              name={`${userData && userData.details.first_name || ""} ${userData &&
                userData.details.last_name || ""}`}
              job={userData && userData.details.profesion || ""}
              member={
                userData
                  ? userData.union_members[0]
                    ? userData.union_members[0].name
                    : "None"
                  : "None"
              }
              city={userData && userData.details.city}
            />
            <View style={styles.buttons}>
              {!nextScreen &&
                profileOptions.map((option, idx) => (
                  <Button
                    key={idx}
                    size="small"
                    margin={styles.button.margin}
                    icon={option.icon}
                    text={option.name}
                    onPress={() => this.handleProfileOptionPress(option)}
                  />
                ))}
              {nextScreen &&
                myProfileOptions.map((option, idx) => (
                  <Button
                    key={idx}
                    size="small"
                    margin={styles.button.margin}
                    icon={option.icon}
                    text={option.name}
                    onPress={() => navigate(option.navigate, { userData: this.state.userData, })}
                  />
                ))}
            </View>
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}

// export default connect(
//   state => {
//     return {

//     };
//   },
//   dispatch => {
//     return {
//       saveUserData: data => {
//         dispatch(saveUserDataInRedux(data));
//       },
//     };
//   },
// )(MyProfileView);

export default withNavigationFocus(MyProfileView);
