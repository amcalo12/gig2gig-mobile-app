import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container: {
    width: resize(340),
    alignSelf: "flex-end",
  },
  title: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 20,
    marginVertical: 10,
    color: colorPalette.purple
  },
  input: {
    width: "100%"
  },
});