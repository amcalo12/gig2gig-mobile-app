import { createStackNavigator } from "react-navigation";
import CheckInView from "./checkIn";
import UpdatesView from "./checkIn/updates";
import Waitlist from "./checkIn/waitlist";
import Online from "./online";
import OfflineInstantFeedback from "./checkIn/OfflineInstantFeedback";

const UpcomingStack = createStackNavigator(
  {
    CheckIn: {
      screen: CheckInView,
      navigationOptions: { header: null }
    },
    Online: {
      screen: Online,
      navigationOptions: { header: null }
    },
    Updates: {
      screen: UpdatesView,
      navigationOptions: { header: null }
    },
    Waitlist: {
      screen: Waitlist,
      navigationOptions: { header: null }
    },
    OfflineInstantFeedback: {
      screen: OfflineInstantFeedback,
      navigationOptions: { header: null }
    }
  },
  {
    initialRouteName: "CheckIn",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

export default UpcomingStack;
