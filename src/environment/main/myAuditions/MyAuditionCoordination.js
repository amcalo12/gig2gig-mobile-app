import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import MyAuditionView from ".";
import UpcomingStack from "./upcoming/CheckInCoordination";
import CheckInView from "./upcoming/checkIn";
import UpdatesView from "./upcoming/checkIn/updates";
import Waitlist from "./upcoming/checkIn/waitlist";
import Online from "./upcoming/online";
import PastStack from "./past/PastCoordination";
import OfflineInstantFeedback from "./upcoming/checkIn/OfflineInstantFeedback";
import InfoView from "../marketplace/filter/info";
import AuditionDetailView from "../auditions/auditionDetail";
import AuditionRoleDetailView from "../auditions/auditionRoleDetail";

const MyAuditionStack = createStackNavigator(
  {
    MyAuditionMain: {
      screen: MyAuditionView,
      navigationOptions: { header: null }
    },
    Upcoming: {
      screen: CheckInView,
      navigationOptions: { header: null }
    },
    Online: {
      screen: Online,
      navigationOptions: { header: null }
    },
    Updates: {
      screen: UpdatesView,
      navigationOptions: { header: null }
    },
    Waitlist: {
      screen: Waitlist,
      navigationOptions: { header: null }
    },
    Past: {
      screen: PastStack,
      navigationOptions: { header: null }
    },
    OfflineInstantFeedback: {
      screen: OfflineInstantFeedback,
      navigationOptions: { header: null }
    },
    Info: {
      screen: InfoView,
      navigationOptions: {
        header: null
      }
    },
    AuditionDetail: {
      screen: AuditionDetailView,
      navigationOptions: {
        header: null
      }
    },
    AuditionRoleDetail: {
      screen: AuditionRoleDetailView,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "MyAuditionMain",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

export default MyAuditionStack;
