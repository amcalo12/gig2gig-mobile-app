import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import DeviceInfo from 'react-native-device-info';
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: "flex-start",
    alignItems: 'center',
    // backgroundColor: 'rgba(0, 0, 0, 0.15)',
  },
  info:{
    //marginTop: isIos ? height * 0.03 : height * 0.010,
    flexDirection: 'column',
    // justifyContent: 'space-between',
    width: width * 0.89666,
    height: height * 0.905037481259,
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    // paddingHorizontal: 20,
    // paddingVertical: 20
  },
  measure:{
    ...Platform.select({
      ios: {
        height: resize(DeviceInfo.hasNotch() ? 12 : 10, "height")
      },
    }),
  },
   searchContainer:{
    flexDirection:"row",
    alignItems: 'center',
    paddingLeft: 15,
    backgroundColor:"#FFFFFF",
    borderBottomWidth:1,
    borderTopWidth:1,
    borderLeftWidth:1,
    borderRightWidth:1,
    borderRadius: 20,
    borderColor:"transparent",
    marginHorizontal: 10,
    height: height * 0.0554722638681,
      width: width * 0.893333333333,
      alignSelf:"center"
  },
  searchInput:{
    color: "#4D2545",
    fontFamily:fontStyles.nexa_light,
    paddingLeft:10,
    paddingRight:10,
    letterSpacing: 1,
  },
  filterButton:{
    paddingRight:15
  },
  filtersContainer:{
    paddingHorizontal: height * 0.02,
    marginTop: height * 0.05
  },
  filterLabel:{
    fontFamily:fontStyles.nexa_bold,
    fontSize:14,
    color:colorPalette.purple,
    marginBottom: height * 0.01
  },
  filters:{
    flexDirection:"row",
    flexWrap: "wrap",
    // marginBottom: height * 0.02
  },
  buttonAction:{
    position:"absolute",
    bottom:height * 0.05,
    width:"100%",
    alignItems:"center"
  }
});