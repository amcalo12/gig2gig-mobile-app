import React, { Component } from "react";
import { View, AsyncStorage, Image } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import InfoLabel from "utils/components/infoLabel";
import Button from "utils/components/secondaryButton";
import { AndroidBackHandler } from "react-navigation-backhandler";
import { getState, getCountry } from "utils/utils";
import { getUserData, handleBackPress } from "./MyInfoViewModel";
import moment from "moment";

class MyInfoView extends Component {
  state = {
    userData: null
  };

  async componentDidMount() {
    await getUserData(this);
  }

  render() {
    const { userData } = this.state;
    const {
      navigation: { navigate }
    } = this.props;
    return (
      <AndroidBackHandler onBackPress={() => handleBackPress(this)}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => handleBackPress(this)}
            barStyle="dark-content"
            text="My Info"
          />
          <Content>
            {userData && (
              <View>
                <View style={styles.userImage}>
                  <Image
                    style={styles.userPicture}
                    source={userData ? { uri: userData.image.url } : null}
                  />
                </View>
                <View style={styles.container}>
                  <InfoLabel text={userData.details.first_name} />
                  <InfoLabel text={userData.details.last_name} />
                  <InfoLabel text={userData.email} />
                  <InfoLabel text="*********" />
                  <InfoLabel text={userData.details.stage_name === "" ||
                    userData.details.stage_name === null
                    ? "Stage Name"
                    : userData.details.stage_name} />
                  <InfoLabel text={userData.details.profesion} />
                  <InfoLabel
                    text={
                      userData.details.url === "" ||
                        userData.details.url === null
                        ? "Website"
                        : userData.details.url
                    }
                  />
                  {/* <InfoLabel text={userData.details.address} />
                  <InfoLabel text={userData.details.city} />
                  <InfoLabel text={getState(userData.details.state).name} />
                  <InfoLabel text={userData.details.zip} /> */}
                  <InfoLabel text={userData.details.country != null ? getCountry(String(userData.details.country)).name : 'Country'} />
                  <InfoLabel
                    noLine
                    text={
                      userData.details.birth === "" ||
                        userData.details.birth === null
                        ? "Birth Date"
                        : moment(userData.details.birth).format("MM/DD/YYYY")
                    }
                  />
                </View>
              </View>
            )}
            <View style={styles.button}>
              <Button
                text="Edit"
                onPress={() => navigate("MyInfoEdit", { userData })}
              />
            </View>
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default MyInfoView;
