import React, { Component } from "react";
import { View, RefreshControl, Text, AsyncStorage } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Button from "utils/components/squareButton";
import Loading from "utils/components/loading";
import SearchInput from "utils/components/searchInput";
import marketplaceViewModel from "./marketplaceViewModel";
import MCard from "utils/components/marketplaceCard";
import Featured from "utils/components/featured";
class MarketplaceView extends Component {
  state = {
    textSearch: "",
    loading: true,
    marketplaceOptions: [],
    listData: [],
    setCards: true,
    refreshControlLoad: false,
    newNotification: false,
    subscription: null,
    marketplaceFeature: []
  };
  async componentDidMount() {
    this.onFocus();
    await marketplaceViewModel.handleGetCategories(this);
    this.props.navigation.addListener("didFocus", this.onFocus);
  }
  onFocus = async () => {
    this.setState({ newNotification: false });
    const newNotification = await AsyncStorage.getItem("newNotification");
    newNotification
      ? this.setState({ newNotification: true })
      : this.setState({ newNotification: false });

    const userData = JSON.parse(await AsyncStorage.getItem("userData"));
    this.setState({
      userData,
      subscription: userData.details.subscription
    });
  };
  renderFilters = action => {
    return this.state.marketplaceOptions.map((option, idx) => (
      <Button
        key={idx}
        size="small"
        text={option.name}
        onPress={() => action({ option })}
      />
    ));
  };
  renderCards = action => {
    return this.state.listData.map((data, i) => {
      return <MCard key={i} data={data} onPress={() => action({ data })} />;
    });
  };
  render() {
    const {
      navigation: { navigate }
    } = this.props;
    const {
      textSearch,
      loading,
      newNotification,
      subscription,
      refreshControlLoad,
      marketplaceOptions,
      listData,
      notFoudData,
      marketplaceFeature
    } = this.state;
    let featuredData = null;
    if (marketplaceFeature) {
      if (
        marketplaceFeature.featured &&
        marketplaceFeature.featured.length > 0
      ) {
        let marketPlaceList = marketplaceFeature.featured;
        marketPlaceList.map(item => {
          if (item.featured === "yes") {
            featuredData = item;
          }
        });
      }
    }
    return (
      <Container>
        <Header
          rightIcon="notification"
          barStyle="dark-content"
          text="Marketplace"
          newNotification={newNotification}
        />
        {subscription != 1 ? (
          <View style={styles.searchContainer}>
            <SearchInput
              textInput={textSearch}
              onChangeText={text =>
                marketplaceViewModel.setTextSearch(this, text)
              }
            />
          </View>
        ) : null}
        {subscription != "" && (
          <Content
            refreshControl={
              <RefreshControl
                refreshing={refreshControlLoad}
                onRefresh={async () =>
                  await marketplaceViewModel.handleGetCategories(this, true)
                }
              />
            }
          >
            {loading ? (
              <Loading style={{ top: "40%", alignSelf: "center" }} />
            ) : notFoudData ? (
              <View style={styles.container}>
                <Text>There are no vendors yet</Text>
              </View>
            ) : listData.length === 0 ? (
              <View style={styles.container}>
                {/* {marketplaceFeature.featured_image &&
                marketplaceFeature.featured ? (
                  <Featured
                    img={
                      marketplaceFeature.featured[0].image
                        ? marketplaceFeature.featured[0].image.url
                        : ""
                    }
                    marketplaceFeature={marketplaceFeature.featured[0]}
                    onPress={() => {
                      navigate("Info", {
                        data: marketplaceFeature.featured[0]
                      });
                    }}
                  />
                ) : null} */}
                {featuredData && featuredData !== null ? (
                  <Featured
                    img={featuredData.image ? featuredData.image.url : ""}
                    marketplaceFeature={featuredData}
                    onPress={() => {
                      navigate("Info", {
                        data: featuredData
                      });
                    }}
                  />
                ) : null}
                {this.renderFilters(info =>
                  navigate("Filter", { info, data: marketplaceOptions })
                )}
              </View>
            ) : (
              <View style={styles.containerCards}>
                {this.renderCards(info => navigate("Info", info))}
              </View>
            )}
          </Content>
        )}
      </Container>
    );
  }
}
export default MarketplaceView;
