import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Card from "utils/components/mediaCards";
import { shareContent, openUrl } from "utils/utils";
import { shareMsj } from "resources/l10n";
class AuditionMaterialsDetailsView extends Component {

  shareFile = async (data) => {
    const message = `${shareMsj.myMedia.message}\n${data.url}`
    const response = await shareContent(message);
    if (response) {
      showMsj("Shared", false);
    }
    return response;
  }

  openFile = (url) => {
    console.log(url);
    openUrl(url);
  }

  render() {
    const { navigation: { navigate, goBack, state: { params: { title, data } } } } = this.props;
    console.log('data', data);
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => goBack()}
          barStyle="dark-content"
          text={title}
        />
        <Content>
          <View style={styles.container}>
            {
              data &&
                data.length != 0 &&
                Array.isArray(data) ?
                data.map((option, i) => {
                  return (
                    <Card
                      key={i}
                      type={option.type}
                      fileText={option.name}
                      //deleteFile={this.deleteFile}
                      data={option}
                      mediaImage={option.thumbnail != null ? option.thumbnail : option.url}
                      isAudition={true}
                      shareFile={(data) => this.shareFile(data)}
                      openFile={this.openFile}
                    />
                  )
                })
                :
                <Text>
                  Not Have materials
              </Text>
            }
            {}
            {/* <Card type="document"  fileText="Script.pdf"/>
            <Card type="mp3"  fileText="Music.mp3"/> */}
          </View>
        </Content>
      </Container>
    );
  }
}
export default AuditionMaterialsDetailsView;
