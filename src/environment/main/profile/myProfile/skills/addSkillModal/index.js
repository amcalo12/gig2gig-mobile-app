import React, { Component } from "react";
import {
	View,
	Modal
} from "react-native";
import Header from "utils/components/appHeader";
import { Container, Content } from 'native-base';
import styles from "./index.styles";
import { showMsj, openUrl, shareContent } from "utils/utils";
import Input from "utils/components/mainInput";
import MainButton from "utils/components/mainButton";
import { createSkillsInUser } from "api/userApi";

class AddSkillModal extends Component {
	state = {
		visible: this.props.visible,
		skill: '',
		loading: false
	}

	componentWillUpdate(nextProps, nextState) {
		if (nextProps.visible == true && this.state.visible == false) {
			this.setState({
				visible: nextProps.visible
			})
		}
		if (nextProps.visible == false && this.state.visible == true) {
			this.setState({
				visible: nextProps.visible
			})
		}
	}
	async componentDidMount() {
	}

	onPressAddVideo = async () => {

		const { skill } = this.state

		this.setState({
			loading: true,
		})

		if (skill != '' && skill != null) {
			try {
				let sendData = {
					name: skill,
				}

				await createSkillsInUser(sendData)

				this.props.handleBackBackModal()

				showMsj('Successfull')

				this.setState({
					loading: false,
					skill: ''
				})
			} catch (error) {
				console.log(error)
				this.setState({
					loading: false
				})
			}
		} else {
			showMsj('Skill field is required', true)
			this.setState({
				loading: false,
			})
		}
	}

	render() {

		const {
			visible,
			skill,
			loading
		} = this.state

		return (
			<Modal
				animationType={'slide'}
				visible={visible}
			>
				<Container>
					<Header
						leftIcon="backDark"
						leftFn={() => this.props.handleBackBackModal()}
						barStyle="dark-content"
						// text="Add skill"
						text="Add New Skills"
					/>
					<Content
						bounces={false}
						contentContainerStyle={styles.content}
					>
						<Input
							data={skill}
							placeholder={'Add New Skills'}
							// placeholder={'Skill'}
							elevation={1.5}
							// customWidth={styles.input.width}
							onEdit={(skill) => this.setState({ skill })}
						/>
						<MainButton
							text={'Add skill'}
							loading={loading}
							onPress={() => this.onPressAddVideo()}
						/>
					</Content>
				</Container>
			</Modal>
		);
	}
}
export default AddSkillModal;
