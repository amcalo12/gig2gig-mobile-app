import React, { Component } from "react";
import { TouchableOpacity, Text, View, Linking, Platform } from "react-native";
import { Card, Spinner } from "native-base";
import styles from "./index.styles";
import moment from "moment";
import { Image } from 'react-native-elements';
import HTML from "react-native-render-html";
import { resize } from "utils/utils";
import { Assets } from "resources/assets";

type Props = {
  body: String,
  time_ago: String,
  url_media: String,
  title: String,
  onPress: Function
}

export default function PostBlog(props: Props) {

  const {
    body,
    time_ago,
    url_media,
    title,
    onPress,
    id
  } = props

  return (
    <TouchableOpacity
      activeOpacity={0.6}
      style={styles.container}
      onPress={() => onPress()}>
      <Card
        style={styles.cardContainer}>
        <Image
          // source={{ uri: url_media }}
          source={Assets.gig2gig}
          resizeMode={"cover"}
          placeholderStyle={{ backgroundColor: 'rgba(0,0,0,0.2)' }}
          style={styles.avatar}
          // defaultSource={Assets.gig_avatar}
        // PlaceholderContent={
        //   <Spinner
        //     color={'rgba(0,0,0,0.5)'}
        //     size={'small'}
        //   />
        // }
        />
        <View
          style={styles.row}
        >
          <Text
            style={styles.titleTxt}
          >
            {title}
          </Text>
          <Text
            style={styles.dateTxt}
          >
            {time_ago}
          </Text>
        </View>
        <Text
          style={styles.bodyTxt}
          numberOfLines={3}
        >
          {body.replace(/(<([^>]+)>)/g, "")}
        </Text>
        {/* <HTML
          html={body}
          containerStyle={{
            marginTop: resize(5),
          }}
          onLinkPress={(evt, href) => { Linking.openURL(href); }}
        /> */}
      </Card>
    </TouchableOpacity>
  );
}

PostBlog.defaultProps = {
  body: '',
  time_ago: '',
  url_media: '',
  title: '',
  onPress: () => { }
}