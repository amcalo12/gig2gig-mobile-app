import React, { Component } from "react";
import { View, Image, TouchableOpacity, AsyncStorage, TextInput } from "react-native";
import { Container, Content, Text, Card } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import ImageHeader from "utils/components/imageHeader";
import Button from "utils/components/mainButton";
import ButtonClear from "utils/components/secondaryButton";
import { StackActions } from "react-navigation";
import QRCode from "react-native-qr-generator";
import moment from "moment";
import PlusIcon from "resources/images/plus-icon.png";
import { colorPalette } from "resources/colors";
import myAuditionsApi from "api/myAuditionsApi";
import Loading from "utils/components/loading";
import { showMsj } from "utils/utils";
import { openSubscriptionDialog, checkSubscriptionAndRedirect } from "utils/cons";

class CheckInView extends Component {
  state = {
    auditionData: null,
    QRData: null,
    assignedNumber: "",
    loading: false,
    isAssignedNumber: false
  };

  async componentDidMount() {
    this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
    console.log('userData :>> ', this.userData);
    const auditionData = await this.props.navigation.state.params.data;
    const idUser = await this.props.navigation.state.params.idUser;
    console.log(JSON.stringify(auditionData));
    let QRData = JSON.stringify({
      appointmentId: auditionData.appointment_id,
      auditionId: auditionData.auditions_id,
      userId: idUser,
      rolId: auditionData.rol,
      hour: auditionData.hour
    });
    this.setState({
      auditionData,
      QRData,
      assignedNumber: auditionData.assign_no
        ? auditionData.assign_no !== null
          ? auditionData.assign_no
          : ""
        : "",
      isAssignedNumber: auditionData.assign_no
        ? auditionData.assign_no !== null
          ? true
          : false
        : false
    });
  }
  async onAddAssignedNumberClick() {
    if (this.state.QRData !== null) {
      this.setState({ loading: true });
      const auditionData = await this.props.navigation.state.params.data;
      const idUser = await this.props.navigation.state.params.idUser;
      const body = {
        user_id: idUser,
        appointment_id: auditionData.appointment_id,
        assign_no: this.state.assignedNumber
      };
      try {
        const res = await myAuditionsApi.addAssignedNumber(body);
        console.log("response== assigned number=====");
        console.log(JSON.stringify(res));
        this.setState({ loading: false, isAssignedNumber: true });
        if (this.props.navigation.state) {
          if (this.props.navigation.state.params) {
            if (this.props.navigation.state.params.refreshListCallback) {
              this.props.navigation.state.params.refreshListCallback();
            }
          }
        }
      } catch (e) {
        this.setState({ loading: false });

        if (e) {
          if (e.data) {
            if (e.data.message) {
              showMsj(e.data.message, true);
            }
          }
        }
      }
    }
  }

  render() {
    const { auditionData, QRData } = this.state;
    const {
      navigation: { navigate, dispatch }
    } = this.props;
    return (
      <Container>
        <ImageHeader
          size="small"
          image={auditionData ? auditionData.media : null}
        >
          <Header leftIcon="back" leftFn={() => this.props.navigation.pop()} />
        </ImageHeader>
        {auditionData && (
          <Content>
            <View style={styles.container}>
              <Text style={styles.title}>{auditionData.title}</Text>
              <Text style={styles.subTitle}>
                {auditionData.round ? "Round " + auditionData.round : ""}
              </Text>
              <Text style={styles.subTitle}>
                {moment(auditionData.date).format("MMMM Do, YYYY")}
              </Text>
              <Text style={styles.appointment}>
                {auditionData.hour && (
                  <Text style={styles.bold}>
                    {moment(auditionData.hour, "h:mma").format("h:mma")}
                  </Text>
                )}
                {auditionData.hour && ` Appointment`}
              </Text>
              {auditionData.group_number != null ? <Text style={styles.subTitle}>
                {"Group " + auditionData.group_number}
              </Text> : null}
              {QRData && (
                <QRCode
                  style={styles.code}
                  size={200}
                  value={QRData}
                  foregroundColor="#000000"
                  backgroundColor="#FFFFFF"
                />
              )}

              <View style={styles.buttons}>
                <View
                  activeOpacity={0.6}
                  style={styles.inputTextContainer}
                  onPress={() => { }}
                >
                  <TextInput
                    placeholder={"Add Number"}
                    style={styles.assignedNumberInput}
                    placeholderTextColor={colorPalette.purple}
                    placeholderStyle={{ textAlign: "center" }}
                    editable={!this.state.loading}
                    keyboardType="number-pad"
                    onChangeText={text => {
                      this.setState({ assignedNumber: text });
                    }}
                    value={
                      this.state.assignedNumber !== ""
                        ? this.state.assignedNumber.toString()
                        : ""
                    }
                  />
                  {this.state.loading && (
                    <Loading
                      style={{
                        position: "absolute",
                        left: 10,
                        padding: 10
                      }}
                    />
                  )}
                  {!this.state.loading && (
                    <TouchableOpacity
                      style={{
                        position: "absolute",
                        left: 10,
                        padding: 10
                      }}
                      onPress={() => {
                        this.onAddAssignedNumberClick();
                      }}
                    >
                      <Image source={PlusIcon} />
                    </TouchableOpacity>
                  )}
                </View>
              </View>
              <View style={styles.buttons}>
                <ButtonClear
                  text="View Updates"
                  onPress={() => navigate("Updates", { data: auditionData })}
                />
              </View>
              <View style={styles.buttons}>
                <ButtonClear
                  text="View Waitlist"
                  onPress={() => navigate("Waitlist", { data: auditionData })}
                />
              </View>
              <View style={styles.buttons}>
                <ButtonClear
                  text="Instant Feedback"
                  onPress={async () => {
                    if (this.userData.isPaidUser) {
                      const user_id = await this.props.navigation.state.params
                        .idUser;
                      navigate("OfflineInstantFeedback", {
                        data: auditionData,
                        userId: user_id
                      });
                    } else {
                      checkSubscriptionAndRedirect(this.userData, 'SubscriptionPlan', this.props)
                      // openSubscriptionDialog(() => {
                      //   this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true })
                      // })
                    }

                  }}
                />
              </View>
              <Button
                text="Finish"
                onPress={() => dispatch(StackActions.popToTop())}
              />
            </View>
          </Content>
        )}
      </Container>
    );
  }
}
export default CheckInView;
