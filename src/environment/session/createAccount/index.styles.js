import { Dimensions, StyleSheet, Platform } from "react-native";
// import { fontSizeDefault } from "utils/utils";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");


export default StyleSheet.create({
	container: {
		height: height
	},
	imageBackground: {
		height: height,
		width: width,
		position: "absolute",

	},
	backgroundImage: {
		height: height,
		width: width,
	},
	background: {
		width: '100%',
		height: height * 0.87,
		zIndex: 0,
		justifyContent: "space-around",
		position: "relative"
	},
	headText: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 21,
		color: colorPalette.white,
		alignSelf: "center"
	},
	formContainer: {
		alignItems: "center",
	},
	shortContainer: {
		width: width * 0.70,
		flexDirection: "row",
		justifyContent: "space-between",
	},
	logoContainer: {
		flexDirection: "row",
		alignItems: "flex-start",
		marginBottom: height * 0.08,
	},
	text: {
		color: colorPalette.white,
		fontFamily: fontStyles.break_semibold,
		fontSize: 51
	},
	mediumText: {
		marginTop: height * 0.015
	},
	formText: {
		color: colorPalette.white,
		fontFamily: fontStyles.nexa_bold,
		fontSize: 13
	},
	withMargin: {
		marginVertical: 15
	},
	positionText: {
		position: "absolute",
		bottom: 20,
		alignSelf: 'center',
	},
	userImage: {
		backgroundColor: "white",
		justifyContent: "center",
		alignItems: "center",
		width: height * 0.212893553223,
		height: height * 0.212893553223,
		borderRadius: 130,
		alignSelf: "center",
		overflow: "hidden"
	},
	userIcon: {
		alignSelf: "center",
		marginLeft: width * 0.05,
	},
	userPicture: {
		width: "100%",
		height: "100%",
		resizeMode: "cover"
	},
	loadingContainer: {
		position: "relative"
	},
	loading: {
		position: "absolute",
		bottom: resize(6, "height"),
		right: resize(-36),
		zIndex: 100
	},
	displayRow: {
		width: '75%',
		flexDirection: 'row',
		justifyContent: 'space-between'
	}
});
export const width1 = resize(119);
export const width2 = resize(145);