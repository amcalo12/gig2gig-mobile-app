import { StyleSheet, Platform } from "react-native";
import { fontStyles } from "resources/fonts";
import { colorPalette } from "resources/colors";

export const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: colorPalette.SHADOW_COLOR
  },
  modalSubContainer: {
    borderRadius: 8,
    backgroundColor: colorPalette.white,
    shadowOpacity: 0.2,
    elevation: 5,
    width: "80%",
    flex: 1,
    marginVertical: 30,
    //height: Platform.OS == "ios" ? "50%" : "90%",
    padding: 10,
    justifyContent: "center"
  },
  roleContainer: {
    height: 50,
    width: 50,
    // height: height * 0.0704647676162,
    // width: height * 0.0704647676162,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },
  roleImage: {
    height: "100%",
    width: "100%"
  },
  icon: {
    marginRight: 20,
    // height: 20,
    // width: 20
  }
});
