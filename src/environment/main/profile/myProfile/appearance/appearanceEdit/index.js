import React, { Component } from "react";
import { View, Platform } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import Input from "utils/components/mainInput";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
import Button from "utils/components/mainButton";
import DateSelect from "utils/components/dateSelect";
import appearanceEditViewModel from "./appearanceEditViewModel";
import Picker from "react-native-picker";

class AppearanceEditView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: "",
      weight: "",
      eyes: "",
      hair: "",
      race: "",
      flare: "",
      gender_pronouns: "",
      loading: false,
    };
  }
  componentWillMount() {
    const {
      navigation: {
        state: {
          params: { userInfo },
        },
      },
    } = this.props;
    if (userInfo) {
      if (userInfo !== null) {
        this.setState({
          height: userInfo.height ? userInfo.height : "",
          weight: userInfo.weight ? userInfo.weight : "",
        });
      }
    }
  }

  changeHair = (hair) => {
    this.setState({ hair: hair.name });
  };
  changeEye = (eye) => {
    this.setState({ eyes: eye.name });
  };
  changeRace = (race) => {
    console.log(" NEW RACE ", race);
    // this.setState({ race: race.name });
    this.setState({ race: race });
  };
  changeFlare = (flare) => {
    this.setState({ flare: flare.name });
  };

  changePronouns = (pronouns) => {
    this.setState({ gender_pronouns: pronouns.name });
  };

  render() {
    const {
      navigation: {
        navigate,
        goBack,
        state: {
          params: { userInfo },
        },
      },
    } = this.props;
    const {
      height,
      weight,
      eyes,
      hair,
      race,
      flare,
      gender_pronouns,
      loading,
    } = this.state;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => {
            goBack();
            Picker.hide();
          }}
          barStyle="dark-content"
          text="Appearance"
        />
        <Content contentContainerStyle={{ paddingBottom: 100 }}>
          <View style={styles.container}>
            <View style={styles.form}>
              {Platform.OS == "ios" ? (
                <Input
                  data={`${height} Height`}
                  isTouchable
                  elevation={1.5}
                  onPress={() => appearanceEditViewModel.showPickerIOS(this)}
                  customWidth={styles.input.width}
                />
              ) : (
                <DateSelect
                  customWidth={"100%"}
                  placeholder={`${userInfo.height} height`}
                  icon={DownArrowIcon}
                  data={appearanceEditViewModel.getFeets()}
                  onChange={(height) => this.setState({ height })}
                  elevation={1.5}
                  lng
                />
              )}
              <Input
                // placeholder={`${userInfo.weight}`}
                placeholder="Weight"
                data={`${weight}`}
                keyboardType={"decimal-pad"}
                elevation={1.5}
                // value={this.state.weight}
                onEdit={(weight) => this.setState({ weight })}
                customWidth={styles.input.width}
              />
              <Input
                placeholder={userInfo.hair}
                data={hair}
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("Hair", { onGoBack: this.changeHair });
                }}
              />
              <Input
                placeholder={userInfo.eyes}
                data={eyes}
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("EyeColor", { onGoBack: this.changeEye });
                }}
              />
              <Input
                placeholder="Ethnicity"
                data={race}
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("Ethnicity", {
                    onGoBack: this.changeRace,
                    race: userInfo.race,
                  });
                }}
              />

              <Input
                data={flare}
                // placeholder={
                //   userInfo.personal_flare
                //     ? userInfo.personal_flare !== ""
                //       ? userInfo.personal_flare
                //       : "Personal Flare"
                //     : "Personal Flare"
                // }
                placeholder={
                  userInfo.personal_flare
                    ? userInfo.personal_flare !== ""
                      ? userInfo.personal_flare
                      : "Personal Flair"
                    : "Personal Flair"
                }
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("PersonalFlare", { onGoBack: this.changeFlare });
                }}
              />

              <Input
                data={gender_pronouns}
                placeholder={
                  userInfo.gender_pronouns
                    ? userInfo.gender_pronouns != null
                      ? userInfo.gender_pronouns
                      : "Gender Pronouns"
                    : "Gender Pronouns"
                }
                elevation={1.5}
                customWidth={styles.input.width}
                isTouchable
                onPress={() => {
                  Picker.hide();
                  navigate("GenderPronouns", { onGoBack: this.changePronouns });
                }}
              />
            </View>
            <View style={styles.button}>
              <Button
                loading={loading}
                text="Update"
                onPress={() => appearanceEditViewModel.handlePressSave(this)}
              />
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
export default AppearanceEditView;
