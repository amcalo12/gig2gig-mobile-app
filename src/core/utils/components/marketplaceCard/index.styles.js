import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils"
import { DEVICE } from "utils/cons";


export default StyleSheet.create({
  card: {
    // width: resize(149),
    // borderWidth: 5,
    // borderColor: 'red',
    // width: (DEVICE.DEVICE_WIDTH - 50),
    width: resize(320),
    height: resize(185, "height"),
    borderRadius: 4,
    overflow: "hidden",
    marginBottom: resize(20, "height"),
  },
  imageContainer: {
    width: "100%",
    height: "70%"
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  textContainer: {
    height: "30%",
    justifyContent: "center",
    alignItems: 'center',
    paddingHorizontal: resize(10),
  },
  text: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 14,
    color: colorPalette.purple,
    textAlign: 'left'
  }
});