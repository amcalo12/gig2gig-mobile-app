import { createStackNavigator } from "react-navigation";
import RepresentationView from ".";
import RepresentationEditView from "./representationEdit"

const RepresentationStack = createStackNavigator(
  {
     Representation: {
       screen: RepresentationView,
       navigationOptions: {
         header: null
       }
     },
      RepresentationEdit: {
       screen: RepresentationEditView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "Representation",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default RepresentationStack;
