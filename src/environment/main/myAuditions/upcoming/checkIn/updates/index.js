import React, { Component } from "react";
import { View, Image } from "react-native";
import { Container, Content, Text } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import ImageHeader from "utils/components/imageHeader";
import CheckedIcon from "resources/images/myAuditions/checked.png";
import CheckIcon from "resources/images/myAuditions/check.png";
import Loading from "utils/components/loading";
import moment from "moment";
import myAuditionsApi from "api/myAuditionsApi";
class CheckInView extends Component {

	state = {
		data: null,
		updates: [],
		messages: null,
		loading: false
	}

	async componentDidMount() {
		const data = this.props.navigation.state.params.data;
		this.setState({
			data,
			loading: true
		});
		try {
			const response = await myAuditionsApi.getAuditionUpdate(data.appointment_id);
			console.log(response);
			this.setState({
				messages: response.data.reverse(), loading: false
			})
		} catch (e) {
			console.log(e)
			this.setState({ loading: false });
		}
	}

	render() {
		const { data, loading, messages } = this.state;
		const { navigation: { goBack } } = this.props;
		return (
			<Container>
				<ImageHeader image={data ? data.media : null}>
					<Header
						leftIcon="back"
						leftFn={() => goBack()}
					/>
				</ImageHeader>
				<Content contentContainerStyle={styles.content}>

					{
						data ?
							<View style={styles.container}>
								<Text style={styles.title}>{data.title}</Text>
								<Text style={styles.subTitle}>{moment(data.date).format("MMMM Do, YYYY")}</Text>
								<Text style={styles.titleUpdate}>Updates</Text>
								{

								}
								{loading && <Loading />}

								{
									(messages && messages.length > 0) ?
										messages.map((option, i) => (
											<View key={i} style={styles.update}>
												<View style={styles.updateCheck}>
													<Image source={i === 0 ? CheckedIcon : CheckIcon} />
												</View>
												<View style={styles.labels}>
													<Text style={styles.updateLabel}>{option.title}</Text>
													<Text style={styles.updateLabelTime}>{moment(moment(option.create_at).format()).fromNow()}</Text>
												</View>
											</View>
										))
										:
										<Text style={styles.titleUpdate}>No updates yet</Text>
								}

							</View>
							:
							<Loading />
					}
				</Content>
			</Container>
		);
	}
}
export default CheckInView;
