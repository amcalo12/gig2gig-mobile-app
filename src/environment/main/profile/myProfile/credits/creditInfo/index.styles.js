import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container: {
    height:resize(520, "height"),
    width: resize(375),
    flexDirection:"column",
    justifyContent:"space-between",
  },
  labels:{
    paddingLeft: resize(40),
  },
  labelGroup:{
    flexDirection:"row"
  },
});