import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const getAppearance = async props =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.APPEARANCE}${API_ROUTES.APPEARANCE.GET_APPEARANCE}`,
  );
const createAppearance = async props =>
  apiHandler(
    API_METHOD.POST,
    `${API_MODULES.APPEARANCE}`,
    props
  );
const updateAppearance = async (props, id) =>
  apiHandler(
    API_METHOD.PUT,
    `${API_MODULES.APPEARANCE}${API_ROUTES.APPEARANCE.UPDATE}/${id}`,
    props
  );


export default {
  getAppearance,
  createAppearance,
  updateAppearance
};
