import React, { Component } from "react";
import { WebView } from 'react-native-webview';
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import termsOfUseViewModel from './termsOfUseViewModel'
class SubscriptionView extends Component {
	state = {
		info: ''
	}
	componentDidMount = async () => {
		await termsOfUseViewModel.getInfo(this)
	};
	render() {
		const { navigation: { navigate, dispatch } } = this.props;
		const { info } = this.state
		return (
			<Container>
				<Header
					leftIcon="backDark"
					leftFn={() => dispatch(StackActions.popToTop())}
					barStyle="dark-content"
					text="Terms of Use"
				/>
				<WebView
					style={styles.container}
					source={{ html: info }}
				/>
			</Container>
		);
	}
}
export default SubscriptionView;
