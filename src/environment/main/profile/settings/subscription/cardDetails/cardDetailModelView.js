import { showMsj } from "utils/utils";
import { AsyncStorage } from 'react-native';
import moment from 'moment';
import subscriptionApi from "api/subscriptionApi";

const addPaymentMethod = async (context, data) => {
  context.setState({data})
}
const updateCard = async (context, data) => {
  context.setState({data})
}
const getPaymentInfo = async (context) => {
  try {
    const date = moment().add(1, 'M').format('MM-YYYY')
    const res = await subscriptionApi.getPaymentMethod()
    if(res.id){
      context.setState({data: res, date})
    }else{
      context.setState({data:null})
    }
    context.setState({loading:false})
  } catch (error) {
    context.setState({loading:false})
    if(error.data.data){
      
    }else{
      return showMsj('Error', true)
    }
  }
}
const getPlan = async (context) => {
  const userData = JSON.parse(await AsyncStorage.getItem("userData"));
  context.setState({plan:userData.details.subscription})
}
export default {
  getPaymentInfo,
  addPaymentMethod,
  getPlan,
  updateCard
}