import pushNotificationApi from "api/pushNotificationApi";
import { showMsj } from "utils/utils";
import { errorsValidateMsj } from "resources/l10n";
import { getUserSettings, updateUserSettings } from "api/userApi";

const getMyAuditionConfig = async (context) => {
	try {
		const res = await getUserSettings()
		const data = res.data

		const viewFeedback = data.filter(res => res.setting == 'FEEDBACK')[0]
		const viewRecommendations = data.filter(res => res.setting == 'RECOMMENDATION')[0]

		context.setState({
			loading: false,
			viewFeedback,
			viewRecommendations
		})
	} catch (error) {
		console.log(error);
	}
}

const updateFeedback = async (context, key, id) => {
	const { state } = context

	try {
		state[key].value = state[key].value === 0 ? 1 : 0
		await updateUserSettings({
			value: state[key].value === 0 ? false : true
		}, id)
		context.setState({
			...state
		})
	} catch (error) {
		console.log('error', error)
	}
}

export default {
	updateFeedback,
	getMyAuditionConfig
}