import React from 'react';
import { View, ImageBackground, Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles';

//Images
import Back from '../../../../resources/images/audition/demo.jpeg';
import Info from '../../../../resources/images/marketplace/info.png';

const Featured = ({ marketplaceFeature, img, onPress }) => (
	<TouchableOpacity style={styles.container}
		activeOpacity={1}
		onPress={() => {
			onPress()
		}}>
		<View style={styles.imageContainer}>
			<ImageBackground
				style={styles.image}
				source={{ uri: img }}
				resizeMode='cover'>
				<ImageBackground
					source={Info}
					resizeMode='stretch'
					style={styles.ticket}>
					<Text style={styles.infoText}>Featured</Text>
				</ImageBackground>
			</ImageBackground>
		</View>
		<View style={styles.info}>
			<Text style={styles.infoText2}>{marketplaceFeature.title}</Text>
		</View>
	</TouchableOpacity>
);
export default Featured;
