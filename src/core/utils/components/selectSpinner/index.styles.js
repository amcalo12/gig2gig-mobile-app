import { StyleSheet, Platform, Dimensions } from 'react-native';
import { fontStyles } from 'resources/fonts';
import { colorPalette } from 'resources/colors';
import { resize } from "utils/utils";
const { width, height } = Dimensions.get("screen");

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  top: {
    width: "60%",
    position: 'absolute',
    top: 0
  },
  selectContainer: {
    width: "60%",
    backgroundColor: "#FFF",
    maxHeight: "50%",
    ...Platform.select({
      ios: {
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.2,
        elevation: 10,
        position: "relative",
        borderColor: "rgb(255,255,255)",
        borderWidth: 1
      }
    })
  },
  selectScroll: {
    ...Platform.select({
      android: {
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.2,
        elevation: 10,
        position: "relative",
        borderColor: "rgb(255,255,255)",
        borderWidth: 1
      }
    })
  },
  selectTitle: {
    width: width * 0.6,
    backgroundColor: "rgb(255,255,255)",
    paddingVertical: height * 0.0149925037481,
    alignItems: "center",
    justifyContent: "center"
  },
  selectOptions: {
    width: width * 0.6,
    backgroundColor: "rgb(255,255,255)",
    paddingVertical: height * 0.0149925037481,
    alignItems: "center",
    justifyContent: "center"
  },
  selectOptionsTitleText: {
    textAlign: "center",
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple
  },
  selectOptionsText: {
    textAlign: "center",
    fontFamily: fontStyles.nexa_light,
    color: colorPalette.purple
  }
});