import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Label from "utils/components/unionMemberLabel";
import { AndroidBackHandler } from 'react-navigation-backhandler';
import { hairOptions } from "resources/l10n";
import Input from "utils/components/mainInput";

class HairView extends Component {
  state = {
    hairs: hairOptions,
    other: "",
    isOther: false
  }
  handleBackPress = () => {
    const { navigation: { goBack } } = this.props;
    return goBack()
    return true;
  }
  changeChecked = (option, i) => {
    const { navigation: { navigate, state: { params: { onGoBack } } } } = this.props;
    const { hairs } = this.state
    const hairs2 = []
    hairs.map(res => {
      if (res.name === option.name) {
        res.checked = true
      } else {
        res.checked = false
      }

      if (res.name == "Other") {
        this.state.isOther = res.checked
      }
      hairs2.push(res)
    })
    this.setState({ hairs: hairs2 })
    onGoBack(option)
  }
  render() {
    const { hairs } = this.state
    const { navigation: { navigate, state: { params: { onGoBack } } } } = this.props;
    return (
      <AndroidBackHandler onBackPress={() => this.handleBackPress()}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => this.handleBackPress()}
            barStyle="dark-content"
            text="Hair Color"
          />
          <Content>
            <View style={styles.container}>
              {
                hairs.map((option, idx) => (
                  <Label key={idx} text={option.name} checked={option.checked} onPress={() => this.changeChecked(option, idx)} />
                ))
              }
              {this.state.isOther ?
                <View style={{ marginTop: 10, marginRight: 10, marginLeft: 0 }}>
                  <Input placeholder="Add Hair Color" data={this.state.other} onEdit={other => {
                    this.setState({ other }, () => {
                      onGoBack({ name: other })
                    })
                  }}
                    elevation={3} customWidth={styles.input.width} />
                </View>
                : null}
            </View>
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default HairView;
