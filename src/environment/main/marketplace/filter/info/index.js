import React, { Component } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { Container, Content, Text } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import ImageHeader from "utils/components/imageHeader";
import LocationIcon from "resources/images/marketplace/location-icon.png";
import PhoneIcon from "resources/images/marketplace/phone-icon.png";
import WebIcon from "resources/images/marketplace/web-icon.png";
import DownloadIcon from "resources/images/download-clear.png";
import { Linking } from "react-native";
import { resize } from "utils/utils";
import infoViewModel from "./infoViewModel";
class InfoView extends Component {
  render() {
    const {
      navigation: { goBack }
    } = this.props;
    const {
      image,
      title,
      address,
      phone_number,
      url_web,
      services
    } = this.props.navigation.state.params.data;
    return (
      <Container>
        <ImageHeader
          image={
            image ? image.url : "https://f4.bcbits.com/img/a1173748748_16.jpg"
          }
        >
          <Header
            leftIcon="back"
            leftFn={() => goBack()}
            rightIcon={DownloadIcon}
            rightFn={() => infoViewModel.share(title)}
          />
        </ImageHeader>
        <Content>
          <View style={styles.content}>
            <Text style={styles.title}>{title}</Text>
            {address != '' && address != null ? <TouchableOpacity
              style={styles.label}
              onPress={() =>
                Linking.openURL(
                  `https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(
                    address
                  )}`
                )
              }
            >
              <View style={styles.labelLeft}>
                <Image source={LocationIcon} />
              </View>
              <View style={styles.labelRight}>
                <Text style={styles.labelText}>{address}</Text>
              </View>
            </TouchableOpacity> : null}
            {phone_number != "" && phone_number != null ?
              <TouchableOpacity
                style={styles.label}
                onPress={() => Linking.openURL(`tel:${phone_number}`)}
              >
                <View style={styles.labelLeft}>
                  <Image source={PhoneIcon} />
                </View>
                <View style={styles.labelRight}>
                  <Text style={styles.labelText}>{phone_number}</Text>
                </View>
              </TouchableOpacity>
              : null}
            {url_web != "" && url_web != null && url_web && (
              <TouchableOpacity
                style={styles.label}
                onPress={async () => {
                  const res = await Linking.canOpenURL(`${url_web}`);
                  res
                    ? Linking.openURL(`${url_web}`)
                    : Linking.openURL(`https://${url_web}`);
                }}
              >
                <View style={styles.labelLeft}>
                  <Image source={WebIcon} />
                </View>
                <View style={styles.labelRight}>
                  <Text style={styles.labelText}>{url_web}</Text>
                </View>
              </TouchableOpacity>
            )}

            <View>
              <Text style={styles.services}>Services</Text>
              <Text style={styles.serviceText}>{services}</Text>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
export default InfoView;
