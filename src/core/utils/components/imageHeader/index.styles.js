import { Dimensions, StyleSheet, Platform } from "react-native";
import { resize } from "utils/utils"
const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");

export default StyleSheet.create({
  imageBackground: {
      width: width,
      backgroundColor:"#f1f1f1"
      // position: "absolute",
      // zIndex:1
  },
  large:{
    height: height * 0.289355322339,
  },
  small:{
    height:resize(110, "height")
  },
  // backgroundImage: {
  //   height: "100%",
  //   width: width,
  // },
});