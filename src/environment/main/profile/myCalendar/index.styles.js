import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    width: '100%',
    alignItems:"center",
    alignSelf: 'center',
    marginTop : resize(20, "height")
  },
});