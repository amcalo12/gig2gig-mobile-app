import React, { Component } from 'react';
import { Image, Platform, StatusBar, View } from 'react-native';
import { Header as NBHeader, Left, Body, Right, Title, Button, Text } from 'native-base';
import styles from './index.styles';
// import MenuIcon from '../../assets/img/icons/Header/menu-icon.png';
// import MessageIcon from '../../assets/img/icons/Header/message-icon.png'
import BackIcon from "resources/images/back-icon.png"
import { SafeAreaView } from 'react-navigation';
export default class Header extends Component {
  // inside your component class
  componentWillMount() {
    if (Platform.OS === 'android') {
      StatusBar.setTranslucent(true);
    }
  }

  render() {
    const { name, leftIcon, rightIcon, style, texto } = this.props;
    return (

      <View
        style={[styles.header]}
      >
        <StatusBar backgroundColor="transparent" barStyle="light-content" />
        <SafeAreaView />
        <View style={[styles.grid, , style]}>
          {
            leftIcon && leftIcon != 'none' ? (
              <View style={texto ? styles.isText : styles.headerLeft}>
                <Button
                  transparent
                  style={{ padding: 10 }}
                  onPress={this.props.leftFn}
                >
                  <Image source={leftIcon} />
                </Button>
              </View>) : (<View style={styles.headerLeft} />)
          }
          <View style={styles.headerBody}>
            <View style={styles.logoHeader}>
              <Text style={styles.logoHeaderText}>gig</Text>
              <Text style={[styles.logoHeaderText, styles.mediumLogo]}>2</Text>
              <Text style={styles.logoHeaderText}>gig</Text>
            </View>
          </View>
          {
            rightIcon && rightIcon != 'none' ? (
              <Right style={texto ? styles.isText : styles.headerRight}>
                <Button
                  transparent
                  onPress={this.props.rightFn}
                >
                  {
                    texto ?
                      <Text>{texto}</Text> :
                      <Image source={rightIcon} />
                  }
                </Button>
              </Right>) : (<Right style={styles.headerRight} />)
          }
        </View>
      </View>
    );
  }
}

Header.defaultProps = {
  name: 'gig2gig',
  leftIcon: BackIcon,
  rightIcon: "none"
}