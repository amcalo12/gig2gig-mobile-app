import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text,
  Image
} from 'react-native';
import styles from './index.styles';
import CheckIcon from "resources/images/check-clear.png";
import CheckedIcon from "resources/images/check-checked.png";

export default class InfoLabel extends Component {
  getStyles = () => {
    const { noLine } = this.props;
    let labelStyles = [styles.item];

    if (noLine) {
      labelStyles.push(styles.noLine);
    }

    return labelStyles;
  }
  render() {
    const { text, checked, onPress } = this.props;
    return (
      <TouchableOpacity style={this.getStyles()} onPress={() => onPress()}>
        <Text style={styles.itemText}>{text}</Text>
        <Image style={styles.icon} source={checked ? CheckedIcon : CheckIcon} />
      </TouchableOpacity>
    );
  }
}