import { apiHandler } from "./apiMethods";
import { API_METHOD, API_ROUTES, API_MODULES } from "utils/cons";

const getSettings = async () =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.SETTING}`,
  );


export default {
  getSettings
};
