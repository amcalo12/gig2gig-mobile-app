import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
    customDotContainer:{
        flexDirection:'row', 
        alignItems:'center',
    },
    days:{
        fontFamily: fontStyles.nexa_bold,
        fontSize:15
    }
  });