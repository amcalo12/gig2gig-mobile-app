import React, { Component } from "react";
import {
  Image,
  Platform,
  StatusBar,
  View,
  AsyncStorage,
  TouchableOpacity,
} from "react-native";
import {
  Header as NBHeader,
  Left,
  Body,
  Right,
  Title,
  Button,
  Text,
  Spinner,
} from "native-base";
import styles from "./index.styles";
import BackIcon from "resources/images/back-icon.png";
import BackIconPurple from "resources/images/back-icon-purple.png";
import NotificationIcon from "resources/images/notification-icon.png";
import FilterIcon from "resources/images/tabNavigation/filter-icon.png";
import SearchInput from "utils/components/searchInput";
import firebase from "react-native-firebase";
import Notification from "environment/main/Notifications";
import CloseIcon from "resources/images/close-icon-purple.png";
import { SafeAreaView } from "react-navigation";

//Image
import Flag from "../../../../resources/images/flag.png";
import { Assets } from "resources/assets";
import { fontStyles } from "resources/fonts";
export default class Header extends Component {
  constructor(props) {
    super(props);
    const { newNotification } = this.props;
    this.state = {
      newNotification,
    };
  }
  componentDidMount() {
    this._getNotification();
  }
  _getNotification = async () => {
    this.notificationListener = firebase
      .notifications()
      .onNotification(async (notification) => {
        this.setState({ newNotification: true });
      });
  };
  componentWillMount() {
    if (Platform.OS === "android") {
      StatusBar.setTranslucent(true);
    }
  }
  show = () => {
    this.refs.modal.setModalVisible();
  };
  componentWillReceiveProps(n) {
    this.setState({ newNotification: n.newNotification });
  }
  handleLeftHeader = () => {
    const { leftIcon } = this.props;
    let component;
    if (leftIcon === "close") {
      console.log("close");
      component = (
        <Left style={styles.headerLeft}>
          <Button
            transparent
            style={{ width: "100%" }}
            onPress={this.props.leftFn}
          >
            <Image source={CloseIcon} />
          </Button>
        </Left>
      );
    } else if (leftIcon && leftIcon != "none") {
      component = (
        <Left
          style={[
            styles.headerLeft,
            this.props.rightText && this.props.rightText !== "none"
              ? { flex: 3 }
              : { flex: 1 },
          ]}
        >
          <Button
            transparent
            style={{ width: "100%" }}
            onPress={this.props.leftFn}
          >
            <Image
              source={
                leftIcon === "back"
                  ? BackIcon
                  : leftIcon === "backDark"
                  ? BackIconPurple
                  : leftIcon
              }
            />
          </Button>
        </Left>
      );
    } else {
      component = <Left style={styles.headerLeft} />;
    }
    return component;
  };
  handleRightHeader = () => {
    const { newNotification } = this.state;
    const {
      rightIcon,
      rightFn,
      showSecond,
      loadingFlag,
      rightFn2,
      rightText,
    } = this.props;
    let component;
    if (rightText && rightText !== "none") {
      component = (
        <Right style={{ flex: 3 }}>
          <TouchableOpacity style={styles.rowIcons} onPress={() => rightFn()}>
            <Text
              style={[
                styles.logoHeaderText,
                { fontSize: 14, fontFamily: fontStyles.nexa_bold },
              ]}
            >
              {this.props.rightText}
            </Text>
          </TouchableOpacity>
        </Right>
      );
    } else if (rightIcon === "none") {
      component = <Right style={styles.headerRight} />;
    } else if (rightIcon && rightIcon != "none") {
      component = (
        <Right style={styles.headerRight}>
          <View style={styles.rowIcons}>
            {showSecond && (
              <Button
                style={styles.separator}
                transparent
                onPress={() => rightFn2()}
              >
                {loadingFlag ? (
                  <Spinner color={"white"} />
                ) : (
                  <Image source={Flag} />
                )}
              </Button>
            )}
            <Button
              transparent
              onPress={async () => {
                rightIcon === "notification" ? this.show() : rightFn();
                rightIcon === "notification" &&
                  (await AsyncStorage.removeItem("newNotification"));
                rightIcon === "notification" &&
                  this.setState({ newNotification: false });
              }}
            >
              <Image
                source={
                  rightIcon === "notification" ? NotificationIcon : rightIcon
                }
              />
            </Button>
          </View>

          {rightIcon === "notification" && newNotification && (
            <View style={styles.bangeNotification} />
          )}
          {rightIcon === "notification" && <Notification ref="modal" />}
        </Right>
      );
    }
    return component;
  };
  handleBodyHeader = () => {
    const { clear, text, isSelect, onPress } = this.props;
    let component;
    if (!clear) {
      component = (
        <View style={styles.logoHeader}>
          <Text style={styles.logoHeaderText}>gig</Text>
          <Text style={[styles.logoHeaderText, styles.mediumLogo]}>2</Text>
          <Text style={styles.logoHeaderText}>gig</Text>
        </View>
      );
    } else if (isSelect) {
      component = isSelect;
    } else if (text) {
      component = (
        <TouchableOpacity
          activeOpacity={1}
          style={{ flexDirection: "row", alignItems: "center" }}
          onPress={this.props.onTitlePress}
        >
          <Title style={styles.headerTitle}>{text}</Title>
          {this.props.isAudition ? <Image source={Assets.downArrow} /> : null}
        </TouchableOpacity>
      );
    }
    return component;
  };

  withoutSearch = () => {
    const { barStyle, clear, noMargin } = this.props;
    return (
      <View
        style={[styles.header, noMargin && styles.noMargin]}
        iosBarStyle={"light-content"}
        androidStatusBarColor="#31D2F4"
        transparent
        translucent
        noShadow
      >
        <StatusBar backgroundColor="transparent" barStyle="dark-content" />
        {this.handleLeftHeader()}
        <Body style={styles.headerBody}>{this.handleBodyHeader()}</Body>
        {this.handleRightHeader()}
      </View>
    );
  };

  withSearch = () => {
    const {
      onPress,
      onChangeText,
      value,
      onSearch,
      onPressclearData,
    } = this.props;
    return (
      <View style={styles.header}>
        <StatusBar backgroundColor="transparent" barStyle="dark-content" />
        <View style={styles.searchContainer}>
          <SearchInput
            leftIcon={FilterIcon}
            onPressclearData={() => onPressclearData()}
            onChangeText={(text) => onChangeText(text)}
            value={value}
            onSearch={() => onSearch()}
            onPress={onPress}
          />
        </View>
      </View>
    );
  };

  render() {
    const { isSearch } = this.props;
    if (isSearch) {
      return (
        <View>
          <SafeAreaView />
          {this.withSearch()}
        </View>
      );
    } else {
      return (
        <View>
          <SafeAreaView />
          {this.withoutSearch()}
        </View>
      );
    }
  }
}

Header.defaultProps = {
  name: "gig2gig",
  leftIcon: "none",
  rightIcon: "none",
  isSearch: false,
  isSelect: null,
  barStyle: "light-content",
  clear: true,
  text: null,
  noMargin: false,
};
