import { Platform } from "react-native";
import firebase from "react-native-firebase";
import * as Firebase from "firebase";
import moment from "moment";
import RNFetchBlob from "rn-fetch-blob";
let { fetch } = RNFetchBlob;

const defaultMetadata = {
  contentType: "image/jpeg"
};

const defaultName = moment().format("HHmmss");

const instance = firebase.storage();

const uploadMusicFiles = async (
  reference,
  uri,
  name = defaultName,
  metadata = defaultMetadata
) => {
  try {
    let filePath;
    console.log("uri", uri);
    if (Platform.OS === "android") {
      let ext = "." + uri.substring(uri.lastIndexOf('.') + 1, uri.length)
      console.log('name + ext :', name + ext);
      if (uri.includes("raw%3A")) {
        filePath = uri.split("raw%3A")[1].replace(/\%2F/gm, "/");
      } else {
        filePath = uri;
      }
      let temp_filepath = filePath;
      if (temp_filepath.includes("content://com.google.android")) {
        console.log("true");
        return temp_filepath;
      } else if (temp_filepath.includes("content://com.android")) {
        let valor = await RNFetchBlob.fs.stat(filePath);
        console.log("document valor", valor);
        let uploadedFile = await firebase
          .storage()
          .ref(reference)
          .child(name + ext)
          .putFile(valor.path, metadata);
        return uploadedFile.downloadURL;
      } else {
        console.log("false");
        let uploadedFile = await firebase
          .storage()
          .ref(reference)
          .child(name + ext)
          .putFile(uri, metadata);
        return uploadedFile.downloadURL;
      }
    } else {
      let ext = "." + uri.substring(uri.lastIndexOf('.') + 1, uri.length)
      console.log('name + ext :', name + ext);
      let uploadedFile = await firebase
        .storage()
        .ref(reference)
        .child(name + ext)
        .putFile(uri, metadata);
      return uploadedFile.downloadURL;
    }
  } catch (error) {
    console.log("error" + JSON.stringify(error));
    return error;
  }
};

export default uploadMusicFiles;
