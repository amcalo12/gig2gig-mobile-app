import { Dimensions, StyleSheet, Platform } from "react-native";
// import { fontSizeDefault } from "utils/utils";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");


export default StyleSheet.create({
  container: {
    height: height
  },
  imageBackground: {
    height: height,
    width: width,
    position: "absolute",
  },
  backgroundImage: {
    height: height,
    width: width,
  },
  background: {
    width: '100%',
    height: height * 0.87,
    zIndex: 0,
    justifyContent: "center",
    position: "relative"
  },
  textForgot: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 21,
    position: "absolute",
    top: height * 0.05,
    color: "#FFFFFF",
    alignSelf: "center"
  },
  formContainer: {
    alignItems: "center"
  },
  logoContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    marginBottom: height * 0.08,
  },
  text: {
    color: colorPalette.white,
    fontFamily: fontStyles.break_semibold,
    fontSize: 51
  },
  mediumText: {
    marginTop: height * 0.015
  },
  formText: {
    color: colorPalette.white,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 13
  },
  withMargin: {
    marginVertical: 15
  },
  positionText: {
    position: "absolute",
    bottom: 20,
    alignSelf: 'center',
  }
});