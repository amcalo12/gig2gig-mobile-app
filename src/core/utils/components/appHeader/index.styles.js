import { Dimensions, StyleSheet, Platform } from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
	header: {
		width: '100%',
		paddingHorizontal: resize(20),
		flexDirection: 'row',
		justifyContent: 'center',
		marginTop: Platform.OS === 'android' ? 25 : 0
	},
	noMargin: {
		marginTop: 0
	},
	bangeNotification: {
		backgroundColor: 'red',
		minWidth: 10,
		minHeight: 10,
		position: 'absolute',
		top: 8,
		right: 5,
		borderRadius: (10 / 2)
	},
	headerLeft: {
		flex: 1,
	},
	isText: {
		flex: 3
	},
	headerRight: {
		flex: 1
	},
	headerBody: {
		flex: 3,
		justifyContent: 'center',
		alignItems: "center",
	},
	headerTitle: {
		fontFamily: fontStyles.nexa_bold,
		alignSelf: 'center',
		color: colorPalette.purple,
		fontSize: 18,
		letterSpacing: 0.9,
		...Platform.select({
			ios: {
				marginRight: resize(6),
			},
		}),
	},
	logoHeader: {
		flexDirection: "row",
		alignSelf: 'center',
		marginTop: height * 0.002
	},
	logoHeaderText: {
		fontFamily: fontStyles.break_semibold,
		fontSize: 28,
		color: colorPalette.purple
	},
	mediumLogo: {
		marginTop: height * 0.009,
		fontFamily: fontStyles.break_bold
	},
	container: {
		width: width,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: 'center'
	},
	searchContainer: {
		width: resize(330),
		alignSelf: 'center',
	},
	rowIcons: {
		flexDirection: 'row'
	},
	separator: {
		marginRight: resize(15)
	}
});