import React, { Component } from "react";
import { View, Text } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import Input from "utils/components/mainInput";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
import Button from "utils/components/mainButton";
import Select from "utils/components/selectSpinner";
import ButtonClear from "utils/components/secondaryButton";
import DateSelect from "utils/components/dateSelect";
import { auditionFilters, months } from "resources/l10n";
import addCreditViewModel from "./addCreditViewModel";
import { fontStyles } from "resources/fonts";
import moment from "moment";

class AddCredit extends Component {
  state = {
    productionType: "",
    projectName: "",
    role: "",
    director: "",
    show: true,
    month: "",
    year: "",
    endYear: moment().year().toString(),
    loading: false,
  };
  componentDidMount() {
    this.setState({ endYear: moment().year().toString() });
  }
  render() {
    const {
      navigation: { navigate, goBack },
    } = this.props;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => goBack()}
          barStyle="dark-content"
          text="Credits"
        />
        <Content>
          <View style={styles.container}>
            <View style={styles.form}>
              <Input
                icon={DownArrowIcon}
                data={this.state.productionType}
                placeholder={"Production Type"}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
                isTouchable
                onPress={() => addCreditViewModel.select(this)}
              />
              <Input
                alignTop
                placeholder="Project Name"
                onEdit={(projectName) => this.setState({ projectName })}
                data={this.state.projectName}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <Input
                alignTop
                placeholder="Role"
                onEdit={(role) => this.setState({ role })}
                data={this.state.role}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <Input
                alignTop
                placeholder="Director/Production Company"
                onEdit={(director) => this.setState({ director })}
                data={this.state.director}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <View style={styles.inputGroup}>
                <DateSelect
                  placeholder={"Start Year"}
                  icon={DownArrowIcon}
                  data={addCreditViewModel.convertYears()}
                  onChange={(year) => this.setState({ year })}
                  short
                  // lng
                  margin={10}
                  // customWidth={styles.input.width}
                  elevation={1.5}
                  value={this.state.year}
                />
                <Text
                  style={{
                    fontFamily: fontStyles.nexa_bold,
                    fontSize: 18,
                    color: "#4D2545",
                    marginLeft: 10,
                    marginRight: 10,
                  }}
                >
                  -
                </Text>
                <DateSelect
                  placeholder={"End Year"}
                  icon={DownArrowIcon}
                  data={addCreditViewModel.convertYears()}
                  onChange={(year) => this.setState({ endYear: year })}
                  short
                  // lng
                  margin={10}
                  // customWidth={styles.input.width}
                  elevation={1.5}
                  value={this.state.endYear}
                />
              </View>

              <DateSelect
                placeholder={addCreditViewModel.getPlaceHolder(this)}
                icon={DownArrowIcon}
                data={addCreditViewModel.getMonths(this)}
                onChange={(month) => this.setState({ month })}
                value={this.state.month}
                // short
                lng
                margin={10}
                moreContainerStyle={{ paddingRight: 20 }}
                customWidth={styles.input.width}
                elevation={1.5}
              />
            </View>
            <View style={styles.button}>
              <Button
                loading={this.state.loading}
                text="Save"
                onPress={async () =>
                  await addCreditViewModel.handlePressSave(this)
                }
              />
            </View>
          </View>
          <Select
            selectTitle="Select a Production Type"
            data={auditionFilters[2].options}
            onPress={(selected) =>
              addCreditViewModel.handlePressSelect(this, selected)
            }
            ref="select"
          />
        </Content>
      </Container>
    );
  }
}
export default AddCredit;
