import { Dimensions, StyleSheet, Platform } from 'react-native';
import { colorPalette } from 'resources/colors';
import { fontStyles } from 'resources/fonts';
import { resize } from 'utils/utils';
const isIos = Platform.OS === 'ios';

export default StyleSheet.create({
	headerContainer: {
		width: resize(375),
		flexDirection: 'row',
		alignItems: 'center',
		marginBottom: resize(10, 'height'),
	},
	profileContainer: {
		width: '40%',
		alignItems: 'flex-end',
	},
	avatar: {
		width: isIos ? resize(104, 'height') : resize(94, 'height'),
		height: isIos ? resize(104, 'height') : resize(94, 'height'),
		overflow: 'hidden',
		borderRadius: 4,
		backgroundColor: '#d9d9d9',
	},
	image: {
		width: '100%',
		height: '100%',
	},
	profileText: {
		paddingLeft: '5%',
	},
	name: {
		color: colorPalette.purple,
		fontFamily: fontStyles.nexa_bold,
		fontSize: 20,
		width: resize(200),
		letterSpacing: 1,
		...Platform.select({
			ios: {
				marginBottom: resize(5, 'height'),
			},
		}),
	},
	job: {
		color: colorPalette.purple,
		fontFamily: fontStyles.nexa_light,
		fontSize: 16,
		letterSpacing: 0.8,
		...Platform.select({
			ios: {
				marginBottom: resize(5, 'height'),
			},
		}),
	},
	member: {
		color: colorPalette.purple,
		fontFamily: fontStyles.nexa_light,
		fontSize: 16,
		letterSpacing: 0.8,
		...Platform.select({
			ios: {
				marginBottom: resize(6, 'height'),
			},
			android: {
				marginBottom: resize(2, 'height'),
			},
		}),
	},
	city: {
		color: colorPalette.purple,
		fontFamily: fontStyles.nexa_bold,
		fontSize: 15,
		letterSpacing: 0.75,
	},
});
