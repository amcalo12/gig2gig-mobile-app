import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    marginVertical:resize(10, "height"),
    paddingHorizontal: 50,
  },
  text:{
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_light,
    fontSize: 18,
    letterSpacing: 0.9,
  }
});