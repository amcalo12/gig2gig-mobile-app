import React, { Component } from 'react';
import {
  Platform,
  Modal,
  View,
  StatusBar,
  Image,
  TouchableOpacity
} from 'react-native';
import {
  Text,
  Card,
  Icon,
  Input
} from 'native-base';
import styles from './index.styles';
import FilterIcon from "resources/images/tabNavigation/filter-icon.png";
import searchIcon from "resources/images/tabNavigation/search-icon.png";
import FilterButton from "utils/components/searchFilterButton";
import Button from "utils/components/mainButton";
import { resize } from 'utils/utils';
const isIos = Platform.OS === "ios";

export default class SearchModal2 extends Component {
  state = {
    visible: this.props.visible,
    data: this.props.data,
    selected: []
  };

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.visible == true && this.state.visible == false) {
      this.setState({
        visible: nextProps.visible,
        data: nextProps.data,
      })
    }
    if (nextProps.visible == false && this.state.visible == true) {
      this.setState({
        visible: nextProps.visible,
        data: nextProps.data,
      })
    }
  }

  setModalVisible() {
    this.props.onPressBack(this.state.selected)
    this.setState({
      visible: !this.state.visible,
    });
  }

  toggleValue = (i, index) => {
    const { state } = this

    const filter = state.data.filter(res => res.id == i)
    const filter2 = state.selected.filter(res => res == i)
    const index2 = state.selected.findIndex(res => res == i)

    if (filter2.length != 0) {
      if (filter[0].active) {
        state.data[index].active = false
      } else {
        state.data[index].active = true
      }
      state.selected.splice(index2, 1)
    } else {
      if (filter[0].active) {
        state.data[index].active = false
      } else {
        state.data[index].active = true
      }
      state.selected.push(state.data[index].id)
    }
    this.setState({
      ...state
    })
  }

  render() {
    const {
      data,
    } = this.state;

    return (
      <Modal
        transparent={true}
        animationType="fade"
        visible={this.state.visible}
        onRequestClose={() => {
          this.setModalVisible();
        }}>
        <StatusBar hidden={false}
          backgroundColor="white" barStyle="dark-content" />
        <View style={styles.container}>
          <Card
            // style={styles.info}
            style={[styles.info]}
          >
            <View style={styles.filtersContainer}>
              <View style={styles.filters}>
                {
                  data.map((res, i) =>
                    <FilterButton key={i} type={'union'} text={res.title} active={res.active} onPress={() => this.toggleValue(res.id, i)} />
                  )
                }
              </View>
            </View>
            <View style={styles.buttonAction}>
              <Button text={'Search'} onPress={() => this.setModalVisible()} />
            </View>
          </Card>
        </View>
      </Modal>
    );
  }
}