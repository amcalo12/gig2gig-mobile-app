import { StyleSheet } from "react-native";
import { resize } from "utils/utils";
import { colorPalette } from "resources/colors";

//custom

export default StyleSheet.create({
  container: {
    width: resize(336),
    height: resize(164, "height"),
    borderRadius: 10,
    backgroundColor: "rgba(0,0,0,0.1)",
    overflow: "hidden",
    marginTop: resize(20, "h")
  },
  liveBackground: {
    backgroundColor: "#ffffff"
  },
  playContainer: {
    width: resize(51, "h"),
    height: resize(51, "h"),
    backgroundColor: "rgba(255,255,255,0.5)",
    borderRadius: resize(51, "h") / 2,
    alignItems: "center",
    justifyContent: "center"
  },
  nameContainer: {
    height: resize(38, "h"),
    backgroundColor: colorPalette.purple,
    justifyContent: "center",
    paddingLeft: resize(13)
  },
  name: {
    color: "#ffffff",
    fontFamily: "Nexa Light",
    fontSize: 16,
    fontWeight: "400",
    letterSpacing: 0.8,
    marginRight: 40
  },
  liveContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  icon: {
    width: resize(10),
    height: resize(10, "height"),
    marginLeft: resize(8)
  },
  videoContainer: {
    width: "100%",
    height: "80%",
    borderRadius: 10,
    overflow: "hidden"
  },
  video: {
    width: "100%",
    height: "100%"
  },
  principalVideo: {
    width: "100%",
    height: resize(126, "h")
  },
  btnPlay: {
    position: "absolute",
    zIndex: 2,
    width: "100%",
    height: resize(126, "h"),
    justifyContent: "center",
    alignItems: "center"
  },
  play: {
    width: resize(20, "h"),
    height: resize(23, "h")
  }
});
