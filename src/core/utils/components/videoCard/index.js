import React from "react";
import { TouchableOpacity, View, Text, Image, Linking } from "react-native";

//custom
import styles from "./styles";
import { Thumbnail } from "react-native-thumbnail-video";
import LinearGradient from "react-native-linear-gradient";
import { gradientColors } from "utils/utils";

//Images
import playBtn from "resources/images/playBtn.png";

import openPlayer from "./playVideo";
import { resize } from "utils/utils";
import DeleteWhiteIcon from "resources/images/myMedia/delete_white.png";
import ThumbnailImage from "resources/images/myMedia/video_thumbnail.png";
import { Assets } from "resources/assets";

const VideoCard = ({
  containerStyle,
  url,
  name,
  onDeletePress,
  onRename,
  isYouTubeUrl
}) => (
    <View style={[containerStyle]}>
      {isYouTubeUrl && (
        <Thumbnail
          imageHeight={resize(126, "h")}
          imageWidth={"100%"}
          containerStyle={styles.principalVideo}
          type="standard"
          showPlayIcon={false}
          onPress={() => { }}
          url={`https://www.youtube.com/watch?v=${url}`}
        />
      )}
      {!isYouTubeUrl && (
        <Image
          style={{
            height: resize(126, "height"),
            width: "100%"
          }}
          source={ThumbnailImage}
        />
      )}
      <LinearGradient colors={gradientColors} style={styles.nameContainer}>
        <Text style={styles.name}>{name}</Text>
        <TouchableOpacity
          style={{
            position: "absolute",
            right: 40,
            height: resize(22, "height"),
            width: resize(20)
          }}
          onPress={() => {
            onRename();
          }}
        >
          <Image
            style={{
              height: resize(20, "height"),
              width: resize(20),
              resizeMode: "contain",
              tintColor: "#ffffff"
            }}
            source={Assets.rename}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            position: "absolute",
            right: 13,
            height: resize(22, "height"),
            width: resize(20)
          }}
          onPress={() => {
            onDeletePress();
          }}
        >
          <Image
            style={{
              height: resize(23, "height"),
              width: resize(20),
              resizeMode: "contain"
            }}
            source={DeleteWhiteIcon}
          />
        </TouchableOpacity>
      </LinearGradient>

      <View style={styles.btnPlay}>
        <TouchableOpacity
          style={styles.playContainer}
          onPress={() => {
            if (isYouTubeUrl) {
              openPlayer(url);
            } else {
              try {
                Linking.openURL(url);
              } catch (e) { }
            }
          }}
        >
          <Image style={styles.play} resizeMode="contain" source={playBtn} />
        </TouchableOpacity>
      </View>
    </View>
  );

VideoCard.defaultProps = {
  containerStyle: styles.container,
  name: "1.31.19"
};

export default VideoCard;
