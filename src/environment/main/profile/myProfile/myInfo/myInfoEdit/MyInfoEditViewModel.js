import { AsyncStorage, Platform, Keyboard } from "react-native";
import moment from "moment";
import { StackActions, NavigationActions } from 'react-navigation';
import { asyncGetPicture, uploadImage } from "utils/image";
import myProfileApi from "api/myProfileApi"
import userApi from "api/userApi";
import { showMsj, getLocation } from "utils/utils";
import { errorsValidateMsj } from "resources/l10n";
import SystemSetting from 'react-native-system-setting';
import { requestPermission } from 'utils/permissions';
import { IsValidZipCode, emailRegExp, getThumbnail } from "utils/cons";
import ImagePicker from "react-native-image-crop-picker";
import ImageResizer from 'react-native-image-resizer';

export const handleGetMoreImageFormCameraRoll = async (context) => {
  try {
    const image = await asyncGetPicture("photo");
    console.log('image selected edit:>> ', image);
    ImagePicker.openCropper({
      path: image.uri,
      // width: image.width,
      // height: image.height
    }).then(async data => {
      console.log("Crop image", data);
      let newData = image;
      newData['uri'] = data.path

      let thumbnailImage = await getThumbnail(data.path)
      console.log('thumbnail :', thumbnailImage);

      let ext = "." + data.path.substring(data.path.lastIndexOf('.') + 1, data.path.length)
      let fileName = (image.fileName != undefined && image.fileName != null && image.fileName != '') ? image.fileName : (Date.now() + ext)
      context.setState({ image: newData.uri, thumbnail: thumbnailImage.uri, isVisible: false, fileName: fileName, imageChanged: true, });
      // context.setState({ image: newData });
    }).catch(async error => {

      let thumbnailImage = await getThumbnail(image.uri)
      console.log('thumbnail :', thumbnailImage);
      let ext = "." + image.uri.substring(image.uri.lastIndexOf('.') + 1, image.uri.length)
      let fileName = (image.fileName != undefined && image.fileName != null && image.fileName != '') ? image.fileName : (Date.now() + ext)
      context.setState({ image: image.uri, thumbnail: thumbnailImage.uri, isVisible: false, fileName: fileName, imageChanged: true });
      console.log("Crop image error", error);
    });

  } catch (error) {
  }
};

export const handleUploadImage = async (resource) => {
  try {
    const url = await uploadImage({ uri: resource });
    return url;
  } catch (error) {
    return new Promise.reject(error);
  }
};

export const select = (context) => {
  context.refs.select.setModalVisible();
}

export const countrySelect = context => {
  context.refs.country.setModalVisible();
};

export const genderSelect = context => {
  context.refs.gender.setModalVisible();
};

export const handleSelectedState = (selected, context) => {
  const state = selected;
  context.setState({
    state
  });
  select(context);
}

export const handleSelectedCountry = (selected, context) => {
  const country = selected;
  // let state = srt.substring(0, 3);
  // state += "..."
  context.setState({
    country
  });
  countrySelect(context);
};

export const handleSelectedGender = (selected, context) => {
  const gender = selected.name;
  // let state = srt.substring(0, 3);
  // state += "..."
  context.setState({
    gender
  });
  genderSelect(context);
};

export const _showDateTimePicker = (context) => context.setState({
  isDateTimePickerVisible: true
});

export const _hideDateTimePicker = (context) => context.setState({
  isDateTimePickerVisible: false
});

export const _handleDatePicked = (date, context) => {
  const birth = moment(date).format('YYYY-MM-DD');
  context.setState({
    birth
  });
  _hideDateTimePicker(context);
};

export const handleLocation = async (context) => {
  context.setState({
    locationLoad: true
  });
  const isAndroid = Platform.OS === 'android';
  if (isAndroid) {
    const Permissions = await requestPermission('ACCESS_FINE_LOCATION', {
      title: 'Location',
      message: '¿Allow G2G to access the location of this device?'
    });
    if (Permissions) {
      const locationEnable = await SystemSetting.isLocationEnabled();
      if (locationEnable) {
        try {
          const newLocation = await getLocation();
          context.setState({ location: newLocation.formatted_address, locationLoad: false });
        } catch (e) {
          context.setState({ locationLoad: false });
          return showMsj("A location error has ocurred", true);
        }
      } else {
        SystemSetting.switchLocation(() => {
          SystemSetting.isLocationEnabled().then(async (enable) => {
            const state = enable ? 'On' : 'Off';
            if (enable) {
              try {
                const newLocation = await getLocation();
                context.setState({ location: newLocation.formatted_address, locationLoad: false });
              } catch (e) {
                context.setState({ locationLoad: false });
                return showMsj("A location error has ocurred", true);
              }
            } else {
              context.setState({
                locationLoad: false
              });
              return showMsj(errorsValidateMsj.signup.location.messagePermission, true);
            }
          })
        })
      }
    } else {
      context.setState({
        locationLoad: false
      });
      return showMsj(errorsValidateMsj.signup.location.messagePermission, true);
    }
  } else {
    try {
      const newLocation = await getLocation();
      context.setState({ location: newLocation.formatted_address, locationLoad: false });
    } catch (e) {
      context.setState({ locationLoad: false });
      return showMsj("A location error has ocurred", true);
    }
  }
};

export const validateForm = (context) => {
  const {
    first_name,
    last_name,
    email,
    password,
    stage_name,
    profesion,
    location,
    address,
    city,
    state,
    zip,
    birth,
    url,
    country,
    gender,
    genderDesc,
    image
  } = context.state;

  if (image === null) {
    return showMsj(errorsValidateMsj.signup.image.message, true)
  }

  if (first_name === "") {
    return showMsj(errorsValidateMsj.signup.firstName.message, true)
  }
  if (last_name === "") {
    return showMsj(errorsValidateMsj.signup.lastName.message, true)
  }
  if (email === "") {
    return showMsj(errorsValidateMsj.signup.email.message, true)
  }
  if (!emailRegExp.test(email)) {
    return showMsj(errorsValidateMsj.signup.email.invalid, true);
  }

  if (context.props.navigation.state.params.isFromHome && password == '') {
    return showMsj(errorsValidateMsj.signup.password.message, true);
  }

  //  if (stage_name === "") {
  //    return showMsj(errorsValidateMsj.signup.stageName.message, true)
  //  }
  if (profesion === "") {
    return showMsj(errorsValidateMsj.signup.pwTitle.message, true)
  }
  if (country === "") {
    return showMsj(errorsValidateMsj.signup.country.message, true)
  }
  // if (address === "") {
  //   return showMsj(errorsValidateMsj.signup.address.message, true)
  // }
  // if (city === "") {
  //   return showMsj(errorsValidateMsj.signup.city.message, true)
  // }
  // if (zip === "") {
  //   return showMsj(errorsValidateMsj.signup.zip.message, true)
  // }
  if (password != '' && password.length <= 7) {
    return showMsj(errorsValidateMsj.signup.passwordLengthError.message, true)
  }
  console.log('gender :>> ', gender);
  console.log('genderDesc :>> ', genderDesc);
  if (gender === "") {
    return showMsj(errorsValidateMsj.signup.gender.message, true);
  }

  if (gender.toLowerCase() == String("self describe").toLowerCase() && (genderDesc == "" || genderDesc == null)) {
    return showMsj(errorsValidateMsj.signup.gender_desc.message, true);
  }


  // if (zip) {
  //   const validation = IsValidZipCode(zip)
  //   if (!validation) {
  //     return showMsj(errorsValidateMsj.signup.zip.messageError, true)
  //   }
  // }

  return handleUserInfo(context);

}

export const handleUserInfo = async (context) => {
  Keyboard.dismiss();
  context.setState({ loading: true });
  const { navigation: { navigate, dispatch }, navigation } = context.props;
  const userData = JSON.parse(await AsyncStorage.getItem("userData"));
  const {
    image,
    imageChanged,
    first_name,
    last_name,
    email,
    password,
    stage_name,
    profesion,
    location,
    address,
    city,
    state,
    zip,
    birth,
    url,
    thumbnail,
    imageData,
    fileName,
    country,
    gender,
    genderDesc
  } = context.state;
  try {
    const pass = password.length > 0;

    let params = {
      image: imageChanged ? await handleUploadImage(image) : image,
      thumbnail: imageChanged ? await handleUploadImage(thumbnail) : "",
      // file_name: imageChanged ? fileName : imageData.name,
      first_name,
      last_name,
      email,
      password,
      stage_name,
      profesion,
      // location,
      // address,
      // city,
      // state: state.id,
      // zip,
      birth,
      url,
      gender,
      country: country.id,
      file_name: fileName
    }

    if (gender.toLowerCase() ==  String("self describe").toLowerCase()) {
      params['gender_desc'] = genderDesc
    }


    const response = await myProfileApi.editUserInfo(params, userData.id);
    console.log('response :>> ', response);
    const userInfo = await userApi.getUserInfo(userData.id);
    await AsyncStorage.setItem("userData", JSON.stringify(userInfo.data));

    context.setState({ loading: false });
    showMsj(errorsValidateMsj.editProfile.success.message, false)
    // return dispatch(StackActions.popToTop());
    // return navigate("Profile");
    // navigation.goBack(null)
    // return navigation.navigate('Profile')
    // return dispatch(StackActions.popToTop());

    const navigateAction = NavigationActions.navigate({
      routeName: 'App',

      params: {},

      action: NavigationActions.navigate({ routeName: 'Profile' }),
    });

    dispatch(
      StackActions.reset({
        index: 0, //Home screen of Tab A
        // key: null,
        actions: [navigateAction]
      })
    );
    // navigation.dispatch(navigateAction);
  } catch (e) {
    console.log('e :>> ', e);
    context.setState({ loading: false });
    showMsj(errorsValidateMsj.editProfile.error.message, true)
  }
}