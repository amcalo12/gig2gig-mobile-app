import {StyleSheet} from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils"

export default StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.15)'
  },
  info:{
    flexDirection: 'column',
    // justifyContent: 'space-between',
    width: resize(328),
    height: resize(568, "height"),
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    overflow:"hidden",
    position:"relative"
    // paddingHorizontal: 20,
    // paddingVertical: 20
  },
  close:{
    position:"absolute",
    top: resize(20, "height"),
    left:resize(15)
  },
  imageContent:{
    width: "100%",
    height: "20%",
    backgroundColor: "#f1f1f1"
  },
  image:{
    width:"100%",
    height:"100%"
  },
  content:{
    height:"80%",
    alignItems:"center",
    paddingTop: resize(40, "height"),
    paddingBottom: resize(20, "height"),
    justifyContent:"space-between"
  },
  title:{
    fontFamily:fontStyles.nexa_bold,
    fontSize:28,
    color:colorPalette.purple,
    letterSpacing:1.4,
    alignSelf:"center",
    paddingHorizontal: resize(12),
    textAlign:"center",
    marginBottom:resize(10, "height")
  },
  subTitle:{
    fontFamily:fontStyles.nexa_bold,
    fontSize:17,
    color:colorPalette.purple,
    letterSpacing:0.85,
    alignSelf: "center"
  },
  aInfo:{
    alignItems:"center",
    marginTop: resize(40, "height"),
  },
  aInfoText:{
    fontFamily:fontStyles.nexa_bold,
    fontSize:18,
    color:colorPalette.purple,
    marginBottom: resize(10, "height"),
  },
  button:{
    marginBottom:resize(20, "height")
  }
});