import React, { Component } from "react";
import { View, Text, Image, RefreshControl, Share, FlatList } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Loading from "utils/components/loading";
import { showMsj, openUrl, shareContent } from "utils/utils";
import { generalMsj, shareMsj } from "resources/l10n";
import { refreshControlColors } from "resources/colors";
import { StackActions } from "react-navigation";
import OrderIcon from "resources/images/order_icon.png";
import PostBlog from "utils/components/postBlog";
import { getAllBlogs } from "api/supportForum";

class NewAndUpdates extends Component {

  state = {
    loading: true,
    refreshing: false,
    valueSearch: '',
    blogPosts: [
      // {
      //   body: 'quis',
      //   date: '07/08/2019',
      //   url_media: 'https://mott.pe/noticias/wp-content/uploads/2016/11/Janette-Asche.jpg',
      //   title: 'Timon y pumba'
      // },
      // {
      //   body: 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit',
      //   date: '07/08/2019',
      //   url_media: 'https://mott.pe/noticias/wp-content/uploads/2016/11/Janette-Asche.jpg',
      //   title: 'Timon y pumba'
      // },
      // {
      //   body: 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit',
      //   date: '07/08/2019',
      //   url_media: 'https://mott.pe/noticias/wp-content/uploads/2016/11/Janette-Asche.jpg',
      //   title: 'Timon y pumba'
      // },
      // {
      //   body: 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit',
      //   date: '07/08/2019',
      //   url_media: 'https://mott.pe/noticias/wp-content/uploads/2016/11/Janette-Asche.jpg',
      //   title: 'Timon y pumba'
      // },
    ]
  }

  async componentDidMount() {
    await this.getData()
    this.setState({
      loading: false
    })
  }

  getData = async () => {
    try {
      const res = await getAllBlogs()
      this.setState({
        blogPosts: res.data
      })
    } catch (error) {

    }
  }

  _navigateTo = (screen, params = {}) => {
    const { navigation } = this.props

    navigation.navigate(screen, { ...params })
  }

  _onRefresh = async () => {
    this.setState({
      refreshing: true
    })
    await this.getData()

    this.setState({
      refreshing: false
    })
  }

  render() {

    const {
      loading,
      refreshing,
      blogPosts
    } = this.state;

    const { navigation: { navigate, dispatch } } = this.props;

    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          rightIcon={OrderIcon}
          rightFn={() => this.setState({
            blogPosts: blogPosts.reverse()
          })}
          barStyle="dark-content"
          text="New & Updates"
        />
        {
          loading ?
            <Content>
              <Loading />
            </Content>
            :
            <Content
              showsVerticalScrollIndicator={false}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={() => this._onRefresh()}
                  colors={refreshControlColors}
                />
              }
            >
              <FlatList
                showsVerticalScrollIndicator={false}
                data={blogPosts}
                style={{ paddingBottom: 30 }}
                keyExtractor={(a, i) => `${i}`}
                ListEmptyComponent={
                  // <Text style={styles.empty}>Not found news</Text>
                  <Text style={styles.empty}>No News Yet</Text>
                }
                renderItem={(item, i) => (
                  <PostBlog
                    onPress={() => this._navigateTo('NewAndUpdatesDetail', { data: item.item })}
                    {...item.item}
                  />
                )} />
            </Content>
        }
      </Container>
    );
  }
}
export default NewAndUpdates;
