import React, { Component } from 'react';
import { Modal, View, Text, Image, TouchableOpacity } from 'react-native';
import { Card } from 'native-base';
import styles from './index.styles';
import DownArrow from "resources/images/down-arrow-icon.png";
import SelectLabel from "./selectLabel";
import { withNavigation } from "react-navigation";

class MarketplaceSelectFilter extends Component {
  state = {
    modalVisible: false,
    activeOption: ""
  };

  componentDidMount = () => {
    const { onSelected } = this.props;
    const activeOption = this.props.navigation.state.params.info.option.name
    onSelected(activeOption);
    this.setState({
      activeOption
    })
  }



  setModalVisible = () => {
    this.setState({
      modalVisible: !this.state.modalVisible
    });
  }

  setOption = (option) => {
    const { onSelected } = this.props;
    this.setState({
      activeOption: option.name,
      modalVisible: !this.state.modalVisible
    });

    onSelected(option);
  }

  renderItem = (option, idx) => {
    const { activeOption } = this.state;
    if (option.name !== activeOption) {
      return (
        <SelectLabel key={idx} onPress={() => this.setOption(option)} text={option.name} />
      )
    }
  }

  render() {
    const { activeOption } = this.state;
    const { marketplaceOptions } = this.props
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => this.setModalVisible()}
        style={styles.selectContainer}>
        <Text style={styles.headerTitle}>{activeOption}</Text>
        <Image style={styles.arrow} source={DownArrow} />
        <Modal
          transparent={true}
          animationType="fade"
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible();
          }}>
          <View style={styles.container}>
            <Card style={styles.info}>
              <SelectLabel onPress={() => this.setModalVisible()} text={activeOption} isActive />
              {
                marketplaceOptions.map((option, idx) => {
                  return this.renderItem(option, idx)
                })
              }
            </Card>
          </View>
        </Modal>
      </TouchableOpacity>
    );
  }
}

export default withNavigation(MarketplaceSelectFilter);