import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";
const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);

const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  content: {
    width: "100%",
    height: "100%"
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  cardContainer: {
    width: "100%",
    borderRadius: 16
  },
  avatarAndNameContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  empty: {
    width: "100%",
    textAlign: "center",
    paddingTop: resize(20, "h"),
    color: "#757575",
    fontFamily: "Nexa Bold",
    fontSize: resize(10, "h"),
    fontWeight: "300"
  },
  avatar: {
    width: resize(26, "h"),
    height: resize(26, "h"),
    borderRadius: resize(13, "h")
  },
  nameTxt: {
    paddingLeft: resize(7),
    color: "#4d2545",
    fontFamily: "Nexa Bold",
    fontSize: resize(12, "h"),
    fontWeight: "400"
  },
  bodyTxt: {
    paddingTop: resize(25, "h"),
    color: "#757575",
    fontFamily: "Nexa Bold",
    fontSize: resize(10, "h"),
    fontWeight: "300",
    lineHeight: resize(18, "h")
  },
  dateTxt: {
    color: "#7a7a7a",
    fontFamily: "Nexa Light",
    fontSize: resize(10, "h"),
    fontWeight: "400"
  },
  commmentHeader: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: resize(42, "h")
  },
  commentTxt: {
    color: "#4d2545",
    fontFamily: "Nexa Light",
    fontSize: resize(14),
    fontWeight: "400"
  },
  addCommentTxt: {
    color: "#4d2545",
    fontFamily: "Nexa Bold",
    fontSize: resize(14),
    fontWeight: "400",
    paddingLeft: resize(10)
  },
  addComentRow: {
    flexDirection: "row",
    alignItems: "center"
  },
  input: {
    height: resize(60, "h"),
    width: resize(300),
    paddingHorizontal: resize(20),
    color: "#4d2545",
    fontFamily: "Nexa Bold",
    fontSize: resize(14),
    fontWeight: "400",
    paddingTop: resize(15, "h")
  },
  btnSend: {
    backgroundColor: "#4d2545",
    width: resize(72),
    alignItems: "center",
    justifyContent: "center"
  },
  sendTxt: {
    color: "white",
    fontSize: resize(13),
    fontFamily: "Nexa Bold",
    fontWeight: "400"
  },
  cardInputContainer: {
    flexDirection: "row"
  }
});
