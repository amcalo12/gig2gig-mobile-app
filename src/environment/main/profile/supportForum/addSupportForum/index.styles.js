import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container: {

  },

  postTitleInput: {
    width: resize(348),
    height: resize(44, 'h'),
    borderRadius: resize(22, 'h'),
    borderColor: '#d6d6d6',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    paddingHorizontal: resize(22),
    fontFamily: 'Nexa Light',
    fontSize: resize(16),
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: resize(20, 'h')
  },
  postBodyInput: {
    width: resize(348),
    height: resize(253, 'h'),
    borderRadius: resize(22, 'h'),
    borderColor: '#d6d6d6',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    paddingTop: resize(22),
    paddingBottom: resize(22),
    paddingHorizontal: resize(22),
    fontFamily: 'Nexa Light',
    fontSize: resize(16),
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: resize(11, 'h')
  },
  tagContainer: {
    width: resize(348),
    height: resize(44, 'h'),
    borderRadius: resize(22, 'h'),
    borderColor: '#d6d6d6',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#ffffff',
    paddingHorizontal: resize(22),
    fontFamily: 'Nexa Light',
    fontSize: resize(16),
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: resize(11, 'h'),
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: resize(68, 'h')
  },
  tagsTxt: {
    color: '#757575',
    fontFamily: 'Nexa Light',
    fontSize: resize(16),
    fontWeight: '400',
    paddingLeft: resize(10)
  },
  btn: {
    width: resize(326),
    height: resize(44, 'h'),
  }
});