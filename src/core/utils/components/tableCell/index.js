import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import RightArrow from "resources/images/right-arrow.png"
import styles from './index.styles';

export default class TableCell extends Component {
  render(){
    const {title, disabled, typeText, expire, nextIcon, customStyle, sinlgeText, press, card} = this.props;
      return(
          <TouchableOpacity disabled={disabled} style={[styles.button, customStyle && customStyle]} onPress={() => press()}>
            {title?
              <Text style={styles.titleText}>{title}</Text>
              :
              <View style={card && styles.card}>
                {card && <Image source={require('resources/images/cards/visa/Image_5.png')}/>}
                {typeText && <Text style={styles.typeText}>{typeText}</Text>}
                {expire && <Text style={styles.expireText}>{expire}</Text>}
                {sinlgeText && <Text style={styles.sinlgeText}>{sinlgeText}</Text>}
              </View>
            }
            {nextIcon && <Image width={10} height={10} style={styles.icon} source={RightArrow}/>}
          </TouchableOpacity>
      );
  }
}