import React, { Component } from 'react';
import {
	Platform,
	Modal,
	View,
	StatusBar
} from 'react-native';
import {
	Button,
	Text
} from 'native-base';
import styles from './index.styles';
import Loading from '../loading';

export default class modalUploadFiles extends Component {
	state = {
		modalVisible: false,
	};

	setModalVisible() {
		this.setState({
			modalVisible: !this.state.modalVisible
		});
	}


	render() {
		const { message } = this.props;
		return (
			<Modal
				transparent={true}
				animationType="fade"
				visible={this.state.modalVisible}
				onRequestClose={() => {
					this.setModalVisible();
				}}>
				<View style={styles.container}>
					<View style={styles.info}>
						<Loading style={{ top: "10%", alignSelf: 'center', }} />
						<Text style={styles.text}>{message}</Text>
					</View>
				</View>
			</Modal>
		);
	}
}