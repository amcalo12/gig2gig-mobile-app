import React, { Component } from "react";
import { TouchableOpacity, Image, Text, View, Platform } from "react-native";
import { Card } from "native-base";
import styles from "./index.styles";
import Filter from "utils/components/auditionFilterButton";
import Loading from "utils/components/loading";
import { convertTimeToHour, getCityState } from "utils/utils";
import moment from "moment";
import { fontStyles } from "resources/fonts";
class AuditionCard extends Component {
  state = {
    loading: false,
    city: "",
    state: ""
  };

  getTextType = () => {
    const { isRequested, isUpcoming, isPast, isOnline, isSubmitted, isSubmit } = this.props;
    let text = "";
    // if (isRequested) {
    //   text = "Pending";
    // } else if (isUpcoming) {
    //   text = "Check-In";
    // } else if (isPast) {
    //   text = "View";
    // } else if (isOnline) {
    //   text = "Submit";
    // }

    if (isOnline && isSubmit) {
      text = "Submit";
    } else if (isRequested) {
      text = "Pending";
    } else if (isOnline && isSubmitted) {
      text = "View";
    } else if (isUpcoming) {
      text = "Check-In";
    } else if (isPast) {
      text = "View";
    }
    // else if (isOnline) {
    //   text = "Submit";
    // }
    console.log('text :>> ', text);
    return text;
  };
  async componentWillMount() {
    if (this.props.isSearchAudition) {
      let data = this.props.data;
      if (data && data !== null) {
        if (data.online === 0) {
          if (data.location && data.location !== null) {
            if (
              data.location.latitude &&
              data.location.longitude &&
              data.location.latitude !== null &&
              data.location.longitude !== null
            ) {
              let locationObj = await getCityState(
                data.location.latitude,
                data.location.longitude
              );
              if (locationObj != undefined) {
                this.setState({
                  city: locationObj.city,
                  state: locationObj.state
                });
              }
            }
          }
        }
      }
    }
  }

  render() {
    const {
      isRequested,
      isUpcoming,
      isPast,
      text,
      icon,
      onPress,
      data,
      isOnline,
      isSearchAudition
    } = this.props;
    const { loading } = this.state;
    const auditionCover =
      isRequested || isUpcoming || isPast || isOnline ? data.media : data.cover;
    return (
      <TouchableOpacity
        activeOpacity={0.6}
        style={styles.container}
        onPress={() => onPress()}
      >
        <Card style={styles.cardContainer}>
          <View style={styles.leftCard}>
            <View style={styles.filterContainer}>
              {data && data.union && data.union.toUpperCase() !== "ANY" && (
                <Filter type="union" text={data.union} />
              )}
              {data &&
                data.contract &&
                data.contract.toUpperCase() !== "ANY" && (
                  <Filter type="contract" text={data.contract} />
                )}
              {data && data.production && data.online === 0 && (
                <Filter type="production" text={data.production[0]} />
              )}
              {data.online === 1 && (
                <Filter type="online" text={"ONLINE SUBMISSION"} />
              )}
            </View>
            {(isUpcoming || isPast) && (
              <View style={styles.body}>
                <Text numberOfLines={1} style={styles.bodyTitle}>
                  {data.title}
                </Text>
                {isUpcoming && (
                  <Text numberOfLines={2} style={styles.bodyText}>
                    <Text style={styles.bodyTextBold}>
                      {moment(data.date).format("MM/DD")}{" "}
                    </Text>
                    {data.hour &&
                      `${moment(data.hour, "h:mma").format(
                        "h:mma"
                      )} Appointment`}
                  </Text>
                )}
                {isUpcoming && (
                  <Text style={styles.roundBodyText}>
                    {"\n Round: " + data.round}
                  </Text>
                )}


                {(isPast || isOnline) && (
                  <Text numberOfLines={2} style={styles.bodyTextBold}>
                    {/* 2/1/19 */}
                    {isOnline
                      ? moment(data.date).format("MM/DD/YY")
                      : data.online === 0
                        ? moment(data.date).format("MM/DD/YY")
                        : ""}
                    <Text style={styles.roundBodyText}>
                      {"\n Round: " + data.round}
                    </Text>
                  </Text>
                )}
              </View>
            )}
            {!(isUpcoming || isPast) && (
              <View
                style={[
                  styles.mainBody,
                  { height: isSearchAudition ? Platform.OS == "ios" ? "70%" : "70%" : "50%" },
                  // isSearchAudition &&
                  //   Platform.OS === "android" &&
                  //   this.state.city !== "" &&
                  //   this.state.state !== ""
                  //   ? { marginTop: 15 }
                  //   : { marginTop: 0 }
                ]}
              >
                <Text numberOfLines={1} style={styles.titleTextStyle}>
                  {data.title}
                </Text>
                {/* {isOnline && data.end_date != null ?
                  <Text style={styles.bodyTextBold}>
                    {moment(data.end_date).format("MM/DD")}{" "}
                  </Text>
                  : null} */}
                {!isSearchAudition && (
                  <Text style={styles.roundBodyText}>
                    {"Round: " + data.round}
                  </Text>
                )}
                <Text numberOfLines={2} style={styles.mainBodyText}>
                  {data.description}
                </Text>
                {console.log('this.state.city :>> ', this.state.city)}
                {isSearchAudition &&
                  this.state.city != null && this.state.city !== "" &&
                  this.state.state != null && this.state.state !== "" ?
                  <Text
                    style={[styles.mainBodyText, {
                      marginTop: 5, position: "absolute",
                      bottom: 5, fontFamily: fontStyles.nexa_bold
                    }]}
                  >{`${this.state.city}, ${this.state.state}`}</Text>
                  : null}
              </View>
            )}

            {isRequested && (
              <View style={styles.footer}>
                <Text style={styles.footerText}>
                  {data.number_roles} {data.number_roles > 1 ? "Roles" : "Role"}
                </Text>
              </View>
            )}
          </View>
          <View style={styles.rightCard}>
            <View style={styles.imageContainer}>
              <View style={styles.imageView}>
                {loading && (
                  <Loading
                    style={{
                      top: "8%",
                      position: "absolute",
                      alignSelf: "center"
                    }}
                  />
                )}
                <Image
                  onLoadStart={() => this.setState({ loading: true })}
                  onLoadEnd={() => this.setState({ loading: false })}
                  style={styles.image}
                  source={auditionCover ? { uri: auditionCover } : null}
                />
              </View>
            </View>
            <View style={styles.footer}>
              {!(isRequested || isUpcoming || isPast || isOnline) && (
                <Text style={[styles.footerText, styles.footerTextRight]}>
                  {/* {convertTimeToHour(data.create)} */}
                  {moment(data.create).format("MM/DD/YY")}
                </Text>
              )}
              {(isRequested || isUpcoming || isPast || isOnline) && (
                <View style={styles.footerState}>
                  <Text style={styles.footerStateText}>
                    {this.getTextType()}
                  </Text>
                </View>
              )}
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}
export default AuditionCard;

AuditionCard.defaultProps = {
  isRequested: false,
  isUpcoming: false,
  isPast: false
  // data:{
  //   title:""
  // }
};
