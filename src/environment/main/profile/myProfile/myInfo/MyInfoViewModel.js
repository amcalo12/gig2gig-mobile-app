import { AsyncStorage } from "react-native";
import { StackActions } from 'react-navigation';

export const getUserData = async context => {
  const userData = JSON.parse(await AsyncStorage.getItem("userData"));
  console.log(userData)
  context.setState({ userData });
}

export const handleBackPress = context => {
  const { navigation: { dispatch } } = context.props;
  return dispatch(StackActions.popToTop())
  return true;
}