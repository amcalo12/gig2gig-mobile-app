import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import styles from './index.styles';
import SwitchToggle from 'react-native-switch-toggle';

export default class SkillsLabel extends Component {

  render(){
    const {text, value,onPress} = this.props;
      return(
          <View style={styles.container}>
            <Text style={styles.text}>{text}</Text>
            <SwitchToggle
              backgroundColorOn = {styles.switchSettingsActive.backgroundColor}
              backgroundColorOff = {styles.switchSettings.backgroundColor}
              circleColorOff = {styles.switchSettings.color}
              circleColorOn = {styles.switchSettingsActive.color}
              containerStyle={styles.containerStyle}
              circleStyle={styles.circleStyle}
              switchOn={value}
              onPress={()=>onPress()}
            />
          </View>
      );
  }
}