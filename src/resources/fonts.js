export const fontStyles = {
  break_bold: "Break-Bold",
  break_extralight: "Break-Extralight",
  break_light: "Break-Light",
  break_regular: "Break",
  break_semibold: "Break-Semibold",
  nexa_bold: "NexaBold",
  nexa_light: "NexaLight"
};