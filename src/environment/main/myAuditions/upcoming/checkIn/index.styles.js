import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";
const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);

export default StyleSheet.create({
  container: {
    alignItems: "center",
    marginVertical: isIos ? resize(30, "height") : resize(25, "height")
  },
  title: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 28,
    letterSpacing: 1.4,
    color: colorPalette.purple,
    ...Platform.select({
      ios: {
        marginBottom: resize(8, "height")
      }
    })
  },
  subTitle: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 17,
    letterSpacing: 0.85,
    color: colorPalette.purple,
    marginBottom: isIos ? resize(10, "height") : resize(5, "height")
  },
  bold: {
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple
  },
  appointment: {
    fontFamily: fontStyles.nexa_light,
    fontSize: 17,
    letterSpacing: 0.85,
    color: colorPalette.purple,
    marginBottom: isIos ? resize(30, "height") : resize(20, "height")
  },
  code: {
    marginBottom: resize(20, "height")
  },
  qrSize: {
    width: resize(180)
  },
  buttons: {
    marginBottom: resize(20, "height")
  },
  inputTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    height: height * 0.0734632683658,
    width: width * 0.7,
    backgroundColor: "#FFFFFF",
    borderWidth: 3,
    borderColor: colorPalette.purple,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 33
    // paddingHorizontal: width * 0.08
  },
  assignedNoTextStyle: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: colorPalette.purple,
    ...Platform.select({
      ios: {
        marginTop: resize(3, "height")
      }
    })
  },
  assignedNumberInput: {
    width: "80%",
    height: "100%",
    borderStyle: "solid",
    fontFamily: fontStyles.nexa_bold,
    fontSize: resize(16),
    fontWeight: "400",
    alignSelf: "center",
    paddingLeft: 20,
    textAlign: "center",
    color: colorPalette.purple
  }
});
