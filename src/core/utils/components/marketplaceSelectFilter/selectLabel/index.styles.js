import {StyleSheet, Platform} from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils"

const isIos = Platform.OS === "ios";
export default StyleSheet.create({
  label:{
    height:resize(49, "height"),
    width:"80%",
    alignItems:"center",
    justifyContent:"center",
    borderBottomWidth: 1,
    borderBottomColor: "#f0f0f0",
    alignSelf: 'center',
  },
  labelText:{
    fontFamily: fontStyles.nexa_light,
    fontSize:18,
    color:colorPalette.purple,
    letterSpacing:0.8
  },
  activeContainer:{
    flexDirection:"row",
    alignItems:"center",
    alignSelf: 'center',
  },
  active:{
    fontFamily:fontStyles.nexa_bold,
    color:colorPalette.purple,
  },
  arrow:{
    marginLeft:resize(10)
  }
});