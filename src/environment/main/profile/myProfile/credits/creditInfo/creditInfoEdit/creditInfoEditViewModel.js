import Picker from "react-native-picker";
import moment from "moment";
import creditsApi from "api/creditsApi";
import { showMsj } from "utils/utils";
import { months } from "resources/l10n";
const convertMonths = () => {
  const month = [];
  months.map((res, i) => (month[i] = { label: res, value: res }));
  return month;
};
const getPlaceHolder = (context) => {
  // if(context.state.year != '' && context.state.year != 'month'){
  //   return 'month'
  // }else{
  //   return "You must select a year"
  // }
  return "month";
};
const convertYears = () => {
  const year = [];
  const years = getYears();
  years.map(
    (res, i) => (year[i] = { label: res.toString(), value: res.toString() })
  );

  return year;
};
const getYears = () => {
  const year = moment().year();
  const allYears = [];
  for (let i = 0; i < 70; i++) {
    allYears[i] = year - i;
  }
  return allYears;
};
const getMonths = (context) => {
  return convertMonths();

  // const year = moment().year();
  // const yearSet = context.state.year;
  // if (yearSet != "") {
  //   if (year != yearSet) {
  //     return convertMonths();
  //   } else {
  //     const month = moment().month();
  //     const mon = [];
  //     const months = convertMonths();
  //     for (let i = 0; i < month + 1; i++) {
  //       mon[i] = months[i];
  //     }
  //     return mon;
  //   }
  // } else {
  //   return [];
  // }
};
const handlePressSelect = (context, selected) => {
  // context.setState({ productionType: selected.name });
  context.setState({ type: selected.name });

  select(context);
};
export const select = (context) => {
  context.refs.select.setModalVisible();
};
const handlePressSave = async (context) => {
  var { type, name, rol, production, month, year, endYear } = context.state;
  const { id } = context.props.navigation.state.params;
  // if (type === "") {
  //   type = id.type;
  // }
  // if (name === "") {
  //   name = id.name;
  // }
  // if (rol === "") {
  //   rol = id.rol;
  // }
  // if (production === "") {
  //   production = id.production;
  // }
  // if (month === "") {
  //   month = id.month;
  // }
  // if (year === "") {
  //   year = id.year;
  // }
  context.setState({ loading: true });

  if (type === "") {
    context.setState({ loading: false });
    return showMsj("Please select production type", true);
  } else if (name === "") {
    context.setState({ loading: false });
    return showMsj("Please enter project name", true);
  } else if (rol === "") {
    context.setState({ loading: false });
    return showMsj("Please enter role", true);
  } else if (production === "") {
    context.setState({ loading: false });
    return showMsj("Please enter production company", true);
  } else if (year === null || year === "") {
    context.setState({ loading: false });
    return showMsj("Please select start year", true);
  } else if (endYear === null || endYear === "") {
    context.setState({ loading: false });
    return showMsj("Please select end year", true);
  } else if (endYear < year) {
    context.setState({ loading: false });
    return showMsj("End year should be greater than start year", true);
  } else if (
    type != "" &&
    name != "" &&
    rol != "" &&
    production != "" &&
    year != "" &&
    endYear != ""
  ) {
    const body = {
      type,
      name,
      rol,
      production,
      month,
      year,
      end_year: endYear,
    };
    try {
      await creditsApi.updateCredits(body, id.id);
      context.props.navigation.popToTop();
      context.setState({ loading: false });
      context.props.navigation.state.params.onGoBack({ ...body, id: id.id });
      return showMsj("Credit update", false);
    } catch (error) {
      context.setState({ loading: false });
      return showMsj("The credits could not be update", true);
    }
  } else {
    context.setState({ loading: false });
    return showMsj("All fields as required", true);
  }
};
export default {
  handlePressSelect,
  handlePressSave,
  select,
  getMonths,
  convertYears,
  getPlaceHolder,
};
