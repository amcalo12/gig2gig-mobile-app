import firebase from 'react-native-firebase';
import moment from 'moment';

const defaultMetadata = {
  contentType: 'image/jpeg'
};

const defaultName = moment().format();

const instance = firebase.storage();

const uploadAsset = async (
  reference,
  uri,
  name = defaultName,
  metadata = defaultMetadata,
  realName = null
) => {
  const currentTime = moment().format();
  const currentTimeIsDiffToDefaultName = currentTime !== defaultName;
  const curentNameDiffToDefaultName = name !== defaultName;
  let finalName = name;
  if (currentTimeIsDiffToDefaultName && !curentNameDiffToDefaultName) {
    finalName = currentTime;
  }
  console.log({
    finalName,
    name,
    currentTimeIsDiffToDefaultName,
    curentNameDiffToDefaultName,
    currentTime,
    defaultName,
    uri
  });
  return new Promise((resolve, reject) => {
    instance
      .ref(reference)
      .child(finalName)
      .putFile(uri, metadata)
      .then(uploadedFile => {
        resolve(uploadedFile.downloadURL);
      })
      .catch(error => {
        console.log('=============================');
        console.log({ error });
        console.log('=============================');
        reject({
          response: {
            data: { data: `We don't have permissions al archivo ${realName}` }
          }
        });
      });
  });
};

export default uploadAsset;
