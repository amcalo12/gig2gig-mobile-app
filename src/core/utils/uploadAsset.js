import { Platform } from 'react-native';
import firebase from 'react-native-firebase';
import * as Firebase from "firebase";
import moment from 'moment';
import RNFetchBlob from 'rn-fetch-blob';
let { fetch } = RNFetchBlob;

const defaultMetadata = {
	contentType: 'image/jpeg',
}

const defaultName = moment().format('HHmmss');

const instance = firebase.storage();

const uploadAsset = async (reference, uri, name = defaultName, metadata = defaultMetadata) => {
	try {
		let filePath;
		console.log('uri', uri)
		if (Platform.OS !== 'ios') {
			if (uri.includes('raw%3A')) {
				filePath = uri.split('raw%3A')[1].replace(/\%2F/gm, '/');
			} else {
				filePath = uri;
			}
			let valor = await RNFetchBlob.fs.stat(filePath);
			console.log('valor', valor)
			console.log('name', name)
			let ext = "." + uri.substring(uri.lastIndexOf('.') + 1, uri.length)
			console.log('name + ext :', name + ext);
			let uploadedFile = await firebase.storage().ref(reference).child(name + ext).putFile(valor.path, metadata)
			console.log('uploadedFile 111', uploadedFile)
			return uploadedFile.downloadURL;
		} else {
			let ext = "." + uri.substring(uri.lastIndexOf('.') + 1, uri.length)
			console.log('name + ext :', name + ext);
			let uploadedFile = await firebase.storage().ref(reference).child(name + ext).putFile(uri, metadata)
			console.log('uploadedFile 111', uploadedFile)
			return uploadedFile.downloadURL;
		}
	} catch (error) {
		console.log('error', { error })
		return error;
	}
}

export default uploadAsset;