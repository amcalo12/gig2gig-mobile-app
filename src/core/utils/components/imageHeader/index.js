import React, { Component } from "react";
import { ImageBackground } from "react-native";
import styles from "./index.styles";
import Loading from "utils/components/loading";
class ImageHeader extends Component {
  state = {
    loading: false
  }
  render() {
    const { size, flag } = this.props;
    const { loading } = this.state;
    let image = this.props.image;
    if (image != null) {
      image = image.replace("\n", "");
    }
    return (
      <ImageBackground
        onLoadStart={() => this.setState({ loading: true })}
        onLoadEnd={() => this.setState({ loading: false })}
        style={[styles.imageBackground, size === "large" ? styles.large : styles.small]}
        source={image ? { uri: image } : null}>
        {
          loading &&
          <Loading style={{ top: "35%", position: "absolute", alignSelf: 'center', }} />
        }
        {this.props.children}
      </ImageBackground>
    );
  }
}
ImageHeader.defaultProps = {
  size: "large",
  flag: false,
  image: "http://www.shakespearetheatre.org/wp-content/uploads/2014/02/under-35-rj.jpg"
};
export default ImageHeader;
