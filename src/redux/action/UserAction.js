export const TYPE_SAVE_USER_DATA = "TYPE_SAVE_USER_DATA"
export const TYPE_CLEAR_USER_DATA = "TYPE_CLEAR_USER_DATA"

export function saveUserDataInRedux(data) {
  return {
    type: TYPE_SAVE_USER_DATA,
    value: data
  }
}

export function clearUserDataInRedux() {
  return {
    type: TYPE_CLEAR_USER_DATA
  }
}