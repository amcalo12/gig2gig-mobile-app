import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils"

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
	content: {
		marginTop: isIos ? height * 0.01 : height * 0.006,
		// marginBottom: resize(20, "height")
	},
	flag: {
		position: 'absolute',
		bottom: resize(20, 'h'),
		right: resize(15),
		width: resize(50),
		alignItems: 'flex-end',
		height: resize(30),
		justifyContent: 'flex-end'
	},
	container: {
		width: "100%",
		marginTop: height * 0.02,
		paddingHorizontal: height * 0.025,
		marginBottom: resize(30, 'height')
	},
	filtersContainer: {
		flexDirection: "row",
		marginBottom: height * 0.016,
		flexWrap: "wrap",
	},
	filtersMarginBottom: {
		marginBottom: resize(8, "height")
	},
	titleContainer: {
		flexDirection: "row",
		alignItems: 'center',
		marginBottom: height * 0.018,
		width: resize(340),
	},
	title: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 24,
		color: colorPalette.purple,
		letterSpacing: 2,
		width: "70%",
	},
	subtitle: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 15,
		color: colorPalette.purple,
		letterSpacing: 1,
		// marginBottom: height * 0.005,
		width: "30%",
		textAlign: "right",

	},
	text: {
		fontFamily: fontStyles.nexa_light,
		fontSize: 13,
		color: colorPalette.purple,
		letterSpacing: 1,
		lineHeight: height * 0.026
	},
	locationContainer: {
		width: "100%",
		marginBottom: height * 0.018
	},
	locationTitle: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 17,
		color: colorPalette.purple,
		letterSpacing: 2,
		paddingHorizontal: height * 0.025,
		marginBottom: height * 0.016
	},
	locationMap: {
		// height: height * 0.204947526237,
		height: resize(180, "height"),
		width: "100%",
	},
	rolesContainer: {
		marginBottom: resize(10, "height")
	},
	rolesTitle: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: resize(15),
		color: colorPalette.purple,
		letterSpacing: 1,
		marginBottom: height * 0.016,
		paddingHorizontal: height * 0.025,
	},
	rolesScroll: {
		paddingHorizontal: height * 0.025
	},
	extraInfo: {
		marginTop: resize(10, "height"),
		paddingHorizontal: height * 0.025,
	},
	finalInfo: {
		marginTop: resize(25, "height"),
		paddingHorizontal: height * 0.025,
	},
	properties: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: resize(14),
		color: colorPalette.purple,
		letterSpacing: 1,
		marginBottom: height * 0.006
	},
	adjust: {
		fontSize: resize(18),
	},
	buttonContainer: {
		width: width * 0.70,
		backgroundColor: colorPalette.mainButtonBg, borderRadius: 20,
		alignItems: "center", justifyContent: "center", alignSelf: "center",
		padding: 15,
		marginTop: 10
	},
	buttonText: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 13,
		color: colorPalette.white,
		textAlign: 'center',
	},
	borderButtonContainer: {
		width: width * 0.70, borderRadius: 30,
		alignItems: "center", justifyContent: "center", alignSelf: "center",
		padding: 10, marginTop: 10,
		flexDirection: "row", backgroundColor: colorPalette.white,
		borderColor: colorPalette.mainButtonBg, borderWidth: 1
	}
});