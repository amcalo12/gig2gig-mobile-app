import React, { Component } from "react";
import { TouchableOpacity, Image, Text, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import styles from "./index.styles";
import RoleIcon from "resources/images/audition/role-icon.png"
class RoleAudition extends Component {

  render() {
    const {role,onPress,} = this.props;
    let image = this.props.image;
    if (image) {
      image = image.replace("\n","");
    }
    return (
        <TouchableOpacity activeOpacity={0.7} style={styles.container} onPress={()=>onPress()}>
          <View style={styles.roleContainer}>
            <LinearGradient colors={['#4d2545', '#782541']} style={styles.roleContainer}>
              <Image style={ image ? styles.roleImage : null} source={ image ? {uri:image} : RoleIcon}/>
            </LinearGradient>
          </View>
          <Text style={styles.roleTitle}>{role}</Text>
        </TouchableOpacity>
    );
  }
}
export default RoleAudition;

RoleAudition.defaultProps = {
  type:"union",
}