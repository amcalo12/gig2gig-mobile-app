import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils"

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  buttonContainer: {
    width: width * 0.70,
    backgroundColor: colorPalette.mainButtonBg, borderRadius: 20,
    alignItems: "center", justifyContent: "center", alignSelf: "center",
    padding: 15,
    marginTop: 10
  },
  buttonText: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 13,
    color: colorPalette.white,
    textAlign: 'center',
  },
  borderButtonContainer: {
    width: width * 0.70, borderRadius: 30,
    alignItems: "center", justifyContent: "center", alignSelf: "center",
    padding: 10, marginTop: 10,
    flexDirection: "row", backgroundColor: colorPalette.white,
    borderColor: colorPalette.mainButtonBg, borderWidth: 1
  }
});