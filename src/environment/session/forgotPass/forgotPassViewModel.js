import { showMsj } from "utils/utils";
import userApi from "api/userApi";
import { errorsValidateMsj } from "resources/l10n";

export const handleSubmitEmail = async (context) => {
  const { email } = context.state
  context.setState({loading:true})
  if(email != ''){
    try {
      await userApi.rememberPassword({email});
      context.props.navigation.goBack()
      context.setState({loading:false})
      return showMsj('We send your new password to your email', false)
    } catch (error) {
      context.setState({loading:false})
      return showMsj(error.data.data, true)
    }
  }else{
    context.setState({loading:false})
    return showMsj(errorsValidateMsj.login.email.message, true)
  }
}

export default {
  handleSubmitEmail,
}