import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const createCredits = async props =>
  apiHandler(
    API_METHOD.POST,
    `${API_MODULES.CREADITS}${API_ROUTES.CREDITS.CREATE}`,
    props
  );

const updateCredits = async (props, id) =>
  apiHandler(
    API_METHOD.PUT,
    `${API_MODULES.CREADITS}${API_ROUTES.CREDITS.UPDATE}/${id}`,
    props
  );

const getCredits = async props =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.CREADITS}${API_ROUTES.CREDITS.SHOW}`,
  );

const deleteCredits = async props =>
  apiHandler(
    API_METHOD.DELETE,
    `${API_MODULES.CREADITS}${API_ROUTES.CREDITS.DELETE}/${props}`,
  );


export default {
  createCredits,
  getCredits,
  updateCredits,
  deleteCredits
};
