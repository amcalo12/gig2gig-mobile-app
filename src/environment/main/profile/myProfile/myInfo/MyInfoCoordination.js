import { createStackNavigator } from "react-navigation";
import MyInfoView from ".";
import MyinfoEditView from "./myInfoEdit"

const MyInfoStack = createStackNavigator(
  {
     MyInfo: {
       screen: MyInfoView,
       navigationOptions: {
         header: null
       }
     },
      MyInfoEdit: {
       screen: MyinfoEditView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "MyInfo",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default MyInfoStack;
