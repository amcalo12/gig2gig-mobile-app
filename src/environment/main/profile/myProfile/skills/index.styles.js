import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
import DeviceInfo from 'react-native-device-info';
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    paddingLeft: resize(40),
    // marginBottom: resize(30, "height"),
    // justifyContent:"space-between",
    // height:resize(520, "height")
  },
  inputContainer:{
    alignItems:"center",
    position:"relative"
  },
  prueba:{
    position:"absolute",
    width:"100%",
    height:"100%",
    backgroundColor:"red"
  },
  input:{
    width:resize(295),
    height: resize(isIos ? (DeviceInfo.hasNotch() ? 45 : 55) : 55, "height"),
    borderRadius: 28,
  },
  inputContent:{
    height:"100%",
    width:"100%",
    flexDirection:"row",
    alignItems:"center",
    paddingLeft:resize(20)
  },
  text:{
    fontFamily:fontStyles.nexa_bold,
    fontSize:18,
    color:colorPalette.purple,
    marginLeft: resize(15),
    ...Platform.select({
      ios: {
        paddingTop: resize(5, "height"),
      },
    }),
  },
  notFoundText:{
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: colorPalette.purple,
    marginTop:resize(50, "height"),
    textAlign:"center"
  },
  labelGroup:{
    width:resize(375),
    flexDirection: 'row',
  },
  button:{
    marginBottom: resize(30, "height"),
  },
  spinner:{
    marginTop:resize(20, "height")
  }
});