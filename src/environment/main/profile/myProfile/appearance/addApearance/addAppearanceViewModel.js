import appearanceApi from "api/appearanceApi";
import { showMsj } from "utils/utils";
import Picker from "react-native-picker";
const showPickerIOS = (context) => {
  Picker.init({
    pickerTitleText: "Height",
    pickerCancelBtnText: "",
    pickerData: [
      [`1'`, `2'`, `3'`, `4'`, `5'`, `6'`, `7'`, `8'`, `9'`],
      [
        `0"`,
        `1"`,
        `2"`,
        `3"`,
        `4"`,
        `5"`,
        `6"`,
        `7"`,
        `8"`,
        `9"`,
        `10"`,
        `11"`,
      ],
    ],
    onPickerSelect: (data) => {
      context.setState({ height: `${data[0].charAt(0)}.${data[1].charAt(0)}` });
    },
  });
  Picker.toggle();
};
const getFeets = () => {
  const totalFeets = 12;
  const feets = [];
  for (let e = 0; e < totalFeets; e++) {
    for (let i = 0; i < totalFeets; i++) {
      // const position = `${e}${i}`;
      // feets[parseInt(position)] = {
      //   label: `${e}.${i} Height`,
      //   value: parseFloat(`${e}.${i}`)
      // };

      feets[feets.length] = {
        label: `${e}.${i} Height`,
        value: `${e}.${i}`,
      };
    }
  }
  return feets;
};
const handlePressSave = async (context) => {
  const {
    height,
    weight,
    eyes,
    hair,
    race,
    flare,
    gender_pronouns,
  } = context.state;
  context.setState({ loading: true });
  // if (height != "" && weight != "" && eyes != "" && hair != "" && race != "") {
  if (eyes != "" && hair != "" && flare !== "" && gender_pronouns !== "") {
    const body = {
      height: height === "" ? "" : parseFloat(height),
      weight: weight === "" ? "" : parseFloat(weight),
      eyes,
      hair,
      race,
      personal_flare: flare,
      gender_pronouns,
    };
    try {
      const res = await appearanceApi.createAppearance(body);
      console.log(res);
      context.props.navigation.state.params.onGoBack(body);
      context.props.navigation.popToTop();
      context.setState({ loading: false });
      return showMsj("Appearance created", false);
    } catch (error) {
      console.log("err", error);
      context.setState({ loading: false });
      return showMsj("he appearance could not be created", true);
    }
  } else if (hair === "") {
    context.setState({ loading: false });
    return showMsj("Please select hair colour", true);
  } else if (eyes === "") {
    context.setState({ loading: false });
    return showMsj("Please select eye colour", true);
  } else if (flare === "") {
    context.setState({ loading: false });
    return showMsj("Please select personal flare", true);
  } else if (gender_pronouns === "") {
    context.setState({ loading: false });
    return showMsj("Please select gender pronouns", true);
  } else {
    context.setState({ loading: false });
    // return showMsj("All fields as required", true);
  }
};
export default {
  handlePressSave,
  getFeets,
  showPickerIOS,
};
