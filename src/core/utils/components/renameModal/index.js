import React, { Component } from 'react';
import { View, Text, Modal, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
// import { colorPalette } from 'resources/colorPalette';
import { colorPalette } from '../../../../resources/colors';
import { fontStyles } from 'resources/fonts';
import { resize } from 'utils/utils';

export default class RenameModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={this.props.onCloseClick}>
        <TouchableOpacity style={styles.modalContainer} onPress={this.props.onCloseClick} activeOpacity={1}>
          <TouchableOpacity activeOpacity={1} style={[styles.modalSubContainer, this.props.style]}>
            <Text style={{
              fontFamily: fontStyles.nexa_bold, fontSize: resize(16),
              marginBottom: 10, alignSelf: "center", color: colorPalette.mainButtonBg
            }}>{"File Name"}</Text>
            <TextInput
              style={{
                borderBottomColor: colorPalette.mainButtonBg,
                borderBottomWidth: 1, paddingBottom: 10, fontSize: resize(16)
              }}
              placeholder={"Enter name"}
              value={this.props.value}
              onChangeText={this.props.onChangeText}
              returnKeyType={"done"}
            />

            <View style={{ marginTop: 20, flexDirection: 'row' }}>
              <TouchableOpacity
                style={styles.cancelContainer}
                onPress={this.props.onCloseClick}>
                <Text style={{ color: "#ffffff", fontFamily: fontStyles.nexa_light }}>{"Cancel"}</Text>
                {/* <Text >{"Cancel"}</Text> */}
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.submitContainer}
                onPress={this.props.onSubmitClick}>
                {/* <Text style={styles.label}>{"OK"}</Text> */}
                <Text style={{ color: "#ffffff", fontFamily: fontStyles.nexa_light }} >{"OK"}</Text>
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </TouchableOpacity>
      </Modal>
    );
  }
}

export const styles = StyleSheet.create({
  modalContainer: {
    flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: colorPalette.SHADOW_COLOR
  },
  modalSubContainer: {
    borderRadius: 8, backgroundColor: colorPalette.white, shadowOpacity: 0.2, elevation: 5, width: "80%", padding: 20
  },
  submitContainer: {
    flex: 1, alignItems: "center",
    justifyContent: "center", marginLeft: 10,
    backgroundColor: colorPalette.mainButtonBg,
    paddingVertical: 10, paddingHorizontal: 20,
  },
  cancelContainer: {
    flex: 1, alignItems: "center", justifyContent: "center",
    marginRight: 10, backgroundColor: colorPalette.mainButtonBg,
    paddingVertical: 10, paddingHorizontal: 20
  },
  label: {
    color: "#ffffff", textTransform: "uppercase"
  }
})