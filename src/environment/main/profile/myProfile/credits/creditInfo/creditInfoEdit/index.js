import React, { Component } from "react";
import { View, Text } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import Input from "utils/components/mainInput";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
import Button from "utils/components/mainButton";
import Select from "utils/components/selectSpinner";
import ButtonClear from "utils/components/secondaryButton";
import DateSelect from "utils/components/dateSelect";
import { auditionFilters } from "resources/l10n";
import creditInfoEditViewModel from "./creditInfoEditViewModel";
import { fontStyles } from "resources/fonts";

class CreditInfoEditView extends Component {
  state = {
    type: "",
    name: "",
    rol: "",
    show: true,
    production: "",
    month: "",
    loading: false,
    year: "",
    endYear: "",
  };
  componentDidMount() {
    const {
      navigation: {
        state: {
          params: { id },
        },
        navigate,
        goBack,
      },
    } = this.props;
    this.setState({
      year: id.year,
      endYear: id.end_year,
      month: id.month,
      production: id.production,
      name: id.name,
      rol: id.rol,
      type: id.type,
    });
  }
  render() {
    const {
      navigation: {
        state: {
          params: { id },
        },
        navigate,
        goBack,
      },
    } = this.props;
    const { type, name, rol, production, month, year } = this.state;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => goBack()}
          barStyle="dark-content"
          text="Credits"
        />
        <Content>
          <View style={styles.container}>
            <View style={styles.form}>
              <Input
                icon={DownArrowIcon}
                data={type}
                // placeholder={id.type}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
                isTouchable
                onPress={() => creditInfoEditViewModel.select(this)}
              />
              <Input
                // placeholder={id.name}
                data={name}
                onEdit={(name) => this.setState({ name })}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <Input
                // placeholder={id.rol}
                data={rol}
                onEdit={(rol) => this.setState({ rol })}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <Input
                // placeholder={id.production}
                data={production}
                onEdit={(production) => this.setState({ production })}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <View style={styles.inputGroup}>
                <DateSelect
                  placeholder={"Start Year"}
                  icon={DownArrowIcon}
                  data={creditInfoEditViewModel.convertYears()}
                  onChange={(year) => this.setState({ year })}
                  short
                  // lng
                  margin={10}
                  // customWidth={styles.input.width}
                  elevation={1.5}
                  value={this.state.year}
                />
                <Text
                  style={{
                    fontFamily: fontStyles.nexa_bold,
                    fontSize: 18,
                    color: "#4D2545",
                    marginLeft: 10,
                    marginRight: 10,
                  }}
                >
                  -
                </Text>
                <DateSelect
                  placeholder={"End Year"}
                  icon={DownArrowIcon}
                  data={creditInfoEditViewModel.convertYears()}
                  onChange={(year) => this.setState({ endYear: year })}
                  short
                  // lng
                  margin={10}
                  // customWidth={styles.input.width}
                  elevation={1.5}
                  value={this.state.endYear}
                />
              </View>

              <DateSelect
                placeholder={creditInfoEditViewModel.getPlaceHolder(this)}
                icon={DownArrowIcon}
                data={creditInfoEditViewModel.getMonths(this)}
                onChange={(month) => this.setState({ month })}
                value={this.state.month}
                // short
                lng
                margin={10}
                customWidth={styles.input.width}
                elevation={1.5}
              />
            </View>
            <View style={styles.button}>
              <Button
                loading={this.state.loading}
                onPress={async () =>
                  await creditInfoEditViewModel.handlePressSave(this)
                }
                text="Update"
              />
            </View>
          </View>
          <Select
            selectTitle="Select a Production Type"
            data={auditionFilters[2].options}
            onPress={(selected) =>
              creditInfoEditViewModel.handlePressSelect(this, selected)
            }
            ref="select"
          />
        </Content>
      </Container>
    );
  }
}
export default CreditInfoEditView;
