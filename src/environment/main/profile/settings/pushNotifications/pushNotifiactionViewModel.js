import pushNotificationApi from "api/pushNotificationApi";
import { showMsj } from "utils/utils";

const updateNotification = async (context, id) => {
	const { listNotification } = context.state
	const index = listNotification.findIndex(res => res.id === id)
	const status = listNotification[index].status == 'off' ? 'on' : 'off'
	listNotification[index] = { ...listNotification[index], status }
	context.setState(listNotification)
	try {
		const res = await pushNotificationApi.updateNotification(id, { status })
		console.log(res)
	} catch (error) {
		console.log(error);
	}
}
const getNotification = async (context) => {
	context.setState({ loading: true })
	try {
		const res = await pushNotificationApi.getNotification()
		const listNotification = res.data
		console.log(listNotification)
		context.setState({ listNotification, loading: false })
	} catch (error) {
		context.setState({ loading: false });
		showMsj('something went wrong', true);
	}
}

export default {
	getNotification,
	updateNotification
}