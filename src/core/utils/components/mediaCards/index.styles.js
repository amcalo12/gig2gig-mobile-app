import {StyleSheet, Platform, Dimensions} from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils"
const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
  container:{
    width:resize(336),
    height: resize(66, "height"),
    borderRadius:10,
    position:"relative",
    marginBottom:resize(15, "height")
  },
  action:{
    width:"100%",
    height:"100%",
    flexDirection: 'row',
    alignItems: 'center',
  },
  mediaIcon:{
    width:"25%",
    height: "100%",
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:"center",
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    overflow: "hidden",
    backgroundColor:"#c6c6c6"
  },
  mediaImage:{
    width:"100%",
    height:"100%",
    resizeMode:"cover"
  },
  iconLogo:{
    justifyContent:"center",
    alignItems:"center",

  },
  icon:{
    marginBottom: isIos ? resize(6, "height") : resize(3, "height"),
    marginTop: resize(5, "height"),
  },
  iconLogoText:{
    color: '#ffffff',
    fontFamily: fontStyles.nexa_bold,
    fontSize: 16,
    letterSpacing: 0.8,
  },
  file: {
    width:"75%",
    height: "100%",
    flexDirection: 'row',
    alignItems: 'center',
  },
  fileTitle: {
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_light,
    fontSize: 16,
    letterSpacing: 0.8,
    marginHorizontal: resize(18),
    paddingVertical: resize(1, "height"),
  },
  points:{
    position:"absolute",
    bottom:0,
    right:resize(3),
    zIndex:10
  },
});