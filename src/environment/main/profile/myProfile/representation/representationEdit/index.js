import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Input from "utils/components/mainInput";
import Button from "utils/components/mainButton";
import RadioButton from "utils/components/radioButton";
import {
  changeRepresentation,
  changeAllow,
  validate,
  handleBack
 } from "./RepresentationEditViewModel";

class RepresentationEditView extends Component {

  state={
    manager:true,//1
    agent:false,//2
    name:"",
    company:"",
    email:"",
    notifications:false,
    loading:false
  }

  componentDidMount(){
    const userRepresentation = this.props.navigation.state.params.hasRepresentation;
    if (userRepresentation) {
      this.setState({
        manager: userRepresentation.type.toUpperCase() === "MANAGER" ? true : false,
        agent: userRepresentation.type.toUpperCase() === "AGENT" ? true : false,
        name: userRepresentation.name,
        company: userRepresentation.company,
        email: userRepresentation.email,
        notifications: userRepresentation.notifications === 1 ? true : false,
      });
    }
  }
  render() {
    const { manager, agent, name, company, email, notifications, loading} = this.state;
    return (
      <Container>
        <Header
        leftIcon="backDark"
        leftFn={()=>handleBack(this)}
        barStyle="dark-content"
        text="Representation"
        />
        <Content>
          <View style={styles.container}>
            <View style={styles.form}>
              <View style={styles.radios}>
                <RadioButton text="Manager" checked={manager} onPress={()=>changeRepresentation(this)}/>
                <RadioButton text="Agent" checked={agent} onPress={()=>changeRepresentation(this)}/>
              </View>
              <Input placeholder="Name" onEdit={name => this.setState({ name })} data={name} elevation={1.5} customWidth={styles.input.width}/>
              <Input placeholder="Company Name" onEdit={company => this.setState({ company })} data={company} elevation={1.5} customWidth={styles.input.width}/>
              <Input placeholder="Email" onEdit={email => this.setState({ email })} data={email} keyboardType="email-address" elevation={1.5} customWidth={styles.input.width}/>
              <View style={styles.allow}>
                <RadioButton
                text="Allow requested auditions to be sent via email"
                checked={notifications}
                onPress={()=>changeAllow(this)}
                />
              </View>
            </View>
            <View style={styles.button}>
              <Button text="Save" loading={loading}  onPress={()=>validate(this)}/>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
export default RepresentationEditView;
