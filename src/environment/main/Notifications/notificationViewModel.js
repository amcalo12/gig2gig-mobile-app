import pushNotificationApi from "api/pushNotificationApi"

const getNotifications = async (context) => {
	context.setState({ loading: true })
	try {
		const res = await pushNotificationApi.getNotificationHistory()
		context.setState({ messages: res.data });
	} catch (error) {
		context.setState({ notFoundData: true })
	}
	context.setState({ loading: false })
}

const onPressDelete = async (context, i, id) => {
	try {
		const { state } = context
		await pushNotificationApi.deleteNotification(id)
		state.messages.splice(i, 1)
		context.setState({
			...state,
			cellOpen: null
		})
	} catch (error) {
	}
}

export default {
	getNotifications,
	onPressDelete
}