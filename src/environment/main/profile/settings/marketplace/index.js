import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import SkillsLabel from 'utils/components/labelMain';
import RightArrow from "resources/images/right-arrow.png"

class Marketplace extends Component {

  render() {
    const { navigation: { navigate, dispatch } } = this.props;

    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          barStyle="dark-content"
          text="Marketplace"
        />
        <Content
          style={styles.content}
          bounces={false}
        >
          <SkillsLabel
            touch
            text={'Submit Business'}
            icon={RightArrow}
            onPress={() => navigate('SubmitBusiness')} />
          <SkillsLabel
            touch
            text={'Feature Listing'}
            icon={RightArrow}
            onPress={() => navigate('FeatureListing')} />
        </Content>
      </Container>
    );
  }
}
export default Marketplace;
