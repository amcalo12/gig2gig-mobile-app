import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Button } from "native-base";
import styles from "./index.styles";
import LinearGradient from 'react-native-linear-gradient';
import { gradientColors } from "utils/utils";
import Loading from "utils/components/loading";
class MainButton extends Component {
  getInputStyles = () => {
    const { customWidth, customHeight } = this.props;
    let InputStyles;
    if (customWidth && customHeight) {
      InputStyles = [styles.button, { width: customWidth, height: customHeight }];
    } else if (customWidth) {
      InputStyles = [styles.button, { width: customWidth }];
    } else if (customHeight) {
      InputStyles = [styles.button, { height: customHeight }];
    } else {
      InputStyles = [styles.button];
    }

    return InputStyles;

  }
  render() {
    const { text, onPress, customWidth, customFontSize, loading, disable } = this.props;
    return (
      // <TouchableOpacity activeOpacity={0.7} style={styles.button} onPress={()=>onPress()}>
      //     <Text style={styles.buttonText}>{text}</Text>
      // </TouchableOpacity>
      <TouchableOpacity disabled={disable && true} activeOpacity={0.8} onPress={loading ? null : () => onPress()}>
        <LinearGradient colors={gradientColors} style={[this.getInputStyles(), disable && { opacity: 0.5 }]}>
          {
            loading ?
              <Loading style={{ top: 0 }} color="#FFFFFF" />
              :
              <Text
                numberOfLines={2}
                style={customFontSize ? [styles.buttonText, { fontSize: customFontSize }] : [styles.buttonText]}>
                {text}
              </Text>
          }
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}
MainButton.defaultProps = {
  isSecure: false,
  customWidth: null,
  customHeight: null,
  customFontSize: null,
  onPress: () => { }
};
export default MainButton;
