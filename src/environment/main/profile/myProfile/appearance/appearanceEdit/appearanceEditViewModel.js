import appearanceApi from "api/appearanceApi";
import { showMsj } from "utils/utils";
import Picker from "react-native-picker";
const showPickerIOS = (context) => {
  Picker.init({
    pickerTitleText: "Height",
    pickerCancelBtnText: "",
    pickerData: [
      [`1'`, `2'`, `3'`, `4'`, `5'`, `6'`, `7'`, `8'`, `9'`],
      [
        `0"`,
        `1"`,
        `2"`,
        `3"`,
        `4"`,
        `5"`,
        `6"`,
        `7"`,
        `8"`,
        `9"`,
        `10"`,
        `11"`,
      ],
    ],
    onPickerSelect: (data) => {
      context.setState({ height: `${data[0].charAt(0)}.${data[1].charAt(0)}` });
    },
  });
  Picker.toggle();
};

const getFeets = () => {
  const totalFeets = 12;
  const feets = [];
  for (let e = 0; e < totalFeets; e++) {
    for (let i = 0; i < totalFeets; i++) {
      // const position = `${e}${i}`;
      // feets[parseInt(position)] = {
      //   label: `${e}.${i} Height`,
      //   value: `${e}.${i}`
      // };

      feets[feets.length] = {
        label: `${e}.${i} Height`,
        value: `${e}.${i}`,
      };
    }
  }
  return feets;
};
const handlePressSave = async (context) => {
  var {
    height,
    weight,
    eyes,
    hair,
    race,
    flare,
    gender_pronouns,
  } = context.state;
  const {
    navigation: {
      navigate,
      goBack,
      state: {
        params: { userInfo, update },
      },
    },
  } = context.props;
  context.setState({ loading: true });
  // if (height === "") {
  //   height = userInfo.height;
  // }
  // if (weight === "") {
  //   weight = userInfo.weight;
  // }
  if (eyes === "") {
    eyes = userInfo.eyes;
  }
  if (hair === "") {
    hair = userInfo.hair;
  }
  if (race === "") {
    race = userInfo.race;
  }
  if (flare === "") {
    flare = userInfo.personal_flare;
  }
  if (gender_pronouns === "") {
    gender_pronouns = userInfo.gender_pronouns;
  }
  const body = {
    height: parseFloat(height),
    weight: parseFloat(weight),
    eyes,
    hair,
    race,
    personal_flare: flare,
    id: userInfo.id,
    gender_pronouns: gender_pronouns,
  };
  try {
    const res = await appearanceApi.updateAppearance(body, userInfo.id);
    context.props.navigation.goBack();
    context.setState({ loading: false });
    update(body);
    return showMsj("Appearance update", false);
  } catch (error) {
    context.setState({ loading: false });
    return showMsj("he appearance could not be update", true);
  }
};
export default {
  handlePressSave,
  getFeets,
  showPickerIOS,
};
