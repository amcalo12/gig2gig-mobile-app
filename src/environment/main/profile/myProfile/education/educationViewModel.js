import educationApi from "api/educationApi";
import { showMsj } from "utils/utils";

const getEducation = async (context) => {
  try {
    const res = await educationApi.getEducation()
    const listEducation = res.data
    context.setState({listEducation, loading:false})
  } catch (error) {
    context.setState({listEducation:[], loading:false, notFoundData:true})
  }
}
const addEducation = async (context, education) => {
  const oldList = context.state.listEducation
  const listEducation = [...oldList,education]
  context.setState({listEducation, notFoundData:false})
}
const updateEducation = async (context, education) => {
  const listEducation = [...context.state.listEducation]
  const index = listEducation.findIndex(i => i.id === education.id)
  listEducation[index] = education
  context.setState({listEducation})
}
const deleteEducation = async (context, id) => {
  var {listEducation} = context.state
  try {
    const index = listEducation.findIndex(res => id === res.id)
    listEducation[index] = {...listEducation[index], loading:true}
    context.setState({listEducation})
    await educationApi.deleteEducation(id)
    listEducation = listEducation.filter((res) => res.id != id)
    context.setState({listEducation})
    return showMsj('Ecuation delete successful')
  } catch (error) {
    return showMsj('Error', true)
  }
}
export default {
  getEducation,
  addEducation,
  updateEducation,
  deleteEducation
}