import React, { Component } from "react";
import {
  Keyboard,
  RefreshControl,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
  Linking,
  KeyboardAvoidingView
} from "react-native";
import { Container, Content, Spinner, Card } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import PlusIcon from "resources/images/plus-icon.png";
import { showMsj, openUrl, shareContent, resize } from "utils/utils";
import { generalMsj, shareMsj } from "resources/l10n";
import MainButton from "utils/components/mainButton";
import PostComment from "utils/components/postComment";
import { getForum, addCommentToPost } from "api/supportForum";
import Loading from "utils/components/loading";
import { refreshControlColors } from "resources/colors";
import HTML from "react-native-render-html";
import { Assets } from "resources/assets";

class DetailSupportForum extends Component {
  state = {
    loading: true,
    post: {},
    comments: [],
    addComment: false,
    loadingSend: false,
    comment: ""
  };

  async componentDidMount() {
    await this.getData();
    this.setState({
      loading: false
    });
  }

  getData = async () => {
    try {
      const res = await getForum(this.props.navigation.state.params.id);
      this.setState({
        post: res.data.post.post_data,
        comments: res.data.comments
      });
    } catch (error) { }
  };

  _onRefresh = async () => {
    this.setState({
      refreshing: true
    });
    await this.getData();
    this.setState({
      refreshing: false
    });
  };

  _onPressAddComment = async () => {
    await this.setState({
      addComment: !this.state.addComment
    });
    this.secondTextInput.focus();
  };

  _onChange = (key, value) => {
    const { state } = this;

    state[key] = value;

    this.setState({
      ...state
    });
  };

  onPressSendComment = async () => {
    const { comment } = this.state;

    if (comment != "") {
      this.setState({
        loadingSend: true
      });
      try {
        await addCommentToPost(
          {
            body: comment
          },
          this.props.navigation.state.params.id
        );

        await this.getData();

        showMsj("Successfull");

        this.setState({
          comment: "",
          loadingSend: false,
          addComment: false
        });
      } catch (error) {
        showMsj("Error", true);
        this.setState({
          loadingSend: false
        });
      }
    } else {
      showMsj("The comment is empty", true);
    }
  };

  render() {
    const {
      loading,
      post,
      comments,
      refreshing,
      addComment,
      loadingSend,
      comment
    } = this.state;

    const { navigation } = this.props;

    return (
      <KeyboardAvoidingView behavior={"padding"} style={{ flex: 1 }}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => navigation.goBack()}
            barStyle="dark-content"
            text={post ? (post.title ? post.title : "") : ""}
          />
          {loading ? (
            <Content>
              <Loading />
            </Content>
          ) : (
              <Content
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={() => this._onRefresh()}
                    colors={refreshControlColors}
                  />
                }
                showsVerticalScrollIndicator={false}
                style={styles.content}
              >
                <View style={{ padding: 20 }}>
                  <View style={styles.cardContainer}>
                    <View style={styles.header}>
                      <View style={styles.avatarAndNameContainer}>
                        <Image
                          // source={{ uri: post.avatar }}
                          source={post.is_admin == 1 ? Assets.gig_avatar : { uri: post.avatar }}
                          placeholderStyle={{ backgroundColor: "transparent" }}
                          style={styles.avatar}
                          defaultSource={post.is_admin == 1 ? Assets.gig_avatar : Assets.defaultAvatar}
                          PlaceholderContent={
                            <Spinner color={"rgba(0,0,0,0.5)"} size={"small"} />
                          }
                        />
                        <Text style={styles.nameTxt}>{post.title}</Text>
                      </View>
                      <Text style={styles.dateTxt}>{post.time_ago}</Text>
                    </View>
                    {/* <Text style={styles.bodyTxt}>{post.body.replace(/(<([^>]+)>)/g, "")}</Text> */}
                    <HTML
                      html={post.body}
                      containerStyle={{
                        marginTop: resize(5),
                      }}
                      onLinkPress={(evt, href) => { Linking.openURL(href); }}
                    />
                  </View>
                  <View style={styles.commmentHeader}>
                    <Text style={styles.commentTxt}>Comment</Text>
                    <TouchableOpacity
                      onPress={() => this._onPressAddComment()}
                      style={styles.addComentRow}
                    >
                      <Image source={PlusIcon} />
                      <Text style={styles.addCommentTxt}>Add Comment</Text>
                    </TouchableOpacity>
                  </View>
                  <FlatList
                    style={{ paddingBottom: 40 }}
                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={
                      <Text style={styles.empty}>
                        This post not have comments
                    </Text>
                    }
                    keyExtractor={(a, i) => `${i}`}
                    data={comments}
                    renderItem={item => <PostComment {...item.item} />}
                  />
                </View>
              </Content>
            )}
        </Container>
        {addComment && (
          <Card style={styles.cardInputContainer}>
            <TextInput
              ref={input => {
                this.secondTextInput = input;
              }}
              style={styles.input}
              placeholder={"Add your comment..."}
              multiline
              value={comment}
              onChangeText={text => this._onChange("comment", text)}
            />
            <TouchableOpacity
              style={styles.btnSend}
              onPress={() => (loadingSend ? {} : this.onPressSendComment())}
            >
              {loadingSend ? (
                <Spinner color={"white"} size={"small"} />
              ) : (
                  <Text style={styles.sendTxt}>Send</Text>
                )}
            </TouchableOpacity>
          </Card>
        )}
      </KeyboardAvoidingView>
    );
  }
}
export default DetailSupportForum;
