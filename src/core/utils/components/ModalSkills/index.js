import React, { Component } from "react";
import {
  Modal,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Platform,
} from "react-native";
import { Text, Card } from "native-base";
import styles from "./index.styles";
import Button from "utils/components/mainButton";
import { resize } from "utils/utils";
import Label from "utils/components/unionMemberLabel";
import { unionMembers } from "resources/l10n";
import PlusIcon from "resources/images/plus-icon.png";
import Loading from "../loading";
const isIos = Platform.OS === "ios";
export default class ModalSkills extends Component {
  state = {
    modalVisible: false,
    checked: false,
    unionMembers: unionMembers,
    text: "",
  };

  setModalVisible() {
    this.setState({
      modalVisible: !this.state.modalVisible,
    });
  }

  handleClick(e) {
    this.setState({
      checked: !this.state.checked,
    });
  }

  toggleCheck = (index, value) => {
    let { unionMembers } = this.state;
    const { onSelected } = this.props;
    unionMembers[index].active = !value;
    this.setState({ unionMembers });
    const result = unionMembers.filter((item) => item.active === true);
    let filtered = [];
    const data = result.map((option, idx) => {
      let name = option.name;
      filtered.push({ name });
    });
    onSelected(filtered);
  };

  render() {
    const {
      stylesData,
      skillsList,
      text,
      onChangeText,
      addSkills,
      load,
    } = this.props;
    let skillsToRender = skillsList.filter((skill) => {
      let title = skill.name.toUpperCase();
      return title.match(new RegExp(text.toUpperCase(), "g"));
    });
    return (
      <Modal
        transparent={true}
        animationType="fade"
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setModalVisible();
        }}
      >
        <View style={styles.container}>
          <Card
            style={[
              styles.info,
              {
                top: isIos ? stylesData.py - 8 : stylesData.py - 30,
                width: stylesData.width,
              },
            ]}
          >
            {load && <Loading style={styles.spinner} />}
            <View style={styles.inputContainer}>
              {/* <Image style={styles.icon} source={PlusIcon}/> */}
              <TextInput
                style={styles.input}
                placeholder="Find a skill"
                selectionColor={styles.input.color}
                onChangeText={(text) => onChangeText(text)}
                value={text}
              />
            </View>
            <ScrollView style={styles.scrollView}>
              {skillsToRender.map((option, idx) => (
                <View key={idx} style={styles.textContainer}>
                  <Text style={styles.text}>
                    {option.name.slice(0, 1).toUpperCase() +
                      option.name.slice(1, option.name.length)}
                  </Text>
                  <TouchableOpacity
                    activeOpacity={0.6}
                    onPress={() => addSkills(option)}
                  >
                    <Image source={PlusIcon} />
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
            <Button
              text="Done"
              customWidth={resize(174)}
              onPress={() => this.setModalVisible()}
            />
          </Card>
        </View>
      </Modal>
    );
  }
}
