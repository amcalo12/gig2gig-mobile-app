import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { Container, Content, Card } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Input from "utils/components/mainInput";
import PlusIcon from "resources/images/plus-icon.png";
import { StackActions } from "react-navigation";
import { AndroidBackHandler } from "react-navigation-backhandler";
import Label from "utils/components/labelMain";
import DeleteIcon from "resources/images/delete-icon.png";
import ModalSkills from "utils/components/ModalSkills";
import myProfileApi from "api/myProfileApi";
import { showMsj } from "utils/utils";
import { generalMsj, skillsMsj } from "resources/l10n";
import Loading from "utils/components/loading";
import AddSkillModal from "./addSkillModal";
class SkillsView extends Component {
  state = {
    stylesData: {},
    skillsList: [],
    searchText: "",
    userSkills: [],
    loading: false,
    loadSkill: false,
    loadDelete: false,
    visible: false,
  };

  async componentDidMount() {
    this.setState({ loading: true });
    this.getUserSkills();
  }

  getUserSkills = async () => {
    // this.setState({loading:true});
    try {
      const response = await myProfileApi.getSkillsList();
      this.setState({ skillsList: response.data });
      const user = await myProfileApi.getUserSkillsList();
      const filtered = response.data.filter(
        (o) => !user.data.some((v) => v.name === o.name)
      );
      this.setState({
        skillsList: filtered,
        userSkills: user.data,
        loading: false,
      });
    } catch (e) {
      this.setState({ loading: false });
      console.log(e);
      if (e.status !== 404 && e.status !== "404") {
        return showMsj(generalMsj.error.message, true);
      }
    }
  };

  handleBackPress = () => {
    const {
      navigation: { dispatch },
    } = this.props;
    return dispatch(StackActions.popToTop());
    return true;
  };

  show = (data) => {
    this.setState({ stylesData: data });
    this.refs.modalSkills.setModalVisible();
  };

  addSkills = async (skill) => {
    if (!this.state.loadSkill) {
      this.setState({ loadSkill: true });
      try {
        const response = await myProfileApi.addSkill({ skills_id: skill.id });
        console.log(response);
        await this.getUserSkills();
        this.setState({ loadSkill: false });
        showMsj(skillsMsj.add.success, false);
      } catch (e) {
        console.log(e);
        this.setState({ loadSkill: false });
        return showMsj(generalMsj.error.message, true);
      }
    }
  };

  deleteSkill = async (skill) => {
    const { userSkills, loadDelete } = this.state;
    if (!loadDelete) {
      this.setState({ loadDelete: true });
      let userSkillsLength = userSkills.length;
      // var index = userSkills.indexOf(skill);
      // userSkills.splice(index, 1);
      try {
        const response = await myProfileApi.deleteSkill(skill.id);
        await this.getUserSkills();
        if (userSkillsLength === 1) {
          this.setState({
            loadDelete: false,
            userSkills: [],
          });
        } else {
          this.setState({
            loadDelete: false,
          });
        }
        showMsj(response.data, true);
      } catch (e) {
        console.log(e);
        this.setState({ loadDelete: false });
        return showMsj(generalMsj.error.message, true);
      }
    }
  };

  toggleModal = async () => {
    this.setState({
      visible: true,
    });
  };

  handleBackBackModal = async () => {
    await this.getUserSkills();
    this.setState({
      visible: false,
    });
  };

  render() {
    const {
      skillsList,
      searchText,
      userSkills,
      loading,
      loadSkill,
      loadDelete,
    } = this.state;
    const {
      navigation: { navigate },
    } = this.props;
    return (
      <AndroidBackHandler onBackPress={() => this.handleBackPress()}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => this.handleBackPress()}
            barStyle="dark-content"
            text="Skills"
            rightText="Add New Skills +"
            rightIcon="text"
            // rightIcon={PlusIcon}
            rightFn={() => this.toggleModal()}
          />
          {!loading && (
            <View style={styles.inputContainer}>
              <Card style={styles.input}>
                <TouchableOpacity
                  activeOpacity={0.6}
                  style={styles.inputContent}
                  ref={(button) => (this.modal = button)}
                  onPress={() => {
                    this.modal.measure((a, b, width, height, px, py) =>
                      this.show({ a, b, width, height, px, py })
                    );
                  }}
                >
                  {/* <Image source={PlusIcon} /> */}
                  {/* <Text style={styles.text}>Add New Skills</Text> */}
                  <Text style={styles.text}>Select Skills</Text>
                </TouchableOpacity>
              </Card>

              <ModalSkills
                stylesData={this.state.stylesData}
                skillsList={skillsList}
                onChangeText={(searchText) => this.setState({ searchText })}
                text={searchText}
                addSkills={this.addSkills}
                load={loadSkill}
                ref="modalSkills"
              />
            </View>
          )}
          <Content>
            {userSkills.length === 0 && !loading && (
              <Text style={styles.notFoundText}>
                You don't have skills added
              </Text>
            )}
            {loading && <Loading style={{ marginTop: 20 }} />}
            <View style={styles.container}>
              {loadDelete && <Loading />}
              {userSkills.length > 0 &&
                userSkills.map((option, idx) => (
                  <Label
                    key={idx}
                    text={option.name}
                    icon={DeleteIcon}
                    onPress={() => this.deleteSkill(option)}
                  />
                ))}
            </View>
          </Content>
          <AddSkillModal
            visible={this.state.visible}
            handleBackBackModal={() => this.handleBackBackModal()}
          />
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default SkillsView;
