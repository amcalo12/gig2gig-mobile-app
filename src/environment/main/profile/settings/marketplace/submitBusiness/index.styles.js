import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  content: {
    alignItems: 'center',
    paddingVertical: resize(15),

  },
  input: {
    width: resize(299),
    height: resize(198, 'h')
  },
  imgBtn: {
    height: '100%',
    width: '100%',
    borderRadius: 33,
    alignItems: 'center',
    paddingTop: resize(14, 'h')
  },
  imgBtnWithImg: {
    height: '100%',
    width: '100%',
    borderRadius: 33,
    alignItems: 'center',
  },
  touchableContainer: {
    width: resize(299),
    height: resize(161, 'h'),
    maxHeight: 161,
    borderRadius: 33,
    marginBottom: resize(32, 'h')
  },
  photoTxt: {
    color: '#4d2545',
    fontFamily: 'Nexa Bold',
    fontSize: resize(20),
    fontWeight: '400',
  },
  img: {
    marginTop: resize(12, 'h')
  }
});