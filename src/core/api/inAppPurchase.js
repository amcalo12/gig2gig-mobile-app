import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";



const subscriptionSuccess = async props =>
  apiHandler(
    API_METHOD.POST,
    API_ROUTES.SUBSCRIPTION.PURCHASE,
    props
  );

const expireStatusUpdate = async props =>
  apiHandler(
    API_METHOD.POST,
    API_ROUTES.SUBSCRIPTION.EXPIRE,
    props
  );
export default {
  subscriptionSuccess,
  expireStatusUpdate
};
