import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Spinner } from "native-base"
import styles from './index.styles';

export default class SkillsLabel extends Component {

  render() {
    const { text, icon, touch, onPress } = this.props;

    if (touch) {
      return (
        <TouchableOpacity activeOpacity={0.5} style={styles.container} onPress={() => onPress()}>
          <Text style={styles.text}>{text}</Text>
          <View style={styles.iconContainer}>
            <Image style={styles.icon} source={icon} />
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.container}>
          <Text style={styles.text}>{text}</Text>
          <TouchableOpacity style={styles.iconContainer} onPress={() => onPress()}>
            <Image style={styles.icon} source={icon} />
          </TouchableOpacity>
        </View>
      );
    }
  }
}