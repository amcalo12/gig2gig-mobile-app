import React, { Component } from "react";
import { View, AsyncStorage, Platform } from "react-native";
import { Container, Content, Text } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Button from "utils/components/squareButton";
import { homeOptions } from "resources/l10n";
import { SHARED_KEY, subscriptionAlert, IN_APP_SKU, subscriptionGeneralAlert, MESSAGES, showAlert, checkSubscriptionAndRedirect, EXPORT_USER_PLAN_NAME, IN_APP_PRICE, IS_SANDBOX_ENVIRONMENT } from "utils/cons";
import * as RNIap from 'react-native-iap';
import inAppPurchase from "api/inAppPurchase";
import moment from "moment";

const itemSubs = Platform.select({
	ios: [
		'com.g2g.phone.ios.annualMembership',
		'com.g2g.phone.ios.monthMembership', // dooboolab
	],
	android: [
		'com.g2g.phone.android.annualmembership', // subscription
		'com.g2g.phone.android.monthmembership', // subscription
	],
});

const SUBSCRIPTIONS = {
	// This is an example, we actually have this forked by iOS / Android environments
	ALL: ['com.g2g.phone.android.annualmembership', 'com.g2g.phone.android.monthmembership'],
}

class HomeView extends Component {
	constructor(props) {
		super(props)
		this.currentActiveReceipt = ""
	}

	state = {
		newNotification: false
	}

	async componentDidMount() {
		// let purchaseData = (new Date(1590660526528)).toUTCString()
		// console.log('purchaseData :>> ', purchaseData);
		// let expireAt = (new Date(moment(purchaseData).add(5, 'm').toDate())).toUTCString()
		// console.log('expireAt 11:>> ', expireAt);

		// let expireMonth = (new Date(moment(purchaseData).add(1, 'M').toDate())).toUTCString()
		// console.log("HomeView -> componentDidMount -> expireMonth", expireMonth)
		// let expireYear = (new Date(moment(purchaseData).add('years', 1).toDate())).toUTCString()
		// console.log("HomeView -> componentDidMount -> expireYear", expireYear)


		const result = await RNIap.initConnection();
		console.log('result :>> ', result);
		this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
		console.log('userData :>> home', this.userData);

		if (this.userData.subscription != null && this.userData.subscription.name != EXPORT_USER_PLAN_NAME) {
			this.getItems()
			if (this.userData.subscription.purchase_platform == Platform.OS) {
				const isSubscribe = await this.isSubscribed();
				console.log('isSubscribe :>> ', isSubscribe);
				console.log('this.currentActiveReceipt :>> ', this.currentActiveReceipt);

				if (!isSubscribe) {
					// subscriptionGeneralAlert(MESSAGES.PLAN_EXPIRE, () => {
					// 	this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true })
					// })
					let params = {
						user_id: this.userData.id,
						original_transaction: this.userData.subscription.original_transaction
					}
					const expire = await inAppPurchase.expireStatusUpdate(params)
					this.userData['isPaidUser'] = false
					await AsyncStorage.setItem("userData", JSON.stringify(this.userData));
					console.log('expire :>> ', expire);
				} else if (!this.userData.isPaidUser && isSubscribe && this.currentActiveReceipt != '') {
					let params = {}
					if (Platform.OS == "ios") {
						params = {
							user_id: this.userData.details.user_id,
							product_id: this.currentActiveReceipt.product_id,
							original_transaction: this.currentActiveReceipt.original_transaction_id,
							current_transaction: this.currentActiveReceipt.transaction_id,
							ends_at: moment.utc((new Date(Number(this.currentActiveReceipt.expires_date_ms))).toUTCString()).format('YYYY-MM-DD HH:mm:ss'),
							purchase_platform: Platform.OS,
							purchased_at: moment.utc((new Date(Number(this.currentActiveReceipt.purchase_date_ms))).toUTCString()).format('YYYY-MM-DD HH:mm:ss'),// purchaseData,
							name: IN_APP_SKU.IOS_MONTH == this.currentActiveReceipt.product_id ? "Monthly" : "Annual",
							transaction_receipt: this.currentActiveReceipt.transaction_receipt,
							purchased_price: IN_APP_SKU.IOS_MONTH == this.currentActiveReceipt.product_id ? IN_APP_PRICE.MONTH : IN_APP_PRICE.YEAR,
						}
					} else {
						let purchaseData = (new Date(this.currentActiveReceipt.transactionDate)).toUTCString()
						console.log('purchaseData :>> ', purchaseData);
						// let expireAt = (new Date(moment(purchaseData).add(5, 'm').toDate())).toUTCString()
						let expireAt = ""
						if (IN_APP_SKU.ANDROID_MONTH == this.currentActiveReceipt.productId) {
							expireAt = (new Date(moment(purchaseData).add(1, 'M').toDate())).toUTCString()
						} else {
							expireAt = (new Date(moment(purchaseData).add('years', 1).toDate())).toUTCString()
						}
						console.log('expireAt 11:>> ', expireAt);

						params = {
							user_id: this.userData.details.user_id,
							product_id: this.currentActiveReceipt.productId,
							original_transaction: this.currentActiveReceipt.purchaseToken,
							current_transaction: this.currentActiveReceipt.transactionId,
							ends_at: moment.utc(expireAt).format('YYYY-MM-DD HH:mm:ss'),
							purchase_platform: Platform.OS,
							purchased_at: moment.utc(purchaseData).format('YYYY-MM-DD HH:mm:ss'),// purchaseData,
							name: IN_APP_SKU.ANDROID_MONTH == this.currentActiveReceipt.productId ? "Monthly" : "Annual",
							transaction_receipt: this.currentActiveReceipt.purchaseToken,
							purchased_price: IN_APP_SKU.ANDROID_MONTH == this.currentActiveReceipt.productId ? IN_APP_PRICE.MONTH : IN_APP_PRICE.YEAR,
						}
					}
					console.log('params :>> ', params);
					this.subscriptionSuccess(params)
				}
			} else if (!this.userData.isPaidUser) {
				// showAlert(MESSAGES.PLAN_EXPIRE_DIFFERENT_OS)
				// subscriptionGeneralAlert(MESSAGES.PLAN_EXPIRE_DIFFERENT_OS, () => {
				// 	this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true })
				// })
			}
		} else {
			// subscriptionGeneralAlert(MESSAGES.SUBSCRIPTION_ALERT_PREMIUM, () => {
			// 	this.props.navigation.navigate('SubscriptionPlan', { isFromHome: true })
			// })
		}

		this.onFocus()
		this.props.navigation.addListener('didFocus', this.onFocus);
	}

	subscriptionSuccess = async (params) => {
		try {
			const purchaseSuccess = await inAppPurchase.subscriptionSuccess(params)
			// this.setState({ loading: false })
			console.log('purchaseSuccess :>> ', purchaseSuccess);
			let data = purchaseSuccess.data
			data['isPaidUser'] = data.is_premium ? true : false
			await AsyncStorage.setItem("userData", JSON.stringify(data));
			// alert("purchase success api call")
		} catch (e) {
			console.log(e);
			// this.setState({ loading: false })
		}
	}

	getItems = async () => {
		try {
			// const products = await RNIap.getProducts(itemSkus);
			const products = await RNIap.getSubscriptions(itemSubs);
			console.log('Products', products);
			// this.setState({ productList: products });
		} catch (err) {
			console.log('err.code', err.code);
			console.log('err.message', err.message);
		}
	};

	isSubscribed = async () => {
		if (Platform.OS === 'ios') {
			const receipt = await RNIap.getReceiptIOS()
			console.log('result :>> ', receipt);
			const validateReceipt = await RNIap.validateReceiptIos({
				'receipt-data': receipt,
				password: SHARED_KEY,
			}, IS_SANDBOX_ENVIRONMENT)
			console.log('validateReceipt :>> ', validateReceipt);
			const { latest_receipt_info: latestReceiptInfo } = validateReceipt;

			const isSubValid = !!latestReceiptInfo.find(receiptData => {
				const expirationInMilliseconds = Number(receiptData.expires_date_ms);
				const nowInMilliseconds = Date.now();
				console.log('checkAvailablePurchase :>> ', expirationInMilliseconds > nowInMilliseconds ? receiptData : false);
				if (expirationInMilliseconds > nowInMilliseconds) {
					this.currentActiveReceipt = receiptData
					this.currentActiveReceipt['transaction_receipt'] = receipt
				}

				return expirationInMilliseconds > nowInMilliseconds;
			});

			console.log('isSubValid :>> ', isSubValid);
			return isSubValid
		}

		if (Platform.OS === 'android') {
			// When an active subscription expires, it does not show up in
			// available purchases anymore, therefore we can use the length
			// of the availablePurchases array to determine whether or not
			// they have an active subscription.
			const availablePurchases = await RNIap.getAvailablePurchases();
			console.log('availablePurchases android :>> ', availablePurchases);
			for (let i = 0; i < availablePurchases.length; i++) {
				console.log('android id:>> ', availablePurchases[i].productId);
				if (SUBSCRIPTIONS.ALL.includes(availablePurchases[i].productId)) {
					console.log('availablePurchases[i].productId :>> ', availablePurchases[i].productId);
					this.currentActiveReceipt = availablePurchases[i]
					return true;
				}
			}
			return false;
		}
	}

	componentWillUnmount() {
		RNIap.endConnection();
	}

	onFocus = async () => {

		this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
		console.log('this.userData onFocus================:>> ', this.userData);
		this.setState({ newNotification: false })
		const newNotification = await AsyncStorage.getItem('newNotification')
		newNotification ?
			this.setState({ newNotification: true }) :
			this.setState({ newNotification: false })
	}
	renderItem = (option, idx) => {
		const { navigation: { navigate } } = this.props;
		return (
			<Button
				key={idx}
				text={option.name}
				icon={option.icon}
				onPress={() => {
					// if (option.screen == "Marketplace" && !this.userData.isPaidUser) {
					// 	checkSubscriptionAndRedirect(this.userData, 'SubscriptionPlan', this.props)
					// 	return
					// }
					navigate(option.screen, option.params ? option.params : null)
				}}
			/>
		)
	}

	render() {
		const { newNotification } = this.state
		const { navigation: { navigate } } = this.props;
		return (
			<Container>
				<Header
					newNotification={newNotification}
					rightIcon="notification"
					barStyle="dark-content"
					clear={false}
				/>
				<Content>
					<View style={styles.container}>
						{
							homeOptions.map((option, idx) => (
								this.renderItem(option, idx)
							))
						}
					</View>
				</Content>
			</Container>
		);
	}
}
export default HomeView;
