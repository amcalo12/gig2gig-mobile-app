import React, { Component } from 'react';
import { View, Text, FlatList, StatusBar, Platform, TouchableOpacity, ImageBackground, Alert, AsyncStorage } from 'react-native';
import Background from "resources/images/login/background.png";
import styles from "./index.styles";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { SafeAreaView, StackActions, NavigationActions } from 'react-navigation';
import * as RNIap from 'react-native-iap';
import { IN_APP_SKU, IN_APP_PRICE } from 'utils/cons';
import Button from "utils/components/mainButton";
import inAppPurchase from 'api/inAppPurchase';
import moment from 'moment';
import firebase from "react-native-firebase";

export default class PurchaseComplete extends Component {
  constructor(props) {
    super(props);
    this.userData = {}
    // this.currentIndex = 0
    this.state = {
      loading: false
    };
  }

  async componentDidMount() {
    const result = await RNIap.initConnection();
    console.log('sku :>> ', this.props.navigation.state.params.sku);
    console.log('planName :>> ', this.props.navigation.state.params.planName);
    this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
    console.log('userData :>> purchase', this.userData);
  }

  requestSubscription = async (sku) => {
    try {
      this.setState({ loading: true })
      // firebase.crashlytics().log(sku)
      // firebase.crashlytics().log("Request Begin")
      const purchase = await RNIap.requestSubscription(sku);
      firebase.analytics().logEvent("PURCHASE_DONE", {})
      // firebase.crashlytics().log("Purchase Data Get")
      const ackResult = await RNIap.finishTransaction(purchase);
      console.log('purchase :>>11 ', purchase);
      console.log('purchase :>>11 ', ackResult);
      // return purchase
      let purchaseData = (new Date(purchase.transactionDate)).toUTCString()
      console.log('purchaseData :>> ', purchaseData);
      // let expireAt = (new Date(moment(purchaseData).add(5, 'm').toDate())).toUTCString()
      let expireAt = ""
      if (IN_APP_SKU.IOS_MONTH == sku || IN_APP_SKU.ANDROID_MONTH == sku) {
        expireAt = (new Date(moment(purchaseData).add(1, 'M').toDate())).toUTCString()
      } else {
        expireAt = (new Date(moment(purchaseData).add('years', 1).toDate())).toUTCString()
      }
      // (new Date(moment(purchaseData).add(5, 'm').toDate())).toUTCString()

      console.log('expireAt 11:>> ', expireAt);
      let params = {
        user_id: this.userData.details.user_id,
        product_id: purchase.productId,
        original_transaction: Platform.OS == "ios" ? purchase.originalTransactionIdentifierIOS : purchase.purchaseToken,
        current_transaction: purchase.transactionId,
        ends_at: moment.utc(expireAt).format('YYYY-MM-DD HH:mm:ss'),
        purchase_platform: Platform.OS,//    <ios, android, web>,
        purchased_at: moment.utc(purchaseData).format('YYYY-MM-DD HH:mm:ss'),// purchaseData,
        name: IN_APP_SKU.IOS_MONTH == sku || IN_APP_SKU.ANDROID_MONTH == sku ? "Monthly" : "Annual",
        transaction_receipt: Platform.OS == "ios" ? purchase.transactionReceipt : purchase.purchaseToken,
        purchased_price: IN_APP_SKU.IOS_MONTH == sku || IN_APP_SKU.ANDROID_MONTH == sku ? IN_APP_PRICE.MONTH : IN_APP_PRICE.YEAR,
      }
      // firebase.crashlytics().log("Purchase params send")
      this.subscriptionSuccess(params)
    } catch (err) {
      this.setState({ loading: false })
      firebase.analytics().logEvent("PURCHASE_ERROR", { error: err.code })
      console.log('err :>> ', err);
      console.log('err :>>11 ', err.code);
      // firebase.crashlytics().recordError(22, err.code);
      Alert.alert(err.message);
    }
  };

  subscriptionSuccess = async (params) => {
    try {
      // firebase.crashlytics().log("Purchase stared")
      const purchaseSuccess = await inAppPurchase.subscriptionSuccess(params)
      // firebase.crashlytics().log("Purchase success")
      this.setState({ loading: false })
      console.log('purchaseSuccess :>> ', purchaseSuccess);

      let data = purchaseSuccess.data
      data['isPaidUser'] = data.is_premium ? true : false
      await AsyncStorage.setItem("userData", JSON.stringify(data));

      this.props.navigation.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "AppIntro" })]
        })
      );
    } catch (e) {
      // firebase.crashlytics().recordError(33, e);
      this.setState({ loading: false })
      firebase.analytics().logEvent("PURCHASE_SUCCESS_ERROR", { error: e })
      console.log(e);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="transparent"
        />
        <ImageBackground source={Background} style={styles.background}>
          <Text style={styles.text}>{'Your registration completed successfully, Please complete your subscription by click on below button.'}</Text>
          <View style={styles.logoContainer}>
            {/* <TouchableOpacity
              style={styles.positionText}
              onPress={() => this.requestSubscription('com.g2g.phone.ios.monthMembership')}
            >
              <Text style={[styles.formText, { fontSize: 20 }]}>
                Pay for Subscription
                </Text>
            </TouchableOpacity> */}
            <Button
              text={"Continue"}
              onPress={() => this.requestSubscription(this.props.navigation.state.params.sku)}
              loading={this.state.loading}
            />
          </View>
        </ImageBackground>
      </View>
    );
  }
}
