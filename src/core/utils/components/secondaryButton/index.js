import React, { Component } from "react";
import { Text, TouchableOpacity, Image } from "react-native";
import styles from "./index.styles";
import Loading from "../loading";


class SecondaryButton extends Component {
  render() {
    const {text, onPress, customWidth,customFontSize, icon, loading} = this.props;
    return (
        <TouchableOpacity
        activeOpacity={0.8}
        onPress={loading ? null : ()=>onPress()}
        style={ customWidth ?[styles.button, {width:customWidth}] : [styles.button]}
        >
        {
          loading &&
          <Loading style={{position:"absolute", left:0, right:0}}/>
        }
        {
          icon &&
          <Image style={styles.icon} source={icon}/>
        }
          <Text
          style={ customFontSize ? [styles.buttonText, {fontSize:customFontSize}] : [styles.buttonText]}>
          {text}
          </Text>
        </TouchableOpacity>
    );
  }
}
SecondaryButton.defaultProps = {
  customWidth:null,
  customFontSize:null,
  icon:null
};
export default SecondaryButton;
