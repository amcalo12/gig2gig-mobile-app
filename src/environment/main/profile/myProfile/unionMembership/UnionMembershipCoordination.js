import { createStackNavigator } from "react-navigation";
import UnionMembershipView from ".";

const UnionMembershipStack = createStackNavigator(
  {
     UnionMembership: {
       screen: UnionMembershipView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "UnionMembership",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default UnionMembershipStack;
