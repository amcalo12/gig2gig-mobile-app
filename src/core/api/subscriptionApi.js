import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const setPlan = async props =>
  apiHandler(
    API_METHOD.POST,
    `${API_MODULES.SUBSCRIPTIONS}`,
    props
  );
const addPaymentMethod = async props => 
  apiHandler(
    API_METHOD.POST,
    `${API_MODULES.SUBSCRIPTIONS}${API_ROUTES.SUBSCRIPTIONS.CREATE}`,
    props
  )
const updatePaymentMethod = async props => 
  apiHandler(
    API_METHOD.POST,
    `${API_MODULES.SUBSCRIPTIONS}${API_ROUTES.SUBSCRIPTIONS.UPDATE}`,
    props
  )
const getPaymentMethod = async props => 
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.SUBSCRIPTIONS}${API_ROUTES.SUBSCRIPTIONS.SHOW}`,
  )

const updatePlan = async props => 
  apiHandler(
    API_METHOD.POST,
    `${API_MODULES.SUBSCRIPTIONS}`,
    props
  )
export default {
  setPlan,
  addPaymentMethod,
  getPaymentMethod,
  updatePlan,
  updatePaymentMethod
};
