import {StyleSheet, Platform, Dimensions} from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils"
const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
  container:{
    height:resize(48, "height"),
    width: resize(48, "height"),
    borderRadius:40,
    backgroundColor:colorPalette.purple,
    alignItems: 'center',
    justifyContent:"center",
    position:"absolute",
    bottom:resize(20, "height"),
    right:resize(20),
    zIndex:10,
    overflow:"hidden"
  },
  content:{
    height:"100%",
    width:"100%",
    alignItems:"center",
    justifyContent:"center"
  }

});