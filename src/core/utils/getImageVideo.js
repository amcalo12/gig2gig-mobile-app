import ImagePicker from 'react-native-image-picker';

const getImageVideo = async (mediaType = 'photo') => {
  const options = {
    title: 'Select image',
    cameraType: 'back',
    mediaType,
    maxWidth: 640,
    maxHeight: 427,
    storageOptions: {
      cameraRoll: false,
      skipBackup: true,
      path: 'gg2gg',
    },
  };

  return new Promise((resolve, reject) => {
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        reject(response);
      } else if (response.error) {
        console.log('response  3333:', response);
        reject(response);
      } else {
        resolve(response);
      }
    });
  });
}

export default getImageVideo;