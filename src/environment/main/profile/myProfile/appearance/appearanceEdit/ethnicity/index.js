import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import Label from "utils/components/unionMemberLabel";
import { AndroidBackHandler } from "react-navigation-backhandler";
import { ethnicityOptions } from "resources/l10n";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";

class EthnicityView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      race: ethnicityOptions
    };
    if (this.props.navigation.state.params.race != undefined) {
      this.selected = this.props.navigation.state.params.race.split(",");
      this.state.race.map(data => {
        this.selected.map(item => {
          if (data.name == item) {
            data.checked = true;
          }
        });
      });
    }
  }

  // state = {
  //   race: ethnicityOptions
  // }
  handleBackPress = () => {
    const {
      navigation: { goBack }
    } = this.props;
    return goBack();
    return true;
  };
  changeChecked = (option, i) => {
    const {
      navigation: {
        navigate,
        state: {
          params: { onGoBack }
        }
      }
    } = this.props;
    const { race } = this.state;
    const race2 = [];
    let selectedRace = "";
    race.map(res => {
      if (res.name === option.name) {
        res.checked = !res.checked;
        // res.checked = true
      }

      if (res.checked) {
        selectedRace = selectedRace + "," + res.name;
      }
      // else {
      //   res.checked = false
      // }
      race2.push(res);
    });
    this.setState({ race: race2 });
    // onGoBack(option)
    onGoBack(selectedRace.replace(/^,/, ""));
  };
  render() {
    const {
      navigation: { navigate }
    } = this.props;
    const { race } = this.state;
    return (
      <AndroidBackHandler onBackPress={() => this.handleBackPress()}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => this.handleBackPress()}
            barStyle="dark-content"
            text="Ethnicity"
          />
          <Content>
            <View style={styles.container}>
              <Text style={styles.title}>{"Select All that Apply"}</Text>
              {race.map((option, idx) => (
                <Label
                  key={idx}
                  text={option.name}
                  checked={option.checked}
                  onPress={() => this.changeChecked(option, idx)}
                />
              ))}
            </View>
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default EthnicityView;
