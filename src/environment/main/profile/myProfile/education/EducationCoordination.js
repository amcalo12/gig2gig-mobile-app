import { createStackNavigator } from "react-navigation";
import MyInfoView from ".";
import EducationView from ".";
import EducationInfoView from "./educationInfo";
import AddEducation from "./addEducation";
import EducationInfoEdit from "./educationInfo/educationInfoEdit";

const EducationStack = createStackNavigator(
  {
     Education: {
       screen: EducationView,
       navigationOptions: {
         header: null
       }
     },
     EducationInfo: {
       screen: EducationInfoView,
       navigationOptions: {
         header: null
       }
     },
     EducationInfoEdit: {
       screen: EducationInfoEdit,
       navigationOptions: {
         header: null
       }
     },
     AddEducation: {
      screen: AddEducation,
      navigationOptions: {
        header: null
      }
    },
  },
  {
    initialRouteName: "Education",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default EducationStack;
