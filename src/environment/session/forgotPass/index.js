import React, { Component } from "react";
import { View, Text, ImageBackground, StatusBar, TouchableOpacity, Image } from "react-native";
import { Container, Content } from "native-base";
import Background from "resources/images/login/background.png";
import styles from "./index.styles";
import Input from "utils/components/mainInput";
import Button from "utils/components/mainButton";
import Header from "utils/components/mainHeader";
import forgotPassViewModel from './forgotPassViewModel';
class ForgotPass extends Component {
  state = {
    email: "",
    loading: false
  }

  handleForgotPassword = async () => {
    await forgotPassViewModel.handleSubmitEmail(this)
  }
  render() {
    const { email } = this.state;
    const { navigation: { goBack } } = this.props;
    return (
      <Container style={styles.container}>
        <View style={styles.imageBackground}>
          <Image source={Background} style={styles.backgroundImage} />
        </View>
        <Header leftFn={() => goBack()} />
        <View style={styles.background}>
          <Text style={styles.textForgot}>Forgot Password</Text>
          <View style={styles.formContainer}>
            <Input placeholder="Email" onEdit={(email) => this.setState({ email })} data={email} />
            <Button loading={this.state.loading} text="Submit" onPress={() => this.handleForgotPassword()} />
          </View>
        </View>
      </Container>
    );
  }
}
export default ForgotPass;
