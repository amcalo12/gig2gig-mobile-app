import { createStackNavigator } from "react-navigation";
import MusicView from ".";

const MusicStack = createStackNavigator(
  {
     Music: {
       screen: MusicView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "Music",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default MusicStack;
