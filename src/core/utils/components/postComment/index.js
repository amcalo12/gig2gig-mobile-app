import React, { Component } from "react";
import { TouchableOpacity, Text, View, Platform } from "react-native";
import { Card, Spinner } from "native-base";
import styles from "./index.styles";
import moment from "moment";
import { Image } from 'react-native-elements';

type Props = {
  avatar: String,
  body: String,
  time_ago: String,
  name: String
}

export default function PostComment(props: Props) {

  const {
    avatar,
    body,
    time_ago,
    name
  } = props

  return (
    <View
      style={styles.cardContainer}>
      <View
        style={styles.header}
      >
        <View
          style={styles.avatarAndNameContainer}
        >
          <Image
            source={{ uri: avatar }}
            placeholderStyle={{ backgroundColor: 'transparent' }}
            style={styles.avatar}
            PlaceholderContent={
              <Spinner
                color={'rgba(0,0,0,0.5)'}
                size={'small'}
              />
            }
          />
          <Text
            style={styles.nameTxt}
          >
            {name}
          </Text>
        </View>
        <Text
          style={styles.dateTxt}
        >
          {time_ago}
        </Text>
      </View>
      <Text
        style={styles.bodyTxt}
        numberOfLines={4}
      >
        {body}
      </Text>
    </View>
  );
}

PostComment.defaultProps = {
  avatar: '',
  body: '',
  time_ago: '',
  name: '',
}
