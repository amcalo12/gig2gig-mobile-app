import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");


export default StyleSheet.create({
  container:{
    alignItems:"center",
    paddingRight: height * 0.022,
  },
  roleContainer: {
    height: height * 0.0704647676162,
    width: height * 0.0704647676162,
    borderRadius: 50,
    justifyContent:"center",
    alignItems:"center",
    overflow:"hidden"
  },
  roleTitle:{
    fontFamily:fontStyles.nexa_bold,
    fontSize:12,
    color:colorPalette.purple,
    marginTop: height * 0.008,
  },
  roleImage:{
    height:"100%",
    width:"100%"
  }
});