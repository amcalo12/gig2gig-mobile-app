import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Loading from "utils/components/loading";
import { StackActions } from 'react-navigation';
import InfoLabel from "utils/components/infoLabel";
import { AndroidBackHandler } from 'react-navigation-backhandler';
import PlusIcon from "resources/images/myCalendar/plus-icon.png";
import educationViewModel from './educationViewModel';
import DeleteIcon from "resources/images/delete-icon.png";
class EducationView extends Component {
  state = {
    listEducation: [],
    loading: true
  }
  componentDidMount = async () => {
    await educationViewModel.getEducation(this)
  };
  handleBackPress = () => {
    const {navigation : {dispatch}} = this.props;
    return dispatch(StackActions.popToTop())
    return true;
  }
  addEducation = (data) => {
    educationViewModel.addEducation(this, data)
  }
  updateEducation = (data) => {
    educationViewModel.updateEducation(this, data)
  }
  renderLabels = () => 
    this.state.listEducation.map((res,i) => {
      return <TouchableOpacity key={i} onPress={()=>this.props.navigation.navigate("EducationInfo", {id:res, 'onGoBack': this.updateEducation})}>
                <InfoLabel
                  loading={res.loading && res.loading}
                  icon={DeleteIcon}
                  onPress={() => educationViewModel.deleteEducation(this, res.id)}
                  paddingLeft={false}
                  text={`Education ${i+1}`}
                  subtext={`${res.school} ${res.year}`}
                />
              </TouchableOpacity>
  })
  render() {
    const {navigation : {navigate, dispatch}} = this.props;
    return (
      <AndroidBackHandler onBackPress={()=>this.handleBackPress()}>
        <Container>
          <Header
          leftIcon="backDark"
          leftFn={()=>this.handleBackPress()}
          barStyle="dark-content"
          text="Education & Training"
          rightIcon={PlusIcon}
          rightFn={()=>navigate("AddEducation", {'onGoBack': this.addEducation})}
          />
          <Content>
            {this.state.loading?
              <Loading style={{top:"40%", alignSelf: 'center',}}/>
              :
              this.state.notFoundData?
                <Text style={styles.notFoundText}>There's nothing here yet</Text>
                :
                <View style={styles.container}>
                  {this.renderLabels()}
                </View>
            }
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default EducationView;
