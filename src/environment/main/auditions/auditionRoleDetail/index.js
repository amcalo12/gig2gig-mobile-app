import React, { Component } from "react";
import { View, Alert } from "react-native";
import { Container, Content, Text } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import DownloadIcon from "resources/images/download-clear.png";
import Role from "utils/components/roleAudition";
import Filter from "utils/components/auditionFilterButton";
import ImageHeader from "utils/components/imageHeader";
import Button from "utils/components/mainButton";
import ButtonClear from "utils/components/secondaryButton";
import AddIcon from "resources/images/audition/add-icon.png";
import { convertTimeToHour, showMsj, shareContent } from "utils/utils";
import auditionsApi from "api/auditionsApi";
import { generalMsj, shareMsj } from "resources/l10n";
import { StackActions } from "react-navigation";
import myMediaApi from "api/myMediaApi";
import myProfileApi from "api/myProfileApi";

class AuditionRoleDetailView extends Component {
  state = {
    roleData: {},
    auditionData: {},
    loadingSave: false,
    loadingRequest: false,
    loadingMaterials: false
  };

  componentDidMount() {
    const roleData = this.props.navigation.state.params.roleData;
    const auditionData = this.props.navigation.state.params.auditionData;
    this.setState({
      roleData,
      auditionData
    });
  }

  saveAuditionMaterials = async id => {
    this.setState({ loadingMaterials: true });
    try {
      console.log("audition", id);
      const response = await myMediaApi.addMediaToUser({ audition: id });
      this.setState({ loadingMaterials: false });
      return showMsj("Audition materials added successfully", false);
    } catch (e) {
      console.log(e);
      this.setState({ loadingMaterials: false });
      if (e.status && e.status === 406) {
        return showMsj(e.data.data, true);
      }
      showMsj(generalMsj.error.message, true);
    }
  };

  checkForRepresentation = async (audition, type, online) => {
    //type 1 para guardarlo en upcoming
    //type 2 es para guardarlo en request
    try {
      const response = await myProfileApi.getRepresentation();
      console.log("representation=====");
      console.log(response);
      this.saveAudition(audition, type, online);
    } catch (e) {
      this.askToAddRepresentation(audition, type, online);
      console.log(e);
    }
  };
  askToAddRepresentation(audition, type, online) {
    Alert.alert(
      "",
      "Currently you don't have Representation",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "Go to Representation",
          onPress: () =>
            this.props.navigation.navigate("RepresentationEditView", {
              hasRepresentation: null,
              isFromAuditionDetail: true
            })
        }
      ],
      { cancelable: false }
    );
  }
  saveAudition = async (audition, type, online) => {
    const {
      navigation: { dispatch }
    } = this.props;
    console.log(audition);
    this.setState({
      loadingSave: type === 1 ? true : false,
      loadingRequest: type === 2 ? true : false
    });
    const params = {
      appointment: audition.appointmentId,
      rol: audition.rol,
      type: type,
      online
    };
    console.log('params save Audition:', params);
    // return
    try {
      const response = await auditionsApi.requestAudition(params);
      this.setState({
        loadingSave: false,
        loadingRequest: false
      });
      showMsj(response.data, false);
      return dispatch(StackActions.popToTop());
    } catch (e) {
      console.log(e);
      this.setState({
        loadingSave: false,
        loadingRequest: false
      });

      if (e.status && e.status === 406) {
        if (e.data.data) {
          return showMsj(e.data.data, true);
        } else {
          return showMsj(e.data.error, true);
        }
      }
      showMsj(generalMsj.error.message, true);
    }
  };

  shareAudition = context => {
    const { auditionData } = this.state;
    const message = `${shareMsj.Auditions.first} ${auditionData.title} ${shareMsj.Auditions.second}
                    \n${shareMsj.Auditions.third}
                    ${shareMsj.Auditions.Apple}
                    ${shareMsj.Auditions.Android}
                    `;
    shareContent(message);
  };

  render() {
    const {
      roleData,
      auditionData,
      loadingSave,
      loadingRequest,
      loadingMaterials
    } = this.state;
    const {
      navigation: { goBack }
    } = this.props;
    let online = auditionData.online === 1;
    let walk = auditionData.walkAppont ? auditionData.walkAppont.length : 0;
    return (
      <Container>
        <ImageHeader image={roleData.image && roleData.image.url}>
          <Header
            leftIcon="back"
            leftFn={() => goBack()}
            rightIcon={DownloadIcon}
            rightFn={() => this.shareAudition(this)}
          />
        </ImageHeader>
        <Content style={styles.content}>
          <View style={styles.container}>
            <View style={styles.filtersContainer}>
              <Filter
                small={false}
                text={auditionData.union}
                marginBottom={styles.filtersMarginBottom.marginBottom}
              />
              <Filter
                small={false}
                type="contract"
                text={auditionData.contract}
                marginBottom={styles.filtersMarginBottom.marginBottom}
              />
              {auditionData.production &&
                auditionData.production.map((option, idx) => (
                  <Filter
                    key={idx}
                    small={false}
                    type="production"
                    text={option}
                    marginBottom={styles.filtersMarginBottom.marginBottom}
                  />
                ))}
              {auditionData.online === 1 && (
                <Filter
                  small={false}
                  type="online"
                  text={"ONLINE SUBMISSION"}
                  marginBottom={styles.filtersMarginBottom.marginBottom}
                />
              )}
            </View>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{auditionData.title}</Text>
              <Text style={styles.subtitle}>
                {convertTimeToHour(auditionData.create)}
              </Text>
            </View>
            <Text style={styles.roleTitle}>{roleData.name}</Text>
            <Text style={styles.text}>{roleData.description}</Text>
          </View>
          {/* {auditionData.online !== 1 ? (
            <ButtonClear
              text="Audition Materials to My Media"
              customFontSize={13}
              customWidth="auto"
              icon={AddIcon}
              loading={loadingMaterials}
              onPress={() =>
                this.saveAuditionMaterials(auditionData.auditionId)
              }
            />
          ) : null} */}
          {/* <View style={styles.actionButtonsContainer}>
            {online || walk !== 0 ? (
              <Button
                text="Save Audition"
                customFontSize={13}
                customWidth={styles.actionButtons.width}
                loading={loadingSave}
                onPress={() =>
                  this.checkForRepresentation(auditionData, 1, online)
                }
              />
            ) : null}
            {!online ? (
              <Button
                text="Request Audition from Agent"
                customFontSize={13}
                customWidth={styles.actionButtons.width}
                loading={loadingRequest}
                onPress={() =>
                  this.checkForRepresentation(auditionData, 2, online)
                }
              />
            ) : null}
          </View> */}
        </Content>
      </Container>
    );
  }
}
export default AuditionRoleDetailView;
