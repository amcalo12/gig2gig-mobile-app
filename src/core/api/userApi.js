import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const login = async props =>
	apiHandler(
		API_METHOD.POST,
		API_ROUTES.AUTH.LOGIN,
		props
	);

const createAccount = async props =>
	apiHandler(
		API_METHOD.POST,
		API_ROUTES.AUTH.CREATE_ACCOUNT,
		props
	);


const rememberPassword = async props =>
	apiHandler(
		API_METHOD.POST,
		API_ROUTES.AUTH.REMEMBER_PASSWORD,
		props
	);

const getUserInfo = async id =>
	apiHandler(
		API_METHOD.GET,
		`${API_ROUTES.AUTH.GET_USER_INFO}${id}`
	);

export const getUserSettings = async () =>
	apiHandler(
		API_METHOD.GET,
		`users/settings`
	);

export const updateUserSettings = async (data, id = '') =>
	apiHandler(
		API_METHOD.PUT,
		`users/settings/${id}`,
		data
	);

export const createSkillsInUser = async (data) =>
	apiHandler(
		API_METHOD.POST,
		`a/skills/create`,
		data
	);

const getUnionMembershipList = async () =>
	apiHandler(
		API_METHOD.GET,
		API_ROUTES.MY_PROFILE.GET_UNION_MEMBERSHIP_LIST
	);

const updateUnionMembershipList = async props =>
	apiHandler(
		API_METHOD.PUT,
		API_ROUTES.MY_PROFILE.UPDATE_UNION_MEMBERSHIP_LIST,
		props
	);

export const setFeatureListing = async props =>
	apiHandler(
		API_METHOD.POST,
		API_ROUTES.FEATURE.FEATURE_CREATE,
		props
	);

export const addSocial = async (user_id, data) => {
	apiHandler(API_METHOD.PUT,
		`${API_ROUTES.MY_PROFILE.ADD_SOCIAL}${user_id}/update`,
		data
	)
}

export default {
	login,
	rememberPassword,
	createAccount,
	getUserInfo,
	getUnionMembershipList,
	updateUnionMembershipList
};
