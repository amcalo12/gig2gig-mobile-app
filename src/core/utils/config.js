import { createStore, combineReducers, compose } from "redux";

import { createReducer } from "utils/utils";
import { userReducer } from "src/redux/reducer/UserReducer";

export default () => {
  // eslint-disable-next-line no-underscore-dangle
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const combinedReducers = combineReducers({
    createReducer: createReducer,
    userReducer: userReducer
  });
  const store = createStore(combinedReducers, composeEnhancers());
  return { store };
};
