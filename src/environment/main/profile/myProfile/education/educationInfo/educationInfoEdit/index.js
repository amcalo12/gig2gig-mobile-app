import React, { Component } from "react";
import { View, Text } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import Input from "utils/components/mainInput";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
import Button from "utils/components/mainButton";
import DateSelect from "utils/components/dateSelect";
import editEducationViewModel from "./editEducationViewModel";
class EducationInfoEditView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      school: "",
      degree: "",
      instructor: "",
      location: "",
      show: true,
      year: "",
      loading: false
    };
  }

  componentWillMount() {
    const {
      navigation: {
        state: {
          params: { id }
        },
        navigate,
        goBack
      }
    } = this.props;
    const { school, degree, instructor, location, year } = id;
    this.setState({
      school: school,
      degree: degree,
      instructor: instructor,
      location: location,
      year: year
    });
  }
  render() {
    const {
      navigation: {
        state: {
          params: { id }
        },
        navigate,
        goBack
      }
    } = this.props;
    const { school, degree, instructor, location, year, loading } = this.state;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => goBack()}
          barStyle="dark-content"
          text="Education & Training"
        />
        <Content>
          <View style={styles.container}>
            <View style={styles.form}>
              <Input
                data={school}
                placeholder="School"
                data={this.state.school}
                onEdit={school => this.setState({ school })}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <Input
                data={degree}
                placeholder="Degree/Course"
                data={this.state.degree}
                onEdit={degree => this.setState({ degree })}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <Input
                data={instructor}
                placeholder="Instructor (optional)"
                data={this.state.instructor}
                onEdit={instructor => this.setState({ instructor })}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <Input
                data={location}
                placeholder="Location (optional)"
                data={this.state.location}
                onEdit={location => this.setState({ location })}
                elevation={1.5}
                customWidth={styles.input.width}
                margin={10}
              />
              <DateSelect
                placeholder={"Year"}
                icon={DownArrowIcon}
                data={editEducationViewModel.convertYears(this)}
                onChange={year => this.setState({ year })}
                short
                elevation={1.5}
                value={year}
              />
            </View>
            <View style={styles.button}>
              <Button
                text="Update"
                loading={loading}
                onPress={async () =>
                  await editEducationViewModel.handlePressSave(this)
                }
              />
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
export default EducationInfoEditView;
