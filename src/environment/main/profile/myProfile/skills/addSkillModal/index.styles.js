import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  content: {
    alignItems: 'center',
    paddingTop: resize(20, 'h'),
    height: '100%',
    justifyContent: 'space-between',
    paddingBottom: resize(40, 'h')
  }
});