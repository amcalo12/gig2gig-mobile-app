import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  buttonContainer: {
    width: width * 0.885333333333,
    height: resize(90, "height"),
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 4,
    borderColor: colorPalette.purple,
    borderRadius: 4,
    marginBottom: 5
  },
  noIcon: {
    justifyContent: "center",
    height: resize(69, "height")
  },
  small: {
    height: isIos ? resize(79, "height") : resize(69, "height")
  },
  imageContainer: {
    width: "30%",
    alignItems: "center"
  },
  smallImageContainer: {
    width: "20%",
    alignItems: "center",
    marginRight: resize(10)
  },
  buttonText: {
    fontFamily: fontStyles.nexa_light,
    fontSize: isIos ? 23 : 20,
    color: colorPalette.purple,
    paddingVertical: resize(10, "height"),
    alignItems: "center"
  }
});
