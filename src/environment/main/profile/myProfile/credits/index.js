import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Loading from "utils/components/loading";
import { StackActions } from "react-navigation";
import PlusIcon from "resources/images/myCalendar/plus-icon.png";
import InfoLabel from "utils/components/infoLabel";
import { AndroidBackHandler } from "react-navigation-backhandler";
import creditsViewModel from "./creditsViewModel";
import DeleteIcon from "resources/images/delete-icon.png";

class CreditsView extends Component {
  state = {
    listCredits: [],
    loading: true,
  };
  componentDidMount = async () => {
    await creditsViewModel.getCredits(this);
  };

  handleBackPress = () => {
    const {
      navigation: { dispatch },
    } = this.props;
    return dispatch(StackActions.popToTop());
    return true;
  };
  addCredit = (data) => {
    creditsViewModel.addCredit(this, data);
  };
  updateCredit = async (data) => {
    // creditsViewModel.updateCredit(this, data);
    await creditsViewModel.getCredits(this);
  };
  renderLabels = () =>
    this.state.listCredits.map((res, i) => {
      return (
        <TouchableOpacity
          key={i}
          onPress={() =>
            this.props.navigation.navigate("CreditInfo", {
              id: res,
              onGoBack: this.updateCredit,
            })
          }
        >
          <InfoLabel
            loading={res.loading && res.loading}
            icon={DeleteIcon}
            onPress={() => creditsViewModel.deleteCredit(this, res.id)}
            paddingLeft={false}
            text={res.name}
            // subtext={`${res.month} ${res.year}`}
            subtext={`${res.year} - ${res.end_year}`}
          />
        </TouchableOpacity>
      );
    });

  render() {
    const {
      navigation: { navigate },
    } = this.props;
    console.log(this.state.listCredits);

    return (
      <AndroidBackHandler onBackPress={() => this.handleBackPress()}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => this.handleBackPress()}
            barStyle="dark-content"
            text="Credits"
            rightIcon={PlusIcon}
            rightFn={() => navigate("AddCredit", { onGoBack: this.addCredit })}
          />
          <Content>
            {this.state.loading ? (
              <Loading style={{ top: "40%", alignSelf: "center" }} />
            ) : this.state.notFoundData ? (
              <Text style={styles.notFoundText}>There's nothing here yet</Text>
            ) : (
              <View style={styles.container}>{this.renderLabels()}</View>
            )}
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default CreditsView;
