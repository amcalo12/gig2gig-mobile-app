import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  content: {
    marginTop: isIos ? 5 : 5,
    padding: 17
  },
  title: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: isIos ? 26 : 24,
    letterSpacing: 1.4,
    color: colorPalette.purple,
    ...Platform.select({
      ios: {
        marginBottom: resize(8, "height")
      }
    })
  },
  subTitle: {
    fontFamily: fontStyles.nexa_light,
    fontSize: isIos ? 18 : 16,
    letterSpacing: 0.85,
    color: colorPalette.purple,
    marginBottom: isIos ? resize(10, "height") : resize(7, "height")
  },
  messageTitle: {
    fontFamily: fontStyles.nexa_semibold,
    fontSize: isIos ? 16 : 14,
    color: colorPalette.purple,
    marginBottom: isIos ? resize(10, "height") : resize(7, "height")
  },
  feedbackMessage: {
    fontFamily: fontStyles.nexa_light,
    fontSize: isIos ? 13 : 12,
    letterSpacing: 0.85,
    color: colorPalette.purple,
    marginBottom: isIos ? resize(30, "height") : resize(20, "height")
  }
});
