import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Loading from "utils/components/loading";
import styles from './index.styles';

export default class InfoLabel extends Component {

	getStyles = () => {
		let labelStyles = [styles.container];
		const { isShort, noLine, customSize, paddingLeft, subtext } = this.props;
		if (isShort) {
			labelStyles.push(styles.short)
		}
		if (noLine) {
			labelStyles.push(styles.noLine)
		}
		if (customSize) {
			labelStyles.push({ width: customSize })
		}
		if (paddingLeft === false) {
			labelStyles.push({ paddingLeft: 0 })
		}
		if (subtext) {
			labelStyles.push(styles.containerSubtext)
		}


		return labelStyles;

	}

	render() {
		const { text, subtext, loading, icon, onPress } = this.props;
		return (
			<View style={this.getStyles()}>
				{
					loading ?
						<Loading style={{ top: 0, left: -25, alignSelf: 'center' }} />
						:
						<View>
							<Text style={styles.text}>{text}</Text>
							{
								subtext &&
								<Text style={styles.subtext}>{subtext}</Text>
							}
							{
								icon &&
								<TouchableOpacity style={styles.iconContainer} onPress={() => { onPress() }}>
									<Image source={icon} />
								</TouchableOpacity>
							}
						</View>
				}
			</View>
		);
	}
}