import { Platform, Dimensions } from 'react-native';

import DocumentPicker from 'react-native-document-picker';

//custom
const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");

const getFile = async (typeCustom = false, secondCustom = false) => {
	try {
		const file = await new Promise((resolve, reject) => {
			DocumentPicker.pick({
				top: height,
				left: width,
				type: [DocumentPicker.types.allFiles]
			})
				.then(res => {
					console.log(
						res.uri,
						res.type, // mime type
						res.name,
						res.size
					);
					resolve(res);
				})
				.catch(err => {
					if (DocumentPicker.isCancel(err)) {
						// User cancelled the picker, exit any dialogs or menus and move on
					} else {
						reject(err);
					}
				});
		});
		console.log(file);
		let type = file.name.split('.');
		type = type[type.length - 1].toLowerCase();
		let metadata = {
			contentType: `application/${type}`
		};

		if (!typeCustom) {
			if (type === 'mp3') {
				type = 1;
				metadata['contentType'] = 'audio/mpeg';
			} else if (type === 'mp4') {
				type = 2;
				metadata['contentType'] = 'video/mp4';
			} else if (type === 'mov') {
				type = 2;
				metadata['contentType'] = 'video/quicktime';
			} else if (type === 'pdf' || type === 'doc' || type === 'docx') {
				type = 3;
			} else if (
				type === 'jpg' ||
				type === 'jpeg' ||
				type === 'gif' ||
				type === 'png'
			) {
				type = 4;
			} else {
				return new Promise.reject({
					errorCode: 'FNR',
					string: 'The type of extension is not allowed.'
				});
			}
		} else {
			if (type === typeCustom || type === secondCustom) {
				type = typeCustom;
			} else {
				return new Promise.reject({
					errorCode: 'FNR',
					string: 'The type of extension is not allowed.'
				});
			}
		}

		const newFile = {
			filename: file.name,
			type,
			metadata,
			url: decodeURI(
				Platform.OS === 'ios' ? String(file.uri).replace('file:', '') : file.uri
			)
		};

		return new Promise.resolve(newFile);
	} catch (error) {
		return new Promise.reject(error);
	}
};

export default getFile;
