import { createStackNavigator } from "react-navigation";
import AuditionMaterialsView from ".";
import AuditionMaterialsDetailsView from "./auditionMaterialsDetails";

const AuditionMaterialsStack = createStackNavigator(
  {
     AuditionMaterials: {
       screen: AuditionMaterialsView,
       navigationOptions: {
         header: null
       }
     },
     AuditionMaterialsDetails: {
       screen: AuditionMaterialsDetailsView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "AuditionMaterials",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default AuditionMaterialsStack;
