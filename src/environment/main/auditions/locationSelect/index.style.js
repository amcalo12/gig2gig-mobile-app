import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");


export const styles = StyleSheet.create({
  searchInput: {
    color: "#4D2545",
    fontFamily: fontStyles.nexa_light,
    letterSpacing: 1,
    paddingLeft: 10,
    paddingRight: 10
    // height: height * 0.0554722638681,
    // width: width * 0.893333333333
  },
});