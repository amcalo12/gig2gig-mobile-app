import React, { Component } from 'react';
import { Modal, View, Text, Image, TouchableOpacity } from 'react-native';
import { Card } from 'native-base';
import styles from './index.styles';
import DownArrow from "resources/images/down-arrow-icon.png"

export default class SelectLabel extends Component {


  render() {
    const { isActive, text, onPress } = this.props;

    if (isActive) {
      return (
        <TouchableOpacity onPress={() => onPress()} style={[styles.label, styles.activeContainer]}>
          <Text style={[styles.labelText, styles.active]}>{text}</Text>
          <Image style={styles.arrow} source={DownArrow} />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity onPress={() => onPress()} style={styles.label}>
          <Text style={styles.labelText}>{text}</Text>
        </TouchableOpacity>
      );
    }
  }
}