import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    paddingTop: 25,
    alignItems: 'center',
  },
  errorTxt:{
    color: colorPalette.purple,
    fontSize:18,
    width:'100%',
    textAlign:'center',
    marginTop:20
  },
  form:{
    width:"100%",
    justifyContent:"center",
    alignItems: 'center',
  },
  text:{
    fontFamily:fontStyles.nexa_light,
    fontSize:18,
    color:colorPalette.purple,
    textAlign:"center",
    width:"80%",
    marginBottom: resize(20, "height"),
  },
  input:{
    width:resize(300)
  }
});