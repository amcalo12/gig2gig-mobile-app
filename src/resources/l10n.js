//HOME ICONS
import AuditionIcon from "resources/images/home/auditions-icon.png";
import myAuditionIcon from "resources/images/home/my-auditions.png";
import checkInIcon from "resources/images/home/check-in.png";
import myMediaIcon from "resources/images/home/my-media.png";
import marketplaceIcon from "resources/images/home/marketplace.png";
//PROFILE ICONS
import MyProfileIcon from "resources/images/profile/my-profile.png";
import MyMediaProfileIcon from "resources/images/profile/my-media.png";
import MyCalendarIcon from "resources/images/profile/my-calendar.png";
import SettingsProfileIcon from "resources/images/profile/settings.png";
import LogOut from "resources/images/profile/logout.png";
import CreditsIcon from "resources/images/profile/credits-icon.png";
import EducationIcon from "resources/images/profile/education-icon.png";
import UnionIcon from "resources/images/profile/union-icon.png";
import SkillsIcon from "resources/images/profile/skills-icon.png";
import RepresentationIcon from "resources/images/profile/representation-icon.png";
import AppearanceIcon from "resources/images/profile/appearance-icon.png";
import VideoIcon from "resources/images/profile/video-icon.png";
import MusicIcon from "resources/images/profile/music-icon.png";
import ForumIcon from "resources/images/profile/forum.png";
import BlogIcon from "resources/images/profile/blog.png";
import PhotoIcon from "resources/images/profile/photo-icon.png";
import SheetIcon from "resources/images/profile/sheet-icon.png";
import AuditionMediaIcon from "resources/images/profile/audition-icon.png";
import { Assets } from "./assets";

export const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];
export const monthsNumber = [
  "01",
  "02",
  "03",
  "04",
  "05",
  "06",
  "07",
  "08",
  "09",
  "10",
  "11",
  "12"
];
export const auditionFilters = [
  {
    name: "Union Status",
    type: "union",
    options: [
      {
        name: "ANY"
      },
      {
        name: "UNION"
      },
      {
        name: "NONUNION"
      }
    ]
  },
  {
    name: "Contract Type",
    type: "contract",
    options: [
      {
        name: "ANY"
      },
      {
        name: "PAID"
      },
      {
        name: "UNPAID"
      },
      {
        name: "ACADEMIC"
      }
    ]
  },
  {
    name: "Production Type",
    type: "production",
    options: [
      {
        name: "ANY"
      },
      {
        name: "THEATER"
      },
      {
        name: "FILM"
      },
      {
        name: "TV & VIDEO"
      },
      {
        name: "COMMERCIALS"
      },
      {
        name: "MODELING"
      },
      {
        name: "PERFORMING ARTS"
      },
      {
        name: "VOICEOVER"
      }
    ]
  },
  {
    name: "Event Type",
    type: "union",
    options: [
      {
        name: "Personal"
      },
      {
        name: "Professional"
      }
    ]
  }
];

export const calendarProductionType = {
  name: "Production Type",
  type: "production",
  options: [
    {
      name: "ANY"
    },
    {
      name: "THEATER"
    },
    {
      name: "FILM"
    },
    {
      name: "TV & VIDEO"
    },
    {
      name: "COMMERCIALS"
    },
    {
      name: "MODELING"
    },
    {
      name: "PERFORMING ARTS"
    },
    {
      name: "VOICEOVER"
    },
    {
      name: "PERSONAL"
    }
  ]
}

export const unionMembers = [
  {
    name: "None"
  },
  {
    name: "SAG-AFTRA"
  },
  {
    name: "Equity (U.S.)"
  },
  {
    name: "NonEquity (U.K.)"
  },
  {
    name: "Equity (Ireland)"
  },
  {
    name: "ACTRA"
  },
  {
    name: "AFM"
  },
  {
    name: "AGMA"
  },
  {
    name: "AGVA"
  },
  {
    name: "BECTU"
  },
  {
    name: "Equity Eligible"
  },
  {
    name: "SAG-AFTRA Eligible"
  }
];
export const homeOptions = [
  {
    name: "Search Auditions",
    icon: AuditionIcon,
    screen: "Auditions",
    params: null
  },
  {
    name: "My Auditions",
    icon: myAuditionIcon,
    screen: "MyAuditionMain",
    params: { checkIn: 0 }
  },
  {
    name: "Check-In",
    icon: checkInIcon,
    screen: "MyAuditionMain",
    params: { checkIn: 1 }
  },
  {
    name: "My Media",
    icon: myMediaIcon,
    screen: "MyMedia",
    params: null
  },
  {
    name: "Marketplace",
    icon: marketplaceIcon,
    screen: "Marketplace",
    params: null
  }
];
export const marketplaceOptions = [
  {
    name: "Photographers",
    option: "photographers"
  },
  {
    name: "Coaches",
    option: "coaches"
  },
  {
    name: "Classes",
    option: "classes"
  },
  {
    name: "Health & Support",
    option: "h&s"
  },
  {
    name: "Gigs",
    option: "gigs"
  },
  {
    name: "Gear",
    option: "gear"
  }
];

export const profileOptions = [
  {
    name: "My Profile",
    icon: MyProfileIcon,
    navigate: 1
  },
  {
    name: "Subscription",
    icon: Assets.subscription,
    navigate: "SubscriptionDetail"
  },
  {
    name: "My Media",
    icon: MyMediaProfileIcon,
    navigate: "MyMedia"
  },
  {
    name: "My Calendar",
    icon: MyCalendarIcon,
    navigate: "MyCalendar"
  },
  {
    name: "Support Forum",
    icon: ForumIcon,
    navigate: "SupportForum"
  },
  {
    name: "News & Updates",
    icon: BlogIcon,
    navigate: "NewAndUpdates"
  },
  {
    name: "Settings",
    icon: SettingsProfileIcon,
    navigate: "Settings"
  },
  {
    name: "Log out",
    icon: LogOut,
    navigate: 0
  }
];

export const myProfileOptions = [
  {
    name: "My Info",
    icon: MyProfileIcon,
    navigate: "MyInfo"
  },
  {
    name: "Credits",
    icon: CreditsIcon,
    navigate: "Credits"
  },
  {
    name: "Education & Training",
    icon: EducationIcon,
    navigate: "Education"
  },
  {
    name: "Union Membership",
    icon: UnionIcon,
    navigate: "UnionMembership"
  },
  {
    name: "Skills",
    icon: SkillsIcon,
    navigate: "Skills"
  },
  {
    name: "Representation",
    icon: RepresentationIcon,
    navigate: "Representation"
  },
  {
    name: "Appearance",
    icon: AppearanceIcon,
    navigate: "Appearance"
  },
  {
    name: "Social",
    icon: Assets.social_share,
    navigate: "Social"
  }
];

export const myMediaOptions = [
  {
    name: "Resume & Docs",
    icon: myMediaIcon,
    navigate: "Documents"
  },
  {
    name: "Videos",
    icon: VideoIcon,
    navigate: "Videos"
  },
  {
    name: "Music",
    icon: MusicIcon,
    navigate: "Music"
  },
  {
    name: "Photos",
    icon: PhotoIcon,
    navigate: "Photos"
  },
  {
    name: "Sheet Music",
    icon: SheetIcon,
    navigate: "SheetMusic"
  },
  {
    name: "Audition Materials",
    icon: AuditionMediaIcon,
    navigate: "AuditionMaterials"
  }
];

export const settingsOptions = [
  {
    name: "Push Notifications",
    navigate: "PushNotifications"
  },
  // {
  // 	name: "Subscription",
  // 	navigate: "Subscription"
  // },
  {
    name: "Audition Feedback",
    navigate: "AuditionFeedback"
  },
  {
    name: "Marketplace",
    navigate: "MarketplaceComing"
  },
  {
    name: "Terms of Use",
    navigate: "TermsOfUse"
  },
  {
    name: "Privacy Policy",
    navigate: "PrivacyPolicy"
  },
  // {
  //   name: "Help",
  //   navigate: "Help",
  //   data: "help"
  // },
  {
    name: "App Info",
    navigate: "AppInfo"
  },
  {
    name: "Contact Us",
    navigate: "ContactUs"
  }
];

export const hairOptions = [
  {
    name: "Black"
  },
  {
    name: "Brown"
  },
  {
    name: "Auburn"
  },
  {
    name: "Chestnut"
  },
  {
    name: "Red"
  },
  {
    name: "Gray"
  },
  {
    name: "White"
  },
  {
    name: "Bald"
  },
  // {
  //   name: "Multicolored/Dyed"
  // },
  {
    name: "Blonde"
  },
  {
    name: "Strawberry Blonde"
  },
  {
    name: "Other"
  }
];

export const ethnicityOptions = [
  {
    name: "Asian"
  },
  {
    name: "African Descent"
  },
  {
    name: "Indigenous People"
  },
  {
    name: "Latino / Hispanic"
  },
  {
    name: "Middle Eastern"
  },
  {
    name: "South Asian / Indian"
  },
  {
    name: "European Descent"
  },
  {
    name: "Other"
  }
];

export const flareOptions = [
  {
    name: "Tattoos"
  },
  {
    name: "Piercings"
  },
  {
    name: "Other"
  }
];

export const pronounsOptions = [
  {
    name: "he/him/his"
  },
  {
    name: "she/her/hers"
  },
  {
    name: "they/them/theirs"
  },
  {
    name: "ze/zir/zirs"
  },
  {
    name: "ze/hir/hirs"
  }
];

// export const ethnicityOptions = [
//   {
//     name: "Asia"
//   },
//   {
//     name: "Black / African Descent"
//   },
//   {
//     name: "Indigenous People"
//   },
//   {
//     name: "Latino / Hispanic"
//   },
//   {
//     name: "Middle Eastern"
//   },
//   {
//     name: "South Asian / Indian"
//   },
//   {
//     name: "White / European Descent"
//   }
// ];

export const eyeColorOptions = [
  {
    name: "Amber"
  },
  {
    name: "Blue"
  },
  {
    name: "Brown"
  },
  {
    name: "Gray"
  },
  {
    name: "Green"
  },
  {
    name: "Hazel"
  },
  {
    name: "Red"
  },
  {
    name: "Violet"
  }
];

export const generalMsj = {
  error: {
    message: "An Error has ocurred"
  },
  email: {
    error: "Please, enter an email",
    invalid: "Please enter a valid email"
  }
};
export const errorsValidateMsj = {
  login: {
    email: {
      message: "Please, enter an email"
    },
    password: {
      message: "Please, enter your password"
    },
    error: {
      message: "There is a problem with your username or password"
    }
  },
  signup: {
    firstName: {
      message: "First name is required"
    },
    gender: {
      message: "Gender Identity is required"
    },
    gender_desc: {
      message: "Please enter gender description"
    },
    personalWebsite: {
      message: "Personal Website is required"
    },
    passwordLengthError: {
      message: "the password must have 8 characters minimum"
    },
    lastName: {
      message: "Last name is required"
    },
    email: {
      message: "Email is required",
      invalid: "Please enter a correct email"
    },
    password: {
      message: "Password is required"
    },
    re_password: {
      message: "Re-Enter your password"
    },
    passwordError: {
      message: "Your password needs to match"
    },
    country: {
      message: "Country is required"
    },
    address: {
      message: "Address is required"
    },
    city: {
      message: "City is Required"
    },
    state: {
      message: "State is required"
    },
    zip: {
      message: "Zip Code is required",
      messageError: "Please, enter a valid zip code"
    },
    birthdate: {
      message: "Birth date is required"
    },
    image: {
      message: "Image is required"
    },
    stageName: {
      message: "Stage Name is required"
    },
    pwTitle: {
      message: "Professional/Working Title is required"
    },
    location: {
      message: "Location is required",
      messagePermission: "G2G needs location permission",
      messageError: "An error has ocurred"
    },
    unionMemberShip: {
      message: "At least one Union Membership is required"
    },
    success: {
      message: "User created successfully"
    },
    error: {
      message: "An error has ocurred, try Again"
    }
  },
  addEvent: {
    dataRequired: {
      message: "All fields are required"
    },
    errorMessage: {
      message: "The event could not be added"
    },
    endDateInvalid: {
      message: "Select a valid end date"
    }
  },
  editProfile: {
    success: {
      message: "User updated successfully"
    },
    error: {
      message: "An error has ocurred, try Again"
    }
  }
};

export const validateRepresentation = {
  name: {
    error: "Please, enter a Name"
  },
  company: {
    error: "Please, enter a Company Name"
  },
  save: {
    success: "Saved"
  }
};

export const skillsMsj = {
  add: {
    success: "Skill Added"
  },
  delete: {
    succes: "Skill deleted"
  }
};

export const shareMsj = {
  Auditions: {
    first: "Audition is open for",
    second: "you can apply to be a performer.",
    third: "Download G2G APP for:",
    Apple: `\nApple:
           \nhttps://itunes.apple.com/`,
    Android: `\nAndroid:
             \nhttps://play.google.com/`
  },
  myMedia: {
    message:
      "Hello! I want to share this file with you, so check out this link below and let me know what your thoughts are"
  }
};
