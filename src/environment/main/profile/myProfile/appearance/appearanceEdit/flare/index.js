import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import Label from "utils/components/unionMemberLabel";
import { AndroidBackHandler } from "react-navigation-backhandler";
import { flareOptions } from "resources/l10n";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import Input from "utils/components/mainInput";

class PersonalFlare extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flare: flareOptions,
      other: "",
      isOther: false
    };
    // if (this.props.navigation.state.params.flare != undefined) {
    //   this.selected = this.props.navigation.state.params.flare.split(",");
    //   this.state.flare.map(data => {
    //     this.selected.map(item => {
    //       if (data.name == item) {
    //         data.checked = true;
    //       }
    //     });
    //   });
    // }
  }

  handleBackPress = () => {
    const {
      navigation: { goBack }
    } = this.props;
    return goBack();
    return true;
  };
  changeChecked = (option, i) => {
    const {
      navigation: {
        navigate,
        state: {
          params: { onGoBack }
        }
      }
    } = this.props;
    const { flare } = this.state;
    const flare2 = [];
    let selectedRace = "";
    flare.map(res => {
      if (res.name === option.name) {
        // res.checked = !res.checked;
        res.checked = true
      } else {
        res.checked = false
      }

      if (res.name == "Other") {
        this.state.isOther = res.checked
      }
      // if (res.checked) {
      //   selectedRace = selectedRace + "," + res.name;
      // }
      // // else {
      // //   res.checked = false
      // // }
      flare2.push(res);
    });
    this.setState({ flare: flare2 });
    onGoBack(option)
    // onGoBack(selectedRace.replace(/^,/, ""));
  };
  render() {
    const {
      navigation: {
        navigate,
        state: {
          params: { onGoBack }
        } }
    } = this.props;
    const { flare } = this.state;
    return (
      <AndroidBackHandler onBackPress={() => this.handleBackPress()}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => this.handleBackPress()}
            barStyle="dark-content"
            // text="Personal Flare"
            text="Personal Flair"
          />
          <Content>
            <View style={styles.container}>
              {flare.map((option, idx) => (
                <Label
                  key={idx}
                  text={option.name}
                  checked={option.checked}
                  onPress={() => this.changeChecked(option, idx)}
                />
              ))} 
              {this.state.isOther ?
                <View style={{ marginTop: 10, marginRight: 10, marginLeft: 0 }}>
                  <Input
                    // placeholder="Add Personal Flare"
                    placeholder="Add Personal Flair"
                    data={this.state.other} onEdit={other => {
                      this.setState({ other }, () => {
                        onGoBack({ name: other })
                      })
                    }}
                    elevation={3} customWidth={styles.input.width} />
                </View>
                : null}
            </View>

          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default PersonalFlare;
