import React, { Component } from "react";
import { View, Text, Image, Linking, AsyncStorage } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Button from "utils/components/squareButton";
import MainButton from "utils/components/mainButton";
import { myMediaOptions, generalMsj } from "resources/l10n";
import { StackActions } from 'react-navigation';
import myMediaApi from "api/myMediaApi";
import { showMsj } from "utils/utils";
import VideoIcon from "resources/images/profile/video-icon.png";
import MusicIcon from "resources/images/profile/music-icon.png";
import PhotoIcon from "resources/images/profile/photo-icon.png";
import DocumentIcon from "resources/images/home/my-media.png";
import SheetIcon from "resources/images/profile/sheet-icon.png";
import AuditionMediaIcon from "resources/images/profile/audition-icon.png";
import Loading from "utils/components/loading";

class MyMediaView extends Component {
  state = {
    media: null,
    loading: false,
    subscription: null
  }

  async componentDidMount() {
    this.setState({ loading: true });
    try {
      const response = await myMediaApi.getUserMedia();
      const userData = JSON.parse(await AsyncStorage.getItem("userData"));
      this.setState({
        media: response.data,
        loading: false,
        userData,
        subscription: userData.details.subscription
      });
    } catch (e) {
      showMsj(generalMsj.error.message, true);
    }
  }

  openFileURL = () => {
    let url = "https://firebasestorage.googleapis.com/v0/b/dd-gig2gig.appspot.com/o/phone%2Fusers%2Ffiles%2F174219?alt=media&token=2698043a-c2fe-40c7-a255-b1bb77668a4c"

    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  render() {
    const { media, loading, subscription, userData } = this.state;
    const { navigation: { navigate, dispatch } } = this.props;
    console.log('subscription', subscription)
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          barStyle="dark-content"
          text="My Media"
        />
        <Content>
          {
            loading ?
              <Loading />
              :
              subscription == 3 ?
                <View style={styles.buttons}>
                  {
                    myMediaOptions.map((option, idx) => (
                      <Button key={idx} size="small" margin={styles.button.margin} icon={option.icon} text={option.name} onPress={() => navigate(option.navigate)} />
                    ))
                  }
                </View>
                :
                <View style={styles.containerSub}>
                  <View>
                    <Text style={styles.typeText}>You need plan premium</Text>
                    <Text style={styles.descriptionText}>To have access to my media you must have to have plan premium</Text>
                  </View>
                  <MainButton text='Upgrade' loading={loading} onPress={() => navigate('Settings', { userData })} />
                </View>
          }
        </Content>
      </Container>
    );
  }
}
export default MyMediaView;
