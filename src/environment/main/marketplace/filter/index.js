import React, { Component } from "react";
import { View, Text } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Loading from "utils/components/loading";
import SearchInput from "utils/components/searchInput";
import Select from "utils/components/marketplaceSelectFilter";
import MCard from "utils/components/marketplaceCard";
import filterViewModel from './filterViewModel'
import { DEVICE } from "utils/cons";
// let modal = React.createRef();
class MarketplaceView extends Component {
  constructor() {
    super()
    console.log('DEVICE.DEVICE_WIDTH :>> ', DEVICE.DEVICE_WIDTH);
    this.state = {
      loading: false,
      listData: [],
      listDataWithFilter: [],
      setCards: true
    }
  }
  async componentDidMount() {
    const textFilter = this.props.navigation.state.params.info.option;
    await this.setState({
      textFilter
    });

    await filterViewModel.handleGetCardsForFilter(this)
  }

  show = () => {
    this.selectFilter.setModalVisible();
  }

  renderCards = (action) => {
    if (this.state.listDataWithFilter.length != 0) {
      return this.state.listDataWithFilter.map((data, i) => {
        return <MCard key={i} data={data} onPress={() => action({ data })} />
      })
    } else {
      return this.state.listData.map((data, i) => {
        return <MCard key={i} data={data} onPress={() => action({ data })} />
      })
    }
  }

  render() {
    const { navigation: { navigate, goBack } } = this.props;
    const { textSearch, textFilter, loading } = this.state;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => goBack()}
          barStyle="dark-content"
          text={textFilter}
          isSelect={<Select marketplaceOptions={this.props.navigation.state.params.data} onSelected={async (textFilter) => { await this.setState({ textFilter: textFilter }); filterViewModel.optionSelected(this) }} onRef={(elem) => this.selectFilter = elem} />}
          onPress={() => this.show()}
        />
        <View style={styles.searchContainer}>
          <SearchInput textInput={textSearch} onChangeText={async (textSearch) => { await this.setState({ textSearch }); filterViewModel.searchCardsWithText(this) }} />
        </View>
        {
          this.state.loading ?
            <Loading style={{ top: "30%", alignSelf: 'center', }} />
            :
            <Content>
              <View style={styles.container}>
                {this.state.notFoudData ?
                  <Text style={styles.notFoundDataMess}>There are no vendors yet</Text>
                  :
                  this.renderCards((info) => navigate("Info", info))
                }
              </View>
              {/* <Select onRef={(elem) => this.selectFilter = elem}/> */}
            </Content>
        }
      </Container>
    );
  }
}
export default MarketplaceView;
