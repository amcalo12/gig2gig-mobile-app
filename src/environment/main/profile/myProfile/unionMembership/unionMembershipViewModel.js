import { AsyncStorage } from 'react-native';
import { StackActions } from 'react-navigation';
import userApi from 'api/userApi';
import { showMsj } from 'utils/utils';


export const handleBackPress = async (context) => {
  const userData = JSON.parse(await AsyncStorage.getItem("userData"));
  let { unionMembers, editedUnionMembers } = context.state;

  if (editedUnionMembers !== null) {
    await saveUnionMembershipList(context);
  }

  const { navigation: { dispatch } } = context.props;
  return dispatch(StackActions.popToTop())
  return true;
}

export const toggleCheck = (context, index, value, name = '') => {
  let { unionMembers } = context.state;
  console.log('NAME', name)
  if (name == 'None') {
    unionMembers.map((option, idx) => {
      option.active = false
    });
  } else {
    unionMembers.map((option, idx) => {
      if (option.name === 'None') {
        option.active = false
      }
    });
  }
  unionMembers[index].active = !value;
  context.setState({ unionMembers });
  const result = unionMembers.filter(item => item.active === true);
  let filtered = [];
  const data = result.map((option, idx) => {
    let name = option.name;
    filtered.push({ name })
  });

  context.setState({ editedUnionMembers: filtered });
};

export const saveUnionMembershipList = async (context) => {
  let { loading, editedUnionMembers } = context.state;
  context.setState({ loading: true });
  try {
    const response = await userApi.updateUnionMembershipList({ data: editedUnionMembers });
    context.setState({ loading: false });
    return showMsj("Union membership updated", false);
    // console.log(response)
  } catch (e) {
    console.log(e)
    context.setState({ loading: false });
    return showMsj("An Error Has Ocurred", false);
  }
}