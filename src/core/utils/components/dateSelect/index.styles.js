import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    height: height * 0.0734632683658,
    width: width * 0.7,
    backgroundColor: colorPalette.white,
    borderRadius: 33,
    alignItems: "center",
    borderWidth: 1,
  },
  touchableContainer: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center",
  },
  shortInput: {
    flexDirection: "row",
    height: height * 0.0734632683658,
    width: width * 0.357333333333,
    backgroundColor: colorPalette.white,
    borderRadius: 33,
    alignItems: "center",
  },
  input: {
    // height: height * 0.0734632683658,
    // width: width * 0.80,
    // backgroundColor:colorPalette.white,
    // borderRadius: 33,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: "#4D2545",
    paddingLeft: 20,
    paddingRight: 20,
  },
  icon: {
    marginBottom: height * 0.005,
  },
  iconAndroid: {
    position: "absolute",
    right: 15,
    top: 18,
    marginBottom: height * 0.005,
  },
  leftIcon: {
    marginLeft: resize(20),
    marginBottom: height * 0.005,
  },
});
export const pickerSelectStylesLng = StyleSheet.create({
  inputIOS: {
    width: width * 0.7,
    height: "100%",
    paddingLeft: 20,
  },
  inputAndroid: {
    width: width * 0.7,
    height: "100%",
    paddingRight: 20,
    color: "#4D2545",
    marginLeft: 10,
  },
  text: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: "#4D2545",
    paddingLeft: 20,
  },
});
export const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    width: width * 0.28,
    height: "100%",
    paddingLeft: 20,
  },
  inputAndroid: {
    width: width * 0.4,
    height: "100%",
    paddingRight: 20,
    color: "#4D2545",
    marginLeft: 10,
  },
  text: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: "#4D2545",
    paddingLeft: 20,
  },
});
