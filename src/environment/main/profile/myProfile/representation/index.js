import React, { Component } from "react";
import { View, Text } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Loading from "utils/components/loading";
import InfoLabel from "utils/components/infoLabel";
import Button from "utils/components/secondaryButton";
import { AndroidBackHandler } from "react-navigation-backhandler";
import { withNavigationFocus } from "react-navigation";
import { loadRepresentation, handleBackPress } from "./RepresentationViewModel";

class RepresentationView extends Component {
  state = {
    loadScreen: false,
    userRepresentation: {}
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.isFocused) {
      console.log("HOLA");
      loadRepresentation(this);
    }
  }

  async componentDidMount() {
    loadRepresentation(this);
  }

  render() {
    const { loadScreen, userRepresentation } = this.state;
    const {
      navigation: { navigate }
    } = this.props;
    return (
      <AndroidBackHandler onBackPress={() => handleBackPress(this)}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => handleBackPress(this)}
            barStyle="dark-content"
            text="Representation"
          />
          {loadScreen ? (
            <Loading />
          ) : (
            <Content>
              <View style={styles.container}>
                <Text style={styles.headerText}>{userRepresentation.type}</Text>
                <View style={styles.labels}>
                  <InfoLabel text={userRepresentation.name} />
                  <InfoLabel text={userRepresentation.company} />
                  <InfoLabel noLine text={userRepresentation.email} />
                </View>
                <View style={styles.button}>
                  <Button
                    text="Edit"
                    onPress={() =>
                      navigate("RepresentationEdit", {
                        hasRepresentation: userRepresentation,
                        isFromAuditionDetail: false
                      })
                    }
                  />
                </View>
              </View>
            </Content>
          )}
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default withNavigationFocus(RepresentationView);
