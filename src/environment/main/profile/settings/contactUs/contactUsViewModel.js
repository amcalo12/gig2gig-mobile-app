import myCalendarApi from 'api/myCalendarApi';
import { showMsj } from "utils/utils";
import { errorsValidateMsj } from "resources/l10n";

const getInfo = async(context) => {
  const info = context.props.navigation.state.params.data.contact_us
  context.setState({
    info
  })
}

export default {
    getInfo,
}