import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils"

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");

export default StyleSheet.create({
  button: {
    height: height * 0.0734632683658,
    width: width * 0.70,
    backgroundColor: "#FFFFFF",
    borderWidth: 3,
    borderColor:colorPalette.purple,
    flexDirection: 'row',
    alignSelf:"center",
    justifyContent:"space-around",
    alignItems: 'center',
    borderRadius: 33,
    paddingHorizontal: width * 0.08,
    position:"relative"
  },
  icon:{
    marginRight: height * 0.011
  },
  buttonText:{
    fontFamily: fontStyles.nexa_bold,
    fontSize:18,
    color:colorPalette.purple,
    ...Platform.select({
      ios: {
        marginTop: resize(3, "height"),
      },
    }),
  }
});