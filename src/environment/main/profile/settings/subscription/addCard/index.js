import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Input from "utils/components/mainInput";
import Button from "utils/components/mainButton";
import DateSelect from "utils/components/dateSelect";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
import addCardViewModel from './addCardViewModel';
class AddCard extends Component {
  state = {
    cardNumber: '',
    cvvCode: '',
    month: '',
    year: '',
    loading: false
  }
  setCard = (cardNumber) => {
    this.setState({ cardNumber })
  }
  render() {
    const { cardNumber, cvvCode } = this.state
    const { navigation: { navigate, dispatch } } = this.props;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.pop())}
          barStyle="dark-content"
          text="Add Credit Card"
        />
        <Content>
          <View style={styles.container}>
            <View style={styles.imagesContainer}>
              <View style={styles.row}>
                <Image source={require('resources/images/cards/paypal/Image_4.png')} style={styles.img} />
                <Image source={require('resources/images/cards/visa/Image_5.png')} style={styles.img} />
                <Image source={require('resources/images/cards/master/Image_6.png')} style={styles.img} />
                <Image source={require('resources/images/cards/american/Image_7.png')} style={styles.img} />
              </View>
            </View>
            <Input placeholder="Card Number" customWidth={'80%'} onEdit={cardNumber => cardNumber.length <= 16 ? this.setState({ cardNumber }) : null} data={cardNumber} keyboardType={'numeric'} />
            <View style={styles.secondPartContainer}>
              <Input placeholder="CVV" short onEdit={cvvCode => cvvCode.length <= 3 ? this.setState({ cvvCode }) : null} data={cvvCode} keyboardType={'numeric'} />
              <Text style={styles.titleText}>Expiration Date</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <DateSelect
                  placeholder={'Month'}
                  icon={DownArrowIcon}
                  data={addCardViewModel.getMonths(this)}
                  onChange={(month) => this.setState({ month })}
                  short
                  elevation={1.5}
                />
                <DateSelect
                  placeholder={'Year'}
                  icon={DownArrowIcon}
                  data={addCardViewModel.convertYears()}
                  onChange={(year, i) => { this.setState({ year }) }}
                  short
                  elevation={1.5}
                />
              </View>
            </View>
            <Button onPress={() => addCardViewModel.saveCard(this)} customWidth={200} text={'Save'} loading={this.state.loading} />
          </View>
        </Content>
      </Container>
    );
  }
}
export default AddCard;
