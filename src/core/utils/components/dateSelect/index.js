import React, { Component } from "react";
import { View, Image, Platform } from "react-native";
import { Card } from "native-base";
import styles, {
  pickerSelectStyles,
  pickerSelectStylesLng,
} from "./index.styles";
import RNPickerSelect from "react-native-picker-select";

class DateSelect extends Component {
  getStyles() {
    let inputStyles = [];
    let touchableStyles = [];
    const {
      short,
      customWidth,
      margin,
      elevation,
      isIos,
      moreContainerStyle,
    } = this.props;

    if (short) {
      inputStyles.push(styles.shortInput);
    } else {
      inputStyles.push(styles.container);
    }

    if (customWidth) {
      inputStyles.push({
        width: customWidth,
        marginBottom: margin,
        elevation: elevation,
      });
    } else {
      inputStyles.push({ marginBottom: margin, elevation: elevation });
    }
    // if (moreContainerStyle) {
    // inputStyles.push({ paddingRight: 20 });
    // }

    // if (!isIos) {
    //   inputStyles.push({borderColor:"#FFFFFF"})
    // }

    return inputStyles;
  }

  render() {
    const { placeholder, value, data, icon, onChange, lng } = this.props;
    return (
      <Card style={this.getStyles()}>
        {console.log(" PICKER DATA ", data)}
        {console.log(" value ", value)}

        <RNPickerSelect
          Icon={() => null}
          placeholder={{
            label: placeholder,
            value: null,
            color: "#9EA0A4",
          }}
          items={data}
          // items={[
          //   { label: "Football", value: "football" },
          //   { label: "Baseball", value: "baseball" },
          //   { label: "Hockey", value: "hockey" }
          // ]}
          onValueChange={(value) => {
            onChange(value);
          }}
          style={lng ? pickerSelectStylesLng : pickerSelectStyles}
          value={value}
          placeholderTextColor={"black"}
          textInputProps={pickerSelectStyles.text}
          itemKey={value}
        />
        {icon && (
          <View style={Platform.OS === "android" ? styles.iconAndroid : null}>
            <Image style={styles.icon} source={icon} />
          </View>
        )}
      </Card>
    );
  }
}
DateSelect.defaultProps = {
  customWidth: null,
  isSecure: false,
  margin: 20,
  short: false,
  elevation: 0,
  keyboardType: "default",
};
export default DateSelect;
