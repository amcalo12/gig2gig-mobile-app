import { createStackNavigator } from "react-navigation";
import VideosView from ".";

const VideosStack = createStackNavigator(
  {
     Videos: {
       screen: VideosView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "Videos",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default VideosStack;
