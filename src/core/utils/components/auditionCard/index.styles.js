import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);

export default StyleSheet.create({
  container: {
    width: width * 0.893333333333,
    height:
      Platform.OS === "android"
        ? height * 0.176911544228 + 5
        : height * 0.176911544228,
    justifyContent: "center",
    alignItems: "flex-start",
    marginBottom: height * 0.02
    // borderWidth: 1,
  },
  cardContainer: {
    width: "98%",
    height: "90%",
    borderRadius: 4,
    flexDirection: "row"
  },
  leftCard: {
    height: "100%",
    width: "65%",
    paddingHorizontal: height * 0.009
  },
  filterContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    height: "25%",
    paddingTop: height * 0.009
    // backgroundColor:"pink"
  },
  mainBody: {
    height: "70%",
    // height: "50%",
    // backgroundColor: "gray",
    // justifyContent: "flex-end",
    paddingBottom: height * 0.003,
    // borderColor: "red",
    // borderWidth: 2
  },
  mainBodyTitle: {
    fontSize: 16,
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
    letterSpacing: 1.5,
    marginBottom: height * 0.008,
    paddingRight: resize(5)
  },
  titleTextStyle: {
    fontSize: 16,
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
    letterSpacing: 1.5,
    marginTop: 10,
    // marginTop: height * 0.05,
    paddingRight: resize(5)
  },
  mainBodyText: {
    fontSize: 10,
    fontFamily: fontStyles.nexa_light,
    color: colorPalette.purple,
    letterSpacing: 1,
    paddingRight: resize(5),
    marginTop: 5
  },
  roundBodyText: {
    fontSize: 12,
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
    letterSpacing: 1,
    paddingRight: resize(5),
    marginTop: 5
  },
  body: {
    height: "60%",
    justifyContent: "center"
  },
  bodyTitle: {
    fontSize: 16,
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
    letterSpacing: 1.5,
    marginBottom: height * 0.008,
    paddingRight: resize(5)
  },
  bodyText: {
    fontSize: 12,
    fontFamily: fontStyles.nexa_light,
    color: colorPalette.purple,
    letterSpacing: 1,
    flexDirection: "row",
    paddingRight: resize(5)
  },
  bodyTextBold: {
    fontWeight: "bold",
    fontFamily: fontStyles.nexa_bold,
    paddingRight: 10,
    color: colorPalette.purple
  },
  footer: {
    height: "25%",
    justifyContent: "center",
    position: "relative"
  },
  footerText: {
    fontSize: 10,
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple
    // letterSpacing:1
  },
  footerTextRight: {
    alignSelf: "flex-end",
    paddingRight: height * 0.01
  },
  rightCard: {
    // backgroundColor: "orange",
    height: "100%",
    width: "35%"
    // width: width * 0.341333333333
  },
  imageContainer: {
    height: "75%",
    position: "relative"
  },
  imageView: {
    height: "100%",
    width: "110%",
    position: "absolute",
    right: -(width * 0.015),
    top: -(height * 0.006),
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    borderBottomLeftRadius: 19,
    backgroundColor: "#f1f1f1",
    overflow: "hidden"
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  footerState: {
    width: "90%",
    height: "100%",
    backgroundColor: colorPalette.purple,
    alignItems: "center",
    justifyContent: "center",
    borderTopLeftRadius: 19,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    borderBottomLeftRadius: 4,
    position: "absolute",
    bottom: -(height * 0.006),
    right: -(width * 0.015)
  },
  footerStateText: {
    fontSize: 12,
    fontFamily: fontStyles.nexa_bold,
    color: "#FFFFFF"
  }
});
