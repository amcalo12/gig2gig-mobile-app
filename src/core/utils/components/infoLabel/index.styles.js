import { StyleSheet, Platform, Dimensions } from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils"
const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
	container: {
		height: isIos ? resize(65, "height") : resize(60, "height"),
		borderBottomWidth: 1,
		borderBottomColor: '#d6d6d6',
		justifyContent: 'center',
		paddingLeft: resize(20, "height"),
	},
	containerSubtext: {
		height: isIos ? resize(85, "height") : resize(80, "height"),
	},
	text: {
		color: colorPalette.purple,
		fontFamily: fontStyles.nexa_bold,
		fontSize: 18,
	},
	subtext: {
		marginTop: isIos ? resize(12, "height") : resize(8, "height"),
		color: colorPalette.purple,
		fontFamily: fontStyles.nexa_light,
		fontSize: 16,
	},
	short: {
		width: "50%"
	},
	noLine: {
		borderBottomWidth: 0,
	},
	iconContainer: {
		position: 'absolute',
		width: 50,
		height: 50,
		justifyContent: 'center',
		alignItems: 'flex-end',
		right: 25
	}
});