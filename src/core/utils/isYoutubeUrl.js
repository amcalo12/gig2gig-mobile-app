var links =
  ['x',
    'https://youtube.com/watch?v=01jcwGApTWA&t',
    'https://youtu.be/01jcwGApTWA?t=31',
    'https://www.youtube.com/watch?v=Ujqdle7CvIU&spfreload=10',
    'https://www.youtube.com/watch?v=nQ9ww9E_1C4',
    'https://www.youtube.com/embed/nQ9ww9E_1C4',
    'https://www.youtube.com/embed/nQ9ww9E_1C4?autoplay=1',
    'https://www.youtube.com/embed/nQ9ww9E_1C4?playlist=XGSy3_Czz8k&loop=1',
    'https://www.youtube.com',
    'http://anothersite.com'];

export default YouTubeGetID = (url) => {
  var ID = '';
  url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  if (url[2] !== undefined) {
    ID = url[2].split(/[^0-9a-z_\-]/i);
    ID = ID[0];
  }
  else {
    ID = false;
  }
  return ID;
}