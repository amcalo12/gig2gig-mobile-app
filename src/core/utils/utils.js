import { Dimensions, Platform, Alert, Linking, Share } from "react-native";
import { Toast } from "native-base";
import { geoLocationKey } from "utils/cons";
import moment from "moment";
import states from "utils/states.json";
import country from "../../resources/country_list.json"
import axios from 'axios';
const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);
/**
 *
 * @param {*} initialState
 * @param {Object} handlers
 * @returns {function}
 * @see http://redux.js.org/docs/recipes/ReducingBoilerplate.html
 */
export const createReducer = (initialState, handlers) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  function reducer(state = initialState, action) {
    if (Object.prototype.hasOwnProperty.call(handlers, action.type)) {
      return handlers[action.type](state, action);
    }
    return state;
  };

export const resize = (sizeDesign, type = "width") => {
  const widthDesign = 375;
  const heightDesign = 667;

  const currentSize = type === "width" ? widthDesign : heightDesign;
  const diviceSize = type === "width" ? width : height;
  const percent = (sizeDesign * 100) / currentSize;
  const percentJS = percent / 100;
  return diviceSize * percentJS;
};

export const gradientColors = ["#4d2545", "#782541"];

export const showMsj = (msj, error = false, duration = 2000) =>
  Toast.show({
    text: msj,
    position: "bottom",
    type: !error ? "success" : "danger",
    duration: duration,
    textStyle: {
      textAlign: "center"
    }
  });

export const getRange = dates => {
  console.log(dates);
  var allDates = {};
  const startDate = moment(dates.start_date, "YYYY-MM-DD").format("YYYY-MM-DD");
  const lastDate = moment(dates.end_date, "YYYY-MM-DD").format("YYYY-MM-DD");
  if (startDate === lastDate) {
    const endDate = moment(lastDate, "YYYY-MM-DD").format("YYYY-MM-DD");
    allDates[endDate] = {
      marked: true,
      customStyles: {
        container: {
          marginTop: 16,
          backgroundColor: "#D8893A",
          width: "100%",
          height: 4,
          borderRadius: 2
        },
        text: {
          position: "absolute",
          marginTop: -9,
          color: "#4D2545"
        }
      }
    };
  } else {
    const firstDate = moment(startDate, "YYYY-MM-DD").format("YYYY-MM-DD");
    allDates[firstDate] = {
      marked: true,
      customStyles: {
        container: {
          marginTop: 16,
          backgroundColor: "#D8893A",
          width: "100%",
          borderTopLeftRadius: 2,
          borderBottomLeftRadius: 2,
          height: 4,
          borderRadius: 0
        },
        text: {
          position: "absolute",
          marginTop: -9,
          color: "#4D2545"
        }
      }
    };

    const interDays = moment(lastDate).diff(startDate, "days") - 1;
    if (interDays != 0) {
      for (let i = 0; i <= interDays; i++) {
        const more = i + 1;
        var newDate = moment(startDate, "YYYY-MM-DD")
          .add(more, "days")
          .format("YYYY-MM-DD");
        allDates[newDate] = {
          marked: true,
          customStyles: {
            container: {
              marginTop: 16,
              backgroundColor: "#D8893A",
              width: "100%",
              height: 4,
              borderRadius: 0
            },
            text: {
              position: "absolute",
              marginTop: -9,
              color: "#4D2545"
            }
          }
        };
      }
    }
    const endDate = moment(lastDate, "YYYY-MM-DD").format("YYYY-MM-DD");
    allDates[endDate] = {
      marked: true,
      customStyles: {
        container: {
          marginTop: 16,
          backgroundColor: "#D8893A",
          width: "100%",
          borderTopRightRadius: 2,
          borderBottomRightRadius: 2,
          height: 4,
          borderRadius: 0
        },
        text: {
          position: "absolute",
          marginTop: -9,
          color: "#4D2545"
        }
      }
    };
  }
  return allDates;
};

function getCurrentPosition() {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      async position => {
        resolve(position);
      },
      error => reject(error)
    );
    // { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 });
  });
}

export const getLocation = async (latitude = null, longitude = null) => {
  if (!latitude && !longitude) {
    console.log('if block :>> ');
    const { coords } = await getCurrentPosition();
    console.log('coords :>> ', coords);
    const { latitude, longitude } = coords;
    console.log('latitude :>> ', latitude);
    console.log('longitude :>> ', longitude);
    var lat = latitude,
      lon = longitude;
  } else {
    console.log('else block :>> ');
    var lat = latitude,
      lon = longitude;
  }
  const myApiKey = geoLocationKey;

  return await fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?address=${lat},${lon}&key=${myApiKey}`
  ).then(async response => {
    const json = await response.json();
    console.log("location======");
    console.log(JSON.stringify(json));
    return json.results[0];
  }).catch(error => {
    console.log('error location:>> ', error)
    // return ""
  });
};
export const getCityState = async (latitude, longitude) => {
  try {
    const myApiKey = geoLocationKey;

    return await axios
      .get(`https://maps.googleapis.com/maps/api/geocode/json?address=${latitude},${longitude}&key=${myApiKey}`)
      .then(async (response) => {
        let locationObj = {
          state: "",
          city: ""
        };
        console.log('response :>> ', response.data);
        // const json = await response.json();
        const json = response.data;
        if (json.results.length > 0) {
          let result = json.results[0];
          let addressComponents = result.address_components;
          if (addressComponents.length > 0) {
            for (let i = 0; i < result.address_components.length; i++) {
              let obj = result.address_components[i];
              let long_name = obj.long_name;
              let types = obj.types
                ? obj.types.length > 0
                  ? obj.types[0]
                  : ""
                : "";
              if (long_name && long_name !== null && long_name.length > 0) {
                if (types !== "") {
                  if (types === "locality") {
                    locationObj.city = long_name;
                  }
                  if (types === "administrative_area_level_1") {
                    locationObj.state = long_name;
                  }
                }
              }
            }
          }
        }
        return locationObj;
      });
    // return await fetch(
    //   `https://maps.googleapis.com/maps/api/geocode/json?address=${latitude},${longitude}&key=${myApiKey}`
    // ).then(async response => {
    //   let locationObj = {
    //     state: "",
    //     city: ""
    //   };
    //   const json = await response.json();
    //   console.log('json fetch :>> ', json);
    //   if (json.results.length > 0) {
    //     let result = json.results[0];
    //     let addressComponents = result.address_components;
    //     if (addressComponents.length > 0) {
    //       for (let i = 0; i < result.address_components.length; i++) {
    //         let obj = result.address_components[i];
    //         let long_name = obj.long_name;
    //         let types = obj.types
    //           ? obj.types.length > 0
    //             ? obj.types[0]
    //             : ""
    //           : "";
    //         if (long_name && long_name !== null && long_name.length > 0) {
    //           if (types !== "") {
    //             if (types === "locality") {
    //               locationObj.city = long_name;
    //             }
    //             if (types === "administrative_area_level_1") {
    //               locationObj.state = long_name;
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }
    //   return locationObj;
    // });
  } catch (e) {
    console.log("location get error", e);
    return locationObj;
  }
};
export const directionMaps = direction => {
  Linking.canOpenURL(direction)
    .then(supported => {
      if (!supported) {
        Alert.alert("coordinates not valid");
      } else {
        return Linking.openURL(direction);
      }
    })
    .catch(err => console.log(err));
};

// export const directionMaps = (latitude, longitude) => {
//   const scheme = Platform.OS === "ios" ? "maps:0,0?q=" : "geo:0,0?q=";
//   const latLng = `${latitude},${longitude}`;
//   const label = "My current Location";
//   const url =
//     Platform.OS === "ios" ?
//     `${scheme}${label}@${latLng}` :
//     `${scheme}${latLng}(${label})`;

//   Linking.openURL(url);
// };

export const updateMomentLocale = () => {
  moment.updateLocale("en", {
    relativeTime: {
      future: "in %s",
      past: "%s ago",
      s: "A few seconds",
      ss: "%d seconds",
      m: "A minute",
      mm: "%d minutes",
      h: "An hour",
      hh: "%dh",
      d: "A day",
      dd: "%dd",
      M: "A month",
      MM: "%dm",
      y: "A year",
      yy: "%d years"
    }
  });
};

export const convertTimeToHour = time => {
  updateMomentLocale();
  let setDate = moment(moment(time).format()).fromNow();

  return setDate;
  // var timeStr = moment(time, "hh:mm:ss").format("h:mm");
  // timeStr = timeStr.split(':');
  // var hour = timeStr[0],
  //     minutes = timeStr[1];

  // if (minutes > 30){
  //   return parseInt(hour) + 1
  // }else{
  //   return hour
  // }
};

export const getState = id => {
  const result = states.find(state => state.id === id);
  return result;
};

export const getCountry = id => {
  const result = country.find(country => country.id === id);
  return result;
};

export const openUrl = url => {
  Linking.canOpenURL(url).then(supported => {
    if (supported) {
      Linking.openURL(url);
    } else {
      console.log("Don't know how to open URI: " + url);
      showMsj("Can't open your file", true);
    }
  });
};

export const shareContent = async message => {
  try {
    const result = await Share.share({
      message: message
    });

    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
        return true;
      } else {
        // shared
        return true;
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
      return false;
    }
  } catch (error) {
    return false;
  }
};
