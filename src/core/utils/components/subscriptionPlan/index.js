import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import Button from "utils/components/mainButton";
import styles from './index.styles';

export default class InfoLabel extends Component {


  render(){
    const {title, text, buttonText, onPress, disable} = this.props;
      return(
          <View style={styles.subscriptionContainer}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.text}>{text}</Text>
            {
              disable ?
              <Button text={buttonText} disable onPress={()=> onPress({type:title,description:text})}/>
              :
              <Button text={buttonText} onPress={()=> onPress({type:title,description:text})}/>
            }
          </View>
      );
  }
}