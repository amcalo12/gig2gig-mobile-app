import { Platform, AsyncStorage, Keyboard, Alert } from "react-native";
import { errorsValidateMsj } from "resources/l10n";
import { IsValidZipCode, emailRegExp, getThumbnail, IN_APP_SKU, IN_APP_PRICE, MESSAGES, isSubscribedActive, showAlertWithClick, parseError, showAlert } from "utils/cons";
import moment from "moment";
import SystemSetting from "react-native-system-setting";
import { requestPermission } from "utils/permissions";
import { getLocation, showMsj } from "utils/utils";
import { asyncGetPicture, uploadImage } from "utils/image";
import userApi from "api/userApi";
import uploadAsset from "utils/uploadAsset";
import { NavigationActions, StackActions } from "react-navigation";
import firebase from "react-native-firebase";
import pushNotificationApi from "api/pushNotificationApi";
import DeviceInfo from "react-native-device-info";
import ImagePicker from 'react-native-image-crop-picker';
import * as RNIap from 'react-native-iap';
import inAppPurchase from 'api/inAppPurchase';

export const handleBackPress = context => {
  const { step } = context.state;
  const {
    navigation: { goBack }
  } = context.props;
  if (step === 4) {
    context.setState({ step: 3 });
  } else if (step === 3) {
    context.setState({ step: 2 });
  } else if (step === 2) {
    context.setState({ step: 1 });
  } else {
    return goBack();
  }
  return true;
};
export const handleNextStep = async context => {
  Keyboard.dismiss();
  const {
    step,
    firstName,
    lastName,
    email,
    password,
    re_password,
    address,
    city,
    state,
    zip,
    birthdate,
    image,
    stageName,
    pwTitle,
    location,
    unionMemberShip,
    personalWebsite,
    gender,
    genderDesc,
    country
  } = context.state;

  if (step === 1) {
    if (firstName === "") {
      return showMsj(errorsValidateMsj.signup.firstName.message, true);
    }
    if (lastName === "") {
      return showMsj(errorsValidateMsj.signup.lastName.message, true);
    }
    if (email === "") {
      return showMsj(errorsValidateMsj.signup.email.message, true);
    }
    if (!emailRegExp.test(email)) {
      return showMsj(errorsValidateMsj.signup.email.invalid, true);
    }
    if (password === "") {
      return showMsj(errorsValidateMsj.signup.password.message, true);
    }
    if (re_password === "") {
      return showMsj(errorsValidateMsj.signup.re_password.message, true);
    }
    if (password !== re_password) {
      return showMsj(errorsValidateMsj.signup.passwordError.message, true);
    }
    if (password.length <= 7 && re_password.length <= 7) {
      return showMsj(
        errorsValidateMsj.signup.passwordLengthError.message,
        true
      );
    }
  }
  if (step === 2) {
    if (country === "") {
      return showMsj(errorsValidateMsj.signup.country.message, true);
    }
    // if (address === "") {
    //   return showMsj(errorsValidateMsj.signup.address.message, true);
    // }
    // if (city === "") {
    //   return showMsj(errorsValidateMsj.signup.city.message, true);
    // }
    // if (state === "") {
    //   return showMsj(errorsValidateMsj.signup.state.message, true);
    // }
    // if (zip === "") {
    //   return showMsj(errorsValidateMsj.signup.zip.message, true);
    // }
    // if (zip) {
    //   const validation = IsValidZipCode(zip);
    //   if (!validation) {
    //     return showMsj(errorsValidateMsj.signup.zip.messageError, true);
    //   }
    // }
    // comment this validation because birthdate is optional
    // if (birthdate === "") {
    // 	return showMsj(errorsValidateMsj.signup.birthdate.message, true)
    // }
  }
  if (step === 3) {
    if (image === "") {
      return showMsj(errorsValidateMsj.signup.image.message, true);
    }
    // if (stageName === "") {
    //   return showMsj(errorsValidateMsj.signup.stageName.message, true)
    // }
    if (pwTitle === "") {
      return showMsj(errorsValidateMsj.signup.pwTitle.message, true);
    }
    if (gender === "") {
      return showMsj(errorsValidateMsj.signup.gender.message, true);
    }

    if (gender.toLowerCase() == String("self describe").toLowerCase() && genderDesc == "") {
      return showMsj(errorsValidateMsj.signup.gender_desc.message, true);
    }

    // if (location === "") {
    //   return showMsj(errorsValidateMsj.signup.location.message, true)
    // }
  }
  if (step === 4) {
    if (unionMemberShip.length === 0) {
      return showMsj(errorsValidateMsj.signup.unionMemberShip.message, true);
    }
    // if (personalWebsite === "") {
    // 	return showMsj(errorsValidateMsj.signup.personalWebsite.message, true)
    // }
    return handleUserData(context);
  }

  context.setState({ step: step + 1 });
};

export const show = context => {
  context.refs.modal.setModalVisible();
};

export const select = context => {
  context.refs.select.setModalVisible();
};

export const countrySelect = context => {
  context.refs.country.setModalVisible();
};

export const _showDateTimePicker = context =>
  context.setState({
    isDateTimePickerVisible: true
  });

export const _hideDateTimePicker = context =>
  context.setState({
    isDateTimePickerVisible: false
  });

export const _handleDatePicked = (date, context) => {
  console.log('date :>> ', date);
  const today = moment().format("YYYY-MM-DD");
  var birthdate = moment(date).format("YYYY-MM-DD");
  if (today === birthdate) {
    birthdate = moment(birthdate)
      .subtract(18, "years")
      .format("YYYY-MM-DD");
  }
  context.setState({ birthdate, birth: date });
  _hideDateTimePicker(context);
};

export const handleSelectedState = (selected, context) => {
  const state = selected;
  // let state = srt.substring(0, 3);
  // state += "..."
  context.setState({
    state
  });
  select(context);
};

export const handleSelectedCountry = (selected, context) => {
  const country = selected;
  // let state = srt.substring(0, 3);
  // state += "..."
  context.setState({
    country
  });
  countrySelect(context);
};

export const handleLocation = async context => {
  context.setState({
    locationLoad: true
  });
  const isAndroid = Platform.OS === "android";
  if (isAndroid) {
    const Permissions = await requestPermission("ACCESS_FINE_LOCATION", {
      title: "Location",
      message: "¿Allow G2G to access the location of this device?"
    });
    if (Permissions) {
      const locationEnable = await SystemSetting.isLocationEnabled();
      if (locationEnable) {
        try {
          const newLocation = await getLocation();
          context.setState({
            location: newLocation.formatted_address,
            locationLoad: false
          });
        } catch (e) {
          context.setState({ locationLoad: false });
          return showMsj("A location error has ocurred", true);
        }
      } else {
        SystemSetting.switchLocation(() => {
          SystemSetting.isLocationEnabled().then(async enable => {
            const state = enable ? "On" : "Off";
            if (enable) {
              try {
                const newLocation = await getLocation();
                context.setState({
                  location: newLocation.formatted_address,
                  locationLoad: false
                });
              } catch (e) {
                context.setState({ locationLoad: false });
                return showMsj("A location error has ocurred", true);
              }
            } else {
              context.setState({
                locationLoad: false
              });
              return showMsj(
                errorsValidateMsj.signup.location.messagePermission,
                true
              );
            }
          });
        });
      }
    } else {
      context.setState({
        locationLoad: false
      });
      return showMsj(errorsValidateMsj.signup.location.messagePermission, true);
    }
  } else {
    try {
      const newLocation = await getLocation();
      context.setState({
        location: newLocation.formatted_address,
        locationLoad: false
      });
    } catch (e) {
      context.setState({ locationLoad: false });
      return showMsj("A location error has ocurred", true);
    }
  }
};

export const handleSelectedUnionMembers = (context, data) => {
  context.setState({
    unionMemberShip: data,
    unionMemberShipName: data.length > 0 ? data[0].name : ""
  });
};

export const handleGetMoreImageFormCameraRoll = async context => {
  try {
    // const permissions = await requestPermission("READ_EXTERNAL_STORAGE", {
    //   title: 'Gallery',
    //   message: '¿Allow G2G to access to your gallery of this device?'
    // });
    // if (permissions) {
    const image = await asyncGetPicture("photo");
    // }
    console.log('image selected :', image);

    ImagePicker.openCropper({
      path: image.uri,
      // width: image.width,
      // height: image.height
    }).then(async data => {
      console.log("Crop image", data);
      let newData = image;
      newData['uri'] = data.path

      let thumbnailImage = await getThumbnail(data.path)
      console.log('thumbnail :', thumbnailImage);

      // resolve(response);
      // context.setState({ image: image });
      let ext = "." + data.path.substring(data.path.lastIndexOf('.') + 1, data.path.length)
      let fileName = (image.fileName != undefined && image.fileName != null && image.fileName != '') ? image.fileName : (Date.now() + ext)
      context.setState({ image: newData, thumbnail: thumbnailImage.uri, isVisible: false, resource_name: fileName }, () => {
        console.log('this.state :>> ', context.state);
      });
    }).catch(async error => {
      let thumbnailImage = await getThumbnail(image.uri)
      console.log('thumbnail :', thumbnailImage);
      let ext = "." + image.uri.substring(image.uri.lastIndexOf('.') + 1, image.uri.length)
      let fileName = (image.fileName != undefined && image.fileName != null && image.fileName != '') ? image.fileName : (Date.now() + ext)
      context.setState({ image: image, thumbnail: thumbnailImage.uri, isVisible: false, resource_name: fileName }, () => {
        console.log('this.state :>> ', context.state);
      });
    });
  } catch (error) { }
};

export const handleUploadImage = async (resource, context) => {
  try {
    const url = await uploadImage({ uri: resource.uri });
    return url;
  } catch (error) {
    return new Promise.reject(error);
  }
};

export const handleUserData = async context => {
  Keyboard.dismiss();
  var {
    personalWebsite,
    firstName,
    lastName,
    email,
    password,
    address,
    city,
    state,
    zip,
    birthdate,
    image,
    stageName,
    pwTitle,
    location,
    unionMemberShip,
    gender,
    genderDesc,
    thumbnail,
    resource_name,
    country
  } = context.state;
  context.setState({ loading: true, messageToShow: MESSAGES.CREATING_ACCOUNT });
  try {
    const min = 1;
    const max = 1000000;
    const rand = min + Math.random() * (max - min);
    const imageUrl = await uploadAsset("profileImage", image.uri, `${rand}`, {
      contentType: image.type
    });

    const thumbnailUrl = await uploadAsset("profileImage", thumbnail, `${rand}`, {
      contentType: image.type
    });

    let params = {
      email,
      password,
      type: "2",
      first_name: firstName,
      last_name: lastName,
      address,
      city,
      state: state.id,
      birth: birthdate,
      location,
      stage_name: stageName,
      profesion: pwTitle,
      zip,
      union_member: unionMemberShip,
      image: imageUrl,
      thumbnail: thumbnailUrl,
      resource_name,
      url: personalWebsite,
      gender,
      country: country.id
    }

    if (gender.toLowerCase() == String("self describe").toLowerCase()) {
      params['gender_desc'] = genderDesc
    }

    const response = await userApi.createAccount(params);
    // context.setState({ loading: false });
    const userInfo = await userApi.login({ email, password, type: "2" });
    handleUserInfo(context, userInfo, { email, password });
    showMsj(errorsValidateMsj.signup.success.message, false);
  } catch (e) {
    console.log("eerror=====>", e);
    context.setState({ loading: false });
    const parsedError = parseError(e);
    return showMsj(parsedError, true);
    // if (e.data.errors.email) {
    //   return showMsj(e.data.errors.email[0], true);
    // } else {
    //   return showMsj(errorsValidateMsj.signup.error.message, true);
    // }
  }
};

const handleUserInfo = async (context, response, userData) => {
  const {
    navigation: { navigate }
  } = context.props;
  let data = response.data
  data['isPaidUser'] = data.is_premium ? true : false
  const storeUser = [
    ["Authorization", response.token_type + " " + response.access_token],
    ["email", userData.email],
    ["password", userData.password],
    ["userData", JSON.stringify(data)]
  ];
  console.log('userData ======================================:>> ', data);
  await AsyncStorage.multiSet(storeUser);
  try {
    const token = await firebase.messaging().getToken();
    console.log("tokentokentoken", token);
    let uniqueId = await DeviceInfo.getUniqueId();
    await pushNotificationApi.setPushToken({
      pushkey: token,
      device_id: uniqueId,
      device_type: Platform.OS
    });
  } catch (error) {
    context.setState({ loading: false })
    console.log("error", error);
  }
  if (context.props.navigation.state.params.sku == "free") {
    context.setState({ loading: false })
    return context.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "AppIntro" })]
      })
    );
  } else {
    // return context.props.navigation.navigate('PurchaseComplete', {
    //   sku: context.props.navigation.state.params.sku,
    //   planName: context.props.navigation.state.params.planName
    // })
    context.setState({ messageToShow: MESSAGES.CHECK_PURCHASE_AVAILABLE })
    let params = {
      email: data.email,
      time: Date.now(),
      error: MESSAGES.CHECK_PURCHASE_AVAILABLE
    }
    firebase.crashlytics().recordError(111, JSON.stringify(params));
    const isSubscribe = await isSubscribedActive()
    console.log('this.isSubscribed() :>> ', isSubscribe);
    let logParams = {
      email: data.email,
      time: Date.now(),
      error: isSubscribe
    }
    firebase.crashlytics().recordError(222, JSON.stringify(logParams));
    if (isSubscribe) {
      showAlertWithClick(MESSAGES.STORE_ALERT, () => {
        context.setState({ loading: false, messageToShow: "" })
        return context.props.navigation.dispatch(
          StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "AppIntro" })]
          })
        );
      })
      return
    } else {
      context.setState({ messageToShow: MESSAGES.PROCESS_SUBSCRIPTION })
      let logParams = {
        email: data.email,
        time: Date.now(),
        error: MESSAGES.PROCESS_SUBSCRIPTION
      }
      firebase.crashlytics().recordError(333, JSON.stringify(logParams));
      requestSubscription(context, context.props.navigation.state.params.sku, data)
    }
  }
};

const requestSubscription = async (context, sku, userData) => {
  try {
    // firebase.crashlytics().log(sku)
    // firebase.crashlytics().log("Request Begin")
    const purchase = await RNIap.requestSubscription(sku);
    firebase.analytics().logEvent("PURCHASE_DONE", {})
    // firebase.crashlytics().log("Purchase Data Get")
    const ackResult = await RNIap.finishTransaction(purchase);
    console.log('purchase :>>11 ', purchase);
    console.log('purchase :>>11 ', ackResult);
    // return purchase
    let purchaseData = (new Date(purchase.transactionDate)).toUTCString()
    console.log('purchaseData :>> ', purchaseData);
    // let expireAt = (new Date(moment(purchaseData).add(5, 'm').toDate())).toUTCString()
    let expireAt = ""
    if (IN_APP_SKU.IOS_MONTH == sku || IN_APP_SKU.ANDROID_MONTH == sku) {
      expireAt = (new Date(moment(purchaseData).add(1, 'M').toDate())).toUTCString()
    } else {
      expireAt = (new Date(moment(purchaseData).add('years', 1).toDate())).toUTCString()
    }
    // (new Date(moment(purchaseData).add(5, 'm').toDate())).toUTCString()

    console.log('expireAt 11:>> ', expireAt);
    let params = {
      user_id: userData.details.user_id,
      product_id: purchase.productId,
      original_transaction: Platform.OS == "ios" ? purchase.originalTransactionIdentifierIOS : purchase.purchaseToken,
      current_transaction: purchase.transactionId,
      ends_at: moment.utc(expireAt).format('YYYY-MM-DD HH:mm:ss'),
      purchase_platform: Platform.OS,//    <ios, android, web>,
      purchased_at: moment.utc(purchaseData).format('YYYY-MM-DD HH:mm:ss'),// purchaseData,
      name: IN_APP_SKU.IOS_MONTH == sku || IN_APP_SKU.ANDROID_MONTH == sku ? "Monthly" : "Annual",
      transaction_receipt: Platform.OS == "ios" ? purchase.transactionReceipt : purchase.purchaseToken,
      purchased_price: IN_APP_SKU.IOS_MONTH == sku || IN_APP_SKU.ANDROID_MONTH == sku ? IN_APP_PRICE.MONTH : IN_APP_PRICE.YEAR,
    }
    // firebase.crashlytics().log("Purchase params send")
    context.setState({ messageToShow: MESSAGES.UPDATE_ACCOUNT })
    let logParams = {
      email: userData.email,
      time: Date.now(),
      error: "App store Purchase complete success"
    }
    firebase.crashlytics().recordError(444, JSON.stringify(logParams));
    subscriptionSuccess(context, params, userData)
  } catch (err) {
    // showAlert(err.message);
    Alert.alert(err.message);
    // context.setState({ loading: false }, () => {
    //   showAlert(err.message);
    // })

    let logParams = {
      email: userData.email,
      time: Date.now(),
      error: err.message
    }
    firebase.crashlytics().recordError(555, JSON.stringify(logParams));
    // firebase.analytics().logEvent("PURCHASE_ERROR", { error: err.code })
    console.log('err :>> ', err);
    console.log('err :>>11 ', err.code);
    // firebase.crashlytics().recordError(22, err.code);

  }
};

const subscriptionSuccess = async (context, params, userData) => {
  try {
    // firebase.crashlytics().log("Purchase stared")
    const purchaseSuccess = await inAppPurchase.subscriptionSuccess(params)
    // firebase.crashlytics().log("Purchase success")
    context.setState({ loading: false, messageToShow: "" })
    console.log('purchaseSuccess :>> ', purchaseSuccess);
    let logParams = {
      email: userData.email,
      time: Date.now(),
      error: "Subscription Purchase successfully!"
    }
    firebase.crashlytics().recordError(666, JSON.stringify(logParams));
    let data = purchaseSuccess.data
    data['isPaidUser'] = data.is_premium ? true : false
    await AsyncStorage.setItem("userData", JSON.stringify(data));

    context.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "AppIntro" })]
      })
    );
  } catch (e) {
    // firebase.crashlytics().recordError(33, e);
    context.setState({ loading: false, messageToShow: "" })
    const parsedError = parseError(e);
    // firebase.analytics().logEvent("PURCHASE_SUCCESS_ERROR", { error: e })
    let logParams = {
      email: userData.email,
      time: Date.now(),
      error: parsedError
    }
    firebase.crashlytics().recordError(777, JSON.stringify(logParams));
    console.log(e);
  }
}

export default {
  handleBackPress,
  handleNextStep,
  show,
  select,
  _showDateTimePicker,
  _hideDateTimePicker,
  _handleDatePicked,
  handleSelectedState,
  handleSelectedCountry,
  handleLocation,
  handleSelectedUnionMembers,
  handleGetMoreImageFormCameraRoll,
  handleUploadImage
};
