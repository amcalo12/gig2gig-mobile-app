import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image, Alert, Platform, AsyncStorage } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions, NavigationActions } from 'react-navigation';
import Input from "utils/components/mainInput";
import DownArrowIcon from "resources/images/down-arrow-icon.png";
import Button from "utils/components/mainButton";
import Select from "utils/components/selectSpinner";
import states from "utils/states.json";
import { getState, showMsj, getCountry } from "utils/utils";
import DateTimePicker from 'react-native-modal-datetime-picker';
import Loading from "utils/components/loading";
import { maximumDate, minimumDate } from "utils/cons";
import {
  handleGetMoreImageFormCameraRoll,
  select,
  countrySelect,
  genderSelect,
  handleSelectedState,
  handleSelectedCountry,
  handleSelectedGender,
  _showDateTimePicker,
  _hideDateTimePicker,
  _handleDatePicked,
  handleUserInfo,
  handleLocation,
  validateForm
} from "./MyInfoEditViewModel";
import moment from "moment";
import RenameModal from "utils/components/renameModal";
import myProfileApi from "api/myProfileApi";
import userApi from "api/userApi";
import { fontStyles } from "resources/fonts";
import { colorPalette } from "resources/colors";
import pushNotificationApi from "api/pushNotificationApi";
import DeviceInfo from "react-native-device-info";

// const countryList = require('../../../../resources/country_list.json');
const countryList = require('../../../../../../resources/country_list.json');
const genders = [
  { name: 'Agender', value: 'agender' },
  { name: 'Female', value: 'female' },
  { name: 'Gender diverse', value: 'gender diverse' },
  { name: 'Gender expansive', value: 'gender expansive' },
  { name: 'Gender fluid', value: 'gender fluid' },
  { name: 'Genderqueer', value: 'genderqueer' },
  { name: 'Intersex', value: 'intersex' },
  { name: 'Male', value: 'male' },
  { name: 'Non-binary', value: 'non-binary' },
  { name: 'Transfemale/transfeminine', value: 'transfemale/transfeminine' },
  { name: 'Transmale/transmasculine', value: 'transmale/transmasculine' },
  { name: 'Two-spirit', value: 'two-spirit' },
  { name: 'Self describe', value: 'self describe' },
  { name: 'Prefer not to answer', value: 'Prefer not to answer' },
];
class MyInfoEditView extends Component {

  constructor(props) {
    super(props)
    this.image = null
    const userData = this.props.navigation.state.params.userData;
    console.log('userData edit :>> ', userData);
  }

  state = {
    loading: false,
    locationLoad: false,
    image: null,
    thumbnail: null,
    url: '',
    imageChanged: false,
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    stage_name: '',
    profesion: '',
    location: '',
    address: '',
    city: '',
    state: '',
    zip: '',
    birth: '',
    isDateTimePickerVisible: false,
    isVisible: false,
    fileName: "",
    imageData: "",
    country: '',
    gender: '',
    genderDesc: ""
  }

  async componentDidMount() {
    const userData = this.props.navigation.state.params.userData;
    console.log('userData edit :>> ', userData);
    if (this.props.navigation.state.params.isFromHome) {
      console.log('if condition :>> ');
      this.setState({ email: userData.email })
      return
    }
    this.setState({
      url: userData.details.url,
      image: userData.image != null ? userData.image.url : '',
      fileName: userData.image != null ? userData.image.name : '',
      imageData: userData.image,
      first_name: userData.details.first_name,
      last_name: userData.details.last_name,
      email: userData.email,
      stage_name: userData.details.stage_name,
      profesion: userData.details.profesion,
      location: userData.details.location,
      address: userData.details.address,
      city: userData.details.city,
      state: await getState(userData.details.state),
      zip: userData.details.zip,
      gender: userData.details.gender,
      genderDesc: userData.details.gender_desc,
      birth: userData.details.birth === null || userData.details.birth === "" ? '' : userData.details.birth,
      country: userData.details.country != null ? await getCountry(String(userData.details.country)) : ''
    });
  }

  onCloseClick = () => {
    this.setState({ isVisible: false })
  }

  onSubmitClick = async () => {
    this.setState({ loading: true });
    try {
      let params = {
        id: this.state.imageData.id,
        name: this.state.fileName
      }
      const response = await myProfileApi.renameResource(params)
      console.log('response :', response);
      this.setState({ loading: false });
    } catch (error) {
      this.setState({ loading: false });
      showMsj(errorsValidateMsj.editProfile.error.message, true)
    }
    this.setState({ isVisible: false, })
  }

  onChangeText = (fileName) => {
    this.setState({ fileName })
  }

  renderRenameModal = () => {
    return (
      <RenameModal
        isVisible={this.state.isVisible}
        onCloseClick={this.onCloseClick}
        onSubmitClick={this.onSubmitClick}
        value={this.state.fileName}
        onChangeText={this.onChangeText}
      />
    )
  }

  showModal = () => {
    Alert.alert(
      "Sign Out",
      "Are you sure you want to sign out?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => this.logOut()
        }
      ],
      { cancelable: false }
    );
  };

  logOut = async () => {
    try {
      let uniqueId = await DeviceInfo.getUniqueId();
      await pushNotificationApi.setPushToken({
        pushkey: "",
        device_id: uniqueId,
        device_type: Platform.OS
      });
    } catch (error) {
      console.log(error);
    }
    AsyncStorage.clear();
    return this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "Initial" })]
      })
    );
  };

  render() {
    const {
      loading,
      locationLoad,
      image,
      first_name,
      last_name,
      email,
      password,
      stage_name,
      profesion,
      location,
      address,
      city,
      state,
      zip,
      birth,
      url,
      imageData,
      country,
      gender,
      genderDesc
    } = this.state;
    const { navigation: { goBack } } = this.props;
    return (
      <Container>
        <Header
          leftIcon={this.props.navigation.state.params.isFromHome ? 'none' : "backDark"}
          leftFn={() => goBack()}
          barStyle="dark-content"
          text="My Info"
        />
        <Content enableResetScrollToCoords={false}>
          {this.renderRenameModal()}
          {this.props.navigation.state.params.isFromHome ? <TouchableOpacity style={{ alignSelf: 'flex-end', marginRight: 20 }} onPress={() => this.showModal()}>
            <Text style={{
              padding: 5,
              fontFamily: fontStyles.nexa_bold,
              fontSize: 16,
              color: colorPalette.mainButtonBg,
              textAlign: 'center',
            }}>{"Logout"}</Text>
          </TouchableOpacity>
            : null}
          <TouchableOpacity style={styles.userImage} onPress={() => handleGetMoreImageFormCameraRoll(this)}>
            {/* <Image style={styles.userPicture} source={image ? {
              uri: imageData.thumbnail != null ?
                imageData.thumbnail : image
            } : null} /> */}
            <Image style={styles.userPicture} source={this.state.imageChanged ? { uri: this.state.thumbnail } :
              this.state.imageData.thumbnail != null ? { uri: this.state.imageData.thumbnail } : image ? { uri: image } : null} />
          </TouchableOpacity>
          <View style={styles.container}>
            <Input placeholder="First Name" data={first_name} onEdit={first_name => this.setState({ first_name })} elevation={3} customWidth={styles.input.width} margin={10} />
            <Input placeholder="Last Name" data={last_name} onEdit={last_name => this.setState({ last_name })} elevation={3} customWidth={styles.input.width} margin={10} />
            <Input placeholder="Email" data={email} onEdit={email => this.setState({ email })} elevation={2} customWidth={styles.input.width} margin={10} />
            <Input placeholder="Password" data={password} onEdit={password => this.setState({ password })} elevation={2} customWidth={styles.input.width} margin={10} isSecure={true} />
            <Input placeholder="Stage Name" autoCapital={true} data={stage_name} onEdit={stage_name => this.setState({ stage_name })} elevation={2} customWidth={styles.input.width} margin={10} />
            <Input placeholder="Professional/Working Title" autoCapital={true} data={profesion} onEdit={profesion => this.setState({ profesion })} elevation={2} customWidth={styles.input.width} margin={10} />
            <Input
              // placeholder="Personal Website"
              placeholder="Website (optional)"
              data={url} onEdit={url => this.setState({ url })} elevation={2} customWidth={styles.input.width} margin={10} />
            <View style={styles.loadingContainer}>
              {
                locationLoad &&
                <Loading style={styles.loading} />
              }
              {/* <Input placeholder="Location" data={location} elevation={2} customWidth={styles.input.width} margin={10} isTouchable onPress={()=>handleLocation(this)}/> */}
            </View>
            {console.log('country :>> ', gender)}
            <Input placeholder="Country" data={country.name ? country.name : ""} icon={DownArrowIcon} isTouchable onPress={(data) => countrySelect(this)} />
            <Input placeholder="Gender Identity"
              data={gender ? gender : ""}
              icon={DownArrowIcon}
              isTouchable
              autoCapital={true}
              onPress={(data) => genderSelect(this)} />

            {gender != null && gender.toLowerCase() == String("self describe").toLowerCase() ?
              <Input placeholder="Self describe"
                data={genderDesc ? genderDesc : ""}
                onEdit={genderDesc => this.setState({ genderDesc })} elevation={3}
                customWidth={styles.input.width}
                margin={10} />
              : null}
            {/* <Input placeholder="Address" data={address} onEdit={address => this.setState({ address })} elevation={2} customWidth={styles.input.width} margin={10} />
            <Input placeholder="City" data={city} onEdit={city => this.setState({ city })} elevation={2} customWidth={styles.input.width} customWidth={styles.input.width} margin={10} />
            <Input placeholder="State" data={state.name} icon={DownArrowIcon} elevation={2} customWidth={styles.input.width} margin={10} isTouchable onPress={() => select(this)} />
            <Input placeholder="Zip" data={zip.toString()} onEdit={zip => this.setState({ zip })} elevation={2} customWidth={styles.input.width} margin={10} keyboardType="number-pad" /> */}
            {
              console.log('123123', birth)
            }
            {
              console.log('moment(birth).format(`MM/DD/YYYY`)', moment(birth).format('MM/DD/YYYY'))
            }
            <Input placeholder="Birth Date" data={birth === '' || birth === null ? 'Birth Date' : moment(birth).format('MM/DD/YYYY')} elevation={2} customWidth={styles.input.width} isTouchable onPress={() => _showDateTimePicker(this)} />
          </View>
          <View style={styles.button}>
            <Button loading={loading} text="Save" onPress={() => validateForm(this)} />
          </View>
          <Select
            selectTitle="Select a State"
            data={states}
            onPress={(selected) => handleSelectedState(selected, this)}
            ref="select"
          />
          <Select
            selectTitle="Select a Country"
            data={countryList}
            onPress={(selected) => handleSelectedCountry(selected, this)}
            ref="country" />
          <Select
            selectTitle="Gender Identity"
            data={genders}
            onPress={(selected) => handleSelectedGender(selected, this)}
            ref="gender" />
          <DateTimePicker
            maximumDate={maximumDate()}
            minimumDate={minimumDate()}
            date={this.state.birth != "" ? new Date(this.state.birth) : maximumDate()}
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={(date) => _handleDatePicked(date, this)}
            onCancel={() => _hideDateTimePicker(this)}
          />
        </Content>
      </Container>
    );
  }
}
export default MyInfoEditView;
