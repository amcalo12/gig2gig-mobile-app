import ImagePicker from 'react-native-image-picker';

export default ImageSelect = async (title = 'image') => {
  const options = {
    title,
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  return new Promise((resolve) => {
    ImagePicker.showImagePicker(options, (response) => {
      resolve(response)
    })
  })
}