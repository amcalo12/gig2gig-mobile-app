import { createStackNavigator } from "react-navigation";
import SkillsView from ".";

const SkillsStack = createStackNavigator(
  {
     Skills: {
       screen: SkillsView,
       navigationOptions: {
         header: null
       }
     }
  },
  {
    initialRouteName: "Skills",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default SkillsStack;
