import { createStackNavigator } from "react-navigation";
import MyCalendarView from ".";
import MyCalendarAddView from "./addEvent"

const MyCalendarStack = createStackNavigator(
  {
     MyCalendar: {
       screen: MyCalendarView,
       navigationOptions: {
         header: null
       }
     },
      AddEvent: {
       screen: MyCalendarAddView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "MyCalendar",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default MyCalendarStack;
