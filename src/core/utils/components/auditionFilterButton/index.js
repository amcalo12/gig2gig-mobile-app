import React, { Component } from "react";
import { TouchableOpacity, Image, Text, View } from "react-native";
import styles from "./index.styles"
class AuditionFilterButton extends Component {

	getContainerStyles = () => {
		const { type, small, marginBottom } = this.props;
		let buttonStyles = [];
		let containerStyle = "";
		if (type === "union") {
			containerStyle = "unionContainer";
		} else if (type === "contract") {
			containerStyle = "contractContainer";
		} else if (type === "production") {
			containerStyle = "productionContainer";
		}
		if (type === "online") {
			containerStyle = "onlineContainer";
		}
		buttonStyles.push(styles[containerStyle]);

		if (small) {
			buttonStyles.push(styles.container);
		} else {
			buttonStyles.push(styles.containerBig);
		}

		if (marginBottom) {
			buttonStyles.push({ marginBottom: marginBottom });
		}

		return buttonStyles;
	}

	getTextStyles = () => {
		const { type, small } = this.props;
		let textStyles = [];
		let textType = "";
		if (type === "union") {
			textType = "unionText";
		} else if (type === "contract") {
			textType = "contractText";
		} else if (type === "production") {
			textType = "productionText";
		}
		if (type === "online") {
			textType = "onlineText";
		}
		textStyles.push(styles[textType]);

		if (small) {
			textStyles.push(styles.text);
		} else {
			textStyles.push(styles.textBig);
		}

		return textStyles;
	}

	render() {
		const { text } = this.props;

		return (
			<TouchableOpacity
				activeOpacity={1}
				style={this.getContainerStyles()}>
				<Text style={this.getTextStyles()}>{text.toUpperCase()}</Text>
			</TouchableOpacity>
		);
	}
}
export default AuditionFilterButton;
AuditionFilterButton.defaultProps = {
	type: "union",
	text: "UNION",
	small: true
}