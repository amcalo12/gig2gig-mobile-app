import React, { Component } from "react";
import { View, Text, TextInput, TouchableOpacity, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import PlusIcon from "resources/images/plus-icon.png"
import { showMsj, openUrl, shareContent } from "utils/utils";
import { generalMsj, shareMsj } from "resources/l10n";
import MainButton from "utils/components/mainButton";
import { getTopics, addPost } from "api/supportForum";
import alertMessaje from "utils/alertMessaje";
import ModalTopics from "utils/components/ModalTopics";

class AddSupportForum extends Component {

  state = {
    loading: true,
    formData: {
      title: {
        value: '',
        type: '',
        required: true
      },
      body: {
        value: '',
        type: '',
        required: true
      },
    },
    visible: false,
    topics: [],
    topic_ids: []
  }

  async componentDidMount() {
    try {
      const topics = await getTopics()
      this.setState({
        loading: false,
        topics: topics.data
      })
    } catch (error) {
      this.setState({
        loading: false,
      })
      return showMsj('Not found topics!', true)
    }
  }

  _onPressAddForum = async () => {
    const {
      formData: {
        body,
        title
      },
      topic_ids
    } = this.state

    try {
      if (body != '' && title != '' && topic_ids.length != 0) {
        this.setState({
          loading: true,
          visible: false
        })

        const dataSent = {
          title: title.value,
          body: body.value,
          type: 'forum',
          topic_ids
        }

        await addPost(dataSent)

        showMsj('Successful')
        if (this.props.navigation) {
          if (this.props.navigation.state) {
            if (this.props.navigation.state.params) {
              if (this.props.navigation.state.params.onAddSupportForum) {
                this.props.navigation.state.params.onAddSupportForum();
              }
            }
          }
        }
        this.props.navigation.goBack()

        this.setState({
          loading: false,
          visible: false
        })
      } else {
        alertMessaje('All fields are required')
      }
    } catch (error) {
      this.setState({
        loading: false,
        visible: false
      })
    }
  }

  _onChange = (key, value) => {
    const { state } = this

    state.formData[key].value = value
    state.visible = false

    this.setState({
      ...state
    })
  }

  render() {

    const {
      loading,
      formData,
      visible,
      topics,
      topic_ids
    } = this.state;

    const { navigation } = this.props;

    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => navigation.goBack()}
          barStyle="dark-content"
          text="Support Forum"
        />
        <Content
          bounces={false}
        >
          <TextInput
            placeholder={'Post Title'}
            style={styles.postTitleInput}
            value={formData.title.value}
            onChangeText={(text) => this._onChange('title', text)}
          />
          <TextInput
            placeholder={'Post'}
            style={styles.postBodyInput}
            value={formData.body.value}
            multiline
            onChangeText={(text) => this._onChange('body', text)}
          />
          <TouchableOpacity
            style={styles.tagContainer}
            onPress={() => this.setState({ visible: true })}
          >
            <Image
              source={PlusIcon}
            />
            <Text
              style={styles.tagsTxt}
            >
              {
                topic_ids.length != 0 ?
                  topic_ids.map(res => {
                    const filter = topics.filter(resx => resx.id == res.id)
                    if (filter.length != 0) {
                      return `${filter[0].title} `
                    }
                    return ``
                  })
                  :
                  `Tags`
              }
            </Text>
          </TouchableOpacity>
          <MainButton
            loading={loading}
            text={'Post'}
            customWidth={styles.btn.width}
            customHeight={styles.btn.height}
            onPress={() => this._onPressAddForum()}
          />
        </Content>
        {
          visible &&
          <ModalTopics
            visible={visible}
            topics={topics}
            topic_ids={topic_ids}
            context={this}
          />
        }
      </Container>
    );
  }
}
export default AddSupportForum;
