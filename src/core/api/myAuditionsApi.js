import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const getAuditionsRequested = async (is_Online) =>
  apiHandler(API_METHOD.GET, API_ROUTES.MY_AUDITIONS.GET_AUDITIONS_REQUESTED + "?only_online=" + is_Online);

const saveChecking = async (props, id) =>
  apiHandler(
    API_METHOD.PUT,
    `${API_ROUTES.MY_AUDITIONS.SAVE_CHECKING}${id}`,
    props
  );

const getAuditionUpdate = async id =>
  apiHandler(
    API_METHOD.GET,
    `${API_ROUTES.MY_AUDITIONS.GET_AUDITIONS_UPDATES}${id}`
  );

const getAuditionsPast = async (is_Online) =>
  apiHandler(API_METHOD.GET, API_ROUTES.MY_AUDITIONS.GET_AUDITIONS_PAST+ "?only_online=" + is_Online);

const getUserFeedback = async id =>
  apiHandler(
    API_METHOD.GET,
    `${API_ROUTES.MY_AUDITIONS.GET_FINAL_FEEDBACK}${id}`
  );

const getWaitlist = async id =>
  apiHandler(API_METHOD.GET, `appointments/auditions/${id}`);

const getUserFeedbackRecomendation = async (audition_id, appointment_id) =>
  apiHandler(
    API_METHOD.GET,
    `a/auditions/${audition_id}/feeback/recommendations-marketplaces?appointment_id=${appointment_id}`
  );

const getUpcomingAuditions = async (is_Online) =>
  apiHandler(API_METHOD.GET, API_ROUTES.AUDITIONS.UPCOMING_AUDITION + "?only_online=" + is_Online);

const getTimesToUpcoming = async id =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.AUDITION_UPCOMING_TIME}${id}${API_ROUTES.AUDITIONS.GET_UPCOMING_TIME}`
  );

const updateToUpcoming = async id =>
  apiHandler(API_METHOD.PUT, `${API_ROUTES.AUDITIONS.UPDATE_TO_UPCOMING}${id}`);

const addAssignedNumber = async props =>
  apiHandler(
    API_METHOD.POST,
    `${API_ROUTES.MY_AUDITIONS.ADD_ASSIGNED_NUMBER}`,
    props
  );

const getInstantFeedback = async (appointment_id, user_id) =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.AUDITION}${appointment_id}${API_ROUTES.MY_AUDITIONS.GET_INSTANT_FEEDBACK}${user_id}`
  );

const swipeToRemoveAudition = async id =>
  apiHandler(API_METHOD.DELETE, `${API_ROUTES.AUDITIONS.DELETE_AUDITION}${id}`);

export default {
  getAuditionsRequested,
  saveChecking,
  getAuditionUpdate,
  getAuditionsPast,
  getUserFeedback,
  getUpcomingAuditions,
  getTimesToUpcoming,
  updateToUpcoming,
  getUserFeedbackRecomendation,
  getWaitlist,
  addAssignedNumber,
  getInstantFeedback,
  swipeToRemoveAudition
};
