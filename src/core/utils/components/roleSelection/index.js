import React, { Component } from 'react';
import { View, Text, Modal, Image, FlatList, TouchableOpacity } from 'react-native';
import { styles } from './index.styles';
import { Assets } from 'resources/assets';
import { resize } from 'utils/utils';
import Button from "utils/components/mainButton";
import { colorPalette } from 'resources/colors';
import RoleIcon from "resources/images/audition/role-icon.png"
import CheckIcon from "resources/images/check-clear.png";
import CheckedIcon from "resources/images/check-checked.png";
import LinearGradient from 'react-native-linear-gradient';
import { fontStyles } from 'resources/fonts';
import { ApplyButton } from '../generalbutton';

export default class RoleSelectionModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roles: this.props.rolesList
    };
  }

  onItemClick = (item, index) => {
    // this.props.rolesList[index].checked = !this.props.rolesList[index].checked
    // this.props.onItemSelect(this.props.rolesList)
    this.state.roles[index].checked = !this.state.roles[index].checked
    this.setState({ roles: this.state.roles })
    // this.props.onItemSelect(this.state.roles)
  }

  renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", margin: 10 }} onPress={() => this.onItemClick(item, index)}>
        <View style={styles.roleContainer}>
          <LinearGradient colors={['#4d2545', '#782541']} style={styles.roleContainer}>
            <Image style={item.image ? styles.roleImage : null} source={item.image ? {
              uri: item.image.thumbnail != null ?
                item.image.thumbnail : item.image.url
            } : RoleIcon} />
          </LinearGradient>
        </View>

        <Text style={{ flex: 1, color: colorPalette.mainButtonBg, fontFamily: fontStyles.nexa_bold, marginLeft: 10 }}>{item.name}</Text>
        <Image style={styles.icon} source={item.checked ? CheckedIcon : CheckIcon} />
      </TouchableOpacity>
    )
  }

  componentWillReceiveProps(props) {
    this.setState({ roles: props.rolesList })
  }

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.isVisible}
        onRequestClose={this.props.onCloseClick}>
        <View style={styles.modalContainer}>
          <View style={styles.modalSubContainer}>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
              <TouchableOpacity style={{ position: "absolute", left: 5 }} onPress={this.props.onCloseClick}>
                <Image source={Assets.close} />
              </TouchableOpacity>
              <Text style={{
                color: colorPalette.mainButtonBg, fontFamily: fontStyles.nexa_bold,
                fontSize: resize(18),
              }}>{"Select Roles"}</Text>
            </View>
            <View style={{ flex: 1 }}>
              {console.log('roles :', this.state.roles)}
              <FlatList
                style={{ flex: 1, marginHorizontal: 20, }}
                showsVerticalScrollIndicator={false}
                data={this.state.roles}
                extraData={this.state}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => item + index}
              />
            </View>
            <View>

              {
                this.props.isRequest ?
                  <ApplyButton title={"Request Audition from Agent"} onClick={() => {
                    this.props.onCloseClick();
                    this.props.onClick(this.state.roles);
                    // this.checkForRepresentation(auditionData, 2, online)
                  }} />
                  :
                  this.props.isSubmit ?
                    <ApplyButton style={{ backgroundColor: "#FEA84B" }} title={"Submit"} onClick={() => {
                      this.props.onCloseClick();
                      this.props.onClick(this.state.roles);
                      // this.checkForRepresentation(auditionData, 2, online)
                    }} /> :
                    <ApplyButton style={{ backgroundColor: "#FEA84B" }} title={this.props.isOnline ? "Save for later" : "Save Audition"} onClick={() => {
                      this.props.onCloseClick();
                      this.props.onClick(this.state.roles);
                      // this.checkForRepresentation(auditionData, 2, online)
                    }} />
              }

              {/* <Button
                text="Request Audition from Agent"
                customFontSize={13}
                onPress={() => {
                }}
              /> */}
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
