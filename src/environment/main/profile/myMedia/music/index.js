import React, { Component } from "react";
import { View, Text, RefreshControl, Platform } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import ButtonAction from "utils/components/buttonActionFloat";
import PlusIcon from "resources/images/myMedia/plus-icon.png";
import Card from "utils/components/mediaCards";
import getFile from "utils/getFiles";
import uploadAsset from "utils/uploadAsset";
import uploadMusicFiles from "utils/uploadMusicFiles";
import Loading from "utils/components/loading";
import { showMsj, openUrl, shareContent } from "utils/utils";
import myMediaApi from "api/myMediaApi";
import Modal from "utils/components/modalUploadFiles";
import { generalMsj, shareMsj } from "resources/l10n";
import { refreshControlColors } from "resources/colors";
import SystemSetting from "react-native-system-setting";
import { requestPermission } from "utils/permissions";
import RenameModal from "utils/components/renameModal";
import myProfileApi from "api/myProfileApi";
class MusicView extends Component {
  constructor(props){
    super(props)
    this.currentData = null
  }
  state = {
    loading: false,
    refreshing: false,
    files: null,
    modalMessage: "",
    isVisible:false,
    fileName:"",
    selectedFile:null
  };

  async componentDidMount() {
    this.getFiles();
  }

  _onRefresh = async () => {
    this.getFiles(true);
  };

  getFiles = async (refresh = false) => {
    if (refresh) {
      this.setState({ refreshing: true });
    } else {
      this.setState({ loading: true });
    }
    try {
      const response = await myMediaApi.getUserMediaByType("audio");
      this.setState({
        files: response.data,
        loading: false,
        refreshing: false
      });
    } catch (e) {
      this.setState({ loading: false, refreshing: false });
      return showMsj(generalMsj.error.message, true);
    }
  };

  saveUrlFile = async () => {
    const file = await this.getResource();
    console.log('file :', file);
		if (file) {
			this.setState({ isVisible: true, selectedFile: file })
		}
    // const fileUrl = await this.uploadFile();
    // if (fileUrl) {
    //   this.setState({ modalMessage: "Uploading your file" });

    //   try {
    //     const response = await myMediaApi.addUserMedia(fileUrl);
    //     this.refs.modal.setModalVisible();
    //     showMsj(response.data, false);
    //     this.getFiles();
    //   } catch (e) {
    //     this.refs.modal.setModalVisible();
    //     return showMsj("An error has ocurred saving your file", true);
    //   }
    // }
  };

  uploadFile = async () => {
    // const file = await this.getResource();
    const file = this.state.selectedFile;
    if (file) {
      try {
        this.refs.modal.setModalVisible();
        // let fileName = file.filename
        //   ? file.filename
        //   : "MUSIC_" + new Date().getTime() + ".pdf";
        let fileName = this.state.fileName
        const fileUrl = await uploadMusicFiles(
          // "phone/users/files",
          "my_media",
          file.url,
          fileName,
          file.metadata
        );
        let fileData = {
          url: fileUrl,
          name: fileName,
          type: "audio"
        };
        return fileData;
      } catch (e) {
        this.refs.modal.setModalVisible();
        return showMsj("An error has ocurred Uploading your file", true);
      }
    }
  };

  // saveUrlFile = async () => {
  //   const fileUrl = await this.uploadFile();
  //   if (fileUrl) {
  //     this.setState({ modalMessage: "Uploading your file" });

  //     try {
  //       const response = await myMediaApi.addUserMedia(fileUrl);
  //       this.refs.modal.setModalVisible();
  //       showMsj(response.data, false);
  //       this.getFiles();
  //     } catch (e) {
  //       this.refs.modal.setModalVisible();
  //       return showMsj("An error has ocurred saving your file", true);
  //     }
  //   }
  // };

  // uploadFile = async () => {
  //   const file = await this.getResource();
  //   if (file) {
  //     try {
  //       this.refs.modal.setModalVisible();
  //       let fileName = file.filename
  //         ? file.filename
  //         : "MUSIC_" + new Date().getTime() + ".pdf";
  //       const fileUrl = await uploadMusicFiles(
  //         "phone/users/files",
  //         file.url,
  //         fileName,
  //         file.metadata
  //       );
  //       let fileData = {
  //         url: fileUrl,
  //         name: fileName,
  //         type: "audio"
  //       };
  //       return fileData;
  //     } catch (e) {
  //       this.refs.modal.setModalVisible();
  //       return showMsj("An error has ocurred Uploading your file", true);
  //     }
  //   }
  // };

  getResource = async () => {
    try {
      if (Platform.OS === "android") {
        const Permissions = await requestPermission("READ_EXTERNAL_STORAGE", {
          title: "Storage",
          message: "Allow G2G to access the storage of this device?"
        });
        if (Permissions) {
          const myFile = await getFile(1);
          return myFile;
        } else {
          SystemSetting.openAppSystemSettings();
        }
      } else {
        const myFile = await getFile(1);
        return myFile;
      }
    } catch (e) {
      // this.refs.modal.setModalVisible();
      // return showMsj('An error has ocurred getting your file', true);
    }
  };

  deleteFile = async id => {
    this.setState({ modalMessage: "Deleting your file" });
    this.refs.modal.setModalVisible();
    try {
      const response = await myMediaApi.deleteMediaUserFile(id);
      this.refs.modal.setModalVisible();
      showMsj(response.data, false);
      this.getFiles();
    } catch (e) {
      this.refs.modal.setModalVisible();
      showMsj("An error has ocurred deleting your file", true);
    }
  };

  shareFile = async data => {
    const message = `${shareMsj.myMedia.message}\n${data.url}`;
    const response = await shareContent(message);
    if (response) {
      showMsj("Shared", false);
    }
    return response;
  };

  openFile = url => {
    openUrl(url);
  };

  onCloseClick = () => {
		this.setState({ isVisible: false })
	}

	onSubmitClick = async () => {
		this.setState({ isVisible: false })
		if (this.state.selectedFile != null) {
			const fileUrl = await this.uploadFile();
    if (fileUrl) {
      this.setState({ modalMessage: "Uploading your file" });

      try {
        const response = await myMediaApi.addUserMedia(fileUrl);
        this.refs.modal.setModalVisible();
        showMsj(response.data, false);
        this.getFiles();
      } catch (e) {
        this.refs.modal.setModalVisible();
        return showMsj("An error has ocurred saving your file", true);
      }
    }
		} else {
			this.setState({ loading: true });
			try {
				let params = {
					id: this.currentData.id,
					name: this.state.fileName
				}
				const response = await myProfileApi.renameResource(params)
				console.log('response :', response);
				this.getFiles();
				// this.setState({ loading: false });
			} catch (error) {
				this.setState({ loading: false });
				showMsj(errorsValidateMsj.editProfile.error.message, true)
			}
		}
	}

	onChangeText = (fileName) => {
		this.setState({ fileName })
	}

	renderRenameModal = () => {
		return (
			<RenameModal
				isVisible={this.state.isVisible}
				onCloseClick={this.onCloseClick}
				onSubmitClick={this.onSubmitClick}
				value={this.state.fileName}
				onChangeText={this.onChangeText}
			/>
		)
	}

  renameClick = (data) => {
		console.log('data :', data);
		this.currentData = data
		this.setState({ isVisible: true, fileName: data.name })
	}

  render() {
    const { loading, files, modalMessage } = this.state;
    const {
      navigation: { navigate, dispatch }
    } = this.props;
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          barStyle="dark-content"
          text="Music"
        />
        <Content
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this._onRefresh()}
              colors={refreshControlColors}
            />
          }
        >
          {this.renderRenameModal()}
          <View style={styles.container}>
            {loading && <Loading />}
            {files &&
              files.map((option, i) => (
                <Card
                  key={i}
                  type="audio"
                  fileText={option.name}
                  data={option}
                  deleteFile={this.deleteFile}
                  shareFile={data => this.shareFile(data)}
                  openFile={this.openFile}
                  renameFile={(data) => this.renameClick(data)}
                />
              ))}
            {files && files.length === 0 && (
              <Text style={styles.text}>You don't have any files added</Text>
            )}
          </View>
        </Content>
        <ButtonAction icon={PlusIcon} onPress={() => this.saveUrlFile()} />
        <Modal ref="modal" message={modalMessage} />
      </Container>
    );
  }
}
export default MusicView;
