import { StyleSheet } from 'react-native';
import { fontStyles } from 'resources/fonts';
import { colorPalette } from 'resources/colors';
import { resize } from "utils/utils"

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  info: {
    alignItems: "center",
    width: resize(300),
    height: resize(520, 'h'),
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    paddingVertical: resize(20, 'h'),
  },
  empty: {
    width: '100%',
    textAlign: 'center',
    paddingTop: resize(20)
  },
  title: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: colorPalette.purple,
    alignSelf: 'center',
  },
  scrollView: {
    marginBottom: resize(6, "height"),
    width: resize(240),
  }
});