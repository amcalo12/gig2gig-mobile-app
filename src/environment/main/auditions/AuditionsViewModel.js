import auditionsApi from "api/auditionsApi";
import { generalMsj } from "resources/l10n";
import { showMsj } from "utils/utils";

export const loadAuditions = async (refresh, context) => {
	if (refresh) {
		context.setState({ refreshing: true });
	} else {
		context.setState({ loading: true });
	}
	try {
		const response = await auditionsApi.getAuditions();
		context.setState({ auditions: response.data.reverse(), loading: false, refreshing: false, auditionFilters: {}, handleNewData: true });
	} catch (e) {
		if (refresh) {
			context.setState({ refreshing: false });
		} else {
			context.setState({ loading: false });
		}
		if (e.data.data) {
			return showMsj(e.data.data, true);
		}
		return showMsj(generalMsj.error.message, true);
	}
}

export const _onRefresh = async (context) => {
	context.state.auditionFilters['lat'] = ""
	context.state.auditionFilters['lng'] = ""
	context.state.locationTitle = ""
	loadAuditions(true, context);
}

export const show = (context) => {
	context.refs.modal.setModalVisible();
}


export const findAudition = async (context, modalVisibleCall) => {
	const { filterText, auditionFilters } = context.state;
	context.setState({ loading: true });
	if (modalVisibleCall) {
		context.refs.modal.setModalVisible();
	}

	console.log('auditionFilters', auditionFilters)
	if (filterText == '' || filterText == null) {
		if (auditionFilters.base) {
			delete auditionFilters.base
		}
	}

	if (Object.keys(auditionFilters).length === 0) {
		return loadAuditions(false, context);
	}
	console.log('auditionFilters', auditionFilters)
	try {
		const auditions = await auditionsApi.getFilterAuditions(auditionFilters);
		context.setState({ auditions: auditions.data, loading: false });
	} catch (e) {
		console.log(e)
		context.setState({ loading: false, auditions: [] });
	}
}