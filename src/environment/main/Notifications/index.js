import React, { Component } from "react";
import { Modal, View, TouchableOpacity, Image, StatusBar } from "react-native";
import { Text, Container, Content } from "native-base";
import styles from "./index.styles";
import CloseIcon from "resources/images/close-icon-purple.png";
import Loading from "utils/components/loading";
import MessageIcon from "resources/images/notifications/message-icon.png";
import CheckedIcon from "resources/images/notifications/checked-icon.png";
import AuditionIcon from "resources/images/notifications/audition-icon.png";
import FileIcon from "resources/images/notifications/file-icon.png";
import Label from "utils/components/notificationLabel";
import notificationViewModel from "./notificationViewModel";
import Header from "utils/components/appHeader";
import BackIcon from "resources/images/back-icon.png";
import Swipeout from "react-native-swipeout";
export default class Notifications extends Component {
  state = {
    modalVisible: false,
    loading: false,
    notFoundData: false,
    messages: [],
    cellOpen: null
  };

  async setModalVisible() {
    this.setState({
      modalVisible: !this.state.modalVisible
    });
    if (!this.state.modalVisible) {
      await notificationViewModel.getNotifications(this);
    }
  }

  render() {
    const { loading, notFoundData, messages, cellOpen } = this.state;
    return (
      <Modal
        transparent={true}
        animationType="fade"
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setModalVisible();
        }}
      >
        <Container>
          {/* <View
        style={styles.header}
        >
          <TouchableOpacity
          style={styles.close}
          onPress={()=>this.setModalVisible()}
          >
            <Image source={CloseIcon}/>
          </TouchableOpacity>
          <Text style={styles.headerText}>Notifications</Text>
        </View> */}
          <Header
            noMargin
            leftIcon="close"
            leftFn={() => this.setModalVisible()}
            barStyle="dark-content"
            text="Notifications"
          />
          <StatusBar backgroundColor="transparent" barStyle="dark-content" />
          <Content>
            {loading ? (
              <Loading style={{ top: "40%", alignSelf: "center" }} />
            ) : notFoundData ? (
              <Text style={styles.notFoundStyle}>
                There are no notifications yet
              </Text>
            ) : (
                  <View style={styles.notificationContainer}>
                    {messages.map((item, i) => (
                      <Swipeout
                        style={styles.swipeout}
                        key={i}
                        // openRight={cellOpen == i}
                        close={cellOpen != i}
                        onClose={(sectionId, rowId, direction) => { }}
                        onOpen={(sectionId, rowId, direction) => {
                          this.setState({
                            cellOpen: i
                          });
                        }}
                        right={[
                          {
                            onPress: () => notificationViewModel.onPressDelete(this, i, item.id),
                            text: "Delete",
                            color: "white",
                            backgroundColor: "#93183e",
                            activeOpacity: 0.7
                          }
                        ]}
                      >
                        <Label
                          icon={MessageIcon}
                          text={`${item.title}`}
                          isFromNotification={true}
                          message={`${item.message}`}
                        />

                      </Swipeout>
                    ))}
                    {/* <Label icon={CheckedIcon} text={"You have successfully checked in to  Aladdin the Musical "}/>
                    <Label icon={AuditionIcon} text={"Your audition Aladdin the Musical has successfully been added to Upcoming"}/>
                    <Label icon={FileIcon} text={"Your file Resume has successfully been uploaded to My Files"}/> */}
                  </View>
                )}
          </Content>
        </Container>
      </Modal>
    );
  }
}
