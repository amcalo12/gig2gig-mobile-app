import settingsApi from 'api/settingsApi'
import { AsyncStorage } from 'react-native';

const getUserData = async (context) => {
  const userData = JSON.parse(await AsyncStorage.getItem("userData"));
  context.setState({
    userData
  });
}
const getSettings = async (context) => {
  try {
    const res = await settingsApi.getSettings()
    const listSettings = res.data[0]
    context.setState({listSettings})
  } catch (error) {
  }
}

export default {
  getUserData,
  getSettings
}