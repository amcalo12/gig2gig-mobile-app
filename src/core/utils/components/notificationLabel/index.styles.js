import { StyleSheet, Platform, Dimensions } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";
const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
  notification: {
    width: width,
    flexDirection: "row",
    alignItems: "flex-start",
    marginBottom: resize(25, height),
    paddingHorizontal: resize(20)
  },
  notificationIcon: {
    marginRight: 15
    // width: "20%"
    // alignItems:"flex-start",
    // paddingTop: resize(2.5, "height"),
  },
  iconImage: {
    width: resize(37, "height"),
    height: resize(37, "height")
  },
  labels: {
    // width: "80%"
    flex: 1
  },
  notificationLabel: {
    fontFamily: fontStyles.nexa_light,
    color: colorPalette.purple,
    fontSize: 14,
    marginBottom: resize(20, "height"),
    lineHeight: height * 0.026,
    paddingHorizontal: resize(1)
  },
  bold: {
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple
  },
  notificationLabelTime: {
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
    fontSize: 10
  }
});
