import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const myCalendar = async props =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.CALENDAR}${API_ROUTES.MYCALENDAR.EVENTS}`,
  );

const addEvent = async props =>
  apiHandler(
    API_METHOD.POST,
   `${API_MODULES.CALENDAR}${API_ROUTES.MYCALENDAR.ADD_EVENT}`,
    props,
  );


export default {
  myCalendar,
  addEvent
};
