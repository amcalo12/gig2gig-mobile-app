import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    height: resize(470, "height"),
    alignItems:"center",
    justifyContent:"space-between"
  },
  buttons:{
    alignItems:"center",
  },
  button:{
    margin:resize(12, "height")
  }
});