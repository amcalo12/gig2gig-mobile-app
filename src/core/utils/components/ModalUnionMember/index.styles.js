import {StyleSheet} from 'react-native';
import { fontStyles } from 'resources/fonts';
import { colorPalette } from 'resources/colors';
import { resize } from "utils/utils"

export default StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'rgba(0, 0, 0, 0.15)'
  },
  info:{
    alignItems:"center",
    width: resize(290),
    height: resize(631, "height"),
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    // paddingHorizontal: 20,
    paddingVertical: 20
  },
  title:{
    fontFamily: fontStyles.nexa_bold,
    fontSize:18,
    color:colorPalette.purple,
    alignSelf: 'center',
  },
  scrollView: {
    marginBottom:resize(6, "height"),
    width:resize(240),
  }
});