import { createStackNavigator } from "react-navigation";
import MyProfileStack from "./myProfile/MyProfileCoordination";
import MyMediaStack from "./myMedia/MyMediaCoordination";
import MyCalendarStack from "./myCalendar/MyCalendarCoordination";
import SettingsStack from "./settings/SettingsCoordination";
import SupportForumStack from './supportForum/SupportForumCoordination';
import NewsAndUpdatesStack from './newsAndUpdates/NewsAndUpdatesCoordination';
import SubscriptionDetail from "./myProfile/subscriptionDetail";

const ProfileStack = createStackNavigator(
  {
    MyProfile: {
      screen: MyProfileStack,
      navigationOptions: {
        header: null
      }
    },
    SubscriptionDetail: {
      screen: SubscriptionDetail,
      navigationOptions: {
        header: null
      }
    },
    MyMedia: {
      screen: MyMediaStack,
      navigationOptions: {
        header: null
      }
    },
    MyCalendar: {
      screen: MyCalendarStack,
      navigationOptions: {
        header: null
      }
    },
    SupportForum: {
      screen: SupportForumStack,
      navigationOptions: {
        header: null
      }
    },
    NewAndUpdates: {
      screen: NewsAndUpdatesStack,
      navigationOptions: {
        header: null
      }
    },
    Settings: {
      screen: SettingsStack,
      navigationOptions: {
        header: null
      }
    },
  },
  {
    initialRouteName: "MyProfile",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default ProfileStack;
