import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";

export default StyleSheet.create({
  container: {
    width: resize(350),
    alignSelf: 'center',
    marginTop: resize(18, 'h'),
  },
  cardContainer: {
    width: "100%",
    borderRadius: 16,
    paddingVertical: resize(22),
    paddingHorizontal: resize(14),
  },
  avatar: {
    width: '100%',
    height: resize(150, 'h'),
    borderRadius: 8,
    overflow: 'hidden'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: resize(17, 'h')
  },
  titleTxt: {
    color: '#4d2545',
    fontFamily: 'Nexa Bold',
    fontSize: resize(18),
    fontWeight: '400',
  },
  dateTxt: {
    color: '#4d2545',
    fontFamily: 'Nexa Light',
    fontSize: resize(10),
    fontWeight: '400',
  },
  bodyTxt: {
    color: '#757575',
    fontFamily: 'Nexa Light',
    fontSize: resize(11),
    fontWeight: '400',
    letterSpacing: resize(0.2),
    lineHeight: resize(14, 'h'),
    marginTop: resize(10, 'h')
  }
});