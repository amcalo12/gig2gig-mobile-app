import React, { Component } from 'react';
import {
	Platform,
	Modal,
	View,
	StatusBar,
	Image,
	TouchableOpacity
} from 'react-native';
import {
	Text,
	Card,
	Input
} from 'native-base';
import styles from './index.styles';
import FilterIcon from "resources/images/tabNavigation/filter-icon.png";
import searchIcon from "resources/images/tabNavigation/search-icon.png";
import FilterButton from "utils/components/searchFilterButton";
import Button from "utils/components/mainButton";
import { auditionFilters, auditionFilters2 } from 'resources/l10n';
const isIos = Platform.OS === "ios";

export default class SearchModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modalVisible: false,
			filters: [...auditionFilters]
		};
	}

	setModalVisible() {
		this.setState({
			modalVisible: !this.state.modalVisible
		});
	}

	componentDidUpdate = (prevProps) => {
		let { filters } = this.state;
		let { handleNewData, handleData } = this.props;
		if (handleNewData !== prevProps.handleNewData && handleNewData) {
			handleData();
			let newFilter = [...filters];
			for (let index = 0; index < newFilter.length; index++) {
				const element = newFilter[index].options;
				for (let index2 = 0; index2 < element.length; index2++) {
					newFilter[index].options[index2].active = false;
				}
			}
			this.setState({ filters: newFilter })
		}
	}

	toggleValue = (type, idx) => {
		let filters = [...this.state.filters];
		const { text, filterSearch, filtersFn } = this.props;
		const find = filters.find(word => word.type === type);
		const result = filters.indexOf(find);
		if (text.length > 0) {
			filterSearch.base = text;
		}
		if (type !== "production") {
			filters[result].options.map((option, i) => {
				option.active = i === idx ? (option.active ? !option.active : true) : false;
				if (i === idx && option.active) {
					filterSearch[type] = option.name;
				} else if (i === idx && !option.active) {
					delete filterSearch[type];
				}
			})
		} else {
			let arr = [];
			filters[result].options[idx].active = filters[result].options[idx].active ? !filters[result].options[idx].active : true;
			filters[result].options.map((option, i) => {
				if (option.active) {
					arr.push(option.name);
				}
			})

			if (arr.length > 0) {
				filterSearch[type] = arr.join()
			} else {
				delete filterSearch[type];
			}
		}
		this.setState({
			filters
		});

		filtersFn(filterSearch);
	}


	render() {
		const { text, onPress, styleData, onChangeText } = this.props;
		const { filters } = this.state;
		return (
			<Modal
				transparent={true}
				animationType="fade"
				visible={this.state.modalVisible}
				onRequestClose={() => {
					this.setModalVisible();
				}}>
				<StatusBar hidden={false}
					backgroundColor="white" barStyle="dark-content" />
				<View style={styles.container}>
					<Card
						// style={styles.info}
						style={[styles.info, { marginTop: styleData ? (isIos ? styleData.py - styles.measure.height : styleData.py - 35) : 0 }]}
					>
						<View style={styles.searchContainer}>
							<Image source={searchIcon} />
							<Input
								onChangeText={onChangeText}
								placeholderTextColor='#4D2545'
								placeholder="Search"
								value={text}
								style={styles.searchInput}
							/>
							<TouchableOpacity
								onPress={() => this.setModalVisible()}
								style={styles.filterButton}
							>
								<Image source={FilterIcon} />
							</TouchableOpacity>
						</View>
						<View style={styles.filtersContainer}>
							{
								filters.map((option, idx) =>
									option.name != 'Event Type' &&
									(
										<View key={idx}>
											<Text style={styles.filterLabel}>{option.name}</Text>
											<View key={idx} style={styles.filters}>
												{
													option.options.map((item, i) => (
														<FilterButton key={i} type={option.type} text={item.name} active={item.active ? item.active : false} onPress={() => this.toggleValue(option.type, i)} />
													)
													)
												}
											</View>
										</View>
									)
								)
							}
						</View>
						<View style={styles.buttonAction}>
							<Button text={'Search'} onPress={() => onPress()} />
						</View>
					</Card>
				</View>
			</Modal>
		);
	}
}