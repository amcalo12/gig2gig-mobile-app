import {StyleSheet, Platform} from 'react-native';
import { fontStyles } from 'resources/fonts';
import { colorPalette } from 'resources/colors';
import { resize } from "utils/utils"

export default StyleSheet.create({
  container:{
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'rgba(0, 0, 0, 0.15)'
  },
  info:{
    alignItems:"center",
    // width: resize(290),
    height: resize(500, "height"),
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    // paddingHorizontal: 20,
    paddingBottom: 20,
    borderWidth:1,
    borderRadius:28,
    position: "relative"
  },
  inputContainer:{
    flexDirection:"row",
    width:"100%",
    height:"15%",
    alignItems:"center",
    paddingHorizontal: resize(20),
  },
  icon:{
    marginRight:resize(15)
  },
  input:{
    width:"70%",
    ...Platform.select({
      ios: {
        height: "50%",
      },
    }),
    fontFamily: fontStyles.nexa_bold,
    fontSize:18,
    color: "#4D2545",
    marginTop: resize(4, "height"),
  },
  title:{
    fontFamily: fontStyles.nexa_bold,
    fontSize:18,
    color:colorPalette.purple,
    alignSelf: 'center',
  },
  scrollView: {
    marginBottom:resize(10, "height"),
    width:"100%",
    height:"45%",
    paddingHorizontal:resize(20),
  },
  spinner:{
    position:"absolute",
    top:resize(100),
    left:0,
    right:0
  },
  textContainer:{
    width:"100%",
    borderBottomWidth:1,
    borderColor:"#c6c6c6",
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between"
  },
  text:{
    fontFamily:fontStyles.nexa_light,
    fontSize:20,
    color:colorPalette.purple,
    paddingVertical: resize(20, "height")
  }
});