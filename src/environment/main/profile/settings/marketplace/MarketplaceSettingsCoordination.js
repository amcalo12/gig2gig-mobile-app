import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import Marketplace from './index';
import SubmitBusiness from './submitBusiness';
import FeatureListing from './featureListing';

const MarketplaceSettingsStack = createStackNavigator(
  {
    MarketplaceSet: {
      screen: Marketplace,
      navigationOptions: {
        header: null
      }
    },
    SubmitBusiness,
    FeatureListing
  },
  {
    initialRouteName: "MarketplaceSet",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default MarketplaceSettingsStack;
