import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");

export default StyleSheet.create({
  button: {
    height: height * 0.0734632683658,
    width: width * 0.70,
    backgroundColor: colorPalette.mainButtonBg,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: 'center',
    borderRadius: 33,
  },
  buttonText: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    color: colorPalette.white,
    textAlign: 'center',
    paddingHorizontal: resize(10),
    ...Platform.select({
      ios: {
        marginTop: resize(3, "height"),
      },
    }),
  }
});