import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import styles from './style';
import { colorPalette } from 'resources/colors';

export default class GeneralModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Modal
        // animationType="slide"
        transparent={true}
        visible={this.props.visible}
        onRequestClose={this.props.onCloseClick}>
        <TouchableOpacity
          style={styles.modalContainer}
          onPress={() => { }}>
          <View style={styles.modalSubContainer}>
            {/* <Text style={styles.title}>{"Gig2Gig"}</Text> */}

            <ActivityIndicator size="large" style={{ marginVertical: 8 }} color={colorPalette.mainButtonBg} />
            <Text style={styles.text}>{this.props.message}</Text>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}
