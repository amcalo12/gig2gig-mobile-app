import React from "react";
import { createBottomTabNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import HomeView from "environment/main/home";
import AuditionStack from "environment/main/auditions/AuditionCoordination";
import MyAuditionStack from "environment/main/myAuditions/MyAuditionCoordination";
import MarketplaceStack from "environment/main/marketplace/MarketplaceCoordination";
import ProfileStack from "environment/main/profile/ProfileCoordination";
import CustomTabNavigator from "utils/components/tabNavigator";


const MainStack = createBottomTabNavigator(
  {
    Home: {
      screen: HomeView,
      navigationOptions: {
        header: null
      }
    },
    Auditions: {
      screen: AuditionStack,
      navigationOptions: {
        header: null
      }
    },
    MyAuditions: {
      screen: MyAuditionStack,
      navigationOptions: {
        header: null
      }
    },
    Marketplace: {
      screen: MarketplaceStack,
      navigationOptions: {
        header: null
      }
    },
    Profile: {
      screen: ProfileStack,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'Home',
    tabBarComponent: props => <CustomTabNavigator {...props} />,
    tabBarPosition: "bottom",
    // lazy: true,
    // backBehavior: "initialRoute"
  },
  // {
  //   initialRouteName: "Home",
  //   headerMode: "none",
  //   defaultNavigationOptions: {
  //     gesturesEnabled: false
  //   },
  // }
);

export default MainStack;
