## React native core

### Dev enviroment

- Use **Node** version `12.2.0`
- If you use `nvm`, use **nvm version** `0.34.0`
- **Npm version** `6.9.0`
- For install `node_modules` use `yarn`, **NO `npm`**
- install `node_modules` with command `yarn install`
- Cocoapods are included in the project, **IT'S NOT NECESSARY TO EXECUTE THE COMMAND** `pod install`
- **All changes are documented in the CHANGELOG.md file with the following structure:**

```
  kindOfChange = ['ADD', 'FIX', 'REMOVE', 'UPDATE', 'REDESING'] //for example
  ## [kind_of_change] year-month-day hour:minutes

  Example:
  ## [ADD, FIX] 2019-05-18 9:08
  * Add login
  * Fix login
```

- **Name of the commitments with the following structure.**

```
## year-month-day hour:minutes = The reference that was created in the CHANGELOG.md file before the commit

See file CHANGELOG.md ## year-month-day hour:minutes

Example:
See file CHANGELOG.md ##2019-05-18 9:08
```

Before run this proyect you should run this commands in order to get compability with android X
libraries

```
yarn add jetifier --dev
npx jetify
```

if react-native run-android fail in react-native-firebase lib add this
then you have to add in react-native-firebase/android an file called gradle.properties that contain

```
android.useAndroidX=false
android.enableJetifier=false
```

- **Do npx jetify this always you add a new**

you can find all replaces name packages for android X in https://gist.github.com/janicduplessis/df9b5e3c2b2e23bbae713255bdb99f3c

## Resolve Problems :fire::fire_engine:

_Post install please **replace** in **node_modules** libs contained in folder **replace_libs**_

_Post install please **remove** this in lib **react-native-native-video-player**_

```
// file: android/app/src/main/java/com/wog.videoplayer.VideoPlayerPackage.java
	  @Override // remove this if RN > 0.45.0
      public List<Class<? extends JavaScriptModule>> createJSModules() {
          return Collections.emptyList();
      }
```

- **when you add a new library, remember fix the version installed in the `package.json`**

- For link fonts use `react-native link`

- Happy coding :fire::v:
