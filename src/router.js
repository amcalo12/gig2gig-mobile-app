import React, { Component } from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import Initial from "environment/session/SessionCoordination";
import MainStack from "environment/main/MainCoordination";
import AppIntroView from "environment/session/appIntro";
import SubscriptionPlan from "environment/session/subscription_plan";
import PurchaseComplete from "environment/session/completePurchase";

const App = createStackNavigator(
  {
    Initial: {
      screen: Initial,
      navigationOptions: { header: null }
    },
    App: {
      screen: MainStack,
      navigationOptions: { header: null }
    },
    AppIntro: {
      screen: AppIntroView,
      navigationOptions: {
        header: null
      }
    },
    SubscriptionPlan: {
      screen: SubscriptionPlan,
      navigationOptions: {
        header: null
      }
    },
    PurchaseComplete: {
      screen: PurchaseComplete,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "Initial",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

export default createAppContainer(App);
