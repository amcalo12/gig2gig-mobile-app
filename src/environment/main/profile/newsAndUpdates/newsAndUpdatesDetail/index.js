import React, { Component } from "react";
import { View, Text, Image, RefreshControl, Share, FlatList, Linking } from "react-native";
import { Container, Content, Spinner } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import Loading from "utils/components/loading";
import { showMsj, openUrl, shareContent, resize } from "utils/utils";
import { generalMsj, shareMsj } from "resources/l10n";
import { refreshControlColors } from "resources/colors";
import HTML from "react-native-render-html";

class NewAndUpdatesDetail extends Component {

  state = {
    loading: false,
    post: {
      body: 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderitquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit',
      date: '07/08/2019',
      img: 'https://mott.pe/noticias/wp-content/uploads/2016/11/Janette-Asche.jpg',
      title: 'Timon y pumba'
    },
  }

  async componentDidMount() {
  }

  _navigateTo = (screen, params = {}) => {
    const { navigation } = this.props

    navigation.navigate(screen, { ...params })
  }

  render() {

    const {
      loading,
      post: {
        body,
        img,
        title,
      },
    } = this.state;

    const { navigation: { state: { params: { data } } }, navigation } = this.props;
    console.log(data)
    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => navigation.goBack()}
          barStyle="dark-content"
          text={data.title}
        />
        <Content
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.content}
        >
          <Image
            source={{ uri: data.url_media }}
            placeholderStyle={{ backgroundColor: 'rgba(0,0,0,0.5)' }}
            style={styles.img}
            PlaceholderContent={
              <Spinner
                color={'white'}
                size={'small'}
              />
            }
          />
          {/* <Text
            style={styles.body}
          >
            {data.body}
          </Text> */}
          <HTML
            html={data.body}
            containerStyle={{
              marginTop: resize(5),
            }}
            onLinkPress={(evt, href) => { Linking.openURL(href); }}
          />
        </Content>
      </Container>
    );
  }
}
export default NewAndUpdatesDetail;
