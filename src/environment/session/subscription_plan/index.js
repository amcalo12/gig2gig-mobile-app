import React, { Component } from 'react';
import {
  View, Text, FlatList, Image, StatusBar,
  Platform, TouchableOpacity, ImageBackground, Alert, AsyncStorage
} from 'react-native';
import Background from "resources/images/login/background.png";
import styles from "./index.styles";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { SafeAreaView } from 'react-navigation';
import * as RNIap from 'react-native-iap';
import { IN_APP_SKU, IN_APP_PRICE, IN_APP_FEATURE, isSubscribedActive, MESSAGES, showAlert, showAlertWithClick, parseError } from 'utils/cons';
import { Assets } from 'resources/assets';
import moment from 'moment';
import inAppPurchase from 'api/inAppPurchase';
import Loading from 'utils/components/loading';
import Header from 'utils/components/mainHeader';
import BackIcon from "resources/images/back-icon.png"
import firebase from "react-native-firebase";
import InAppLoader from "../../../core/utils/components/InAppLoader"
import GeneralModal from 'utils/components/GeneralModal';

const itemSubs = Platform.select({
  ios: [
    'com.g2g.phone.ios.annualMembership',
    'com.g2g.phone.ios.monthMembership', // dooboolab
  ],
  android: [
    'com.g2g.phone.android.annualmembership', // subscription
    'com.g2g.phone.android.monthmembership', // subscription
  ],
});

const SUBSCRIPTIONS = {
  // This is an example, we actually have this forked by iOS / Android environments
  ALL: ['com.g2g.phone.android.annualmembership', 'com.g2g.phone.android.monthmembership'],
}

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

export default class SubscriptionPlan extends Component {
  constructor(props) {
    super(props);
    this.isFromHome = this.props.navigation.state.params.isFromHome || false;
    this.userData = {}
    // this.currentIndex = 0
    this.state = {
      currentIndex: 0,
      clickIndex: -1,
      isLoading: false,
      messageToShow: "",
      isModalLoading: false,
      plansList: [
        {
          amount: '$' + IN_APP_PRICE.YEAR,
          duration: 'Annual',
          description: IN_APP_FEATURE.PREMIUM
        },
        {
          amount: '$' + IN_APP_PRICE.MONTH,
          duration: 'Monthly',
          description: IN_APP_FEATURE.PREMIUM
        }
      ]
    };
    if (!this.isFromHome) {
      this.state.plansList[this.state.plansList.length] = {
        amount: 'Free',
        duration: '',
        description: IN_APP_FEATURE.FREE
      }
    }
  }

  async componentDidMount() {
    if (this.isFromHome) {
      this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
      console.log('userData :>> ', this.userData);
    }
    try {
      console.log('products :>> BEFORE');
      const result = await RNIap.initConnection();
      console.log('result :>> ', result);
      this.getItems()
      // const purchaseHistory = await RNIap.getPurchaseHistory();
      // console.log('purchaseHistory :>> ', purchaseHistory);
      const isSubscriptionActive = await isSubscribedActive()
      console.log('isSubscriptionActive :>> ', isSubscriptionActive);

    } catch (err) {
      console.log(err); // standardized err.code and err.message available
      console.log('error code', err.code);
      console.log('error message', err.message);
    }

    purchaseErrorSubscription = RNIap.purchaseErrorListener(
      (error) => {
        console.log('purchaseErrorListener', error);
        // this.setState({ isLoading: false })
        // firebase.analytics().logEvent("PURCHASE_ERROR", { error: error.message })
        // Alert.alert('purchase error', error.message);
        let logParams = {
          email: this.userData.email,
          time: Date.now(),
          error: error.message
        }
        firebase.crashlytics().recordError(888, JSON.stringify(logParams));
        showAlertWithClick(error.message, () => {
          this.setState({ isLoading: false })
        })
      },
    );
  }

  componentWillUnmount() {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
    RNIap.endConnection();
  }

  getItems = async () => {
    try {
      // const products = await RNIap.getProducts(itemSkus);
      const products = await RNIap.getSubscriptions(itemSubs);
      console.log('Products', products);
      // this.setState({ productList: products });
    } catch (err) {
      console.log('err.code', err.code);
      console.log('err.message', err.message);
    }
  };

  getAvailablePurchases = async () => {
    try {
      console.info(
        'Get available purchases (non-consumable or unconsumed consumable)',
      );
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);
      console.log('purchases[0] :>> ', purchases[0]);
      // if (purchases && purchases.length > 0) {
      //   this.setState({
      //     availableItemsMessage: `Got ${purchases.length} items.`,
      //     receipt: purchases[0].transactionReceipt,
      //   });
      // }
    } catch (err) {
      console.warn(err.code, err.message);
      Alert.alert(err.message);
    }
  };

  requestSubscription = async (sku) => {
    try {
      const purchase = await RNIap.requestSubscription(sku);
      const ackResult = await RNIap.finishTransaction(purchase);
      console.log('purchase :>>11 ', purchase);
      console.log('purchase :>>11 ', ackResult);


      console.log('purchase :>>11 ', purchase);
      // return purchase
      let purchaseData = (new Date(purchase.transactionDate)).toUTCString()
      console.log('purchaseData :>> ', purchaseData);
      // let expireAt = (new Date(moment(purchaseData).add(5, 'm').toDate())).toUTCString()
      let expireAt = ""
      if (IN_APP_SKU.IOS_MONTH == sku || IN_APP_SKU.ANDROID_MONTH == sku) {
        expireAt = (new Date(moment(purchaseData).add(1, 'M').toDate())).toUTCString()
      } else {
        expireAt = (new Date(moment(purchaseData).add('years', 1).toDate())).toUTCString()
      }
      console.log('expireAt 11:>> ', expireAt);

      let params = {
        user_id: this.userData.details.user_id,
        product_id: purchase.productId,
        original_transaction: Platform.OS == "ios" ? purchase.originalTransactionIdentifierIOS : purchase.purchaseToken,
        current_transaction: purchase.transactionId,
        ends_at: moment.utc(expireAt).format('YYYY-MM-DD HH:mm:ss'),
        purchase_platform: Platform.OS,//    <ios, android, web>,
        purchased_at: moment.utc(purchaseData).format('YYYY-MM-DD HH:mm:ss'),// purchaseData,
        name: IN_APP_SKU.IOS_MONTH == sku || IN_APP_SKU.ANDROID_MONTH == sku ? "Monthly" : "Annual",
        transaction_receipt: Platform.OS == "ios" ? purchase.transactionReceipt : purchase.purchaseToken,
        purchased_price: IN_APP_SKU.IOS_MONTH == sku || IN_APP_SKU.ANDROID_MONTH == sku ? IN_APP_PRICE.MONTH : IN_APP_PRICE.YEAR,
      }
      this.setState({ messageToShow: MESSAGES.UPDATE_ACCOUNT })
      let logParams = {
        email: this.userData.email,
        time: Date.now(),
        error: "App store Purchase complete success"
      }
      firebase.crashlytics().recordError(444, JSON.stringify(logParams));
      this.subscriptionSuccess(params)
    } catch (err) {
      // this.setState({ isLoading: false, messageToShow: "" })
      console.log('err :>> ', err);
      console.log('err :>>11 ', err.code);
      Alert.alert(err.message);
      let logParams = {
        email: this.userData.email,
        time: Date.now(),
        error: err.message
      }
      firebase.crashlytics().recordError(555, JSON.stringify(logParams));
    }
  };

  subscriptionSuccess = async (params) => {
    try {
      const purchaseSuccess = await inAppPurchase.subscriptionSuccess(params)
      // this.setState({ loading: false })
      console.log('purchaseSuccess :>> ', purchaseSuccess);
      this.setState({ isLoading: false, messageToShow: "" })
      let logParams = {
        email: this.userData.email,
        time: Date.now(),
        error: "Subscription Purchase successfully!"
      }
      firebase.crashlytics().recordError(666, JSON.stringify(logParams));
      let data = purchaseSuccess.data
      data['isPaidUser'] = data.is_premium ? true : false
      await AsyncStorage.setItem("userData", JSON.stringify(data));
      if (this.props.navigation.state.params.callback != undefined) {
        this.props.navigation.state.params.callback();
      }
      this.props.navigation.goBack()
    } catch (e) {
      // this.setState({ isLoading: false })
      const parsedError = parseError(e);
      let logParams = {
        email: this.userData.email,
        time: Date.now(),
        error: parsedError
      }
      firebase.crashlytics().recordError(777, JSON.stringify(logParams));
      showAlertWithClick(parsedError, () => {
        this.setState({ isLoading: false })
      })
      console.log(e);
      // this.setState({ loading: false })
    }
  }

  onItemClick = async (item, index) => {
    // if (index == 2) {
    //   return
    // }
    // Create
    let skuName = '';
    let planName = ''
    if (index == 0) {
      skuName = Platform.OS == 'ios' ? 'com.g2g.phone.ios.annualMembership' : 'com.g2g.phone.android.annualmembership'
      planName = "Annual"
    } else if (index == 1) {
      skuName = Platform.OS == 'ios' ? 'com.g2g.phone.ios.monthMembership' : 'com.g2g.phone.android.monthmembership'
      planName = "Monthly"
    } else {
      skuName = 'free'
      planName = "free"
    }

    if (!this.isFromHome) {
      this.props.navigation.navigate('Create', { sku: skuName, planName: planName })
    } else {
      if (skuName != '') {
        this.setState({ clickIndex: index, isLoading: true, messageToShow: MESSAGES.CHECK_PURCHASE_AVAILABLE })
        let params = {
          email: this.userData.email,
          time: Date.now(),
          error: MESSAGES.CHECK_PURCHASE_AVAILABLE
        }
        firebase.crashlytics().recordError(111, JSON.stringify(params));

        const isSubscribe = await isSubscribedActive()
        console.log('this.isSubscribed() :>> ', isSubscribe);
        let logParams = {
          email: this.userData.email,
          time: Date.now(),
          error: isSubscribe
        }
        firebase.crashlytics().recordError(222, JSON.stringify(logParams));
        if (isSubscribe) {
          // alert(MESSAGES.STORE_ALERT)
          // setTimeout(() => {
          //   showAlert(MESSAGES.STORE_ALERT)
          // }, 800);
          showAlertWithClick(MESSAGES.STORE_ALERT, () => {
            this.setState({ clickIndex: index, isLoading: false })
          })
          return
        } else {
          this.setState({ messageToShow: MESSAGES.PROCESS_SUBSCRIPTION })
          let logParams = {
            email: this.userData.email,
            time: Date.now(),
            error: MESSAGES.PROCESS_SUBSCRIPTION
          }
          firebase.crashlytics().recordError(333, JSON.stringify(logParams));
          this.requestSubscription(skuName)
        }
      } else {
        alert('Something went wrong')
      }
    }
    // let sku = '';
    // if (index == 0) {
    //   sku = Platform.OS == 'ios' ? 'com.g2g.phone.ios.annualMembership' : 'com.g2g.phone.android.annualmembership'
    // } else if (index == 1) {
    //   sku = Platform.OS == 'ios' ? 'com.g2g.phone.ios.monthMembership' : 'com.g2g.phone.android.monthmembership'
    // }
  }

  renderItem = ({ item, index }) => {
    return (
      <View style={[styles.itemContainer, { marginRight: index == (this.state.plansList.length - 1) ? 40 : 0 }]}>
        <View style={styles.headerContainer}>
          {/* <Text style={styles.headerTitle}>{'Gig2Gig +'}</Text> */}
          <Text style={styles.headerTitle}>{item.amount == "Free" ? 'Gig2Gig' : 'Gig2Gig +'}</Text>
        </View>

        <View style={styles.priceContainer}>
          <Text style={styles.price}>{item.amount}</Text>
          <Text style={styles.duration}>{item.duration}</Text>
        </View>

        <TouchableOpacity style={styles.buttonContainer} onPress={() => this.onItemClick(item, index)}>
          {/* {this.state.clickIndex == index && this.state.isLoading ?
            <Loading style={{ top: 0 }} color="#FFFFFF" /> : */}
          <Text style={styles.buttonTitle}>{"SELECT"}</Text>
          {/* } */}
        </TouchableOpacity>

        <Text style={styles.description}>{item.description}</Text>
      </View>
    )
  }

  renderDots = ({ item, index }) => {
    return (
      <View style={[styles.dotContainer, this.state.currentIndex == index ? { backgroundColor: colorPalette.white } : {}]} />
    )
  }

  renderLoadingModal = () => {
    return (
      <GeneralModal
        visible={this.state.isLoading}
        message={this.state.messageToShow}
      />
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="transparent"
        />

        {/* <InAppLoader isLoading={true} /> */}
        {this.renderLoadingModal()}
        <ImageBackground source={Background} style={styles.background}>
          <Header style={{ marginTop: 20 }} leftIcon={this.isFromHome ? BackIcon : 'none'} leftFn={() => this.props.navigation.goBack()} />

          {/* {this.isFromHome ? <TouchableOpacity style={styles.backButton} onPress={() => this.props.navigation.goBack()}>
            <Image source={Assets.close} style={{ tintColor: colorPalette.white }} />
          </TouchableOpacity> : null}
          <View style={styles.logoContainer}>
            <Text style={styles.text}>gig</Text>
            <Text style={[styles.text, styles.mediumText]}>2</Text>
            <Text style={styles.text}>gig</Text>
          </View> */}
          <View style={{ flex: 0.2 }} />
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <FlatList
              style={{ height: 500, paddingBottom: 20 }}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={this.state.plansList}
              extraData={this.state}
              renderItem={this.renderItem}
              onScroll={(e) => {
                let offset = e.nativeEvent.contentOffset.x;
                let index = parseInt(offset / 300);   // your cell width
                this.setState({ currentIndex: index })
              }}
              keyExtractor={(item, index) => item + index}
            />
          </View>
          <View style={{ flex: 0.2 }} >
            <FlatList
              style={{ height: 500, paddingBottom: 20 }}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={this.state.plansList}
              extraData={this.state}
              renderItem={this.renderDots}
              keyExtractor={(item, index) => item + index}
            />
          </View>
        </ImageBackground>
      </View>
    );
  }
}
