import { StyleSheet, Platform, Dimensions } from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils"
const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
const isIos = Platform.OS === "ios";
export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: "relative"
  },
  options: {
    backgroundColor: "white",
    position: "absolute",

    right: resize(20),
    zIndex: 10,
    // height: resize(160, "height"),
    width: resize(152),
    borderRadius: 10,
    zIndex: 100000
  },
  bottom: {
    marginBottom: isIos ? resize(25, "height") : resize(5, "height")
  },
  optionLabel: {
    flexDirection: "row",
    alignItems: "center",
    width: "80%",
    alignSelf: 'center',
    marginVertical: resize(10, "height")
  },
  optionText: {
    fontFamily: fontStyles.nexa_light,
    color: colorPalette.purple,
    letterSpacing: 0.8,
    fontSize: 16,
    marginLeft: resize(20)
  }
});