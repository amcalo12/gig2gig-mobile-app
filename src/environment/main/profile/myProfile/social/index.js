import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import InfoLabel from "utils/components/infoLabel";
import Button from "utils/components/secondaryButton";
import Loading from "utils/components/loading";
import PlusIcon from "resources/images/myCalendar/plus-icon.png";
import { AndroidBackHandler } from "react-navigation-backhandler";
import { getUserData } from "../myInfo/MyInfoViewModel";
const twitter_url_prefix = "https://twitter.com/";
const instagram_url_prefix = "https://www.instagram.com/";
const facebook_url_prefix = "https://www.facebook.com/";
const linkedin_url_prefix = "https://www.linkedin.com/";

class Social extends Component {
  constructor(props) {
    super(props);
    this.userData = this.props.navigation.state.params.userData.details;
    console.log(" SOCIAL LINK USER DATA ", this.userData);
    this.state = {
      twitter:
        this.userData.twitter != undefined ? this.userData.twitter : "Twitter",
      instagram:
        this.userData.instagram != undefined
          ? this.userData.instagram
          : "Instagram",
      facebook:
        this.userData.facebook != undefined
          ? this.userData.facebook
          : "Facebook",
      linkedin:
        this.userData.linkedin != undefined
          ? this.userData.linkedin
          : "LinkedIn",
      loading: false,
      notFoundData: false,
    };
  }

  componentWillMount() {
    let addedTwitterUrl = this.userData.twitter;
    let addedInstagramUrl = this.userData.instagram;
    let addedFacebookUrl = this.userData.facebook;
    let addedLinkedInUrl = this.userData.linkedin;

    this.setState({
      twitter:
        addedTwitterUrl != undefined
          ? addedTwitterUrl.startsWith(twitter_url_prefix)
            ? addedTwitterUrl.replace(twitter_url_prefix, "")
            : addedTwitterUrl
          : "Twitter",
      instagram:
        addedInstagramUrl != undefined
          ? addedInstagramUrl.startsWith(instagram_url_prefix)
            ? addedInstagramUrl.replace(instagram_url_prefix, "")
            : addedInstagramUrl
          : "Instagram",
      facebook:
        addedFacebookUrl != undefined
          ? addedFacebookUrl.startsWith(facebook_url_prefix)
            ? addedFacebookUrl.replace(facebook_url_prefix, "")
            : addedFacebookUrl
          : "Facebook",
      linkedin:
        addedLinkedInUrl != undefined
          ? addedLinkedInUrl.startsWith(linkedin_url_prefix)
            ? addedLinkedInUrl.replace(linkedin_url_prefix, "")
            : addedLinkedInUrl
          : "LinkedIn",
    });
  }
  handleBackPress = () => {
    const {
      navigation: { dispatch },
    } = this.props;
    return dispatch(StackActions.popToTop());
    return true;
  };

  callBack = (data) => {
    this.userData["twitter"] = data.twitter || "";
    this.userData["instagram"] = data.instagram || "";
    this.userData["facebook"] = data.facebook || "";
    this.userData["linkedin"] = data.linkedin || "";
    let addedTwitterUrl = data.twitter || "";
    let addedInstagramUrl = data.instagram || "";
    let addedFacebookUrl = data.facebook || "";
    let addedLinkedInUrl = data.linkedin || "";
    this.setState({
      twitter: addedTwitterUrl
        ? addedTwitterUrl.startsWith(twitter_url_prefix)
          ? addedTwitterUrl.replace(twitter_url_prefix, "")
          : addedTwitterUrl
        : "Twitter",
      instagram: addedInstagramUrl
        ? addedInstagramUrl.startsWith(instagram_url_prefix)
          ? addedInstagramUrl.replace(instagram_url_prefix, "")
          : addedInstagramUrl
        : "Instagram",
      facebook:
        addedFacebookUrl != undefined
          ? addedFacebookUrl.startsWith(facebook_url_prefix)
            ? addedFacebookUrl.replace(facebook_url_prefix, "")
            : addedFacebookUrl
          : "Facebook",
      linkedin:
        addedLinkedInUrl != undefined
          ? addedLinkedInUrl.startsWith(linkedin_url_prefix)
            ? addedLinkedInUrl.replace(linkedin_url_prefix, "")
            : addedLinkedInUrl
          : "LinkedIn",
    });
  };

  render() {
    const {
      navigation: { navigate },
    } = this.props;
    const {
      twitter,
      instagram,
      facebook,
      linkedin,
      loading,
      notFoundData,
    } = this.state;
    return (
      <AndroidBackHandler onBackPress={() => this.handleBackPress()}>
        <Container>
          <Header
            leftIcon="backDark"
            leftFn={() => this.handleBackPress()}
            barStyle="dark-content"
            text="Social"
            rightIcon={notFoundData ? PlusIcon : "none"}
            rightFn={
              notFoundData === true
                ? () =>
                    navigate("AddSocialLink", {
                      isAdd: true,
                      onGoBack: () => {},
                    })
                : null
            }
          />
          <Content>
            {loading ? (
              <Loading style={{ top: "40%", alignSelf: "center" }} />
            ) : notFoundData ? (
              <View style={styles.container}>
                <Text style={styles.notFoundText}>
                  You haven't added your information
                </Text>
              </View>
            ) : (
              <View style={styles.container}>
                <View style={styles.labels}>
                  <InfoLabel text={twitter} />
                  <InfoLabel text={instagram} />
                  <InfoLabel text={facebook} />
                  <InfoLabel noLine text={linkedin} />
                </View>
                <View style={styles.button}>
                  <Button
                    text="Edit"
                    onPress={() =>
                      navigate("AddSocialLink", {
                        userData: this.userData,
                        callBack: this.callBack,
                      })
                    }
                  />
                </View>
              </View>
            )}
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}

export default Social;
