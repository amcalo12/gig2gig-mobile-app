import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const getNotification = async () =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.PUSH_NOTIFICATION}s`
  );
const updateNotification = async (id, props) =>
  apiHandler(
    API_METHOD.PUT,
    `${API_MODULES.PUSH_NOTIFICATION}${API_ROUTES.PUSH_NOTIFICATION.UPDATE}/${id}`,
    props
  );
const getNotificationHistory = async () =>
  apiHandler(
    API_METHOD.GET,
    `${API_MODULES.NOTIFICATION_HISTORY}`
  );
const setPushToken = async (props) => {
  apiHandler(
    API_METHOD.PUT,
    `${API_MODULES.NOTIFICATION_PUSH_TOKEN}`,
    props
  )
}

const deleteNotification = async (id) => {
  apiHandler(
    API_METHOD.DELETE,
    `a/notification-history/delete/${id}`,
  )
}

export default {
  getNotification,
  updateNotification,
  getNotificationHistory,
  setPushToken,
  deleteNotification
};
