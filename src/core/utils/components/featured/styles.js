import { StyleSheet } from 'react-native';
import { resize } from 'utils/utils';
import { fontStyles } from 'resources/fonts';

const styles = StyleSheet.create({
	container: {
		width: '88%',
		marginHorizontal: '5%',
		height: resize(168, 'h'),
		backgroundColor: 'white',
		borderRadius: 4,
		marginBottom: resize(10, 'h'),
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.23,
		shadowRadius: 2.62,
		elevation: 4,
	},
	image: {
		width: '100%',
		height: '100%',
		alignItems: 'flex-end'
	},
	imageContainer: {
		width: '100%',
		height: resize(124, 'h'),
		overflow: 'hidden',
		borderRadius: 4,
	},
	info: {
		width: '100%',
		height: resize(44, 'h'),
		paddingHorizontal: resize(14),
		justifyContent: 'center'
	},
	ticket: {
		top: resize(12, 'h'),
		width: resize(108),
		height: resize(27, 'h'),
		alignItems: 'center',
		justifyContent: 'center'
	},
	infoText: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: resize(14),
		color: 'white'
	},
	infoText2: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: resize(14),
		color: '#4D2545'
	}
})
export default styles;
