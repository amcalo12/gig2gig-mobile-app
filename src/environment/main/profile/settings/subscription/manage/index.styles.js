import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  container:{
    paddingHorizontal:resize(20),
    minHeight:180,
    justifyContent:"space-between",
    paddingVertical:20,
    borderWidth: 1.5,
    marginHorizontal: 30,
    borderColor:colorPalette.purple,
    borderRadius:20,
    marginTop:30
  },
  typeText:{
    alignSelf:"center",
    fontSize:23,
    fontFamily:fontStyles.nexa_bold,
    color:colorPalette.purple
  },
  descriptionText:{
    alignSelf:"center",
    marginTop: 20,
    color: colorPalette.unionColor,
    fontFamily: fontStyles.nexa_light,
    fontSize:22,
    marginBottom: 50,
    textAlign:'center'
  },
  buttons:{
    alignItems:"center",
  },
  secondary:{
    height:resize(49, "height"),
    width:resize(229),
    alignItems: 'center',
    justifyContent: "center",
    color:colorPalette.purple,
    borderWidth: 2,
    borderColor: colorPalette.purple,
    borderRadius: 13,
    marginBottom: resize(20, "height"),
  },
  secondaryText:{
    fontFamily:fontStyles.nexa_bold,
    color:colorPalette.purple,
    fontSize:18
  },
  primaryText: {
    fontFamily: fontStyles.nexa_bold,
    color: "#FFFFFF",
    fontSize: 18
  },
  primary: {
    height: resize(49, "height"),
    width: resize(229),
    alignItems: 'center',
    justifyContent: "center",
    color: colorPalette.purple,
    borderRadius: 13,
    backgroundColor: colorPalette.purple,
  }
});