import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");


export default StyleSheet.create({
  container:{
    width:width,
    height:height * 0.804512743628,
    alignItems: 'center',
    justifyContent:"space-around"
  }
});