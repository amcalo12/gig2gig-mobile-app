import { Platform, Dimensions } from "react-native";
import DocumentPicker from "react-native-document-picker";
const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);
const getFile = async type => {
  //type 1 for music
  //type 2 for Documents
  const fileType =
    type === 1 ? DocumentPicker.types.audio : DocumentPicker.types.pdf;
  try {
    const file = await new Promise((resolve, reject) => {
      DocumentPicker.pick({
        type: [fileType]
      })
        .then(res => {
          console.log(
            res.uri,
            res.type, // mime type
            res.name,
            res.size
          );
          console.log(res);
          resolve(res);
        })
        .catch(err => {
          console.log(err);
          if (DocumentPicker.isCancel(err)) {
            // User cancelled the picker, exit any dialogs or menus and move on
          } else {
            reject(err);
          }
        });
    });

    console.log("file===========");
    console.log(file);
    let metadata = null;
    if (file.name) {
      let mimeType = file.name.split(".");
      mimeType = mimeType[mimeType.length - 1];
      metadata = {
        contentType: `application/${mimeType}`
      };
    } else {
      metadata = {
        contentType: "application/pdf"
      };
    }

    // if (type === 'mp3') {
    //   type = 1
    // } else if (type === 'mp4') {
    //   type = 2
    // } else if (type === 'pdf') {
    //   type = 3
    // }
    // console.log('file1', file.uri)
    // console.log('file2', decodeURIComponent(file.uri))
    const newFile = {
      filename: file.name,
      type,
      metadata,
      url: decodeURI(
        Platform.OS === "ios"
          ? String(file.uri).replace("file: ", "")
          : String(file.uri).replace("file://", "")
      )
    };
    return new Promise.resolve(newFile);
  } catch (error) {
    return new Promise.reject(error);
  }
};

export default getFile;
