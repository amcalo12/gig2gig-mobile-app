import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import styles from "./index.styles";
import MessageIcon from "resources/images/notifications/message-icon.png";
import { fontStyles } from "resources/fonts";

export default class NotificationLabel extends Component {

  render() {
    const { icon, text, message, isFromNotification } = this.props;
    return (
      <View style={styles.notification}>
        {/* <Text>{"Test"}</Text> */}
        <View style={styles.notificationIcon}>
          <Image style={styles.iconImage} source={icon} />
        </View>
        <View style={styles.labels}>
          <Text
            style={[
              styles.notificationLabel,
              isFromNotification
                ? { marginBottom: 0, fontFamily: fontStyles.nexa_bold }
                : {}
            ]}
          >
            {text}
          </Text>

          {isFromNotification && message ?
            <Text style={[styles.notificationLabel, { marginBottom: 10 }]}>
              {message}
            </Text> : null
          }

          {/* {isFromNotification && message && (
            <Text style={[styles.notificationLabel, { marginBottom: 10 }]}>
              {message}
            </Text>
          )} */}
        </View>
        {/* <Text style={styles.notificationLabelTime}>2 minutes ago</Text> */}
      </View>
    );
  }
}
