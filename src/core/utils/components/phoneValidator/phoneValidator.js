import React from 'react';

export const phoneValidation = number => {
	// Remove any character that is not a number
	const numbersOnly = removeFormatFrom(number);
	const length = numbersOnly.length;
	// const hasLeadingOne = numbersOnly.startsWith('1');

	if (!(length == 7 || length == 10 || length == 11)) {
		return number;
	}

	const hasAreaCode = length >= 10;
	let sourceIndex = 0;

	// Leading 1
	// let leadingOne = '';
	// if (hasLeadingOne) {
	//     leadingOne = '1 ';
	//     sourceIndex += 1;
	// }

	// Area code
	let areaCode = '';
	if (hasAreaCode) {
		let areaCodeLength = 3;
		let areaCodeSubstring = numbersOnly.substring(
			sourceIndex,
			areaCodeLength
		);
		areaCode = `(${areaCodeSubstring}) `;
		sourceIndex += areaCodeLength;
	}
	// Prefix, 3 characters
	let prefixLength = 3;

	if (sourceIndex + prefixLength > numbersOnly.length) {
		return number;
	}
	let prefix = numbersOnly.substring(sourceIndex, prefixLength + sourceIndex);
	sourceIndex += prefixLength;

	// Suffix, 4 characters
	const suffixLength = 4;
	if (sourceIndex + suffixLength > numbersOnly.length) {
		return number;
	}
	let suffix = numbersOnly.substring(sourceIndex, suffixLength + sourceIndex);

	const finalNumber = `${areaCode + prefix}-${suffix}`;
	return finalNumber;
}

export const removeFormatFrom = number => {
	return number.replace(/[^0-9]/g, '');
}