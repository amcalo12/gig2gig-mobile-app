import { StyleSheet, Platform } from 'react-native';
import { resize } from 'utils/utils';
import { fontStyles } from 'resources/fonts';
import { colorPalette } from 'resources/colors';
const isIos = Platform.OS === "ios";

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	buttons: {
		marginBottom: resize(20, "height"),
	},
	title: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 28,
		letterSpacing: 1.4,
		color: colorPalette.purple,
		marginBottom: resize(23, 'h'),
		marginTop: resize(30, 'h')
	},
	containerFunctions: {
		flex: 1,
	},
	btnContainer: {
		width: '100%',
		height: resize(60, "height"),
	},
	center: {
		justifyContent: 'center',
		alignItems: 'center'
	},
	subTitle: {
		fontFamily: fontStyles.nexa_bold,
		fontSize: 17,
		letterSpacing: 0.85,
		color: colorPalette.purple,
		marginBottom: isIos ? resize(10, "height") : resize(5, "height"),
	}
})
export default styles;
