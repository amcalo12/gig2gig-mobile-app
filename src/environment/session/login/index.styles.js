import { Dimensions, StyleSheet, Platform } from "react-native";
// import { fontSizeDefault } from "utils/utils";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";

const { width, height } = Dimensions.get(
  Platform.OS === "ios" ? "screen" : "window"
);

export default StyleSheet.create({
  waitScreen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  container: {
    height: height
  },
  background: {
    width: "100%",
    height: "100%",
    zIndex: 0,
    justifyContent: "center",
    position: "relative"
  },
  formContainer: {
    alignItems: "center"
  },
  logoContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    marginBottom: height * 0.08
  },
  text: {
    color: colorPalette.white,
    fontFamily: fontStyles.break_semibold,
    fontSize: 51,
    paddingBottom: 5
  },
  mediumText: {
    marginTop: height * 0.015
  },
  formText: {
    color: colorPalette.white,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 13
  },
  withMargin: {
    marginVertical: 15
  },
  positionText: {
    // position: "absolute",
    // bottom: Platform.OS === "ios" ? 27 : 20,
    marginTop: 23,
    alignSelf: "center"
  }
});
