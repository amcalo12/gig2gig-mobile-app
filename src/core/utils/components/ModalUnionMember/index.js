import React, { Component } from 'react';
import {
	Modal,
	View,
	Image,
	TouchableOpacity,
	ScrollView
} from 'react-native';
import {
	Text
} from 'native-base';
import styles from './index.styles';
import Button from "utils/components/mainButton";
import { resize } from "utils/utils";
import Label from "utils/components/unionMemberLabel";

export default class ModalUnionMember extends Component {
	state = {
		modalVisible: false,
		checked: false,
		unionMembers: [
			{
				name: "None"
			},
			{
				name: "SAG-AFTRA"
			},
			{
				name: "Equity (U.S.)"
			},
			{
				name: "NonEquity (U.K.)"
			},
			{
				name: "Equity (Ireland)"
			},
			{
				name: "ACTRA"
			},
			{
				name: "AFM"
			},
			{
				name: "AGMA"
			},
			{
				name: "AGVA"
			},
			{
				name: "BECTU"
			},
			{
        name: "Equity Eligible"
      },
      {
        name: "SAG-AFTRA Eligible"
      }
		]
	};

	setModalVisible() {
		this.setState({
			modalVisible: !this.state.modalVisible
		});
	}

	handleClick(e) {
		this.setState({
			checked: !this.state.checked
		});
	}

	toggleCheck = (index, value) => {
		let { unionMembers } = this.state;
		const { onSelected } = this.props;
		unionMembers[index].active = !value;
		let filtered = [];
		if (index === 0 && !value) {
			filtered = [
				{ name: "None", active: true },
				{ name: "SAG-AFTRA" },
				{ name: "Equity (U.S.)" },
				{ name: "NonEquity (U.K.)" },
				{ name: "Equity (Ireland)" },
				{ name: "ACTRA" },
				{ name: "AFM" },
				{ name: "AGMA" },
				{ name: "AGVA" },
				{ name: "BECTU" },
				{
					name: "Equity Eligible"
				},
				{
					name: "SAG-AFTRA Eligible"
				}
			]
			this.setState({ unionMembers: filtered });
		} else {
			unionMembers[0].active = false;
			this.setState({ unionMembers });
			const result = unionMembers.filter(item => item.active === true);
			const data = result.map((option, idx) => {
				let name = option.name;
				filtered.push({ name })
			});
		}
		onSelected(filtered);
	};


	render() {
		// const { checked } = this.state;
		return (
			<Modal
				transparent={true}
				animationType="fade"
				visible={this.state.modalVisible}
				onRequestClose={() => {
					this.setModalVisible();
				}}>
				<View style={styles.container}>
					<View style={styles.info}>
						<Text style={styles.title}>Union Membership</Text>
						<ScrollView style={styles.scrollView}>
							{
								this.state.unionMembers.map((option, idx) => (
									<Label key={idx} text={option.name} checked={option.active ? option.active : false} onPress={() => this.toggleCheck(idx, option.active ? option.active : false)} />
								))
							}
						</ScrollView>
						<Button text="Done" customWidth={resize(174)} onPress={() => this.setModalVisible()} />
					</View>
				</View>
			</Modal>
		);
	}
}