import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  headerContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: resize(30),
    marginTop: resize(10, "height"),
  },
  imageContainer: {
    height: resize(35, "height"),
    width: resize(35, "height"),
    borderRadius: 4,
    overflow: "hidden",
    backgroundColor: "#d9d9d9"
  },
  image: {
    height: "100%",
    width: "100%"
  },
  headerText: {
    maxWidth: resize(150),
    color: colorPalette.purple,
    fontFamily: fontStyles.nexa_bold,
    fontSize: 20,
    letterSpacing: 1,
  },
  headerButton: {
    paddingHorizontal: resize(10),
    paddingVertical: resize(10, "height"),
    borderRadius: 4

  },
  headerButtonText: {
    color: '#ffffff',
    fontFamily: fontStyles.nexa_bold,
    fontSize: 14,
    letterSpacing: 0.7,
  },
  container: {
    paddingLeft: (30),
    marginTop: resize(20, "height")
  },
});