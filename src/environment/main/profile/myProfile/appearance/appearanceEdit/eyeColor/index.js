import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Container, Content } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from 'react-navigation';
import Label from "utils/components/unionMemberLabel";
import { AndroidBackHandler } from 'react-navigation-backhandler';
import { eyeColorOptions } from "resources/l10n";

class EyeColorView extends Component {
  state = {
    eyes: eyeColorOptions
  }
  handleBackPress = () => {
    const {navigation : {goBack}} = this.props;
    return goBack()
    return true;
  }
  changeChecked = (option, i) => {
    const {navigation : {navigate, state: {params: {onGoBack}}}} = this.props;
    const {eyes} = this.state
    const eyes2 = []
    eyes.map(res => {
      if(res.name === option.name){
        res.checked = true
      }else{
        res.checked = false
      }
      eyes2.push(res)
    })
    this.setState({eyes: eyes2})
    onGoBack(option)
  }
  render() {
    const {navigation : {navigate}} = this.props;
    const { eyes } = this.state
    return (
      <AndroidBackHandler onBackPress={()=>this.handleBackPress()}>
        <Container>
          <Header
          leftIcon="backDark"
          leftFn={()=>this.handleBackPress()}
          barStyle="dark-content"
          text="Eye Color"
          />
          <Content>
            <View style={styles.container}>
              {
                eyes.map((option, idx) => (
                  <Label key={idx} text={option.name} checked={option.checked} onPress={() => this.changeChecked(option, idx)}/>
                ))
              }
            </View>
          </Content>
        </Container>
      </AndroidBackHandler>
    );
  }
}
export default EyeColorView;
