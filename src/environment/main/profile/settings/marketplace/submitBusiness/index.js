import React, { Component } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import { Container, Content, Card } from "native-base";
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import { StackActions } from "react-navigation";
import SkillsLabel from "utils/components/labelMain";
import RightArrow from "resources/images/right-arrow.png";
import Input from "utils/components/mainInput";
import camera_take_pic from "../../../../../../resources/images/camera_take_pic.png";
import MainButton from "utils/components/mainButton";
import { validateFields } from "utils/validators";
import { showMsj } from "utils/utils";
import ImageSelect from "utils/image_picker";
import { createMarket } from "api/marketplaceApi";
import uploadAsset from "utils/uploadAsset";
import validateEmail from "utils/validate_email";
import { phoneValidation } from "utils/components/phoneValidator/phoneValidator";
import { getThumbnail } from "utils/cons";

class SubmitBusiness extends Component {
  state = {
    loadingBtn: false,
    imageSelect: {},
    thumbnail: "",
    formData: {
      businessName: {
        value: "",
        type: "",
        required: true,
        name: "Title"
      },
      address: {
        value: "",
        type: "",
        required: true,
        name: "Address"
      },
      phone: {
        value: "",
        type: "",
        required: false,
        name: "Phone"
      },
      website: {
        value: "",
        type: "",
        required: false,
        name: "Website"
      },
      email: {
        value: "",
        type: "email",
        required: true,
        name: "Email"
      },
      description: {
        value: "",
        type: "",
        required: true,
        name: "Description"
      },
      img: {
        value: "",
        type: "",
        required: true,
        name: "Image"
      }
    }
  };

  _onPressSubmit = async () => {
    const { formData, imageSelect, thumbnail } = this.state;

    this.setState({
      loadingBtn: true
    });
    console.log(imageSelect);
    try {
      const min = 1;
      const max = 10000;
      const rand = min + Math.random() * (max - min);
      await validateFields(formData);
      const url_img = await uploadAsset(
        "marketplace",
        imageSelect.uri,
        `IMG_${rand}.png`,
        { contentType: imageSelect.type }
      );
      const thumbnailImage = await uploadAsset(
        "marketplace/thumbnail",
        thumbnail.uri,
        `IMG_${rand}.png`,
        { contentType: imageSelect.type }
      );
      console.log('thumbnailImage :', thumbnailImage);
      const dataSend = {
        title: formData.businessName.value,
        services: formData.description.value,
        phone_number: formData.phone.value,
        email: formData.email.value,
        address: formData.address.value,
        image_url: url_img,
        url_web: formData.website.value,
        image_name: `IMG_${rand}.png`,
        thumbnail: thumbnailImage
      };
      console.log('dataSend 1111:', dataSend);
      await createMarket(dataSend);
      showMsj("Successful");
      this.props.navigation.goBack();
      this.setState({ loadingBtn: false });
    } catch (error) {
      this.setState({ loadingBtn: false });
      if (error.data) {
        showMsj(error.data.message, true);
      } else {
        showMsj(error, true);
      }
    }
  };

  _onChange = (key, value) => {
    const { formData } = this.state;
    if (key === "phone") {
      if (value.length < 14) {
        let customPhone = String(value).replace(/[^0-9]/g, "");
        let formatPhone = phoneValidation(customPhone);
        formData[key].value = formatPhone;
        this.setState({
          formData: {
            ...formData
          }
        });
      }
    } else {
      formData[key].value = value;

      this.setState({
        formData: {
          ...formData
        }
      });
    }
  };

  _onSelectImg = async () => {
    const res = await ImageSelect();
    console.log('res :', res);
    const thumbnail = await getThumbnail(res.uri)
    console.log('thumbnail :', thumbnail);
    if (res.uri) {
      this._onChange("img", res.uri);
      this.setState({
        imageSelect: res,
        thumbnail
      });
    }
  };

  render() {
    const {
      navigation: { navigate, dispatch }
    } = this.props;

    const {
      formData: {
        businessName,
        address,
        phone,
        website,
        email,
        description,
        img
      },
      loadingBtn
    } = this.state;

    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          barStyle="dark-content"
          text="Submit Business"
        />
        <Content contentContainerStyle={styles.content}>
          <Input
            placeholder="Title"
            data={businessName.value}
            onEdit={text => this._onChange("businessName", text)}
            elevation={3}
            customWidth={styles.input.width}
            margin={10}
          />
          <Input
            placeholder="Address"
            data={address.value}
            onEdit={text => this._onChange("address", text)}
            elevation={3}
            customWidth={styles.input.width}
            margin={10}
          />
          <Input
            placeholder="Phone"
            data={phone.value}
            onEdit={text => this._onChange("phone", text)}
            elevation={3}
            customWidth={styles.input.width}
            margin={10}
          />
          <Input
            placeholder="Website"
            data={website.value}
            onEdit={text => this._onChange("website", text)}
            elevation={3}
            customWidth={styles.input.width}
            margin={10}
          />
          <Input
            placeholder="Email"
            data={email.value}
            onEdit={text => this._onChange("email", text)}
            elevation={3}
            customWidth={styles.input.width}
            margin={10}
          />
          <Input
            alignTop
            placeholder="Description"
            data={description.value}
            onEdit={text => this._onChange("description", text)}
            elevation={3}
            multiline={true}
            customWidth={styles.input.width}
            customHeigth={styles.input.height}
            margin={10}
          />
          <TouchableOpacity
            activeOpacity={0.5}
            style={styles.touchableContainer}
            onPress={() => this._onSelectImg()}
          >
            <Card
              style={img.value != "" ? styles.imgBtnWithImg : styles.imgBtn}
            >
              {img.value != "" ? (
                <Image source={{ uri: img.value }} style={styles.imgBtn} />
              ) : (
                  <View style={{ alignItems: "center" }}>
                    <Text style={styles.photoTxt}>Photo</Text>
                    <Image source={camera_take_pic} style={styles.img} />
                  </View>
                )}
            </Card>
          </TouchableOpacity>
          <MainButton
            text={"Submit"}
            loading={loadingBtn}
            onPress={() => this._onPressSubmit()}
          />
        </Content>
      </Container>
    );
  }
}
export default SubmitBusiness;
