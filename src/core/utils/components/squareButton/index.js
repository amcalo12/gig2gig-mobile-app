import React, { Component } from "react";
import { TouchableOpacity, Image, Text, View } from "react-native";
import styles from "./index.styles"
class SquareButton extends Component {
  getButtonStyles=()=>{
    const {text, icon, onPress, size, margin} = this.props;
    let buttonStyles;
    if (icon && size==="medium"){
      buttonStyles = [styles.buttonContainer];
    } else if (icon && size === "small") {
      buttonStyles = [styles.buttonContainer, styles.small];
    } else if (!icon && size === "small") {
      buttonStyles = [styles.buttonContainer, styles.noIcon];
    }

    if (margin){
      buttonStyles.push({marginVertical: margin})
    }

    return buttonStyles;

  }
  render() {
    const {text, icon, onPress, size} = this.props;
    return (
        <TouchableOpacity style={ this.getButtonStyles() } onPress={()=>onPress()}>
          {
            icon &&
            <View style={size === "small" ? styles.smallImageContainer : styles.imageContainer}>
              <Image source={icon}/>
            </View>
          }
          <Text style={styles.buttonText}>{text}</Text>
        </TouchableOpacity>
    );
  }
}
SquareButton.defaultProps = {
  icon:null,
  size:"medium",
  margin:null
}
export default SquareButton;
