import { Dimensions, StyleSheet, Platform } from 'react-native';
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from 'utils/utils';

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  header: {
    width: "100%",
  },
  grid: {
    width: "90%",
    flexDirection: "row",
    alignSelf: "center",
    marginTop: Platform.OS === 'android' ? 30 : 0
  },
  headerLeft: {
    flex: 1,
  },
  isText: {
    flex: 3
  },
  headerRight: {
    flex: 1
  },
  headerBody: {
    flex: 3,
    justifyContent: 'center',
  },
  headerTitle: {
    alignSelf: 'center',
    color: 'black',
    fontSize: 20
  },
  logoHeader: {
    flexDirection: "row",
    alignSelf: 'center',
    marginTop: height * 0.002
  },
  logoHeaderText: {
    fontFamily: fontStyles.break_semibold,
    fontSize: 28,
    color: colorPalette.white
  },
  mediumLogo: {
    marginTop: height * 0.009
  }
});