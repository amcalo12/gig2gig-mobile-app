import { StyleSheet, Platform, Dimensions } from 'react-native';
import { resize } from 'utils/utils';
import { fontStyles } from 'resources/fonts';

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");

//custom

const styles = StyleSheet.create({
	container: {
		height: height * 0.0734632683658,
		width: width * 0.75,
		flexDirection: 'row',
		marginBottom: resize(17, 'height')
	},
	input: {
		margin: 0,
		padding: 0,
		color: '#4d2545',
		fontFamily: fontStyles.nexa_bold,
		fontSize: resize(19),
		fontWeight: '400',
		paddingLeft: resize(20),
		shadowColor: 'rgba(0, 0, 0, 0.18)',
		shadowOffset: { width: 3, height: 0 },
		shadowRadius: 8,
		height: height * 0.0734632683658,
		width: width * 0.75,
		backgroundColor: '#ffffff',
		borderRadius: 50,
	},
	dropdownIcon: {
		resizeMode: 'contain',
		width: resize(12),
		height: resize(11, 'height'),
	},
	iconContainer: {
		top: resize(20, 'height'),
		right: resize(15),
	},
	modalViewMiddle: {
		backgroundColor: '#FFF',
		borderBottomWidth: 1,
		height: resize(60, 'height'),
		borderBottomColor: '#d9d9d9'
	},
	modalViewBottom: {
		backgroundColor: '#fff',
		// paddingTop: resize(60),
	},
	done: {
		color: '#4d2545',
		// fontFamily: NEXABOLD,
		fontSize: resize(18),
		fontWeight: '400',
	}
});

export default styles;