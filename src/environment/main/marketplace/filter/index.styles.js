import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts"
import { resize } from "utils/utils";

const { width, height } = Dimensions.get(Platform.OS==="ios"?"screen":"window");


export default StyleSheet.create({
  container:{
    paddingHorizontal: resize(25),
    flexDirection: 'row',
    flexWrap: "wrap",
    // height: resize(470, "height"),
    // alignItems:"center",
    justifyContent:"space-between"
  },
  searchContainer: {
    width:resize(330),
    alignSelf: 'center',
    marginBottom: resize(10, "height"),
  },
  notFoundDataMess:{
    textAlign:'center', 
    width:'100%'
  }
});