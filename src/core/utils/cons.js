import moment from "moment";
import ImageResizer from "react-native-image-resizer";
import { Dimensions, Alert, Linking, Platform } from "react-native";
import * as RNIap from "react-native-iap";
import firebase from "react-native-firebase";

// export const BASE_URL = 'http://180.211.99.165:8072/jaisal/gig2gig_cms_api/public/api/'; //'https://gig2gig.v2.elaniin.dev';
// export const BASE_URL = 'http://180.211.99.165:8072/jaisal/gig2gig_cms_api/public'; //'https://gig2gig.v2.elaniin.dev';
//base url
// export const BASE_URL = "https://4dfa6a763531.ngrok.io";
// export const BASE_URL = "http://202.131.117.92:7024";
export const BASE_URL = "https://cms.gig2gig.com";
export const IS_SANDBOX_ENVIRONMENT = false;

export const SHARED_KEY = '053351a9200e479ab3de60f10121d323';

export const FIREBASE_WEB_CONFIG = {
  apiKey: "AIzaSyDTrKkhJCM4ZNbFXRTq0AE2uKzNlpo3_i4",
  authDomain: "dd-gig2gig.firebaseapp.com",
  databaseURL: "https://dd-gig2gig.firebaseio.com",
  projectId: "dd-gig2gig",
  storageBucket: "dd-gig2gig.appspot.com",
  messagingSenderId: "593196123450",
};
``;

var { height, width } = Dimensions.get("window");

export const DEVICE = {
  DEVICE_HEIGHT: height,
  DEVICE_WIDTH: width,
};

export const IN_APP_SKU = {
  IOS_YEAR: "com.g2g.phone.ios.annualMembership",
  IOS_MONTH: "com.g2g.phone.ios.monthMembership",
  ANDROID_YEAR: "com.g2g.phone.android.annualmembership",
  ANDROID_MONTH: "com.g2g.phone.android.monthmembership",
};

export const IN_APP_PRICE = {
  MONTH: "12.99",
  YEAR: "119.99",
};

export const IN_APP_FEATURE = {
  PREMIUM:
    "Access all free features + Casting Feedback, Push Notifications, and Marketplace.",
  FREE:
    "Scan into Auditions, receive audition updates, and store all your music, photos, video reets, sides audition packets and calendar.",
};

export const EXPORT_USER_PLAN_NAME = "FREE_ANNUAL";

export const geoLocationKey = "AIzaSyBQCT4n3dfAcZJYBfzemWe1IbsPgR8adUo";

export const mapsKey = "AIzaSyCBwvwOsPR82AjeUx5o3FUvr4syuoNFrLI";

export const stripeKey = "pk_test_xUbN9WpUJ0FfGMfueDVfQqBc00s5d5WikM";

export const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const API_ROUTES = {
  AUTH: {
    LOGIN: "login",
    REMEMBER_PASSWORD: "remember",
    CREATE_ACCOUNT: "users/create",
    GET_USER_INFO: "a/users/show/",
    // LOGIN_SOCIAL: "social/login",
    // REGISTER: "register"
  },
  AUDITIONS: {
    GET_AUDITIONS: "auditions/show",
    GET_AUDITION_DATA: "auditions/show/",
    GET_FILTER_AUDITIONS: "auditions/findby",
    REQUEST_AUDITION: "a/auditions/user",
    UPCOMING_AUDITION: "a/auditions/user/upcoming",
    GET_UPCOMING_TIME: "/notwalk",
    UPDATE_TO_UPCOMING: "a/auditions/user/update/",
    GET_AUDITION_WALK: "a/auditions/user/update/",
    DELETE_AUDITION: "a/auditions/user/delete/",
  },
  MY_AUDITIONS: {
    GET_AUDITIONS_REQUESTED: "a/auditions/user/requested",
    SAVE_CHECKING: "a/auditions/user/update/",
    GET_AUDITIONS_UPDATES: "monitor/show/",
    GET_AUDITIONS_PAST: "a/auditions/user/pass",
    GET_FINAL_FEEDBACK: "a/feedbacks/final/",
    ADD_ASSIGNED_NUMBER: "assignNumber",
    GET_INSTANT_FEEDBACK: "/instantfeedbacks/details?user_id=",
  },
  MY_PROFILE: {
    UPDATE_USER: "a/users/update/",
    CREATE_REPRESENTATION: "a/managers",
    GET_REPRESENTATION: "a/managers/byuser",
    EDIT_REPRESENTATION: "a/managers/update/",
    GET_SKILL_LIST: "skills/show",
    GET_USER_SKILL_LIST: "a/skills/byuser",
    ADD_SKILL: "a/skills/add",
    DELETE_SKILL: "a/skills/delete/",
    GET_UNION_MEMBERSHIP_LIST: "a/users/union/list",
    UPDATE_UNION_MEMBERSHIP_LIST: "a/users/union/update",
    ADD_SOCIAL: "a/social-links/",
  },
  MY_MEDIA: {
    GET_MEDIA_USER: "a/media/user/list",
    GET_MEDIA_AUDITION_USER: "a/media/auditon/list",
    GET_MEDIA_USER_BY_TYPE: "a/media/user/list/",
    ADD_MEDIA_USER: "a/media/manager",
    ADD_AUDITION_MEDIA_TO_USER: "a/media/user/add",
    DELETE_USER_FILE: "a/media/manager/",
  },
  MARKETPLACE_CATEGORIES: {
    SEARCH: "marketplaces/search?value=",
    SEARCH_FOR_CATEGORIES: "marketplaces",
  },
  MARKETPLACE: {
    CATEGORIES: "marketplace/categories",
    CARDS_FOR_FILER: "marketplace/cards/filter",
    CARDS_FOR_TEXT: "search?value=",
  },
  MYCALENDAR: {
    EVENTS: "/show",
    ADD_EVENT: "/create_event",
  },
  CREDITS: {
    CREATE: "/create",
    SHOW: "/show",
    UPDATE: "/update",
    DELETE: "/delete",
  },
  EDUCATIONS: {
    CREATE: "/create",
    SHOW: "/show",
    UPDATE: "/update",
    DELETE: "/delete",
  },
  PUSH_NOTIFICATION: {
    UPDATE: "/update",
  },
  APPEARANCE: {
    GET_APPEARANCE: "/byuser",
    UPDATE: "/update",
  },
  SUBSCRIPTIONS: {
    CREATE: "/addpayment",
    SHOW: "/getcard",
    UPDATE: "/updateCard",
  },
  FEATURE: {
    FEATURE_CREATE: "a/marketplace-featured-listing/create",
  },
  SUBSCRIPTION: {
    PURCHASE: "a/users/inAppPurchaseSuccess",
    EXPIRE: "users/handle_expired_users",
  },
};

export const API_MODULES = {
  // AUTH: "auth",
  CALENDAR: "a/calendar",
  MARKETPLACE: "a/marketplaces",
  MARKETPLACE_CATEGORIES: "a/marketplace_categories",
  CREADITS: "a/credits",
  EDUCATION: "a/educations",
  PUSH_NOTIFICATION: "a/notification-setting",
  SETTING: "a/content-settings",
  APPEARANCE: "a/aparences",
  AUDITION_UPCOMING_TIME: "appointments/show/",
  SUBSCRIPTIONS: "a/subscriptions",
  NOTIFICATION_HISTORY: "a/notification-history",
  NOTIFICATION_PUSH_TOKEN: "a/notification-send-pushkey",
  AUDITION: "a/auditions/",
};

export const API_METHOD = {
  POST: "POST",
  GET: "GET",
  PUT: "PUT",
  PATCH: "PATCH",
  DELETE: "DELETE",
};

export const validEmail = (email) => {
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //eslint-disable-line
  return regex.test(email);
};

export const IsValidZipCode = (zip) => {
  return /^[0-9]{5}(?:-[0-9]{4})?$/.test(zip);
};

export const maximumDate = () => {
  const date = new Date(moment().subtract(18, "years"));
  return date;
};

export const minimumDate = () => {
  var date = new Date(moment().subtract(105, "years"));
  return date;
};

export const maximumDateAddEvent = () => {
  const date = null;
  return date;
};

export const minimumDateAddEvent = () => {
  var date = new Date(moment());
  return date;
};

export const createDate = (date) => {
  var date = new Date(moment().subtract(8, "years"));
  const maximumDate = moment().subtract(8, "years").format("YYYY-MM-DD");
  return date;
};

export function setUrl(url) {
  var pattern = /^((http|https|ftp):\/\/)/;
  if (!pattern.test(url)) {
    url = "http://" + url;
  }
  return url;
}

export async function getThumbnail(uri) {
  try {
    const response = await ImageResizer.createResizedImage(
      uri,
      200,
      200,
      "JPEG",
      100
    );
    console.log("response :", response);
    return response;
  } catch (error) {
    console.log("error thumbnail :", error);
    return "";
  }

  // ImageResizer.createResizedImage(image.uri, 250, 250, "PNG", 100).then((response) => {
  //   // response.uri is the URI of the new image that can now be displayed, uploaded...
  //   // response.path is the path of the new image
  //   // response.name is the name of the new image with the extension
  //   // response.size is the size of the new image
  //   console.log('response :', response);
  // }).catch((err) => {
  //   console.log('err 111 :', err);
  //   // Oops, something went wrong. Check that the filename is correct and
  //   // inspect err to get more details.
  // });
}

export function openSubscriptionDialog(onYesClick) {
  subscriptionAlert(() => {
    onYesClick();
    // Linking.openURL('https://www.google.com/')
  });
}

export function subscriptionAlert(onYesClick) {
  Alert.alert(
    "Subscription",
    "You don't have any subscription to access this feature.",
    [
      {
        text: "Cancel",
        style: "destructive",
      },
      {
        text: "Subscribe",
        onPress: () => {
          onYesClick();
        },
      },
    ],
    { cancelable: false }
  );
}

export function isIphoneX() {
  const dimen = Dimensions.get("window");
  return (
    Platform.OS === "ios" &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (dimen.height === 812 ||
      dimen.width === 812 ||
      dimen.height === 896 ||
      dimen.width === 896)
  );
}

export const AUDITION_DROPDOWN = {
  MY_AUDITIONS: "My Auditions",
  ONLINE_SUBMISSION: "Online Submissions",
  FEEDBACK: "Feedback",
};
export function checkSubscriptionAndRedirect(userData, routeName, props) {
  // console.log('userData :>> new log ', userData);
  if (userData.subscription != null) {
    if (
      userData.subscription.purchase_platform == Platform.OS ||
      userData.subscription.name == EXPORT_USER_PLAN_NAME
    ) {
      subscriptionGeneralAlert(MESSAGES.PLAN_EXPIRE, () => {
        props.navigation.navigate(routeName, { isFromHome: true });
      });
    } else if (!userData.isPaidUser) {
      // subscriptionGeneralAlert(MESSAGES.PLAN_EXPIRE_DIFFERENT_OS)
      subscriptionGeneralAlert(MESSAGES.PLAN_EXPIRE_DIFFERENT_OS, () => {
        props.navigation.navigate(routeName, { isFromHome: true });
      });
    }
  } else if (!userData.isPaidUser) {
    subscriptionGeneralAlert(MESSAGES.SUBSCRIPTION_ALERT, () => {
      props.navigation.navigate(routeName, { isFromHome: true });
    });
  }
}

export function subscriptionGeneralAlert(message, onYesClick) {
  Alert.alert(
    "Subscription",
    message,
    [
      {
        text: "Cancel",
        style: "destructive",
      },
      {
        text: "Subscribe",
        onPress: () => {
          onYesClick();
        },
      },
    ],
    { cancelable: false }
  );
}

export function showAlert(msg) {
  Alert.alert(
    "Subscription",
    msg,
    [
      {
        text: "OK",
        onPress: () => {},
      },
    ],
    {
      cancelable: false,
    }
  );
}

export function showAlertWithClick(msg, onYesClick) {
  Alert.alert(
    "Gig2Gig",
    msg,
    [
      {
        text: "OK",
        onPress: () => {
          onYesClick();
        },
      },
    ],
    {
      cancelable: false,
    }
  );
}

export function showAlertWithConfirmation(msg, onYesClick) {
  Alert.alert(
    "Gig2Gig",
    msg,
    [
      {
        text: "Cancel",
        style: "destructive",
      },
      {
        text: "OK",
        onPress: () => {
          onYesClick();
        },
      },
    ],
    {
      cancelable: false,
    }
  );
}

export const MESSAGES = {
  PLAN_EXPIRE:
    "Your subscription plan has expired, please renew your subscription to view this section.",
  // PLAN_EXPIRE_DIFFERENT_OS: 'Your subscription plan has been expired, You need to active plan from your' + (Platform.OS == 'ios' ? ' android ' : ' ios ') + "application",
  // PLAN_EXPIRE_DIFFERENT_OS: 'Your subscription plan has been expired, You started subscription from the' + (Platform.OS == 'ios' ? ' android ' : ' ios ') + 'application if you want to continue here you need to cancel your previous subscription from the' + (Platform.OS == 'ios' ? ' android ' : ' ios ') + 'store.',
  PLAN_EXPIRE_DIFFERENT_OS:
    "Your subscription plan has expired, please renew your subscription to view this section.",
  SUBSCRIPTION_ALERT: "You don't have any subscription to access this feature.",
  SUBSCRIPTION_ALERT_PREMIUM:
    "You don't have any subscription to access PREMIUM feature.",
  // CANCEL_SUBSCRIPTION: "You can cancel subscription from store, please visit App store or Play store."
  CANCEL_SUBSCRIPTION:
    "You can cancel subscription from store, please visit App store or Play store.",
  CANCEL_IOS:
    "You can cancel your subscription from your App Store account settings.",
  CANCEL_ANDROID:
    "You can cancel your subscription from your Google Play account settings.",
  AUDITION_SUBMISSION_DUE:
    "The last date of submission for this audition is due. You can't submit any documents now.",
  PREMIUM_FREE_ANNUAL: "Congratulations!\nYou have a 1-year free subscription.",
  STORE_ALERT:
    "You can not purchase with this account, please change your store account.",
  CHECK_PURCHASE_AVAILABLE: "Checking for an active subscription",
  PROCESS_SUBSCRIPTION: "Processing subscription purchase",
  UPDATE_ACCOUNT: "Updating your account",
  CREATING_ACCOUNT: "Creating Account",
};

const SUBSCRIPTIONS = {
  // This is an example, we actually have this forked by iOS / Android environments
  ALL: [
    "com.g2g.phone.android.annualmembership",
    "com.g2g.phone.android.monthmembership",
  ],
};

export const isSubscribedActive = async () => {
  if (Platform.OS === "ios") {
    try {
      const receipt = await RNIap.getReceiptIOS();
      console.log("result :>> ", receipt);
      const validateReceipt = await RNIap.validateReceiptIos(
        {
          "receipt-data": receipt,
          password: SHARED_KEY,
        },
        IS_SANDBOX_ENVIRONMENT
      );
      console.log("validateReceipt :>> ", validateReceipt);
      const { latest_receipt_info: latestReceiptInfo } = validateReceipt;

      const isSubValid = !!latestReceiptInfo.find((receiptData) => {
        const expirationInMilliseconds = Number(receiptData.expires_date_ms);
        const nowInMilliseconds = Date.now();
        console.log(
          "checkAvailablePurchase :>> ",
          expirationInMilliseconds > nowInMilliseconds ? receiptData : false
        );
        if (expirationInMilliseconds > nowInMilliseconds) {
          this.currentActiveReceipt = receiptData;
          this.currentActiveReceipt["transaction_receipt"] = receipt;
        }

        return expirationInMilliseconds > nowInMilliseconds;
      });

      console.log("isSubValid :>> ", isSubValid);
      return isSubValid;
    } catch (error) {
      console.log("isSubValid catch block :>> ", error);
      let logParams = {
        time: Date.now(),
        error: error,
      };
      firebase.crashlytics().recordError(999, JSON.stringify(logParams));
      return false;
    }
  }

  if (Platform.OS === "android") {
    // When an active subscription expires, it does not show up in
    // available purchases anymore, therefore we can use the length
    // of the availablePurchases array to determine whether or not
    // they have an active subscription.
    const availablePurchases = await RNIap.getAvailablePurchases();
    console.log("availablePurchases android :>> ", availablePurchases);
    for (let i = 0; i < availablePurchases.length; i++) {
      console.log("android id:>> ", availablePurchases[i].productId);
      if (SUBSCRIPTIONS.ALL.includes(availablePurchases[i].productId)) {
        console.log(
          "availablePurchases[i].productId :>> ",
          availablePurchases[i].productId
        );
        this.currentActiveReceipt = availablePurchases[i];
        return true;
      }
    }
    return false;
  }
};

export const parseError = (error) => {
  const responseError = error;
  console.log("responseError.data :>> ", responseError.data);
  if (responseError && responseError.data) {
    console.log(responseError);
    if ("error" in responseError.data) {
      return responseError.data.error;
    } else if ("errors" in responseError.data) {
      return responseError.data.errors.email[0];
    } else if ("message" in responseError.data) {
      return responseError.data.message;
    } else if ("data" in responseError.data) {
      return responseError.data.data;
    }
  }

  return "";
};
