import { Dimensions, StyleSheet, Platform } from "react-native";
import { colorPalette } from "resources/colors";
import { fontStyles } from "resources/fonts";
import { resize } from "utils/utils"

const { width, height } = Dimensions.get(Platform.OS === "ios" ? "screen" : "window");
const isIos = Platform.OS === "ios";

export default StyleSheet.create({
  content: {
    marginTop: isIos ? height * 0.040 : height * 0.03,
  },
  empty: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 16,
    color: '#757575',
  },
  container: {
    paddingHorizontal: resize(25)
  },
  title: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 24,
    letterSpacing: 1.2,
    color: colorPalette.purple,
    marginBottom: isIos ? resize(10, height) : resize(5, height),
  },
  subTitle: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 11,
    letterSpacing: 0.55,
    color: colorPalette.purple,
    marginBottom: isIos ? resize(14, height) : resize(8, height),
  },
  titleUpdate: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: 18,
    letterSpacing: 0.9,
    color: colorPalette.purple,
    marginBottom: isIos ? resize(25, height) : resize(20, height),
  },
  update: {
    width: width,
    flexDirection: 'row',
    alignItems: "flex-start",
    marginBottom: resize(20, height),
  },
  updateCheck: {
    width: "10%",
    alignItems: "flex-start",
    paddingTop: resize(2.5, "height"),
  },
  label: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: resize(35, 'h'),
    alignItems: 'center',
    borderBottomColor: '#E6E6E6',
    borderBottomWidth: 1
  },
  labelName: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: resize(17),
    color: colorPalette.purple,
  },
  labelTime: {
    fontFamily: fontStyles.nexa_bold,
    fontSize: resize(17),
    color: colorPalette.purple,
  },
  updateLabel: {
    fontFamily: fontStyles.nexa_light,
    color: colorPalette.purple,
    fontSize: 14,
    marginBottom: resize(15, "height")
  },
  bold: {
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
  },
  updateLabelTime: {
    fontFamily: fontStyles.nexa_bold,
    color: colorPalette.purple,
    fontSize: 10
  },
});