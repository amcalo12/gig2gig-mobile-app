import { StyleSheet } from "react-native";
import { resize } from "utils/utils";

export default StyleSheet.create({
  container:{
    paddingLeft:resize(40),
    marginBottom: resize(30, "height"),
  },
  userImage:{
    backgroundColor:"#d9d9d9",
    justifyContent:"center",
    alignItems:"center",
    width: resize(142),
    height: resize(142),
    borderRadius:130,
    alignSelf:"center",
    overflow:"hidden",
    marginVertical: resize(10, "height"),
  },
  userPicture:{
    width:"100%",
    height:"100%",
    resizeMode:"cover"
  },
  labelGroup:{
    flexDirection: 'row',
  },
  button:{
    marginBottom: resize(30, "height"),
  }
});