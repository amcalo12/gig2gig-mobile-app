import React, { Component } from "react";
import { View, Text, Image, StyleSheet, ActivityIndicator } from "react-native"
import { DEVICE } from "utils/cons";
import { resize } from "utils/utils";
import { colorPalette } from "resources/colors";


class InAppLoader extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      this.props.isLoading ?
        <View style={styles.container}>
          <View style={styles.containerOpac}></View>
          <ActivityIndicator size="large" style={styles.spinner} color={colorPalette.mainButtonBg} />
        </View>
        : null
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    paddingVertical: resize(50),
    width: DEVICE.DEVICE_WIDTH,
    height: DEVICE.DEVICE_HEIGHT,
    zIndex: 997
  },
  containerOpac: {
    position: 'absolute',
    width: DEVICE.DEVICE_WIDTH,
    height: DEVICE.DEVICE_HEIGHT,
    backgroundColor: 'transparent',
    zIndex: 998,
  },
  spinner: {
    flex: 1,
    alignSelf: 'center',
    zIndex: 999
  }
})

export default InAppLoader;