import { createStackNavigator } from "react-navigation";
import DocumentsView from ".";

const DocumentsStack = createStackNavigator(
  {
     Documents: {
       screen: DocumentsView,
       navigationOptions: {
         header: null
       }
     },
  },
  {
    initialRouteName: "Documents",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
  }
);

export default DocumentsStack;
