import {monthsNumber} from "resources/l10n";
import moment from 'moment';
import { showMsj } from "utils/utils";
import subscriptionApi from "api/subscriptionApi";
import { stripeKey } from "utils/cons";
const getMonths = () => {
  const month = []
  monthsNumber.map((res,i) => month[i] = {label:res,value:res})
  return month
}
const convertYears = () => {
  const year = []
  const years = getYears()
  years.map((res,i) => year[i] = {label: res.toString(),value:res.toString()})

  return year
}
const getYears = () => {
  const year = moment().year()
  const allYears = []
  for (let i = 0; i < 20; i++) {
    allYears[i] = year + i
  }
  return allYears
}
const createBodyData = (context) => {
  const { cardNumber, cvvCode, month, year } = context.state
  const body= {
      "card[number]": cardNumber,
      "card[cvc]": cvvCode,
      "card[exp_month]": parseInt(month),
      "card[exp_year]": parseInt(year)
  }
  var formBody = [];
  for (var property in body) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(body[property]);
      formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");
  return(formBody)
}

const getTokenCard = async (context) => {
  const body = createBodyData(context)
  try {
    const res =  await fetch(`https://api.stripe.com/v1/tokens`, {
      method: 'post',
      body,
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          "Authorization": `Bearer ${stripeKey}`,
      },
    })
    const bodyData = await res.json()
    if(bodyData.error){
      console.log(bodyData.error);
      return showMsj(bodyData.error.message, true)
    }else{
      return bodyData.id
    }
  } catch (error) {
    console.log(bodyData.error)
    return showMsj(error.message, true)
  }
}

const saveCard = async (context) => {
  context.setState({loading:true})
  const { cardNumber, cvvCode, month, year } = context.state
  const {props : { navigation }} = context
  if(cardNumber != '' && cvvCode != '' && month != '' && year != ''){
    const token = await getTokenCard(context)
    try {
      const res = await subscriptionApi.addPaymentMethod({token_stripe: token})
      navigation.state.params.addPayment(res)
      navigation.goBack()
      context.setState({loading:false})
      return showMsj("Successful")
    } catch (error) {
      console.log(error);
      context.setState({loading:false})
      return showMsj(error.data.data, true)
    }
    context.setState({loading:false})
  }else{
    context.setState({loading:false})
    return showMsj("All fields are required", true)
  }

}

export default {
  getTokenCard,
  getMonths,
  convertYears,
  saveCard
}