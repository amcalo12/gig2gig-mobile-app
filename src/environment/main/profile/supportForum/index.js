import React, { Component } from "react";
import { View, Text, Image, RefreshControl, Share, FlatList } from "react-native";
import { Container, Content, Spinner } from 'native-base';
import Header from "utils/components/appHeader";
import styles from "./index.styles";
import ButtonAction from "utils/components/buttonActionFloat";
import PlusIcon from "resources/images/myMedia/plus-icon.png";
import Card from "utils/components/mediaCards";
import uploadAsset from "utils/uploadAsset";
import Loading from "utils/components/loading";
import { showMsj, openUrl, shareContent } from "utils/utils";
import { generalMsj, shareMsj } from "resources/l10n";
import { refreshControlColors } from "resources/colors";
import { StackActions, withNavigationFocus } from "react-navigation";
import SearchInput from "utils/components/searchInput";
import FilterIcon from "resources/images/tabNavigation/filter-icon.png";
import searchIcon from "resources/images/tabNavigation/search-icon.png";
import ForumPost from "utils/components/forumPost";
import { getForum, getAllForum, getAllForumByTitle, getTopics, getByTopics } from "api/supportForum";
import SearchModal2 from './filterModal';

class SupportForum extends Component {

  state = {
    loading: true,
    refreshing: false,
    valueSearch: '',
    forumPosts: [],
    forumPostsSearch: null,
    visible: false,
    topics: []
  }

  async componentDidMount() {
    await this.getData()
    this.setState({
      loading: false
    })
  }

  async componentDidUpdate(props, state) {
    if (props.isFocused !== this.props.isFocused && this.props.isFocused) {
      await this.getData()
    }
  }

  getData = async () => {
    try {
      const res = await getAllForum()
      const topics = await getTopics()

      this.setState({
        forumPosts: res.data,
        topics: topics.data
      })
    } catch (error) {
    }
  }
  refreshForumList = async () => {
    this.setState({
      loading: true
    })
    await this.getData()
    this.setState({
      loading: false
    })
  }

  addTopic = () => {
    this._navigateTo('AddSupportForum', {
      onAddSupportForum: this.refreshForumList.bind(this)
    })
  }

  _navigateTo = (screen, params = {}) => {
    const { navigation } = this.props

    navigation.navigate(screen, { ...params })
  }

  _onRefresh = async () => {
    this.setState({
      refreshing: true
    })
    await this.getData()
    this.setState({
      refreshing: false
    })
  }

  _onSearchText = async (valueSearch) => {
    this.setState({
      valueSearch
    })
    if (valueSearch != '' && valueSearch != null) {
      try {
        const res = await getAllForumByTitle(valueSearch)
        this.setState({
          forumPostsSearch: res.data
        })
      } catch (error) {
        this.setState({
          forumPostsSearch: []
        })
      }
    } else {
      this.setState({
        forumPostsSearch: null
      })
    }
  }

  _onPressSerachFilter = async () => {
    console.log('HOLA')

  }

  onPressBackModal = async (data) => {
    if (data.length != 0) {
      try {
        const res = await getByTopics({ topics_ids: data })
        this.setState({
          visible: false,
          forumPostsSearch: res.data
        })
      } catch (error) {
        this.setState({
          visible: false,
          forumPostsSearch: null
        })
      }
    } else {
      this.setState({
        visible: false,
        forumPostsSearch: null
      })
    }
  }

  render() {

    const {
      loading,
      refreshing,
      valueSearch,
      forumPosts,
      forumPostsSearch,
      visible,
      topics
    } = this.state;

    const { navigation: { navigate, dispatch } } = this.props;

    return (
      <Container>
        <Header
          leftIcon="backDark"
          leftFn={() => dispatch(StackActions.popToTop())}
          rightIcon={FilterIcon}
          rightFn={() => this.setState({ visible: true })}
          barStyle="dark-content"
          text="Support Forum"
        />
        {
          loading ?
            <Content>
              <Loading />
            </Content>
            :
            <Content
              showsVerticalScrollIndicator={false}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={() => this._onRefresh()}
                  colors={refreshControlColors}
                />
              }
            >
              <SearchInput
                onPressclearData={(text) => this._onSearchText(null)}
                styles={styles.searchInput}
                onChangeText={(text) => this._onSearchText(text)}
                value={valueSearch}
                onSearch={() => this._onSearchText()} />
              <FlatList
                showsVerticalScrollIndicator={false}
                data={forumPostsSearch ? forumPostsSearch : forumPosts}
                style={{ paddingBottom: 30 }}
                keyExtractor={(a, i) => `${i}`}
                ListEmptyComponent={
                  <Text style={styles.empty}>Not found forums</Text>
                }
                renderItem={(item, i) => (
                  <ForumPost
                    onPress={() => this._navigateTo('DetailSupportForum', { id: item.item.id })}
                    {...item.item}
                  />
                )} />
            </Content>
        }
        <ButtonAction icon={PlusIcon} onPress={() => this.addTopic()} />
        <SearchModal2
          data={topics}
          onPressBack={(data) => this.onPressBackModal(data)}
          visible={visible}
        />
      </Container>
    );
  }
}
export default withNavigationFocus(SupportForum);
