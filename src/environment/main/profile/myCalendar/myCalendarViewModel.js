import myCalendarApi from 'api/myCalendarApi';
import { showMsj } from "utils/utils";
import { errorsValidateMsj } from "resources/l10n";
import { getRange } from 'utils/utils'

const addEventInCalendar = (date, context) => {
  const range = getRange(date)
  context.setState({ loading: false, ranges: { ...context.state.ranges, ...range } });
}

const _handlerGetEvents = async (context) => {
  context.setState({ loading: true })
  try {
    const res = await myCalendarApi.myCalendar()
    res.data.map(res => {
      addEventInCalendar(res, context)
    })
    context.setState({ loading: false });
  } catch (error) {
    context.setState({ loading: false });
    // dates.map((res) => {
    //     addEventInCalendar(res,context)
    // })
    //return showMsj("error", true)
  }
}

export default {
  _handlerGetEvents,
  addEventInCalendar
}