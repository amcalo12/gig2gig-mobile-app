import React, { Component } from "react";
import { Image, StyleSheet, TouchableOpacity, View, AsyncStorage } from "react-native";
import { Footer, FooterTab } from "native-base";
import HideWithKeyboard from "react-native-hide-with-keyboard";
import home from "resources/images/tabNavigation/home.png";
import homeActive from "resources/images/tabNavigation/home-active.png";
import audition from "resources/images/tabNavigation/audition.png";
import auditionActive from "resources/images/tabNavigation/audition-active.png";
import myAuditions from "resources/images/tabNavigation/my-auditions.png";
import myAuditionsActive from "resources/images/tabNavigation/my-auditions-active.png";
import marketplace from "resources/images/tabNavigation/marketplace.png";
import marketplaceActive from "resources/images/tabNavigation/marketplace-active.png";
// import profile from "resources/images/tabNavigation/burgernav_not.png";
// import profileActive from "resources/images/tabNavigation/burgernav_select.png";
import profile from "resources/images/tabNavigation/profile.png";
import profileActive from "resources/images/tabNavigation/profile-active.png";
import styles from "./index.styles";
import { SafeAreaView, StackActions, NavigationActions } from "react-navigation";
import { checkSubscriptionAndRedirect } from "utils/cons";
import { showMsj } from "utils/utils";

export default class TabNavigator extends Component {

  resetStack(root, subRoot) {
    const resetAction = StackActions.reset({
      index: 0,
      key: root,
      actions: [NavigationActions.navigate({
        routeName: subRoot
      })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    const {
      navigation,
      navigation: { navigate }
    } = this.props;
    // forceInset={{ top: 'always' }}
    return (
      <SafeAreaView style={styles.safe} >
        <HideWithKeyboard>
          <View style={styles.container}>
            <TouchableOpacity
              style={styles.tab}
              activeOpacity={1}
              onPress={async () => {
                this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
                console.log('userData :>> tab', this.userData);

                if (this.userData.is_profile_completed == 0) {
                  showMsj("Please complete your profile first.", true)
                  return
                }
                navigation.popToTop()
                navigate("Home")
              }}
            >
              {navigation.state.index === 0 ? (
                <Image source={homeActive} />
              ) : (
                  <Image source={home} />
                )}
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.tab}
              activeOpacity={1}
              onPress={async () => {
                // navigation.popToTop()
                // navigate("Auditions")
                this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
                console.log('userData :>> tab', this.userData);

                if (this.userData.is_profile_completed == 0) {
                  showMsj("Please complete your profile first.", true)
                  return
                }
                this.resetStack("Auditions", "Audition")
              }}
            >
              {navigation.state.index === 1 ? (
                <Image source={auditionActive} />
              ) : (
                  <Image source={audition} />
                )}
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.tab}
              activeOpacity={1}
              onPress={async () => {
                // navigation.popToTop()
                // navigate("MyAuditions")
                this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
                console.log('userData :>> tab', this.userData);

                if (this.userData.is_profile_completed == 0) {
                  showMsj("Please complete your profile first.", true)
                  return
                }
                this.resetStack("MyAuditions", "MyAuditionMain")
              }}
            >
              {navigation.state.index === 2 ? (
                <Image source={myAuditionsActive} />
              ) : (
                  <Image source={myAuditions} />
                )}
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.tab}
              activeOpacity={1}
              onPress={async () => {
                // navigation.popToTop()
                // navigate("Marketplace")
                this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
                console.log('userData :>> tab', this.userData);

                if (this.userData.is_profile_completed == 0) {
                  showMsj("Please complete your profile first.", true)
                  return
                }
                // if (!this.userData.isPaidUser) {
                //   checkSubscriptionAndRedirect(this.userData, 'SubscriptionPlan', this.props)
                //   return
                // }
                this.resetStack("Marketplace", "Marketplace")
              }}
            >
              {navigation.state.index === 3 ? (
                <Image source={marketplaceActive} />
              ) : (
                  <Image source={marketplace} />
                )}
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.tab, styles.mT]}
              activeOpacity={1}
              onPress={async () => {
                this.userData = JSON.parse(await AsyncStorage.getItem("userData"));
                console.log('userData :>> tab', this.userData);

                if (this.userData.is_profile_completed == 0) {
                  showMsj("Please complete your profile first.", true)
                  return
                }
                this.resetStack("Profile", "MyProfile")
                // const resetAction = StackActions.reset({
                //   index: 0,
                //   key: "Profile",
                //   actions: [NavigationActions.navigate({
                //     routeName: 'MyProfile'
                //   })],
                // });
                // this.props.navigation.dispatch(resetAction);
              }}
            >
              {navigation.state.index === 4 ? (
                <Image source={profileActive} />
              ) : (
                  <Image source={profile} />
                )}
            </TouchableOpacity>
          </View>
        </HideWithKeyboard>
      </SafeAreaView>
    );
  }
}
