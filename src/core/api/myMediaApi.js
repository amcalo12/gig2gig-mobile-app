import { apiHandler } from "./apiMethods";
import { API_METHOD, API_MODULES, API_ROUTES } from "utils/cons";

const getUserMedia = async () =>
  apiHandler(
    API_METHOD.GET,
    API_ROUTES.MY_MEDIA.GET_MEDIA_USER
  );

const getAuditionMediaUser = async () =>
  apiHandler(
    API_METHOD.GET,
    API_ROUTES.MY_MEDIA.GET_MEDIA_AUDITION_USER
  );

const addUserMedia = async props =>
  apiHandler(
    API_METHOD.POST,
    API_ROUTES.MY_MEDIA.ADD_MEDIA_USER,
    props,
  );

const getUserMediaByType = async (type) =>
  apiHandler(
    API_METHOD.GET,
    `${API_ROUTES.MY_MEDIA.GET_MEDIA_USER_BY_TYPE}${type}`
  );

const deleteMediaUserFile = async (id) =>
  apiHandler(
    API_METHOD.DELETE,
    `${API_ROUTES.MY_MEDIA.DELETE_USER_FILE}${id}`
  );

const addMediaToUser = async props =>
  apiHandler(
    API_METHOD.POST,
    API_ROUTES.MY_MEDIA.ADD_AUDITION_MEDIA_TO_USER,
    props
  );



export default {
  getUserMedia,
  getAuditionMediaUser,
  addUserMedia,
  addMediaToUser,
  getUserMediaByType,
  deleteMediaUserFile,
};
