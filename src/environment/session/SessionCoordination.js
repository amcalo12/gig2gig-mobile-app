import { createStackNavigator } from "react-navigation";
import { Animated, Easing } from "react-native";
import LoginView from "environment/session/login";
import ForgotPassView from "environment/session/forgotPass";
import CreateAccountView from "environment/session/createAccount";
import AppIntroView from "environment/session/appIntro";
import SubscriptionPlan from "./subscription_plan";

const Initial = createStackNavigator(
  {
    Login: {
      screen: LoginView,
      navigationOptions: { header: null }
    },
    Create: {
      screen: CreateAccountView,
      navigationOptions: {
        header: null
      }
    },
    Forgot: {
      screen: ForgotPassView,
      navigationOptions: {
        header: null
      }
    },
    SubscriptionPlan: {
      screen: SubscriptionPlan,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "Login",
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

export default Initial;
